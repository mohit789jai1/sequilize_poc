const cron = require('node-cron');
const cronService = require('./services/cron');
cron.schedule('*/30 * * * * *', async function () {
    await cronService.updateLiveTokenValue();
    console.log('Live currency value updated');
},
    { scheduled: true, });

cron.schedule('0 */5 * * * *', async function () {
    await cronService.updateLiveChartValue();
    console.log('Live chart value updated');
},
    { scheduled: true, });

cron.schedule('0 */30 * * * *', async function () {
    await cronService.updatePendingTransaction();
    console.log('Cron for make pending transaction failed');
},
    { scheduled: true, });

cron.schedule('0 */1 * * * *', async function () {
    await cronService.unlockAccount();
    console.log('Cron for make update locked account');
},
    { scheduled: true, });

cron.schedule('0 0 12 * * *', async function () {
    await cronService.removeFiles();
    console.log('Cron for remove unused files');
},
    { scheduled: true, });

cron.schedule('0 0 */1 * * *', async function () {
   // await cronService.sendMailToAdminAboutInsufficientBalance();
    console.log('Cron for send email for insufficient balance');
},
    { scheduled: true, });