const express = require('express');
const winston = require('winston');
require('winston-daily-rotate-file');
const cors = require('cors');
const path = require('path');
const dotenv = require('dotenv');
const helmet = require('helmet');
const xss = require('xss-clean');
const mongoSanitize = require('express-mongo-sanitize');
const { authLimiter } = require('./middleware/rateLimiter');
dotenv.config({ path: '.env' });
global.__basedir = __dirname;
const ejs = require('ejs');
const postmanToOpenApi = require('postman-to-openapi');
const YAML = require('yamljs');
const swaggerUi = require('swagger-ui-express');
require('./config/db.js');
const listEndpoints = require('express-list-endpoints');
const passport = require('passport');
const multer = require('multer');
const store = multer.diskStorage({ destination: 'uploads/' });
const upload = multer({ storage: store });
let cookieParser = require('cookie-parser');
let logger = require('morgan');
const { clientPassportStrategy } = require('./config/clientPassportStrategy');
const { adminPassportStrategy } = require('./config/adminPassportStrategy');
const app = express();

const fileRotateInfoTransport = new winston.transports.DailyRotateFile({
	filename: 'combined-%DATE%.log',
	level: 'info',
	datePattern: 'YYYY-MM-DD',
	maxFiles: '14d',
});

const fileRotateErrorTransport = new winston.transports.DailyRotateFile({
	filename: 'error-%DATE%.log',
	level: 'error',
	datePattern: 'YYYY-MM-DD',
	maxFiles: '14d',
});

const winstonLogger = winston.createLogger({
	level: process.env.LOG_LEVEL || 'info',
	format: winston.format.cli(),
	transports: [
		new winston.transports.Console(),
		fileRotateInfoTransport,
		fileRotateErrorTransport
	],
	exceptionHandlers: [
		new winston.transports.File({ filename: 'exception.log' }),
	],
	rejectionHandlers: [
		new winston.transports.File({ filename: 'rejections.log' }),
	]
});

// throw new Error('An uncaught error!');

const corsOptions = { origin: process.env.ALLOW_ORIGIN, };
app.use(cors(corsOptions));

//template engine
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.use(require('./utils/responseHandler'));

//all routes 
const routes = require('./routes/index');

app.use(upload.array('files'));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'uploads')));
app.use(routes);

// set security HTTP headers
app.use(helmet());

// sanitize request data
app.use(xss());
app.use(mongoSanitize());

clientPassportStrategy(passport);
adminPassportStrategy(passport);

//swagger Documentation
postmanToOpenApi('postman/postman-collection.json', path.join('postman/swagger.yml'), { defaultTag: 'General' }).then(data => {
	let result = YAML.load('postman/swagger.yml');
	result.servers[0].url = '/';
	app.use('/swagger', swaggerUi.serve, swaggerUi.setup(result));
}).catch(e => {
	console.log('Swagger Generation stopped due to some error');
});

// limit repeated failed requests to auth endpoints
if (process.env.NODE_ENV === 'production') {
	app.use('/', authLimiter);
}

app.get('/', (_req, res) => {
	res.status(200).send('Successfully Connected');
});
/*
 * send back a 404 error for any unknown api request
 * app.use((req, res, next) => {
 *   next(new Error('This request not found'));
 * });
 */

require('./cron.js');

if (process.env.NODE_ENV !== 'test') {

	app.listen(process.env.PORT, () => {
		console.log(`your application is running on ${process.env.PORT}`);
	});
} else {
	module.exports = app;
}
