/*
 * modelValidation.js
 * purpose     : request validation
 * description : validate each post and put request as per mongoose model
 *
 */
const joi = require('joi');
const { USER_ROLE } = require('../../constants/authConstant');
const { convertObjectToEnum } = require('../common');
exports.schemaKeys = joi.object({
  _id: joi.string().allow(null).allow(''),
  password: joi.string().min(8).allow(null).allow(''),
  email: joi.string().email().case('lower').allow(null).allow(''),
  firstName: joi.string().allow(null).allow(''),
  isActive: joi.boolean().default(true).allow(null).allow(''),
  phoneNo: joi.string().allow(null).allow(''),
  lastName: joi.string().allow(null).allow(''),
  role: joi.number().integer().valid(...convertObjectToEnum(USER_ROLE)).allow(null).allow(''),
  resetPasswordOTP: joi.object({
    code: joi.string(),
    expireTime: joi.date()
  }).allow(null).allow(''),
  isDeleted: joi.boolean(),
  isEmailVerified: joi.boolean().default(false).allow(null).allow(''),
  isPhoneNoVerified: joi.boolean().default(false).allow(null).allow(''),
  custodialWalletAddress: joi.string().allow(null).allow(''),
}).unknown(true);

exports.updateSchemaKeys = joi.object({
  _id: joi.string().allow(null).allow(''),
  password: joi.string().min(8).allow(null).allow(''),
  email: joi.string().email().case('lower').allow(null).allow(''),
  firstName: joi.string().allow(null).allow(''),
  isActive: joi.boolean().default(true).allow(null).allow(''),
  phoneNo: joi.string().when({
    is: joi.exist(),
    then: joi.required(),
    otherwise: joi.optional()
  }),
  lastName: joi.string().allow(null).allow(''),
  role: joi.number().integer().valid(...convertObjectToEnum(USER_ROLE)).allow(null).allow(''),
  resetPasswordOTP: joi.object({
    code: joi.string(),
    expireTime: joi.date()
  }).allow(null).allow(''),
  isDeleted: joi.boolean(),
  isEmailVerified: joi.boolean().default(true).allow(null).allow(''),
  isPhoneNoVerified: joi.boolean().default(true).allow(null).allow(''),
  custodialWalletAddress: joi.string().allow(null).allow(''),
}).unknown(true);
