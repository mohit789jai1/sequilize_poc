const Joi = require('joi');

const createFile = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    folder: Joi.string().required(),
  }),
};

const getFiles = { query: Joi.object().keys({ name: Joi.string(), }), };

const getFile = { params: Joi.object().keys({ fileId: Joi.string().regex(/^[0-9a-fA-F]{24}$/), }), };

const updateFile = {
  params: Joi.object().keys({ fileId: Joi.string().regex(/^[0-9a-fA-F]{24}$/).label('File'), }),
  body: Joi.object()
    .keys({
      sequence: Joi.number(),
      isActive: Joi.boolean(),
      isDefault: Joi.boolean(),
      parentFile: Joi.string(),
      subFiles: Joi.string(),
      valueFile: Joi.string(),
    })
    .min(1),
};

const deleteFile = { params: Joi.object().keys({ fileId: Joi.string().regex(/^[0-9a-fA-F]{24}$/).label('File'), }), };

module.exports = {
  createFile,
  getFiles,
  getFile,
  updateFile,
  deleteFile,
};
