const mongoose = require('mongoose');
const dbService = require('./dbService');
const encryptor = require('simple-encryptor')(process.env.ENC_SECRET);
/*
 * convertObjectToEnum : convert object to enum
 * @param obj          : {}
 */
const convertObjectToEnum = (obj) => {
	const enumArr = [];
	Object.values(obj).forEach((val) => enumArr.push(val));
	return enumArr;
};

/*
 * randomNumber : generate random numbers.
 * @param length          : number *default 4
 */
const randomNumber = (length = 6) => {
	const numbers = '12345678901234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	let result = '';
	for (let i = length; i > 0; i -= 1) {
		result += numbers[Math.round(Math.random() * (numbers.length - 1))];
	}
	return result;
};

/*
 * generateCode : generate random numbers.
 * @param length          : number *default 24
 */
const generateCode = (length = 24) => {
	const numbers = '1!2@3#4$5%a^b&c*d_e+f6g7h8i9j0k1l3m2n4o5p6q7r9s0t!u@v#w$x%y^z&1*2+3!4ab$c%d*e%f1g#h^i5klm^n*o^p4qr4s&t-u1v#w^x$yz';
	let result = '';
	for (let i = length; i > 0; i -= 1) {
		result += numbers[Math.round(Math.random() * (numbers.length - 1))];
	}
	return result;
};

/*
 * generateCode : generate random numbers.
 * @param length          : number *default 24
 */
const generateRandomString = (length = 8) => {
	const numbers = 'abcdefghijklmnopqrstuvwxyz1234567890abcdefghijklmnopqrstuvwxyz01234567890';
	let result = '';
	for (let i = length; i > 0; i -= 1) {
		result += numbers[Math.round(Math.random() * (numbers.length - 1))];
	}
	return result;
};

const encryption = (text) => {
	return encryptor.encrypt(text);
};

const decryption = (cipher) => {
	return encryptor.decrypt(cipher);
};
/*
 * replaceAll: find and replace al; occurrence of a string in a searched string
 * @param string : string to be replace
 * @param search : string which you want to replace
 * @param replace: string with which you want to replace a string
 */
const replaceAll = (string, search, replace) => string.split(search).join(replace);

/*
 * uniqueValidation: validate Login With Fields while Registration
 * @param Model : Mongoose Model, on which query runs
 * @param data : data , coming from request
 */
const uniqueValidation = async (Model, data) => {
	let filter = {};
	if (data && data['email']) {
		filter = {
			$or: [
				{ 'email': data['email'] },
				{ 'altEmail': data['email'] }
			]
		};
	}
	if (data && data['phoneNo']) {
		filter = {
			$or: [
				{ 'phoneNo': data['phoneNo'] },
				{ 'altPhoneNo': data['phoneNo'] }
			]
		};
	}
	let found = await dbService.getDocumentByQuery(Model, filter);
	if (found) {
		return false;
	}
	return true;
};

const getDifferenceOfTwoDatesInTime = (currentDate, toDate) => {
	let hours = toDate.diff(currentDate, 'hours');
	currentDate = currentDate.add(hours, 'hours');
	let minutes = toDate.diff(currentDate, 'minutes');
	currentDate = currentDate.add(minutes, 'minutes');
	let seconds = toDate.diff(currentDate, 'seconds');
	if (hours) {
		return `${hours} hour, ${minutes} minute and ${seconds} second`;
	}
	return `${minutes} minute and ${seconds} second`;
};

const isImage = (extension) => {
	return (
		extension === 'png' ||
		extension === 'jpeg' ||
		extension === 'jpg' ||
		extension === 'webp' ||
		extension === 'gif' ||
		extension === 'pnm' ||
		extension === 'svg' ||
		extension === 'tiff'
	);
};

module.exports = {
	convertObjectToEnum,
	randomNumber,
	replaceAll,
	uniqueValidation,
	getDifferenceOfTwoDatesInTime,
	isImage,
	generateCode,
	encryption,
	decryption,
	generateRandomString
};
