const axios = require('axios');

const createReferralId = async (postData) => {
	try {
		const growSurfData = await axios.post(`https://api.growsurf.com/v2/campaign/${process.env.GROWSURF_CAMPAIGN_ID}/participant`,
			postData,
			{ headers: { Authorization: process.env.GROWSURF_TOKEN } });
		return { data: growSurfData.data };
	} catch (error) {
		throw new Error(error.message);
	}
};

const getReferralData = async (id) => {
	try {
		const growSurfData = await axios.get(`https://api.growsurf.com/v2/campaign/${process.env.GROWSURF_CAMPAIGN_ID}/participant/${id}`,
			{ headers: { Authorization: process.env.GROWSURF_TOKEN } });
		return { data: growSurfData.data };
	} catch (error) {
		throw new Error(error);
	}
};

module.exports = {
	createReferralId,
	getReferralData
};
