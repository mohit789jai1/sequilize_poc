/*
 * constants
 */

const JWT = {
	CLIENT_SECRET: 'myjwtclientsecret',
	ADMIN_SECRET: 'jhauisdfjbllaoyuweghbdsniladaldiadfhubgc',
	EXPIRES_IN: 900
};

const USER_ROLE = {
	User: 1,
	Admin: 10,
};

const PLATFORM = {
	CLIENT: 1,
	ADMIN: 10
};

let LOGIN_ACCESS = {
	[USER_ROLE.User]: [PLATFORM.CLIENT],
	[USER_ROLE.Admin]: [PLATFORM.ADMIN],
};

const DEFAULT_ROLE = 1;

const MAX_LOGIN_RETRY_LIMIT = 5;
const LOGIN_REACTIVE_TIME = 0;

const SEND_LOGIN_OTP = {
	SMS: 1,
	EMAIL: 2,
};
const DEFAULT_SEND_LOGIN_OTP = SEND_LOGIN_OTP.SMS;

const FORGOT_PASSWORD_WITH = {
	LINK: {
		email: true,
		sms: false
	},
	EXPIRETIME: 30
};
const MAX_RESEND_OTP_RETRY_LIMIT = 3;
const RESEND_OTP_REACTIVE_TIME = 30;
const MAX_VALIDATE_OTP_RETRY_LIMIT = 3;

const ADMIN_EMAIL = ['danny+admin@mybricksfinance.com','callum+admin@techalchemy.co','harrison+admin@mybricksfinance.com','adam+admin@mybricksfinance.com','alex+admin@mybricksfinance.com','sam+admin@mybricksfinance.com'];
const SEND_ADMIN_EMAIL = ['danny+admin@mybricksfinance.com','callum+admin@techalchemy.co','harrison+admin@mybricksfinance.com','adam+admin@mybricksfinance.com','alex+admin@mybricksfinance.com','sam+admin@mybricksfinance.com'];

module.exports = {
	JWT,
	USER_ROLE,
	DEFAULT_ROLE,
	PLATFORM,
	MAX_LOGIN_RETRY_LIMIT,
	LOGIN_REACTIVE_TIME,
	SEND_LOGIN_OTP,
	DEFAULT_SEND_LOGIN_OTP,
	FORGOT_PASSWORD_WITH,
	LOGIN_ACCESS,
	MAX_RESEND_OTP_RETRY_LIMIT,
	RESEND_OTP_REACTIVE_TIME,
	MAX_VALIDATE_OTP_RETRY_LIMIT,
	ADMIN_EMAIL,
	SEND_ADMIN_EMAIL
};