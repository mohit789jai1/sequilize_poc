const PROPERTY_TYPE = {
	RESIDENCE: 1,
	COMMERCIAL: 2
};
const STATUS = {
	NOT_CREATED: 1,
	CREATED: 2,
	PENDING: 3,
	COMPLETED: 4,
	FAILED: 5,
	EXPIRED: 6,
	NEEDS_REVIEW: 7,
	APPROVED: 8,
	DECLINED: 9
};

const PROJECT_SETTING = {
	CURRENCY_TOKEN_VALUE: 1,
	TOKEN_CHART: 2,
	NONCE_MANAGEMENT: 3
};
const TWILIO_NOT_ALLOW_COUNTRY_LIST = ['AF', 'AR', 'AM', 'BS', 'BY', 'BE', 'BJ', 'BR', 'CM', 'CA', 'KY', 'CL', 'CN', 'CO', 'CG', 'CR', 'CU', 'CZ', 'DO', 'EC', 'EG', 'SV', 'ET', 'GF', 'GH', 'GU', 'GN', 'GW', 'HK', 'HU', 'IN', 'ID', 'IR', 'IL', 'CI', 'JO', 'KZ', 'KE', 'KW', 'LB', 'LR', 'MW', 'MY', 'MX', 'MC', 'MA', 'MM', 'NP', 'NZ', 'NI', 'NG', 'OM', 'PK', 'PA', 'PY', 'PE', 'PH', 'PR', 'QA', 'RU', 'RW', 'SA', 'RS', 'ZA', 'KR', 'SS', 'LK', 'SD', 'SY', 'TZ', 'TH', 'TN', 'TR', 'UG', 'AE', 'US', 'UY', 'VN', 'ZM', 'ZW'];

module.exports = {
	PROPERTY_TYPE,
	STATUS,
	PROJECT_SETTING,
	TWILIO_NOT_ALLOW_COUNTRY_LIST
};