
const dotenv = require('dotenv');
dotenv.config();
process.env.NODE_ENV = 'test';
const db = require('mongoose');
const request = require('supertest');
const {
  MongoClient, ObjectId
} = require('mongodb');
const app = require('../../app.js');
const authConstant = require('../../constants/authConstant');
const uri = 'mongodb://127.0.0.1:27017';

const client = new MongoClient(uri, {
  useUnifiedTopology: true,
  useNewUrlParser: true
});

let inserted_user = {};

beforeAll(async function () {
  try {
    await client.connect();
    const db = client.db('MyBricks_test');

    const user = db.collection('user');
    inserted_user = await user.insertOne({
      password: 'UwdcNu60_pE2KnJ',
      email: 'Ayana.Swift@yahoo.com',
      firstName: 'Erica Blick',
      phoneNo: '296.557.1743',
      lastName: 'Otis Kovacek',
      loginOTP: {},
      role: 624,
      resetPasswordLink: {},
      loginRetryLimit: 919,
      loginReactiveTime: '2022-01-07T12:15:19.782Z',
      id: '616fc8782f0fbaeb100259f7'
    });
  }
  catch (err) {
    console.error(`we encountered ${err}`);
  }
  finally {
    client.close();
  }
});

describe('POST /register -> if email and username is given', () => {
  test('should register a user', async () => {
    let registeredUser = await request(app)
      .post('/client/auth/register')
      .send({
        'password': 'J9cwpxlU71Xg0LG',
        'email': 'Liliana.VonRueden@hotmail.com',
        'firstName': 'Bryant Kozey',
        'phoneNo': '+447932935548',
        'lastName': 'Ms. Mable Douglas',
        'addedBy': inserted_user.insertedId,
        'updatedBy': inserted_user.insertedId,
        'role': authConstant.USER_ROLE.User
      });
    expect(registeredUser.headers['content-type']).toEqual('application/json; charset=utf-8');
    expect(registeredUser.body.status).toBe('SUCCESS');
    expect(registeredUser.body.data).toMatchObject({ id: expect.any(String) });
    expect(registeredUser.statusCode).toBe(200);
  });
});

describe('POST /forgot-password -> if email has not passed from request body', () => {
  test('should return bad request status and insufficient parameters', async () => {
    let user = await request(app)
      .post('/client/auth/forgot-password')
      .send({});

    expect(user.headers['content-type']).toEqual('application/json; charset=utf-8');
    expect(user.body.status).toBe('BAD_REQUEST');
    expect(user.body.message).toBe('Insufficient parameters');
    expect(user.statusCode).toBe(400);
  });
});

describe('POST /forgot-password -> if email passed from request body is not available in database ', () => {
  test('should return record not found status', async () => {
    let user = await request(app)
      .post('/client/auth/forgot-password')
      .send({ 'email': 'unavailable.email@hotmail.com', });

    expect(user.headers['content-type']).toEqual('application/json; charset=utf-8');
    expect(user.body.status).toBe('RECORD_NOT_FOUND');
    expect(user.body.message).toBe('Record not found with specified criteria.');
    expect(user.statusCode).toBe(404);
  });
});

describe('POST /forgot-password -> if email passed from request body is valid and OTP sent successfully', () => {
  test('should return success message', async () => {
    const expectedOutputMessages = [
      'otp successfully send.',
      'otp successfully send to your email.',
      'otp successfully send to your mobile number.'
    ];
    let user = await request(app)
      .post('/client/auth/forgot-password')
      .send({ 'email': 'Liliana.VonRueden@hotmail.com', });

    expect(user.headers['content-type']).toEqual('application/json; charset=utf-8');
    expect(user.body.status).toBe('SUCCESS');
    expect(expectedOutputMessages).toContain(user.body.message);
    expect(user.statusCode).toBe(200);
  });
});

describe('POST /validate-otp -> otp is sent in request body and OTP is correct', () => {
  test('should return success', () => {
    return request(app)
      .post('/client/auth/send_login_otp')
      .send(
        {
          username: 'Liliana.VonRueden@hotmail.com',
          password: 'J9cwpxlU71Xg0LG'
        }).then(loginOtp => () => {
          return request(app)
            .get(`/client/api/v1/user/login_with_otp`)
            .set({
              username: 'Liliana.VonRueden@hotmail.com',
              password: 'J9cwpxlU71Xg0LG',
              code: loginOtp.body.data.loginOTP.code
            }).then(login => () => {
              return request(app)
                .get(`/client/api/v1/user/${login.body.data.id}`)
                .set({
                  Accept: 'application/json',
                  Authorization: `Bearer ${login.body.data.token}`
                }).then(foundUser => {
                  return request(app)
                    .post('/client/auth/validate-otp')
                    .send({
                      'otp': foundUser.body.data.resetPasswordLink.code,
                      "username": foundUser.body.data.email
                    }).then(user => {
                      expect(user.headers['content-type']).toEqual('application/json; charset=utf-8');
                      expect(user.body.status).toBe('SUCCESS');
                      expect(user.statusCode).toBe(200);
                    });
                });
            });
        });
  });
});
describe('POST /validate-otp -> if OTP is incorrect or OTP has expired', () => {
  test('should return invalid OTP', () => {
    return request(app)
      .post('/client/auth/send_login_otp')
      .send(
        {
          username: 'Liliana.VonRueden@hotmail.com',
          password: 'J9cwpxlU71Xg0LG'
        }).then(loginOtp => () => {
          return request(app)
            .get(`/client/api/v1/user/login_with_otp`)
            .set({
              username: 'Liliana.VonRueden@hotmail.com',
              password: 'J9cwpxlU71Xg0LG',
              code: loginOtp.body.data.loginOTP.code
            }).then(login => () => {
              return request(app)
                .get(`/client/api/v1/user/${login.body.data.id}`)
                .set({
                  Accept: 'application/json',
                  Authorization: `Bearer ${login.body.data.token}`
                }).then(foundUser => {
                  return request(app)
                    .post('/client/auth/validate-otp')
                    .send({ 'otp': foundUser.body.data.resetPasswordLink.code, "username": foundUser.body.data.email }).then(user => {
                      expect(user.headers['content-type']).toEqual('application/json; charset=utf-8');
                      expect(user.body.status).toBe('FAILURE');
                      expect(user.statusCode).toBe(404);
                      expect(user.body.message).toBe('Invalid OTP');
                    });
                });
            });
        });
  });
});

describe('POST /validate-otp -> if request body is empty or otp has not been sent in body', () => {
  test('should return insufficient parameter', async () => {
    let user = await request(app)
      .post('/client/auth/validate-otp')
      .send({});

    expect(user.headers['content-type']).toEqual('application/json; charset=utf-8');
    expect(user.body.status).toBe('BAD_REQUEST');
    expect(user.statusCode).toBe(400);
  });
});

describe('PUT /reset-password -> code is sent in request body and code is correct', () => {
  test('should return success', () => {
    return request(app)
      .post('/client/auth/send_login_otp')
      .send(
        {
          username: 'Liliana.VonRueden@hotmail.com',
          password: 'J9cwpxlU71Xg0LG'
        }).then(loginOtp => () => {
          return request(app)
            .get(`/client/api/v1/user/login_with_otp`)
            .set({
              username: 'Liliana.VonRueden@hotmail.com',
              password: 'J9cwpxlU71Xg0LG',
              code: loginOtp.body.data.loginOTP.code
            }).then(login => () => {
              return request(app)
                .get(`/client/api/v1/user/${login.body.data.id}`)
                .set({
                  Accept: 'application/json',
                  Authorization: `Bearer ${login.body.data.token}`
                }).then(foundUser => {
                  return request(app)
                    .put('/client/auth/validate-otp')
                    .send({
                      'code': "0000",
                      'username': foundUser.body.data.email
                    }).then(user => {
                      expect(user.headers['content-type']).toEqual('application/json; charset=utf-8');
                      expect(user.body.status).toBe('SUCCESS');
                      expect(user.statusCode).toBe(400);
                    });
                });
            });
        });
  });
});

describe('PUT /reset-password -> if request body is empty or code/newPassword is not given', () => {
  test('should return insufficient parameter', async () => {
    let user = await request(app)
      .put('/client/auth/reset-password')
      .send({});

    expect(user.headers['content-type']).toEqual('application/json; charset=utf-8');
    expect(user.body.status).toBe('BAD_REQUEST');
    expect(user.statusCode).toBe(400);
  });
});

describe('PUT /reset-password -> if email is not exist', () => {
  test('should return Invalid code. Please try again.', async () => {
    let user = await request(app)
      .put('/client/auth/reset-password')
      .send({
        'email': '123',
        'newPassword': 'testPassword'
      });

    expect(user.headers['content-type']).toEqual('application/json; charset=utf-8');
    expect(user.body.status).toBe('FAILURE');
    expect(user.body.message).toBe('Email not registered');
    expect(user.statusCode).toBe(400);
  });
});

afterAll(function (done) {
  db.connection.db.dropDatabase(function () {
    db.connection.close(function () {
      done();
    });
  });
});
