
const dotenv = require('dotenv');
dotenv.config();
process.env.NODE_ENV = 'test';
const db = require('mongoose');
const request = require('supertest');
const {
    MongoClient, ObjectId
} = require('mongodb');
const app = require('../../app.js');
const authConstant = require('../../constants/authConstant');
const uri = 'mongodb://127.0.0.1:27017';

const client = new MongoClient(uri, {
    useUnifiedTopology: true,
    useNewUrlParser: true
});

let inserted_user = {};

beforeAll(async function () {
    try {
        await client.connect();
        const db = client.db('MyBricks_test');

        const user = db.collection('user');
        inserted_user = await user.insertOne({
            password: 'UwdcNu60_pE2KnJ',
            email: 'Ayana.Swift@yahoo.com',
            firstName: 'Erica Blick',
            phoneNo: '+445485457854',
            lastName: 'Otis Kovacek',
            loginOTP: {},
            role: 624,
            resetPasswordLink: {},
            loginRetryLimit: 919,
            loginReactiveTime: '2022-01-07T12:15:19.782Z',
            id: '616fc8782f0fbaeb100259f7'
        });
    }
    catch (err) {
        console.error(`we encountered ${err}`);
    }
    finally {
        client.close();
    }
});

describe('POST /register -> if email and username is given', () => {
    test('should register a user', async () => {
        let registeredUser = await request(app)
            .post('/client/auth/register')
            .send({
                'password': 'J9cwpxlU71Xg0LG',
                'email': 'test@hotmail.com',
                'firstName': 'Bryant Kozey',
                'phoneNo': '+447932935146',
                'lastName': 'Ms. Mable Douglas',
                'addedBy': inserted_user.insertedId,
                'updatedBy': inserted_user.insertedId,
                'role': authConstant.USER_ROLE.User
            });
        expect(registeredUser.headers['content-type']).toEqual('application/json; charset=utf-8');
        expect(registeredUser.body.status).toBe('SUCCESS');
        expect(registeredUser.body.data).toMatchObject({ id: expect.any(String) });
        expect(registeredUser.statusCode).toBe(200);
    });
});

describe('GET /client/api/v1/user/:id-> User found', () => {
    test('should return success', () => {
        return request(app)
            .post('/client/auth/send_login_otp')
            .send(
                {
                    username: 'test@hotmail.com',
                    password: 'J9cwpxlU71Xg0LG'
                }).then(loginOtp => () => {
                    return request(app)
                        .post(`/client/api/v1/user/login_with_otp`)
                        .send({
                            username: 'test@hotmail.com',
                            password: 'J9cwpxlU71Xg0LG',
                            code: loginOtp.body.data.loginOTP.code
                        }).then(login => () => {
                            return request(app)
                                .get(`/client/api/v1/user/${login.body.data.id}`)
                                .set({
                                    Accept: 'application/json',
                                    Authorization: `Bearer ${login.body.data.token}`
                                }).then(foundUser => {
                                    expect(foundUser.headers['content-type']).toEqual('application/json; charset=utf-8');
                                    expect(foundUser.body.status).toBe('SUCCESS');
                                    expect(foundUser.statusCode).toBe(200);
                                });

                        });
                });
    });
});

describe('GET /client/api/v1/user/:id-> User id not found', () => {
    test('should return record not found', () => {
        return request(app)
            .post('/client/auth/send_login_otp')
            .send(
                {
                    username: 'test@hotmail.com',
                    password: 'J9cwpxlU71Xg0LG'
                }).then(loginOtp => () => {
                    return request(app)
                        .post(`/client/api/v1/user/login_with_otp`)
                        .send({
                            username: 'test@hotmail.com',
                            password: 'J9cwpxlU71Xg0LG',
                            code: loginOtp.body.data.loginOTP.code
                        }).then(login => () => {
                            return request(app)
                                .get(`/client/api/v1/user/616fc8782f0fcaeb100259f7`)
                                .set({
                                    Accept: 'application/json',
                                    Authorization: `Bearer ${login.body.data.token}`,
                                }).then(foundUser => {
                                    expect(foundUser.headers['content-type']).toEqual('application/json; charset=utf-8');
                                    expect(foundUser.body.status).toBe('RECORD_NOT_FOUND');
                                    expect(foundUser.statusCode).toBe(404);
                                });

                        });
                });
    });
});

describe('PUT /client/api/v1/user/update/:id-> User id not found', () => {
    test('should return record not found', () => {
        return request(app)
            .post('/client/auth/send_login_otp')
            .send(
                {
                    username: 'test@hotmail.com',
                    password: 'J9cwpxlU71Xg0LG'
                }).then(loginOtp => () => {
                    return request(app)
                        .post(`/client/api/v1/user/login_with_otp`)
                        .send({
                            username: 'test@hotmail.com',
                            password: 'J9cwpxlU71Xg0LG',
                            code: loginOtp.body.data.loginOTP.code
                        }).then(login => () => {
                            return request(app)
                                .put(`/client/api/v1/user/update/616fc8782f0fcaeb100259f7`)
                                .send({
                                    firstName: "id wrong"
                                })
                                .set({
                                    Accept: 'application/json',
                                    Authorization: `Bearer ${login.body.data.token}`
                                }).then(foundUser => {
                                    expect(foundUser.headers['content-type']).toEqual('application/json; charset=utf-8');
                                    expect(foundUser.body.status).toBe('RECORD_NOT_FOUND');
                                    expect(foundUser.statusCode).toBe(404);
                                });

                        });
                });
    });
});

describe('PUT /client/api/v1/user/update/:id-> User updated successfully', () => {
    test('should return success', () => {
        return request(app)
            .post('/client/auth/send_login_otp')
            .send(
                {
                    username: 'test@hotmail.com',
                    password: 'J9cwpxlU71Xg0LG'
                }).then(loginOtp => () => {
                    return request(app)
                        .post(`/client/api/v1/user/login_with_otp`)
                        .send({
                            username: 'test@hotmail.com',
                            password: 'J9cwpxlU71Xg0LG',
                            code: loginOtp.body.data.loginOTP.code
                        }).then(login => () => {
                            return request(app)
                                .put(`/client/api/v1/user/update/${login.body.data.id}`)
                                .send({
                                    firstName: "changed Namne"
                                })
                                .set({
                                    Accept: 'application/json',
                                    Authorization: `Bearer ${login.body.data.token}`
                                }).then(foundUser => {
                                    expect(foundUser.headers['content-type']).toEqual('application/json; charset=utf-8');
                                    expect(foundUser.body.status).toBe('SUCCESS');
                                    expect(foundUser.statusCode).toBe(200);
                                });

                        });
                });
    });
});
describe('PUT /client/api/v1/user/softDelete/:id-> User id not found', () => {
    test('should return record not found', () => {
        return request(app)
            .post('/client/auth/send_login_otp')
            .send(
                {
                    username: 'test@hotmail.com',
                    password: 'J9cwpxlU71Xg0LG'
                }).then(loginOtp => () => {
                    return request(app)
                        .post(`/client/api/v1/user/login_with_otp`)
                        .send({
                            username: 'test@hotmail.com',
                            password: 'J9cwpxlU71Xg0LG',
                            code: loginOtp.body.data.loginOTP.code
                        }).then(login => () => {
                            return request(app)
                                .put(`/client/api/v1/user/softDelete/616fc8782f0fcaeb100259f7`)
                                .set({
                                    Accept: 'application/json',
                                    Authorization: `Bearer ${login.body.data.token}`
                                }).then(foundUser => {
                                    expect(foundUser.headers['content-type']).toEqual('application/json; charset=utf-8');
                                    expect(foundUser.body.status).toBe('RECORD_NOT_FOUND');
                                    expect(foundUser.statusCode).toBe(404);
                                });

                        });
                });
    });
});

describe('PUT /client/api/v1/user/softDelete/:id-> User deleted successfully', () => {
    test('should return success', () => {
        return request(app)
            .post('/client/auth/send_login_otp')
            .send(
                {
                    username: 'test@hotmail.com',
                    password: 'J9cwpxlU71Xg0LG'
                }).then(loginOtp => () => {
                    return request(app)
                        .post(`/client/api/v1/user/login_with_otp`)
                        .send({
                            username: 'test@hotmail.com',
                            password: 'J9cwpxlU71Xg0LG',
                            code: loginOtp.body.data.loginOTP.code
                        }).then(login => () => {
                            return request(app)
                                .put(`/client/api/v1/user/softDelete/${login.body.data.id}`)
                                .set({
                                    Accept: 'application/json',
                                    Authorization: `Bearer ${login.body.data.token}`
                                }).then(foundUser => {
                                    expect(foundUser.headers['content-type']).toEqual('application/json; charset=utf-8');
                                    expect(foundUser.body.status).toBe('SUCCESS');
                                    expect(foundUser.statusCode).toBe(200);
                                });

                        });
                });
    });
});

describe('PUT /client/api/v1/user/change-password-> oldPassword incorrect.', () => {
    test('should return failure', () => {
        return request(app)
            .post('/client/auth/send_login_otp')
            .send(
                {
                    username: 'test@hotmail.com',
                    password: 'J9cwpxlU71Xg0LG'
                }).then(loginOtp => () => {
                    return request(app)
                        .post(`/client/api/v1/user/login_with_otp`)
                        .send({
                            username: 'test@hotmail.com',
                            password: 'J9cwpxlU71Xg0LG',
                            code: loginOtp.body.data.loginOTP.code
                        }).then(login => () => {
                            return request(app)
                                .put(`/client/api/v1/user/change-password`)
                                .send({
                                    oldPassword: `zdfszdfgvdfsvsfd`,
                                    newPassword: "Test@1234"
                                })
                                .set({
                                    Accept: 'application/json',
                                    Authorization: `Bearer ${login.body.data.token}`,
                                })
                                .then(foundUser => {
                                    expect(foundUser.headers['content-type']).toEqual('application/json; charset=utf-8');
                                    expect(foundUser.body.status).toBe('FAILURE');
                                    expect(foundUser.statusCode).toBe(400);
                                });

                        });
                });
    });
});
describe('PUT /client/api/v1/user/change-password-> new password missing.', () => {
    test('should return bad request', () => {
        return request(app)
            .post('/client/auth/send_login_otp')
            .send(
                {
                    username: 'test@hotmail.com',
                    password: 'J9cwpxlU71Xg0LG'
                }).then(loginOtp => () => {
                    return request(app)
                        .post(`/client/api/v1/user/login_with_otp`)
                        .send({
                            username: 'test@hotmail.com',
                            password: 'J9cwpxlU71Xg0LG',
                            code: loginOtp.body.data.loginOTP.code
                        }).then(login => () => {
                            return request(app)
                                .put(`/client/api/v1/user/change-password`)
                                .send({
                                    oldPassword: `zdfszdfgvdfsvsfd`
                                })
                                .set({
                                    Accept: 'application/json',
                                    Authorization: `Bearer ${login.body.data.token}`,
                                })
                                .then(foundUser => {
                                    expect(foundUser.headers['content-type']).toEqual('application/json; charset=utf-8');
                                    expect(foundUser.body.status).toBe('VALIDATION_ERROR');
                                    expect(foundUser.statusCode).toBe(422);
                                });

                        });
                });
    });
});
describe('PUT /client/api/v1/user/change-password-> Password changed successfully.', () => {
    test('should return success', () => {
        return request(app)
            .post('/client/auth/send_login_otp')
            .send(
                {
                    username: 'test@hotmail.com',
                    password: 'J9cwpxlU71Xg0LG'
                }).then(loginOtp => () => {
                    return request(app)
                        .post(`/client/api/v1/user/login_with_otp`)
                        .send({
                            username: 'test@hotmail.com',
                            password: 'J9cwpxlU71Xg0LG',
                            code: loginOtp.body.data.loginOTP.code
                        }).then(login => () => {
                            return request(app)
                                .put(`/client/api/v1/user/change-password`)
                                .send({
                                    oldPassword: `J9cwpxlU71Xg0LG`,
                                    newPassword: "Test@1234"
                                })
                                .set({
                                    Accept: 'application/json',
                                    Authorization: `Bearer ${login.body.data.token}`,
                                })
                                .then(foundUser => {
                                    expect(foundUser.headers['content-type']).toEqual('application/json; charset=utf-8');
                                    expect(foundUser.body.status).toBe('SUCCESS');
                                    expect(foundUser.statusCode).toBe(200);
                                });

                        });
                });
    });
});
afterAll(function (done) {
    db.connection.db.dropDatabase(function () {
        db.connection.close(function () {
            done();
        });
    });
});
