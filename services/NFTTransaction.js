const axios = require('axios');
const crypto = require('crypto');
const Web3 = require('web3');
const User = require('../model/user');
const dbService = require('../utils/dbService');
const Contract = require('../contract/ABI.json');
const NFTContract = require('../contract/nft.json');
const ContractToken = require('../contract/token.json');
const path = [process.env.WBNB, process.env.WBRICKS];
const Transaction = require('../model/transaction');
const moment = require('moment');
const provider = new Web3.providers.HttpProvider(process.env.WEB3_PROVIDER_URL);
const web3 = new Web3(provider);
const token = new web3.eth.Contract(ContractToken, process.env.WBRICKS);
const RouterContract = new web3.eth.Contract(Contract, process.env.ROUTER);
const nftToken = new web3.eth.Contract(NFTContract, process.env.NFT);
const _ = require('lodash');
const activityService = require('./activity');
const transactionService = require('./transaction');

let nfts = module.exports;

nfts.getNFTByWalletAddress = async (walletAddress) => {
    try {
        let balance = await nftToken.methods.balanceOf(walletAddress).call();
        if (parseInt(balance)) {
            let ids = [];
            for (let i = 0; i < parseInt(balance); i++) {
                let id = await nftToken.methods.tokenOfOwnerByIndex(walletAddress, i).call();
                let uri = await nftToken.methods.uri(id).call();
                let object = await axios.get(uri);
                object.data.tokenId = id;
                ids.push(object.data);
            }
            return {
                flag: false,
                data: ids
            };
        }
        return {
            flag: true,
            data: parseInt(balance)
        };
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

nfts.getNFTPrice = async (tokenId) => {
    try {
        let res;
        const config = {
            method: 'get',
            url: `https://deep-index.moralis.io/api/v2/nft/${process.env.NFT}/${tokenId}/transfers?chain=ropsten&format=decimal`,
            headers: {
                Accept: 'application/json',
                'X-API-Key': `${process.env.MORALIS_API_KEY}`
            }
        };
        await axios(config).then((response) => {
            if (response.data.result.length) {
                res = {
                    flag: false,
                    data: response.data.result
                };
            } else {
                res = {
                    flag: false,
                    data: { value: 0 }
                };
            }
        }, (error) => {
            console.error(error);
            res = {
                flag: true,
                data: 'Error to fetch price of NFT'
            };
        });
        if (!res.flag && res.data && res.data.length) {
            for (let i = 0; i < res.data.length; i++) {
                if (res.data[i].value > 0) {
                    res.data = { value: web3.utils.fromWei(res.data[i].value) };
                    break;
                }
            }
            if (!res.data.value) {
                res.data = { value: 0 };
            }
        }
        return res;
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

nfts.NFTtoNFTTransaction = async (loginUser, from, to, tokenId) => {
    try {
        const gasPrice = await web3.eth.getGasPrice();
        const gasValue = await nftToken.methods.safeTransferFrom(from, to, tokenId).estimateGas();
        let storedData = await transactionService.createTransactionWithOutCheck(loginUser, from, to, tokenId, tokenId, 'NFT', 'NFT', null, Web3.utils.fromWei((gasValue * gasPrice).toString()), 'PENDING', 'WITHDRAW', null, moment().format('X'));
        const data = await nftToken.methods.safeTransferFrom(from, to, tokenId).encodeABI();
        let transData = await transactionService.createEthTransaction(loginUser, from, process.env.NFT, 0, process.env.ASSET_SYMBOL, data, true, gasValue);
        if (!transData.flag) {
            await activityService.sendActivityEmail('MyBricks Withdrawal Confirmation', loginUser.email, '/views/withdraw', {
                amount: 1,
                currency: `NFT (Token id :- ${tokenId})`,
                firstName: loginUser.firstName,
                link: `${process.env.TRANSACTION_URL}${transData.data.transactionHash}`,
                server: process.env.NODE_ENV
            });
            await dbService.updateDocument(Transaction, { _id: storedData.id }, {
                transactionHash: transData.data.transactionHash,
                status: transData.data.status,
                requestId: transData.data.requestId
            });
            return {
                flag: false,
                data: transData.data
            };
        }
        return {
            flag: true,
            data: transData.data
        };
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

nfts.NFTtoNFTCalculation = async (from, to, tokenId) => {
    try {
        const gasPrice = await web3.eth.getGasPrice();
        const gasValue = await nftToken.methods.safeTransferFrom(from, to, tokenId).estimateGas();
        if (gasPrice && gasValue) {
            const fees = web3.utils.fromWei((gasValue * gasPrice * 2).toString());
            const balance = await transactionService.getBalance(from);
            if (!balance.flag && balance.data.BNBbalance > fees) {
                return {
                    flag: false,
                    data: { fees }
                };
            } else {
                return {
                    flag: true,
                    data: 'You do not have enough balance to proceed with this transaction.'
                };
            }
        }
        return {
            flag: true,
            data: 'failed to calculate'
        };
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};
