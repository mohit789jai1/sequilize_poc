const axios = require('axios');
const crypto = require('crypto');
const Web3 = require('web3');
const User = require('../model/user');
const dbService = require('../utils/dbService');
const { TrustVault } = require('@trustology/trustvault-nodejs-sdk');
const asn1 = require('asn1.js');
const BN = require('bn.js');
const AWS = require('aws-sdk');
AWS.config.loadFromPath('./config.json');
const kms = new AWS.KMS({
    apiVersion: '2014-11-01',
    region: 'eu-west-2'
});
const {
    replaceAll, generateRandomString
} = require('../utils/common');
const { PROJECT_SETTING } = require('../constants/userConstant');
const Contract = require('../contract/ABI.json');
const ContractToken = require('../contract/token.json');
const path = [process.env.WBNB, process.env.WBRICKS];
const ProjectSetting = require('../model/projectSetting');
const Transaction = require('../model/transaction');
const moment = require('moment');
const provider = new Web3.providers.HttpProvider(process.env.WEB3_PROVIDER_URL);
const web3 = new Web3(provider);
const token = new web3.eth.Contract(ContractToken, process.env.WBRICKS);
const RouterContract = new web3.eth.Contract(Contract, process.env.ROUTER);
const _ = require('lodash');
const mongoose = require('mongoose');
const activityService = require('./activity');

async function findTransactionWithSort (filter, sort = {}) {
    return Transaction.find(filter).sort(sort);
}

async function findOneTransaction (filter) {
    return Transaction.findOne(filter);
}

const SUBJECT_PUBLICKEY_INFO_SCHEMA = asn1.define('SubjectPublicKeyInfo', function () {
    this.seq().obj(
        this.key('algorithmIdentifier').seq(
            this.key('algorithm').optional().objid(),
            this.key('parameters').optional().objid(),
        ),
        this.key('subjectPublicKey').bitstr(),
    );
});

const SIGNATURE_SCHEMA = asn1.define('Signature', function () {
    this.seq().obj(this.key('r').int(), this.key('s').int());
});

let trans = module.exports;

trans.createCustodialWallet = async (user, name = null) => {
    try {
        if (user && !user.isCustodialWalletExists) {
            let WalletName = name ? name : user.email;
            let out = await trans.createSubWallet(WalletName);
            if (out && out.data && out.data.createSubWallet) {
                let subWalletId = out.data.createSubWallet.subWalletId;
                let walletAddress = out.data.createSubWallet.receiveAddressDetails.unverifiedAddress;
                if (!out.flag) {
                    await dbService.updateDocument(User, { _id: user.id }, {
                        custodialWalletAddress: walletAddress,
                        subWalletId: subWalletId,
                        isCustodialWalletExists: true
                    });
                    /*
                     * const gasValue = await token.methods.transfer(walletAddress, 1000 * 1000000000).estimateGas({ from: process.env.CENTRAL_WALLET_ADDRESS })
                     * const gasPrice = await web3.eth.getGasPrice();
                     * let transData = await this.BricksToBricksTransaction(user, 1000 * 1000000000, process.env.CENTRAL_WALLET_ADDRESS, walletAddress);
                     * await this.storeTransaction(user, process.env.CENTRAL_WALLET_ADDRESS, walletAddress, "1000", "1000", "BRICKS", "BRICKS", transData.data.transactionHash, Web3.utils.fromWei((gasValue * gasPrice).toString()), transData.data.status, "BONUS", transData.data.requestId, moment().format("X"))
                     */
                    return {
                        flag: false,
                        data: {
                            message: `Custodial wallet created successfully!`,
                            data: {
                                subWalletId,
                                walletAddress
                            }
                        }
                    };
                }
            }
            return {
                flag: true,
                data: 'Custodial wallet creation failed'
            };
        }
        return {
            flag: true,
            data: 'Custodial wallet creation failed or already exist'
        };
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

trans.createSubWallet = async (name) => {
    try {
        let res;
        let data = JSON.stringify({
            query: `mutation($type: SubWalletType!, $name: String!, $walletId: String!, ) {
            createSubWallet(
                createSubWalletInput: {
                    type: $type,
                    name: $name,
                    walletId: $walletId,
                }
            ) {
                subWalletId
                receiveAddressDetails {
                    addressType
                    path
                    publicKey
                    trustVaultProvenanceSignature
                    unverifiedAddress
                }
            }
        }`,
            variables: {
                'type': process.env.ASSET_SYMBOL,
                'name': name,
                'walletId': process.env.WALLET_ID
            }
        });

        let config = {
            method: 'post',
            url: process.env.TRUSTOLOGY_URL,
            headers: {
                'x-api-key': process.env.TRUST_VAULT_API_KEY,
                'Content-Type': 'application/json'
            },
            data: data
        };

        await axios(config)
            .then(function (response) {
                if (response.data.data) {
                    res = {
                        flag: false,
                        data: response.data.data
                    };
                } else {
                    throw response.data.errors;
                }
            })
            .catch(function (error) {
                console.error(error);
                res = {
                    flag: true,
                    data: error
                };
            });
        return res;
    } catch (error) {
        throw new Error(error.message);
    }
};

trans.getSubWalletDetails = async (subWalletId) => {
    try {
        let trustVault;
        if (process.env.NODE_ENV == 'production' || process.env.NODE_ENV == 'staging') {
            trustVault = new TrustVault({ apiKey: process.env.TRUST_VAULT_API_KEY });
        } else {
            trustVault = new TrustVault({
                apiKey: process.env.TRUST_VAULT_API_KEY,
                environment: 'sandbox'
            });
        }
        return await trustVault.getSubWallet(subWalletId);
    }
    catch (error) {
        console.error(error);
        throw new Error(error.message);
    }

};

trans.findStatusOfRequest = async (requestId) => {
    try {
        let res;
        let data = JSON.stringify({
            query: `query($requestId: String!) {
            getRequest(requestId: $requestId) {
              requestId
              status
              type
              transactionHash
            }
          }`,
            variables: { 'requestId': requestId }
        });

        let config = {
            method: 'post',
            url: process.env.TRUSTOLOGY_URL,
            headers: {
                'x-api-key': process.env.TRUST_VAULT_API_KEY,
                'Content-Type': 'application/json'
            },
            data: data
        };

        await axios(config)
            .then(function (response) {
                if (response.data.data && response.data.data.getRequest.status !== 'ERROR') {
                    res = {
                        flag: false,
                        data: response.data.data.getRequest
                    };
                } else {
                    throw response.data;
                }
            })
            .catch(function (error) {
                console.error(error);
                res = {
                    flag: true,
                    data: error
                };
            });
        return res;
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

trans.getBNBDepositHistory = async (user, subWalletId) => {
    try {
        let res;
        let data = JSON.stringify({
            query: `query getTransactions($subWalletId: String!) {
                user {
                    subWallet(subWalletId: $subWalletId) {
                        transactions(nextToken: null) {
                            items {
                                from {
                                    ... on Address {
                                        id
                                    }
                                }
                                to {
                                    ... on Address {
                                        id
                                    }
                                }
                                amount {
                                    value
                                    timestamp
                                    currency
                                }
                                createdAt
                                updatedAt
                                ... on BlockchainTransaction {
                                    transactionDirection
                                    fee {
                                        value
                                        timestamp
                                        currency
                                    }
                                    transactionHash
                                }
                            }
                            nextToken
                        }
                    }
                }
            }`,
            variables: { 'subWalletId': subWalletId, }
        });

        let config = {
            method: 'post',
            url: process.env.TRUSTOLOGY_URL,
            headers: {
                'x-api-key': process.env.TRUST_VAULT_API_KEY,
                'Content-Type': 'application/json'
            },
            data: data
        };

        await axios(config)
            .then(function (response) {
                if (response.data.data) {
                    res = {
                        flag: false,
                        data: response.data.data.user.subWallet.transactions.items
                    };
                } else {
                    throw response.data.errors[0].message;
                }
            })
            .catch(function (error) {
                console.error(error);
                res = {
                    flag: true,
                    data: error
                };
            });

        if (!res.flag) {
            let records = [];
            if (res.data && res.data.length) {
                let existsTrans = await dbService.getDocumentByQuery(Transaction, { user: user.id });
                existsTrans = _.map(existsTrans, 'transactionHash');
                await Promise.all(_.map(res.data, async (transaction) => {
                    if (moment(user.lastSyncDate) < moment(transaction.updatedAt) && transaction.transactionDirection == 'RECEIVED' && (transaction.to.id).toLowerCase() == (user.custodialWalletAddress).toLowerCase() && (process.env.CENTRAL_WALLET_ADDRESS).toLowerCase() != (transaction.from.id).toLowerCase() && !existsTrans.includes(transaction.transactionHash) && (process.env.DEPOSIT_ALLOW_CURRENCY.split(',')).includes(transaction.amount.currency)) {
                        let createdRecord = await this.storeTransaction(user, transaction.from.id, transaction.to.id, transaction.amount.value, transaction.amount.value, transaction.amount.currency, transaction.amount.currency, transaction.transactionHash, transaction.fee.value, transaction.transactionDirection, 'DEPOSIT', null, moment(transaction.createdAt).format('X'));
                        if (!createdRecord.data) {
                            records.push(createdRecord.data);
                        }
                    }
                }));
            }
            res.data = records;
        }
        return res;
    } catch (error) {
        throw new Error(error.message);
    }
};

trans.getBricksDepositHistory = async (user, walletAddress) => {
    try {
        let res;
        let config = {
            method: 'get',
            url: `${process.env.HISTORY_URL}/api?module=account&action=tokentx&contractaddress=${process.env.WBRICKS}&address=${walletAddress}&startblock=${user.lastSyncBlockNumber || 0}&sort=desc&apikey=${process.env.HISTORY_URL_API_KEY}`,
            headers: { 'Content-Type': 'application/json' }
        };

        await axios(config)
            .then(function (response) {
                if (parseInt(response.data.status) || response.data.message == 'No transactions found') {
                    res = {
                        flag: false,
                        data: response.data.result
                    };
                } else {
                    throw response.data.result;
                }
            })
            .catch(function (error) {
                res = {
                    flag: true,
                    data: error
                };
            });
        if (!res.flag) {
            let records = [];
            if (res.data.length) {
                let existsTrans = await dbService.getDocumentByQuery(Transaction, { user: user.id });
                existsTrans = _.map(existsTrans, 'transactionHash');
                await Promise.all(_.map(res.data, async (transaction) => {
                    if (transaction.to == (user.custodialWalletAddress).toLowerCase() && !existsTrans.includes(transaction.blockHash) && (process.env.CENTRAL_WALLET_ADDRESS).toLowerCase() != (transaction.from).toLowerCase() && (process.env.UNISWAP_PAIR_CONTRACT).toLowerCase() != (transaction.from).toLowerCase()) {
                        let createdRecord = await this.storeTransaction(user, transaction.from, transaction.to, transaction.value / 1000000000, transaction.value / 1000000000, 'BRICKS', 'BRICKS', transaction.blockHash, web3.utils.fromWei((transaction.gasPrice * transaction.gasUsed).toString()), 'COMPLETED', 'DEPOSIT', null, transaction.timeStamp);
                        if (!createdRecord.data) {
                            records.push(createdRecord.data);
                        }
                    }
                }));
                await dbService.updateDocument(User, { _id: user.id }, { lastSyncBlockNumber: res.data[0].blockNumber });
            }
            res.data = records;
        }
        return res;
    } catch (error) {
        throw new Error(error.message);
    }
};

trans.storeMoonPayTransaction = async (user, transactionId) => {
    try {
        let res;
        let config = {
            method: 'get',
            url: `${process.env.MOONPAY_HISTORY_URL}/${transactionId}?apiKey=${process.env.MOONPAY_PRIVATE_KEY}`,
            headers: { 'Content-Type': 'application/json' }
        };

        await axios(config)
            .then(function (response) {
                if (response.data) {
                    if (response.data.walletAddress == user.custodialWalletAddress) {
                        res = {
                            flag: false,
                            data: response.data
                        };
                    } else {
                        res = {
                            flag: true,
                            data: 'Invalid transaction\'s walletId not match with this user.'
                        };
                    }
                } else {
                    throw response.data.result;
                }
            })
            .catch(function (error) {
                res = {
                    flag: true,
                    data: error
                };
            });
        if (!res.flag) {
            let fromValue = res.data.baseCurrencyAmount + res.data.feeAmount + res.data.networkFeeAmount + res.data.extraFeeAmount;
            let gasFees = res.data.feeAmount + res.data.networkFeeAmount + res.data.extraFeeAmount;
            let fromTokenSymbol;
            if (res.data.eurRate == 1) {
                fromTokenSymbol = 'EUR';
            }
            else if (res.data.usdRate == 1) {
                fromTokenSymbol = 'USD';
            }
            else if (res.data.gbpRate == 1) {
                fromTokenSymbol = 'GBP';
            }
            let data = await this.storeTransaction(user, res.data.walletAddress, null, fromValue, res.data.quoteCurrencyAmount, fromTokenSymbol, 'BNB', res.data.cryptoTransactionId, gasFees, res.data.status, 'PURCHASE', transactionId, moment().format('X'));
            if (!data.flag) {
                res = {
                    flag: false,
                    data: data.data
                };
            }
        }
        return res;
    } catch (error) {
        throw new Error(error.message);
    }
};

trans.getHistory = async (user) => {
    try {
        /*
         * let BricksDeposit = await this.getBricksDepositHistory(user, user.custodialWalletAddress)
         * if (BricksDeposit && BricksDeposit.flag) {
         *     return {
         *         flag: true,
         *         data: BricksDeposit.data
         *     }
         * }
         */
        let BNBDeposit = await this.getBNBDepositHistory(user, user.subWalletId);
        if (BNBDeposit && BNBDeposit.flag) {
            return {
                flag: true,
                data: BNBDeposit.data
            };
        }
        await dbService.updateDocument(User, { _id: user.id }, { lastSyncDate: moment().toISOString() });
        let res = await findTransactionWithSort({ user: user.id }, { timestamps: -1 });
        return {
            flag: false,
            data: res
        };
    } catch (error) {
        throw new Error(error.message);
    }
};

trans.getTransactionDetail = async (userId, id) => {
    try {
        let filter = { user: userId };
        var isValid = mongoose.Types.ObjectId.isValid(id);
        if (isValid) {
            filter._id = id;
        }
        else {
            filter.requestId = id;
        }
        let res = await findOneTransaction(filter);
        if (res) {
            return {
                flag: false,
                data: res
            };
        }
        return {
            flag: true,
            data: 'failed to fetch history'
        };
    } catch (error) {
        throw new Error(error.message);
    }
};

trans.createEthTransaction = async (loginUser, from, to, value, type = process.env.ASSET_SYMBOL, contractData = '', isSmartContract = false, gasValue = 0) => {
    try {
        let res;
        if ((from).toLowerCase() == (process.env.CENTRAL_WALLET_ADDRESS).toLowerCase()) {
            let projectSetting = await dbService.getDocumentByQuery(ProjectSetting, { type: PROJECT_SETTING.NONCE_MANAGEMENT });
            await dbService.findOneAndUpdateDocument(ProjectSetting, { type: PROJECT_SETTING.NONCE_MANAGEMENT }, { setting: { pending: projectSetting.setting.pending + 1 } });
        }
        let variables = await this.calculateVariableForEthTransaction(from, to, value, type, contractData, isSmartContract, gasValue);
        if (variables) {
            let data = JSON.stringify({
                query: `mutation (
                    $from: String!,
                    $to: String!,
                    $value: String!,
                    $gasPrice: String!
                    $gas: String!
                    $data: String,
                    $chainId: Int!,
                    $nonce: Int!,
    
                ) {
                    createEthereumTransaction(
                        createTransactionInput: {
                            ethereumTransaction: {
                                fromAddress: $from
                                to: $to
                                value: $value
                                gasPrice: $gasPrice
                                gasLimit: $gas
                                chainId: $chainId
                                data: $data,
                                nonce : $nonce
                            }
                            source: "API",
                            sendToNetworkWhenSigned: true
                            sendToDevicesForSigning: true
                        }
                    ) {
                        ... on CreateEthereumTransactionResponse {
                            requestId
                        }
                        signData {
                            transaction {
                                to
                                fromAddress
                                value
                                gasPrice
                                gasLimit
                                nonce
                                chainId
                                data
                            }
                            hdWalletPath {
                                hdWalletPurpose
                                hdWalletCoinType
                                hdWalletAccount
                                hdWalletUsage
                                hdWalletAddressIndex
                            }
                            unverifiedDigestData {
                                transactionDigest
                                signData
                                shaSignData
                            }
                        }
                        assetRate
                        chainRate
                    }
                }`,
                variables: variables
            });

            let config = {
                method: 'post',
                url: process.env.TRUSTOLOGY_URL,
                headers: {
                    'x-api-key': process.env.TRUST_VAULT_API_KEY,
                    'Content-Type': 'application/json'
                },
                data: data
            };

            await axios(config)
                .then(function (response) {
                    if (response.data.data) {
                        let requestId = response.data.data.createEthereumTransaction.requestId;
                        let signData = response.data.data.createEthereumTransaction.signData.unverifiedDigestData.signData;
                        res = {
                            flag: false,
                            data: {
                                requestId,
                                signData
                            }
                        };
                    } else {
                        throw response.data.errors[0].message;
                    }
                })
                .catch(function (error) {
                    console.error(error);
                    res = {
                        flag: true,
                        data: error
                    };
                });
        } else {
            res = {
                flag: true,
                data: 'Can not calculate variable for this Transaction.'
            };
        }
        if (res.flag) {
            if ((from).toLowerCase() == (process.env.CENTRAL_WALLET_ADDRESS).toLowerCase()) {
                let projectSetting = await dbService.getDocumentByQuery(ProjectSetting, { type: PROJECT_SETTING.NONCE_MANAGEMENT });
                await dbService.findOneAndUpdateDocument(ProjectSetting, { type: PROJECT_SETTING.NONCE_MANAGEMENT }, { setting: { pending: projectSetting.setting.pending - 1 } });
            }
        }
        if (!res.flag) {
            res = await this.addSignature(res.data.requestId, res.data.signData, from);
        }
        return res;
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

trans.transferBNB = async (loginUser, from, to, value) => {
    try {
        const gasPrice = await web3.eth.getGasPrice();
        const gasValue = await web3.eth.estimateGas({ to: to });
        let storedData = await this.createTransactionWithOutCheck(loginUser, from, to, Web3.utils.fromWei(value.toString()), Web3.utils.fromWei(value.toString()).toString(), 'BNB', 'BNB', null, Web3.utils.fromWei((gasValue * gasPrice).toString()), 'PENDING', 'WITHDRAW', null, moment().format('X'));
        let transData = await this.createEthTransaction(loginUser, from, to, value - (gasValue * gasPrice * 2.4));
        if (!transData.flag) {
            await activityService.sendActivityEmail('MyBricks Withdrawal Confirmation', loginUser.email, '/views/withdraw', {
                amount: Web3.utils.fromWei(value.toString()),
                currency: 'BNB',
                firstName: loginUser.firstName,
                link: `${process.env.TRANSACTION_URL}${transData.data.transactionHash}`,
                server: process.env.NODE_ENV
            });
            await dbService.updateDocument(Transaction, { _id: storedData.id }, {
                transactionHash: transData.data.transactionHash,
                status: transData.data.status,
                requestId: transData.data.requestId
            });
            return {
                flag: false,
                data: transData.data
            };
        }
        return {
            flag: true,
            data: transData.data
        };
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

trans.BNBtoBNBCalculation = async (from, to, value) => {
    try {
        const gas = await web3.eth.estimateGas({
            from,
            to
        });
        const gasPrice = await web3.eth.getGasPrice();
        if (gas && gasPrice) {
            const actualValue = web3.utils.fromWei(value.toString());
            const fees = web3.utils.fromWei((gas * gasPrice).toString());
            return {
                flag: false,
                data: {
                    tokenAmount: actualValue,
                    fees,
                    receiveAmount: actualValue - fees
                }
            };
        }
        return {
            flag: true,
            data: 'failed to calculate'
        };
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

trans.changePolicy = async (walletId, publicKey) => {
    try {
        let res;
        let data = JSON.stringify({
            query: `mutation($walletId: String!, $delegateSchedules: [[ScheduleInput!]!]!) {
                createChangePolicyRequest(
                    createChangePolicyRequestInput: {
                        walletId: $walletId
                        delegateSchedules: $delegateSchedules
                    }
                ) {
                    requests {
                        walletId
                        requestId
                        recovererTrustVaultSignature
                        unverifiedDigestData{
                            shaSignData
                            signData
                        }
                        policyTemplate {
                            expiryTimestamp
                            delegateSchedules {
                                keys
                                quorumCount
                            }
                            recovererSchedules {
                                keys
                                quorumCount
                            }
                        }
                    }
                }
            }`,
            variables: {
                'walletId': walletId,
                'delegateSchedules':
                    [[
                        {
                            'quorumCount': 1,
                            'keys': publicKey
                        }
                    ]]
            }
        });

        let config = {
            method: 'post',
            url: process.env.TRUSTOLOGY_URL,
            headers: {
                'x-api-key': process.env.TRUST_VAULT_API_KEY,
                'Content-Type': 'application/json'
            },
            data: data
        };

        await axios(config)
            .then(async function (response) {
                if (response.data.data) {
                    let requestId = response.data.data.createChangePolicyRequest.requests[0].requestId;
                    let signData = response.data.data.createChangePolicyRequest.requests[0].unverifiedDigestData.signData;
                    res = {
                        flag: false,
                        data: {
                            requestId,
                            signData
                        }
                    };
                } else {
                    throw response.data.errors;
                }
            })
            .catch(function (error) {
                console.error(error);
                res = {
                    flag: true,
                    data: error
                };
            });
        if (!res.flag) {
            res = await this.addSignature(res.data.requestId, res.data.signData);
        }
        return res;
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

trans.calculateVariableForEthTransaction = async (from, to, value, type, data, isSmartContract, gasValue) => {
    try {
        const chainId = await web3.eth.getChainId();
        //BSC Mainnet - 56, BSC Testnet - 97
        if (chainId === parseInt(process.env.CHAIN_ID)) {
            let nonce = await web3.eth.getTransactionCount(from);
            const gasPrice = await web3.eth.getGasPrice();
            let gas;
            if (isSmartContract) {
                gas = gasValue;
            } else {
                gas = await web3.eth.estimateGas({ to: to });
            }
            if ((from).toLowerCase() == (process.env.CENTRAL_WALLET_ADDRESS).toLowerCase()) {
                let projectSetting = await dbService.getDocumentByQuery(ProjectSetting, { type: PROJECT_SETTING.NONCE_MANAGEMENT });
                if (projectSetting.setting.pending > -1) {
                    nonce = nonce + projectSetting.setting.pending;
                }
            }
            return {
                nonce: nonce,
                from: from,
                to: to,
                value: value,
                gasPrice: web3.utils.toHex(Math.floor(gasPrice * 2)),
                gas: web3.utils.toHex(gas),
                data: data,
                chainId: chainId,
                // assetSymbol: type
            };
        } else {
            console.log('Invalid ChainID');
        }
        return null;
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

trans.buyBNB = async (walletAddress, email) => {
    try {
        let newEmail = replaceAll(email, '@', '%40');
        let randomCode = generateRandomString();
        let baseURL = `${process.env.MOONPAY_URL}&currencyCode=${process.env.MOONPAY_DEFAULT_CODE}&walletAddress=${walletAddress}&baseCurrencyCode=gbp&email=${newEmail}&colorCode=%23ff655a&externalTransactionId=${randomCode}`;
        let resultURL = await this.generateBNBMoonpaySignature(baseURL);
        if (resultURL) {
            return {
                flag: false,
                data: resultURL
            };
        }
        return {
            flag: true,
            data: 'failed to create transaction URL'
        };
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

trans.generateBNBMoonpaySignature = async (url) => {
    try {
        const originalUrl = url;
        const signature = crypto
            .createHmac('sha256', process.env.MOONPAY_SIGN_KEY)
            .update(new URL(originalUrl).search)
            .digest('base64');

        return `${originalUrl}&signature=${encodeURIComponent(signature)}`;
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

trans.BNBtoBricksCalculation = async (BNBInput, walletAddress, type = 'WITHDRAW') => {
    try {
        let output = await RouterContract.methods.getAmountsOut(web3.utils.toWei(BNBInput), path).call();
        const taxFromContract = await token.methods._enableTax().call();
        let newOutput = parseInt(output[1]);
        if (taxFromContract) {
            newOutput = Math.floor(parseInt(output[1]) - (parseInt(output[1]) * 0.06));
        }
        let gasFeeWalletAddress;
        if (type == 'SWAP') {
            gasFeeWalletAddress = walletAddress;
        } else {
            gasFeeWalletAddress = process.env.CENTRAL_WALLET_ADDRESS;
        }
        output = newOutput / 1000000000;
        const date = Math.floor(Date.now() / 1000);
        const gasValue = await RouterContract.methods.swapExactETHForTokens(newOutput, path, walletAddress, date + 300).estimateGas({
            from: gasFeeWalletAddress,
            value: web3.utils.toWei(BNBInput)
        });
        const gasPrice = await web3.eth.getGasPrice();
        if (output) {
            return {
                flag: false,
                data: {
                    bricks: output,
                    fees: Web3.utils.fromWei((gasValue * gasPrice).toString())
                }
            };
        }
        return {
            flag: true,
            data: 'Enter valid Input'
        };

    }
    catch (error) {
        console.error(error);
        throw new Error(error.message);
    }

};

trans.BNBtoBricksTransaction = async (loginUser, BNBInput, walletAddress) => {
    try {
        let BrickTokenAmount = await this.BNBtoBricksCalculation(BNBInput, walletAddress, 'SWAP');
        if (!BrickTokenAmount.flag) {
            const date = Math.floor(Date.now() / 1000);
            const data = await RouterContract.methods.swapExactETHForTokens(Math.floor(BrickTokenAmount.data.bricks * 1000000000), path, walletAddress, date + 300).encodeABI();
            const gasValue = await RouterContract.methods.swapExactETHForTokens(Math.floor(BrickTokenAmount.data.bricks * 1000000000), path, walletAddress, date + 300).estimateGas({
                from: walletAddress,
                value: web3.utils.toWei(BNBInput)
            });
            const gasPrice = await web3.eth.getGasPrice();
            if (data) {
                let storedData = await this.createTransactionWithOutCheck(loginUser, walletAddress, walletAddress, BNBInput.toString(), BrickTokenAmount.data.bricks.toString(), 'BNB', 'BRICKS', null, Web3.utils.fromWei(Math.floor(gasValue * gasPrice).toString()), 'PENDING', 'SWAP', null, moment().format('X'));
                let transData = await this.createEthTransaction(loginUser, walletAddress, process.env.ROUTER, web3.utils.toWei(BNBInput), process.env.ASSET_SYMBOL, data, true, gasValue);
                if (!transData.flag) {
                    await dbService.updateDocument(Transaction, { _id: storedData.id }, {
                        transactionHash: transData.data.transactionHash,
                        status: transData.data.status,
                        requestId: transData.data.requestId
                    });
                    return {
                        flag: false,
                        data: transData.data
                    };
                }
                return {
                    flag: true,
                    data: transData.data
                };
            }
            return {
                flag: true,
                data: 'Error in router contract'
            };
        }
        return {
            flag: true,
            data: 'Enter valid Input'
        };

    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }

};

trans.getBalance = async (walletAddress) => {
    try {
        const BNBbalance = await web3.eth.getBalance(walletAddress);
        const BRICKSBalance = await token.methods.balanceOf(walletAddress).call();
        const setting = await this.LiveTokenValue();
        return {
            flag: false,
            data: {
                BNBbalance: web3.utils.fromWei(BNBbalance),
                maxUsedBNBBalance: web3.utils.fromWei((BNBbalance - '5000000000000000').toString()),
                BRICKSBalance: BRICKSBalance / 1000000000,
                walletAddress: walletAddress,
                currency: setting
            }
        };
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

trans.BToBTransactionWithGas = async (loginUser, BRICKSInput, from, to) => {
    try {
        let requiredData = await this.transactionPreRequired(loginUser, BRICKSInput, from, to, 'BRICKS_TRANSFER');
        if (requiredData.flag) {
            return {
                flag: true,
                data: requiredData.data
            };
        }
        let originalBricksAmount = BRICKSInput / 1000000000;
        BRICKSInput = BRICKSInput - requiredData.data;
        const data = await token.methods.transfer(to, BRICKSInput).encodeABI();
        const gasValue = await token.methods.transfer(to, BRICKSInput).estimateGas({ from: from });
        if (data) {
            let storedData = await this.createTransactionWithOutCheck(loginUser, from, to, originalBricksAmount.toString(), (BRICKSInput / 1000000000).toString(), 'BRICKS', 'BRICKS', null, requiredData.data / 1000000000, 'PENDING', 'WITHDRAW', null, moment().format('X'));
            let transData = await this.createEthTransaction(loginUser, from, process.env.WBRICKS, 0, process.env.ASSET_SYMBOL, data, true, gasValue);
            if (!transData.flag) {
                await activityService.sendActivityEmail('MyBricks Withdrawal Confirmation', loginUser.email, '/views/withdraw', {
                    amount: originalBricksAmount.toString(),
                    currency: 'BRICKS',
                    firstName: loginUser.firstName,
                    link: `${process.env.TRANSACTION_URL}${transData.data.transactionHash}`,
                    server: process.env.NODE_ENV
                });
                await dbService.updateDocument(Transaction, { _id: storedData.id }, {
                    transactionHash: transData.data.transactionHash,
                    status: transData.data.status,
                    requestId: transData.data.requestId
                });
                return {
                    flag: false,
                    data: transData.data
                };
            }
            return {
                flag: true,
                data: transData.data
            };
        }

        return {
            flag: true,
            data: 'Enter valid Input'
        };

    }
    catch (error) {
        console.error(error);
        throw new Error(error.message);
    }

};

trans.BricksToBricksTransaction = async (loginUser, BRICKSInput, from, to) => {
    try {
        const data = await token.methods.transfer(to, BRICKSInput).encodeABI();
        const gasValue = await token.methods.transfer(to, BRICKSInput).estimateGas({ from: from });
        if (data) {
            let transData = await this.createEthTransaction(loginUser, from, process.env.WBRICKS, 0, process.env.ASSET_SYMBOL, data, true, gasValue);
            if (!transData.flag) {
                return {
                    flag: false,
                    data: transData.data
                };
            }
        }

        return {
            flag: true,
            data: 'Enter valid Input'
        };

    }
    catch (error) {
        console.error(error);
        throw new Error(error.message);
    }

};

trans.LiveTokenValue = async () => {
    return dbService.getDocumentByQuery(ProjectSetting, { type: PROJECT_SETTING.CURRENCY_TOKEN_VALUE });
};

trans.getChartValue = async (currency, fromDate = null, toDate = null) => {
    try {
        let response;
        if (fromDate && toDate) {
            fromDate = moment(fromDate).unix();
            toDate = moment(toDate).unix();
            response = await axios.get(`https://api.coingecko.com/api/v3/coins/mybricks/market_chart/range?vs_currency=${currency}&from=${fromDate}&to=${toDate}`);
            return {
                flag: false,
                data: response.data
            };
        } else {
            response = await dbService.getDocumentByQuery(ProjectSetting, { type: PROJECT_SETTING.TOKEN_CHART });
            return {
                flag: false,
                data: response
            };
        }

    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

trans.calculateGasPrice = async (Input, from, to, flag) => {
    try {
        let gasValue;
        if (flag == 'BNB') {
            let BrickTokenAmount = await this.BNBtoBricksCalculation(Input, from);
            const date = Math.floor(Date.now() / 1000);
            gasValue = await RouterContract.methods.swapExactETHForTokens(Math.floor(BrickTokenAmount.data.bricks * 1000000000).toString(), path, from, date + 300).estimateGas({
                from: from,
                value: web3.utils.toWei(Input)
            });
        } else if (flag == 'BRICKS_TRANSFER') {
            gasValue = await token.methods.transfer(to, Input).estimateGas({ from: from });
        } else {
            return {
                flag: true,
                data: 'invalid flag'
            };
        }
        const gasPrice = await web3.eth.getGasPrice();
        return {
            flag: false,
            data: web3.utils.fromWei((Math.floor(gasValue * gasPrice * 5)).toString())
        };

    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }

};

trans.transactionPreRequired = async (loginUser, Input, from, to, flag) => {
    try {
        let valueForBricksTrans = 0;
        let gasData = await this.calculateGasPrice(Input, from, to, flag);
        if (gasData.flag) {
            return {
                flag: true,
                data: 'Error in calculate gas fee.'
            };
        }
        let BNBtransaction = await this.createEthTransaction(loginUser, process.env.CENTRAL_WALLET_ADDRESS, from, web3.utils.toWei(gasData.data));
        if (BNBtransaction.flag) {
            return {
                flag: true,
                data: BNBtransaction.data
            };
        }
        let BNBToBricksTransactionCalculation = await this.BNBtoBricksCalculation(gasData.data, from);
        if (BNBToBricksTransactionCalculation.flag) {
            return {
                flag: true,
                data: BNBToBricksTransactionCalculation.data
            };
        }
        valueForBricksTrans = BNBToBricksTransactionCalculation.data.bricks;
        const taxFromContract = await token.methods._enableTax().call();
        if (taxFromContract) {
            valueForBricksTrans = Math.floor((BNBToBricksTransactionCalculation.data.bricks + BNBToBricksTransactionCalculation.data.bricks * 0.07) * 1000000000).toString();
        }
        let BricksTransaction = await this.BricksToBricksTransaction(loginUser, valueForBricksTrans, from, process.env.CENTRAL_WALLET_ADDRESS);
        if (BricksTransaction.flag) {
            return {
                flag: true,
                data: BricksTransaction.data
            };
        }
        return {
            flag: false,
            data: valueForBricksTrans
        };

    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }

};

trans.transactionPreRequiredCalculation = async (Input, from, to, flag) => {
    try {
        let gasData = await this.calculateGasPrice(Input, from, to, flag);
        if (gasData.flag) {
            return {
                flag: true,
                data: 'Error in calculate gas fee.'
            };
        }

        let BNBToBricksTransactionCalculation = await this.BNBtoBricksCalculation(gasData.data, from);
        if (BNBToBricksTransactionCalculation.flag) {
            return {
                flag: true,
                data: 'Error in calculate BNB to Bricks.'
            };
        }
        let valueForBricksTrans = BNBToBricksTransactionCalculation.data.bricks;
        const taxFromContract = await token.methods._enableTax().call();
        if (taxFromContract) {
            valueForBricksTrans = Math.floor((BNBToBricksTransactionCalculation.data.bricks + BNBToBricksTransactionCalculation.data.bricks * 0.07) * 1000000000).toString();
        }
        let outputData = {
            tokenAmount: (Input / 1000000000).toString(),
            fees: (valueForBricksTrans / 1000000000).toString(),
            tax: gasData.data,
            receiveAmount: ((Input - valueForBricksTrans) / 1000000000).toString()
        };
        if (parseFloat(outputData.fees * 1.05) > parseFloat(outputData.tokenAmount)) {
            return {
                flag: true,
                data: 'Please increase the withdrawal amount in order to cover the gas fees.'
            };
        } else {
            return {
                flag: false,
                data: outputData
            };
        }

    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }

};

trans.addSignature = async (requestId, signData, from = null) => {
    try {
        let newSignature = await this.getVerifiedSignature(signData);
        let res;
        if (newSignature) {
            let data = JSON.stringify({
                query: `mutation(
        $requestId: String!
        $publicKeySignaturePairs: [PublicKeySignaturePair!]!
    ) {
        addSignature(
            addSignatureInput: {
                requestId: $requestId
                signRequests: [
                    {
                        publicKeySignaturePairs: $publicKeySignaturePairs
                    }
                ]
            }
        ) {
            requestId
        }
    }`,
                variables: {
                    'requestId': requestId,
                    'publicKeySignaturePairs':
                        [newSignature]
                }
            });

            var config = {
                method: 'post',
                url: process.env.TRUSTOLOGY_URL,
                headers: {
                    'x-api-key': process.env.TRUST_VAULT_API_KEY,
                    'Content-Type': 'application/json'
                },
                data: data
            };

            await axios(config)
                .then(function (response) {
                    if (response.data.data) {
                        res = {
                            flag: false,
                            data: response.data.data
                        };
                    } else {
                        throw response.data.errors;
                    }

                })
                .catch(function (error) {
                    console.error(error);
                    res = {
                        flag: true,
                        data: error
                    };
                });
        } else {
            res = {
                flag: true,
                data: 'Failed to generate signature'
            };
        }
        if (!res.flag) {
            let startNewTransaction = true;
            let findStatus;
            if (from) {
                if ((from).toLowerCase() == (process.env.CENTRAL_WALLET_ADDRESS).toLowerCase()) {
                    let projectSetting = await dbService.getDocumentByQuery(ProjectSetting, { type: PROJECT_SETTING.NONCE_MANAGEMENT });
                    let newCount = projectSetting.setting.pending - 1;
                    await dbService.findOneAndUpdateDocument(ProjectSetting, { type: PROJECT_SETTING.NONCE_MANAGEMENT }, { setting: { pending: newCount } });
                }
                while (startNewTransaction) {
                    findStatus = await this.findStatusOfRequest(res.data.addSignature.requestId);
                    if (findStatus && !findStatus.flag && findStatus.data.status == 'CONFIRMED') {
                        startNewTransaction = false;
                        res = findStatus;
                    }
                    if (findStatus && findStatus.flag) {
                        startNewTransaction = false;
                        res = {
                            flag: true,
                            data: 'Error in transaction after confirm the transaction from signature'
                        };
                    }
                }
            }
        }
        return res;
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

trans.getVerifiedSignature = async (message) => {
    try {
        const awsKeyId = process.env.AWS_KEY_ID;
        let publicKey = await this.getPublicKey(awsKeyId);
        if (publicKey) {
            const params = {
                KeyId: awsKeyId,
                Message: Buffer.from(message, 'hex'),
                SigningAlgorithm: 'ECDSA_SHA_256',
                MessageType: 'RAW',
            };
            const sig = await kms.sign(params).promise();
            if (sig.SigningAlgorithm && sig.SigningAlgorithm === 'ECDSA_SHA_256') {
                const signature = sig.Signature;
                const hexSignature = signature.toString('hex');
                const {
                    r, s
                } = this.decodeSignature(Buffer.from(hexSignature, 'hex'));
                const paddedR = Buffer.from(r.toString('hex').padStart(64, '0'), 'hex');
                const paddedS = Buffer.from(s.toString('hex').padStart(64, '0'), 'hex');
                return {
                    publicKey: publicKey,
                    signature: Buffer.concat([paddedR, paddedS]).toString('hex')
                };
            }
        }
        return null;
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

trans.decodeSignature = (signature) => {
    const sig = SIGNATURE_SCHEMA.decode(signature, 'der');
    if (sig && sig.r && sig.s) {
        return {
            r: sig.r.toBuffer(),
            s: sig.s.toBuffer(),
        };
    }
    throw Error(`Unable to decode the signature`);
};
trans.encodeSignature = (signature) => {
    try {
        const input = {
            r: new BN(signature.r),
            s: new BN(signature.s)
        };
        return SIGNATURE_SCHEMA.encode(input, 'der');
    } catch ({
        error, stack, code
    }) {
        throw Error(`Unable to encode the signature`);
    }
};
trans.decodePublicKey = (publicKey) => {
    const publicKeyInfo = SUBJECT_PUBLICKEY_INFO_SCHEMA.decode(publicKey, 'der');
    if (publicKeyInfo && publicKeyInfo.subjectPublicKey && publicKeyInfo.subjectPublicKey.data) {
        return {
            algorithmIdentifier: {
                algorithm: publicKeyInfo.algorithm.join('.'),
                parameters: publicKeyInfo.parameters.join('.'),
            },
            subjectPublicKey: publicKeyInfo.subjectPublicKey.data,
        };
    }
    throw Error(`Unable to decode the publicKey`);
};
trans.getPublicKey = async (awsKeyId) => {
    const key = await kms.getPublicKey({ KeyId: awsKeyId, }).promise();
    const keyVerify = await this.validatePublicKey(awsKeyId);
    if (!keyVerify.flag) {
        const publicKeyDer = key.PublicKey;
        const publicKeyHex = publicKeyDer.toString('hex');
        let decodeKey = Buffer.from(publicKeyHex, 'hex');
        const decodedPublicKey = this.decodePublicKey(decodeKey);
        if (
            decodedPublicKey?.algorithmIdentifier.algorithm === '1.2.840.10045.2.1' &&
            decodedPublicKey?.algorithmIdentifier.parameters === '1.2.840.10045.3.1.7'
        ) {
            return decodedPublicKey.subjectPublicKey.toString('hex');
        }
    }
    return null;
};
trans.validatePublicKey = async (awsKeyId) => {
    let key = await kms.describeKey({ KeyId: awsKeyId, }).promise();
    let errorMessage;
    if (key.KeyMetadata && key.KeyMetadata.Enabled !== true) {
        return {
            flag: true,
            message: `Key ${awsKeyId} is not enabled`
        };
    } else {
        if (key.KeyMetadata && key.KeyMetadata.CustomerMasterKeySpec !== 'ECC_NIST_P256') {
            errorMessage = `Key ${awsKeyId} is not correct type, must be ECC_NIST_P256 but is ${key.KeyMetadata?.CustomerMasterKeySpec}`;
            return {
                flag: true,
                message: errorMessage
            };
        } else {
            if (key.KeyMetadata.KeyUsage !== 'SIGN_VERIFY') {
                errorMessage = `Key ${awsKeyId} does not have the correct usage type.`;
                return {
                    flag: true,
                    message: errorMessage
                };
            }
        }
    }
    return { flag: false };
};

trans.storeTransaction = async (loginUser, fromAddress, toAddress, fromValue, toValue, fromTokenSymbol, toTokenSymbol, transactionHash, gasFees, status, type, requestId, timestamps = moment().format('X')) => {
    let exists = await findOneTransaction({
        user: loginUser.id,
        transactionHash,
        status: { $ne: 'PENDING' }
    });
    if (!exists) {
        const liveValue = await this.LiveTokenValue();
        let transactionData = {
            fromAddress,
            toAddress,
            fromValue,
            toValue,
            fromTokenSymbol,
            toTokenSymbol,
            transactionHash,
            gasFees,
            status,
            type,
            requestId,
            timestamps,
            currentTokenValue: liveValue.setting,
            user: loginUser.id
        };
        return {
            flag: false,
            data: await dbService.createDocument(Transaction, transactionData)
        };
    }
    return {
        flag: true,
        data: 'record already exists'
    };

};

trans.createTransactionWithOutCheck = async (loginUser, fromAddress, toAddress, fromValue, toValue, fromTokenSymbol, toTokenSymbol, transactionHash, gasFees, status, type, requestId, timestamps = moment().format('X')) => {
    const liveValue = await this.LiveTokenValue();
    let transactionData = {
        fromAddress,
        toAddress,
        fromValue,
        toValue,
        fromTokenSymbol,
        toTokenSymbol,
        transactionHash,
        gasFees,
        status,
        type,
        requestId,
        timestamps,
        currentTokenValue: liveValue.setting,
        user: loginUser.id
    };
    return dbService.createDocument(Transaction, transactionData);
};
