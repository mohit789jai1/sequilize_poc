const Activity = require('../model/activity');
const dbService = require('../utils/dbService');
const emailService = require('./email/emailService');
const RequestIp = require('@supercharge/request-ip');

let activity = module.exports;

activity.createActivity = async (req, userId, name, type) => {
	try {
		let body = req.body || req.query || req.params;
		if (body.password) {
			body.password = '';
		}
		if (body.newPassword) {
			body.newPassword = '';
		}
		if (body.recoveryAddress) {
			body.recoveryAddress = '';
		}

		let createData = {
			user: userId,
			name: name,
			ip: RequestIp.getClientIp(req),
			type: type,
			data: body
		};
		return await dbService.createDocument(Activity, createData);
	} catch (error) {
		throw new Error(error.message);
	}
};

activity.sendActivityEmail = async (subject, to, template, data) => {
	try {
		await emailService.sendMail({
			subject,
			to,
			template,
			data
		});
		return;
	} catch (error) {
		throw new Error(error.message);
	}
};
