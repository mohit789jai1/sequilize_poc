const ProjectSetting = require('../model/projectSetting');
const Portfolio = require('../model/portfolio');
const Property = require('../model/property');
const Perk = require('../model/perk');
const File = require('../model/file');
const User = require('../model/user');
const Transaction = require('../model/transaction');
const { PROJECT_SETTING } = require('../constants/userConstant');
const axios = require('axios');
const moment = require('moment');
const dbService = require('../utils/dbService');
const _ = require('lodash');
const fileService = require('../services/file');
const transactionService = require('../services/transaction/transaction');
const activityService = require('../services/activity');
const { SEND_ADMIN_EMAIL } = require('../constants/authConstant');

module.exports = {
    updateLiveTokenValue: async () => {
        let updateData = await axios.get('https://api.coingecko.com/api/v3/simple/price?ids=mybricks%2Cbinancecoin&vs_currencies=usd%2Ceur%2Cgbp&include_24hr_change=true');
        return ProjectSetting.findOneAndUpdate({ type: PROJECT_SETTING.CURRENCY_TOKEN_VALUE }, {
            setting: updateData.data,
            type: PROJECT_SETTING.CURRENCY_TOKEN_VALUE
        }, {
            upsert: true,
            new: true,
            setDefaultsOnInsert: true
        });
    },

    updateLiveChartValue: async () => {
        let updateData = {};
        let currency = ['usd', 'gbp', 'eur'];
        for (let cur of currency) {
            let data = await axios.get(`https://api.coingecko.com/api/v3/coins/mybricks/market_chart?vs_currency=${cur}&days=1`);
            updateData[`1D${cur}`] = data.data;
            data = await axios.get(`https://api.coingecko.com/api/v3/coins/mybricks/market_chart?vs_currency=${cur}&days=7`);
            updateData[`7D${cur}`] = data.data;
            data = await axios.get(`https://api.coingecko.com/api/v3/coins/mybricks/market_chart?vs_currency=${cur}&days=30`);
            updateData[`1M${cur}`] = data.data;
            data = await axios.get(`https://api.coingecko.com/api/v3/coins/mybricks/market_chart?vs_currency=${cur}&days=90`);
            updateData[`3M${cur}`] = data.data;
            data = await axios.get(`https://api.coingecko.com/api/v3/coins/mybricks/market_chart?vs_currency=${cur}&days=max`);
            updateData[`ALL${cur}`] = data.data;
        }

        return ProjectSetting.findOneAndUpdate({ type: PROJECT_SETTING.TOKEN_CHART }, {
            setting: updateData,
            type: PROJECT_SETTING.TOKEN_CHART
        }, {
            upsert: true,
            new: true,
            setDefaultsOnInsert: true
        });
    },

    updatePendingTransaction: async () => {
        return Transaction.updateMany({
            status: 'PENDING',
            timestamps: { $lt: moment().add(-1, 'hours').format('X') }
        }, { status: 'FAILED' });
    },

    unlockAccount: async () => {
        return dbService.bulkUpdate(User, {
            isAccountLock: true,
            accountLockTime: { $lte: moment().toDate() }
        }, {
            isAccountLock: false,
            reasonOfLock: ''
        });
    },

    removeFiles: async () => {
        try {
            let allFiles = [];
            let files = [];
            let portfolioFiles = await Portfolio.find({
                'isActive': true,
                'isDeleted': false
            }, {
                'portfolioPDF': 1,
                'portfolioVideo': 1,
                'portfolioVideoThumbnail': 1,
                'portfolioImage': 1,
                '_id': 0
            });
            let propertyFiles = await Property.find({
                'isActive': true,
                'isDeleted': false
            }, {
                'propertyMainImage': 1,
                'propertyImageCollection': 1,
                '_id': 0
            });
            let perkFiles = await Perk.find({
                'isActive': true,
                'isDeleted': false
            }, {
                'portfolioPerkIcon': 1,
                '_id': 0
            });
            let homepageFiles = await File.find({ type: { $in: ['homepage', 'learning', 'logo'] } }, {
                '_id': 1,
                'posterId': 1
            });
            allFiles.push(portfolioFiles, propertyFiles, perkFiles, homepageFiles);
            allFiles = _.flatten(allFiles);
            await Promise.all(_.map(allFiles, (obj) => {
                obj = obj.toJSON();
                files.push(_.values(obj));
            }));
            files = _.compact(_.flatten(_.flatten(files)));
            let toBeDeleteFiles = await File.find({ _id: { $nin: files } }, { '_id': 1 });
            if (!toBeDeleteFiles.length) {
                return;
            }
            await Promise.all(_.map(toBeDeleteFiles, async (obj) => {
                await fileService.deleteFileById(obj._id);
            }));
            return true;
        } catch (error) {
            console.log(error);
            return;
        }
    },
    sendMailToAdminAboutInsufficientBalance: async () => {
        try {
            let balance = await transactionService.getBalance(process.env.CENTRAL_WALLET_ADDRESS);
            if (!balance.flag && (parseFloat(balance.data.BNBbalance) < 2 || parseInt(balance.data.BRICKSBalance) < 10000)) {
                await activityService.sendActivityEmail('Insufficient balance in central wallet', SEND_ADMIN_EMAIL, '/views/adminEmail', {
                    walletAddress: process.env.CENTRAL_WALLET_ADDRESS,
                    bnb: balance.data.BNBbalance,
                    bricks: balance.data.BRICKSBalance,
                    server: process.env.NODE_ENV
                });
            }
            return true;
        } catch (error) {
            console.log(error);
            return;
        }
    },
};