const Portfolio = require('../model/portfolio');

const findOne = async (filter, populate = []) => {
	return Portfolio.findOne(filter).populate(populate);
};

const findById = async (id, populate = []) => {
	return Portfolio.findById(id).populate(populate);
};

const find = async (filter, populate = []) => {
	return Portfolio.find(filter).populate(populate);
};

const deleteOne = async (filter) => {
	return Portfolio.deleteOne(filter);
};

module.exports = {
	findOne,
	findById,
	find,
	deleteOne
};