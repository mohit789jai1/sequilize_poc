const Perk = require('../model/perk');

const findOne = async (filter, populate = []) => {
	return Perk.findOne(filter).populate(populate);
};

const findById = async (id, populate = []) => {
	return Perk.findById(id).populate(populate);
};

const find = async (filter, populate = []) => {
	return Perk.find(filter).populate(populate);
};

const deleteOne = async (filter) => {
	return Perk.deleteOne(filter);
};

module.exports = {
	findOne,
	findById,
	find,
	deleteOne
};