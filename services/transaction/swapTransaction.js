const Web3 = require('web3');
const dbService = require('../../utils/dbService');
const transactionService = require('./transaction');
const historyTransactionService = require('./historyTransaction');
const Contract = require('../../contract/ABI.json');
const ContractToken = require('../../contract/token.json');
const provider = new Web3.providers.HttpProvider(process.env.WEB3_PROVIDER_URL);
const web3 = new Web3(provider);
const token = new web3.eth.Contract(ContractToken, process.env.WBRICKS);
const RouterContract = new web3.eth.Contract(Contract, process.env.ROUTER);
const path = [process.env.WBNB, process.env.WBRICKS];
const Transaction = require('../../model/transaction');
const moment = require('moment');

let swapTrans = module.exports;

swapTrans.BNBtoBricksCalculation = async (BNBInput, walletAddress, type = 'WITHDRAW') => {
    try {
        let output = await RouterContract.methods.getAmountsOut(web3.utils.toWei(BNBInput), path).call();
        const taxFromContract = await token.methods._enableTax().call();
        let newOutput = parseInt(output[1]);
        if (taxFromContract) {
            newOutput = Math.floor(parseInt(output[1]) - (parseInt(output[1]) * 0.06));
        }
        let gasFeeWalletAddress;
        if (type == 'SWAP') {
            gasFeeWalletAddress = walletAddress;
        } else {
            gasFeeWalletAddress = process.env.CENTRAL_WALLET_ADDRESS;
        }
        output = newOutput / 1000000000;
        const date = Math.floor(Date.now() / 1000);
        const gasValue = await RouterContract.methods.swapExactETHForTokens(newOutput, path, walletAddress, date + 300).estimateGas({
            from: gasFeeWalletAddress,
            value: web3.utils.toWei(BNBInput)
        });
        const gasPrice = await web3.eth.getGasPrice();
        if (output) {
            return {
                flag: false,
                data: {
                    bricks: output,
                    fees: Web3.utils.fromWei((gasValue * gasPrice).toString())
                }
            };
        }
        return {
            flag: true,
            data: 'Enter valid Input'
        };

    }
    catch (error) {
        console.error(error);
        throw new Error(error.message);
    }

};

swapTrans.BNBtoBricksTransaction = async (loginUser, BNBInput, walletAddress) => {
    try {
        let BrickTokenAmount = await this.BNBtoBricksCalculation(BNBInput, walletAddress, 'SWAP');
        if (!BrickTokenAmount.flag) {
            const date = Math.floor(Date.now() / 1000);
            const data = await RouterContract.methods.swapExactETHForTokens(Math.floor(BrickTokenAmount.data.bricks * 1000000000), path, walletAddress, date + 300).encodeABI();
            const gasValue = await RouterContract.methods.swapExactETHForTokens(Math.floor(BrickTokenAmount.data.bricks * 1000000000), path, walletAddress, date + 300).estimateGas({
                from: walletAddress,
                value: web3.utils.toWei(BNBInput)
            });
            const gasPrice = await web3.eth.getGasPrice();
            if (data) {
                let storedData = await historyTransactionService.createTransactionWithOutCheck(loginUser, walletAddress, walletAddress, BNBInput.toString(), BrickTokenAmount.data.bricks.toString(), 'BNB', 'BRICKS', null, Web3.utils.fromWei(Math.floor(gasValue * gasPrice).toString()), 'PENDING', 'SWAP', null, moment().format('X'));
                let transData = await transactionService.createEthTransaction(loginUser, walletAddress, process.env.ROUTER, web3.utils.toWei(BNBInput), process.env.ASSET_SYMBOL, data, true, gasValue);
                if (!transData.flag) {
                    await dbService.updateDocument(Transaction, { _id: storedData.id }, {
                        transactionHash: transData.data.transactionHash,
                        status: transData.data.status,
                        requestId: transData.data.requestId
                    });
                    return {
                        flag: false,
                        data: transData.data
                    };
                }
                return {
                    flag: true,
                    data: transData.data
                };
            }
            return {
                flag: true,
                data: 'Error in router contract'
            };
        }
        return {
            flag: true,
            data: 'Enter valid Input'
        };

    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }

};