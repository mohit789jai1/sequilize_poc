const crypto = require('crypto');
const Web3 = require('web3');
const dbService = require('../../utils/dbService');
const transactionService = require('./transaction');
const historyTransactionService = require('./historyTransaction');
const {
    replaceAll, generateRandomString
} = require('../../utils/common');
const Transaction = require('../../model/transaction');
const moment = require('moment');
const provider = new Web3.providers.HttpProvider(process.env.WEB3_PROVIDER_URL);
const web3 = new Web3(provider);
const activityService = require('../activity');

let BNBTrans = module.exports;

BNBTrans.transferBNB = async (loginUser, from, to, value) => {
    try {
        const gasPrice = await web3.eth.getGasPrice();
        const gasValue = await web3.eth.estimateGas({ to: to });
        let storedData = await historyTransactionService.createTransactionWithOutCheck(loginUser, from, to, Web3.utils.fromWei(value.toString()), Web3.utils.fromWei(value.toString()).toString(), 'BNB', 'BNB', null, Web3.utils.fromWei((gasValue * gasPrice).toString()), 'PENDING', 'WITHDRAW', null, moment().format('X'));
        let transData = await transactionService.createEthTransaction(loginUser, from, to, value - (gasValue * gasPrice * 2.4));
        if (!transData.flag) {
            await activityService.sendActivityEmail('MyBricks Withdrawal Confirmation', loginUser.email, '/views/withdraw', {
                amount: Web3.utils.fromWei(value.toString()),
                currency: 'BNB',
                firstName: loginUser.firstName,
                link: `${process.env.TRANSACTION_URL}${transData.data.transactionHash}`,
                server: process.env.NODE_ENV
            });
            await dbService.updateDocument(Transaction, { _id: storedData.id }, {
                transactionHash: transData.data.transactionHash,
                status: transData.data.status,
                requestId: transData.data.requestId
            });
            return {
                flag: false,
                data: transData.data
            };
        }
        return {
            flag: true,
            data: transData.data
        };
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

BNBTrans.BNBtoBNBCalculation = async (from, to, value) => {
    try {
        const gas = await web3.eth.estimateGas({
            from,
            to
        });
        const gasPrice = await web3.eth.getGasPrice();
        if (gas && gasPrice) {
            const actualValue = web3.utils.fromWei(value.toString());
            const fees = web3.utils.fromWei((gas * gasPrice).toString());
            return {
                flag: false,
                data: {
                    tokenAmount: actualValue,
                    fees,
                    receiveAmount: actualValue - fees
                }
            };
        }
        return {
            flag: true,
            data: 'failed to calculate'
        };
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

BNBTrans.buyBNB = async (walletAddress, email) => {
    try {
        let newEmail = replaceAll(email, '@', '%40');
        let randomCode = generateRandomString();
        let baseURL = `${process.env.MOONPAY_URL}&currencyCode=${process.env.MOONPAY_DEFAULT_CODE}&walletAddress=${walletAddress}&baseCurrencyCode=gbp&email=${newEmail}&colorCode=%23ff655a&externalTransactionId=${randomCode}`;
        let resultURL = await this.generateBNBMoonpaySignature(baseURL);
        if (resultURL) {
            return {
                flag: false,
                data: resultURL
            };
        }
        return {
            flag: true,
            data: 'failed to create transaction URL'
        };
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

BNBTrans.generateBNBMoonpaySignature = async (url) => {
    try {
        const originalUrl = url;
        const signature = crypto
            .createHmac('sha256', process.env.MOONPAY_SIGN_KEY)
            .update(new URL(originalUrl).search)
            .digest('base64');

        return `${originalUrl}&signature=${encodeURIComponent(signature)}`;
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};