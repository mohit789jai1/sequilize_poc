const axios = require('axios');
const Web3 = require('web3');
const User = require('../../model/user');
const dbService = require('../../utils/dbService');
const transactionService = require('./transaction');
const Contract = require('../../contract/ABI.json');
const ContractToken = require('../../contract/token.json');
const path = [process.env.WBNB, process.env.WBRICKS];
const ProjectSetting = require('../../model/projectSetting');
const Transaction = require('../../model/transaction');
const moment = require('moment');
const provider = new Web3.providers.HttpProvider(process.env.WEB3_PROVIDER_URL);
const web3 = new Web3(provider);
const token = new web3.eth.Contract(ContractToken, process.env.WBRICKS);
const RouterContract = new web3.eth.Contract(Contract, process.env.ROUTER);
const _ = require('lodash');
const mongoose = require('mongoose');

let historyTrans = module.exports;

historyTrans.getHistory = async (user) => {
    try {
        let BNBDeposit = await this.getBNBDepositHistory(user, user.subWalletId);
        if (BNBDeposit && BNBDeposit.flag) {
            return {
                flag: true,
                data: BNBDeposit.data
            };
        }
        await dbService.updateDocument(User, { _id: user.id }, { lastSyncDate: moment().toISOString() });
        let res = await transactionService.findTransactionWithSort({ user: user.id }, { timestamps: -1 });
        return {
            flag: false,
            data: res
        };
    } catch (error) {
        throw new Error(error.message);
    }
};

historyTrans.getBNBDepositHistory = async (user, subWalletId) => {
    try {
        let res;
        let data = JSON.stringify({
            query: `query getTransactions($subWalletId: String!) {
                user {
                    subWallet(subWalletId: $subWalletId) {
                        transactions(nextToken: null) {
                            items {
                                from {
                                    ... on Address {
                                        id
                                    }
                                }
                                to {
                                    ... on Address {
                                        id
                                    }
                                }
                                amount {
                                    value
                                    timestamp
                                    currency
                                }
                                createdAt
                                updatedAt
                                ... on BlockchainTransaction {
                                    transactionDirection
                                    fee {
                                        value
                                        timestamp
                                        currency
                                    }
                                    transactionHash
                                }
                            }
                            nextToken
                        }
                    }
                }
            }`,
            variables: { 'subWalletId': subWalletId, }
        });

        let config = {
            method: 'post',
            url: process.env.TRUSTOLOGY_URL,
            headers: {
                'x-api-key': process.env.TRUST_VAULT_API_KEY,
                'Content-Type': 'application/json'
            },
            data: data
        };

        await axios(config)
            .then(function (response) {
                if (response.data.data) {
                    res = {
                        flag: false,
                        data: response.data.data.user.subWallet.transactions.items
                    };
                } else {
                    throw response.data.errors[0].message;
                }
            })
            .catch(function (error) {
                console.error(error);
                res = {
                    flag: true,
                    data: error
                };
            });

        if (!res.flag) {
            let records = [];
            if (res.data && res.data.length) {
                let existsTrans = await dbService.getDocumentByQuery(Transaction, { user: user.id });
                existsTrans = _.map(existsTrans, 'transactionHash');
                await Promise.all(_.map(res.data, async (transaction) => {
                    if (moment(user.lastSyncDate) < moment(transaction.updatedAt) && transaction.transactionDirection == 'RECEIVED' && (transaction.to.id).toLowerCase() == (user.custodialWalletAddress).toLowerCase() && (process.env.CENTRAL_WALLET_ADDRESS).toLowerCase() != (transaction.from.id).toLowerCase() && !existsTrans.includes(transaction.transactionHash) && (process.env.DEPOSIT_ALLOW_CURRENCY.split(',')).includes(transaction.amount.currency)) {
                        let createdRecord = await this.storeTransaction(user, transaction.from.id, transaction.to.id, transaction.amount.value, transaction.amount.value, transaction.amount.currency, transaction.amount.currency, transaction.transactionHash, transaction.fee.value, transaction.transactionDirection, 'DEPOSIT', null, moment(transaction.createdAt).format('X'));
                        if (!createdRecord.data) {
                            records.push(createdRecord.data);
                        }
                    }
                }));
            }
            res.data = records;
        }
        return res;
    } catch (error) {
        throw new Error(error.message);
    }
};

historyTrans.getTransactionDetail = async (userId, id) => {
    try {
        let filter = { user: userId };
        var isValid = mongoose.Types.ObjectId.isValid(id);
        if (isValid) {
            filter._id = id;
        }
        else {
            filter.requestId = id;
        }
        let res = await transactionService.findOneTransaction(filter);
        if (res) {
            return {
                flag: false,
                data: res
            };
        }
        return {
            flag: true,
            data: 'failed to fetch history'
        };
    } catch (error) {
        throw new Error(error.message);
    }
};

historyTrans.storeMoonPayTransaction = async (user, transactionId) => {
    try {
        let res;
        let config = {
            method: 'get',
            url: `${process.env.MOONPAY_HISTORY_URL}/${transactionId}?apiKey=${process.env.MOONPAY_PRIVATE_KEY}`,
            headers: { 'Content-Type': 'application/json' }
        };

        await axios(config)
            .then(function (response) {
                if (response.data) {
                    if (response.data.walletAddress == user.custodialWalletAddress) {
                        res = {
                            flag: false,
                            data: response.data
                        };
                    } else {
                        res = {
                            flag: true,
                            data: 'Invalid transaction\'s walletId not match with this user.'
                        };
                    }
                } else {
                    throw response.data.result;
                }
            })
            .catch(function (error) {
                res = {
                    flag: true,
                    data: error
                };
            });
        if (!res.flag) {
            let fromValue = res.data.baseCurrencyAmount + res.data.feeAmount + res.data.networkFeeAmount + res.data.extraFeeAmount;
            let gasFees = res.data.feeAmount + res.data.networkFeeAmount + res.data.extraFeeAmount;
            let fromTokenSymbol;
            if (res.data.eurRate == 1) {
                fromTokenSymbol = 'EUR';
            }
            else if (res.data.usdRate == 1) {
                fromTokenSymbol = 'USD';
            }
            else if (res.data.gbpRate == 1) {
                fromTokenSymbol = 'GBP';
            }
            let data = await this.storeTransaction(user, res.data.walletAddress, null, fromValue, res.data.quoteCurrencyAmount, fromTokenSymbol, 'BNB', res.data.cryptoTransactionId, gasFees, res.data.status, 'PURCHASE', transactionId, moment().format('X'));
            if (!data.flag) {
                res = {
                    flag: false,
                    data: data.data
                };
            }
        }
        return res;
    } catch (error) {
        throw new Error(error.message);
    }
};

historyTrans.storeTransaction = async (loginUser, fromAddress, toAddress, fromValue, toValue, fromTokenSymbol, toTokenSymbol, transactionHash, gasFees, status, type, requestId, timestamps = moment().format('X')) => {
    let exists = await transactionService.findOneTransaction({
        user: loginUser.id,
        transactionHash,
        status: { $ne: 'PENDING' }
    });
    if (!exists) {
        const liveValue = await transactionService.LiveTokenValue();
        let transactionData = {
            fromAddress,
            toAddress,
            fromValue,
            toValue,
            fromTokenSymbol,
            toTokenSymbol,
            transactionHash,
            gasFees,
            status,
            type,
            requestId,
            timestamps,
            currentTokenValue: liveValue.setting,
            user: loginUser.id
        };
        return {
            flag: false,
            data: await dbService.createDocument(Transaction, transactionData)
        };
    }
    return {
        flag: true,
        data: 'record already exists'
    };

};

historyTrans.createTransactionWithOutCheck = async (loginUser, fromAddress, toAddress, fromValue, toValue, fromTokenSymbol, toTokenSymbol, transactionHash, gasFees, status, type, requestId, timestamps = moment().format('X')) => {
    const liveValue = await transactionService.LiveTokenValue();
    let transactionData = {
        fromAddress,
        toAddress,
        fromValue,
        toValue,
        fromTokenSymbol,
        toTokenSymbol,
        transactionHash,
        gasFees,
        status,
        type,
        requestId,
        timestamps,
        currentTokenValue: liveValue.setting,
        user: loginUser.id
    };
    return dbService.createDocument(Transaction, transactionData);
};