const axios = require('axios');
const Web3 = require('web3');
const User = require('../../model/user');
const dbService = require('../../utils/dbService');
const asn1 = require('asn1.js');
const BN = require('bn.js');
const AWS = require('aws-sdk');
AWS.config.loadFromPath('./config.json');
const kms = new AWS.KMS({
    apiVersion: '2014-11-01',
    region: 'eu-west-2'
});
const { PROJECT_SETTING } = require('../../constants/userConstant');
const Contract = require('../../contract/ABI.json');
const ContractToken = require('../../contract/token.json');
const provider = new Web3.providers.HttpProvider(process.env.WEB3_PROVIDER_URL);
const web3 = new Web3(provider);
const token = new web3.eth.Contract(ContractToken, process.env.WBRICKS);
const RouterContract = new web3.eth.Contract(Contract, process.env.ROUTER);
const path = [process.env.WBNB, process.env.WBRICKS];
const ProjectSetting = require('../../model/projectSetting');
const Transaction = require('../../model/transaction');
const moment = require('moment');

const SUBJECT_PUBLICKEY_INFO_SCHEMA = asn1.define('SubjectPublicKeyInfo', function () {
    this.seq().obj(
        this.key('algorithmIdentifier').seq(
            this.key('algorithm').optional().objid(),
            this.key('parameters').optional().objid(),
        ),
        this.key('subjectPublicKey').bitstr(),
    );
});

const SIGNATURE_SCHEMA = asn1.define('Signature', function () {
    this.seq().obj(this.key('r').int(), this.key('s').int());
});

let trans = module.exports;

trans.findTransactionWithSort = async (filter, sort = {}) => {
    return Transaction.find(filter).sort(sort);
};

trans.findOneTransaction = async (filter) => {
    return Transaction.findOne(filter);
};

trans.LiveTokenValue = async () => {
    return dbService.getDocumentByQuery(ProjectSetting, { type: PROJECT_SETTING.CURRENCY_TOKEN_VALUE });
};

trans.createCustodialWallet = async (user, name = null) => {
    try {
        if (user && !user.isCustodialWalletExists) {
            let WalletName = name ? name : user.email;
            let out = await trans.createSubWallet(WalletName);
            if (out && out.data && out.data.createSubWallet) {
                let subWalletId = out.data.createSubWallet.subWalletId;
                let walletAddress = out.data.createSubWallet.receiveAddressDetails.unverifiedAddress;
                if (!out.flag) {
                    await dbService.updateDocument(User, { _id: user.id }, {
                        custodialWalletAddress: walletAddress,
                        subWalletId: subWalletId,
                        isCustodialWalletExists: true
                    });
                    /*
                     * const gasValue = await token.methods.transfer(walletAddress, 1000 * 1000000000).estimateGas({ from: process.env.CENTRAL_WALLET_ADDRESS })
                     * const gasPrice = await web3.eth.getGasPrice();
                     * let transData = await this.BricksToBricksTransaction(user, 1000 * 1000000000, process.env.CENTRAL_WALLET_ADDRESS, walletAddress);
                     * await this.storeTransaction(user, process.env.CENTRAL_WALLET_ADDRESS, walletAddress, "1000", "1000", "BRICKS", "BRICKS", transData.data.transactionHash, Web3.utils.fromWei((gasValue * gasPrice).toString()), transData.data.status, "BONUS", transData.data.requestId, moment().format("X"))
                     */
                    return {
                        flag: false,
                        data: {
                            message: `Custodial wallet created successfully!`,
                            data: {
                                subWalletId,
                                walletAddress
                            }
                        }
                    };
                }
            }
            return {
                flag: true,
                data: 'Custodial wallet creation failed'
            };
        }
        return {
            flag: true,
            data: 'Custodial wallet creation failed or already exist'
        };
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

trans.createSubWallet = async (name) => {
    try {
        let res;
        let data = JSON.stringify({
            query: `mutation($type: SubWalletType!, $name: String!, $walletId: String!, ) {
            createSubWallet(
                createSubWalletInput: {
                    type: $type,
                    name: $name,
                    walletId: $walletId,
                }
            ) {
                subWalletId
                receiveAddressDetails {
                    addressType
                    path
                    publicKey
                    trustVaultProvenanceSignature
                    unverifiedAddress
                }
            }
        }`,
            variables: {
                'type': process.env.ASSET_SYMBOL,
                'name': name,
                'walletId': process.env.WALLET_ID
            }
        });

        let config = {
            method: 'post',
            url: process.env.TRUSTOLOGY_URL,
            headers: {
                'x-api-key': process.env.TRUST_VAULT_API_KEY,
                'Content-Type': 'application/json'
            },
            data: data
        };

        await axios(config)
            .then(function (response) {
                if (response.data.data) {
                    res = {
                        flag: false,
                        data: response.data.data
                    };
                } else {
                    throw response.data.errors;
                }
            })
            .catch(function (error) {
                console.error(error);
                res = {
                    flag: true,
                    data: error
                };
            });
        return res;
    } catch (error) {
        throw new Error(error.message);
    }
};

trans.findStatusOfRequest = async (requestId) => {
    try {
        let res;
        let data = JSON.stringify({
            query: `query($requestId: String!) {
            getRequest(requestId: $requestId) {
              requestId
              status
              type
              transactionHash
            }
          }`,
            variables: { 'requestId': requestId }
        });

        let config = {
            method: 'post',
            url: process.env.TRUSTOLOGY_URL,
            headers: {
                'x-api-key': process.env.TRUST_VAULT_API_KEY,
                'Content-Type': 'application/json'
            },
            data: data
        };

        await axios(config)
            .then(function (response) {
                if (response.data.data && response.data.data.getRequest.status !== 'ERROR') {
                    res = {
                        flag: false,
                        data: response.data.data.getRequest
                    };
                } else {
                    throw response.data;
                }
            })
            .catch(function (error) {
                console.error(error);
                res = {
                    flag: true,
                    data: error
                };
            });
        return res;
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

trans.createEthTransaction = async (loginUser, from, to, value, type = process.env.ASSET_SYMBOL, contractData = '', isSmartContract = false, gasValue = 0) => {
    try {
        let res;
        // if ((from).toLowerCase() == (process.env.CENTRAL_WALLET_ADDRESS).toLowerCase()) {
        //     let projectSetting = await dbService.getDocumentByQuery(ProjectSetting, { type: PROJECT_SETTING.NONCE_MANAGEMENT });
        //     await dbService.findOneAndUpdateDocument(ProjectSetting, { type: PROJECT_SETTING.NONCE_MANAGEMENT }, { setting: { pending: projectSetting.setting.pending + 1 } });
        // }
        let variables = await this.calculateVariableForEthTransaction(from, to, value, type, contractData, isSmartContract, gasValue);
        if (variables) {
            let data = JSON.stringify({
                query: `mutation (
                    $from: String!,
                    $to: String!,
                    $value: String!,
                    $gasPrice: String!
                    $gas: String!
                    $data: String,
                    $chainId: Int!,
                    $nonce: Int!,
    
                ) {
                    createEthereumTransaction(
                        createTransactionInput: {
                            ethereumTransaction: {
                                fromAddress: $from
                                to: $to
                                value: $value
                                gasPrice: $gasPrice
                                gasLimit: $gas
                                chainId: $chainId
                                data: $data,
                                nonce : $nonce
                            }
                            source: "API",
                            sendToNetworkWhenSigned: true
                            sendToDevicesForSigning: true
                        }
                    ) {
                        ... on CreateEthereumTransactionResponse {
                            requestId
                        }
                        signData {
                            transaction {
                                to
                                fromAddress
                                value
                                gasPrice
                                gasLimit
                                nonce
                                chainId
                                data
                            }
                            hdWalletPath {
                                hdWalletPurpose
                                hdWalletCoinType
                                hdWalletAccount
                                hdWalletUsage
                                hdWalletAddressIndex
                            }
                            unverifiedDigestData {
                                transactionDigest
                                signData
                                shaSignData
                            }
                        }
                        assetRate
                        chainRate
                    }
                }`,
                variables: variables
            });

            let config = {
                method: 'post',
                url: process.env.TRUSTOLOGY_URL,
                headers: {
                    'x-api-key': process.env.TRUST_VAULT_API_KEY,
                    'Content-Type': 'application/json'
                },
                data: data
            };

            await axios(config)
                .then(function (response) {
                    if (response.data.data) {
                        let requestId = response.data.data.createEthereumTransaction.requestId;
                        let signData = response.data.data.createEthereumTransaction.signData.unverifiedDigestData.signData;
                        res = {
                            flag: false,
                            data: {
                                requestId,
                                signData
                            }
                        };
                    } else {
                        throw response.data.errors[0].message;
                    }
                })
                .catch(function (error) {
                    console.error(error);
                    res = {
                        flag: true,
                        data: error
                    };
                });
        } else {
            res = {
                flag: true,
                data: 'Can not calculate variable for this Transaction.'
            };
        }
        // if (res.flag) {
        //     if ((from).toLowerCase() == (process.env.CENTRAL_WALLET_ADDRESS).toLowerCase()) {
        //         let projectSetting = await dbService.getDocumentByQuery(ProjectSetting, { type: PROJECT_SETTING.NONCE_MANAGEMENT });
        //         await dbService.findOneAndUpdateDocument(ProjectSetting, { type: PROJECT_SETTING.NONCE_MANAGEMENT }, { setting: { pending: projectSetting.setting.pending - 1 } });
        //     }
        // }
        if (!res.flag) {
            res = await this.addSignature(res.data.requestId, res.data.signData, from);
        }
        return res;
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

trans.changePolicy = async (walletId, publicKey) => {
    try {
        let res;
        let data = JSON.stringify({
            query: `mutation($walletId: String!, $delegateSchedules: [[ScheduleInput!]!]!) {
                createChangePolicyRequest(
                    createChangePolicyRequestInput: {
                        walletId: $walletId
                        delegateSchedules: $delegateSchedules
                    }
                ) {
                    requests {
                        walletId
                        requestId
                        recovererTrustVaultSignature
                        unverifiedDigestData{
                            shaSignData
                            signData
                        }
                        policyTemplate {
                            expiryTimestamp
                            delegateSchedules {
                                keys
                                quorumCount
                            }
                            recovererSchedules {
                                keys
                                quorumCount
                            }
                        }
                    }
                }
            }`,
            variables: {
                'walletId': walletId,
                'delegateSchedules':
                    [[
                        {
                            'quorumCount': 1,
                            'keys': publicKey
                        }
                    ]]
            }
        });

        let config = {
            method: 'post',
            url: process.env.TRUSTOLOGY_URL,
            headers: {
                'x-api-key': process.env.TRUST_VAULT_API_KEY,
                'Content-Type': 'application/json'
            },
            data: data
        };

        await axios(config)
            .then(async function (response) {
                if (response.data.data) {
                    let requestId = response.data.data.createChangePolicyRequest.requests[0].requestId;
                    let signData = response.data.data.createChangePolicyRequest.requests[0].unverifiedDigestData.signData;
                    res = {
                        flag: false,
                        data: {
                            requestId,
                            signData
                        }
                    };
                } else {
                    throw response.data.errors;
                }
            })
            .catch(function (error) {
                console.error(error);
                res = {
                    flag: true,
                    data: error
                };
            });
        if (!res.flag) {
            res = await this.addSignature(res.data.requestId, res.data.signData);
        }
        return res;
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

trans.calculateVariableForEthTransaction = async (from, to, value, type, data, isSmartContract, gasValue) => {
    try {
        const chainId = await web3.eth.getChainId();
        //BSC Mainnet - 56, BSC Testnet - 97
        if (chainId === parseInt(process.env.CHAIN_ID)) {
            let nonce = await web3.eth.getTransactionCount(from);
            const gasPrice = await web3.eth.getGasPrice();
            let gas;
            if (isSmartContract) {
                gas = gasValue;
            } else {
                gas = await web3.eth.estimateGas({ to: to });
            }
            // if ((from).toLowerCase() == (process.env.CENTRAL_WALLET_ADDRESS).toLowerCase()) {
            //     let projectSetting = await dbService.getDocumentByQuery(ProjectSetting, { type: PROJECT_SETTING.NONCE_MANAGEMENT });
            //     if (projectSetting.setting.pending > -1) {
            //         nonce = nonce + projectSetting.setting.pending;
            //     }
            // }
            return {
                nonce: nonce,
                from: from,
                to: to,
                value: value,
                gasPrice: web3.utils.toHex(Math.floor(gasPrice * 2)),
                gas: web3.utils.toHex(gas),
                data: data,
                chainId: chainId,
                // assetSymbol: type
            };
        } else {
            console.log('Invalid ChainID');
        }
        return null;
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

trans.getBalance = async (walletAddress) => {
    try {
        const BNBbalance = await web3.eth.getBalance(walletAddress);
        const BRICKSBalance = await token.methods.balanceOf(walletAddress).call();
        const setting = await this.LiveTokenValue();
        return {
            flag: false,
            data: {
                BNBbalance: web3.utils.fromWei(BNBbalance),
                maxUsedBNBBalance: web3.utils.fromWei((BNBbalance - '5000000000000000').toString()),
                BRICKSBalance: BRICKSBalance / 1000000000,
                walletAddress: walletAddress,
                currency: setting
            }
        };
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

trans.getChartValue = async (currency, fromDate = null, toDate = null) => {
    try {
        let response;
        if (fromDate && toDate) {
            fromDate = moment(fromDate).unix();
            toDate = moment(toDate).unix();
            response = await axios.get(`https://api.coingecko.com/api/v3/coins/mybricks/market_chart/range?vs_currency=${currency}&from=${fromDate}&to=${toDate}`);
            return {
                flag: false,
                data: response.data
            };
        } else {
            response = await dbService.getDocumentByQuery(ProjectSetting, { type: PROJECT_SETTING.TOKEN_CHART });
            return {
                flag: false,
                data: response
            };
        }

    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

trans.addSignature = async (requestId, signData, from = null) => {
    try {
        let newSignature = await this.getVerifiedSignature(signData);
        let res;
        if (newSignature) {
            let data = JSON.stringify({
                query: `mutation(
        $requestId: String!
        $publicKeySignaturePairs: [PublicKeySignaturePair!]!
    ) {
        addSignature(
            addSignatureInput: {
                requestId: $requestId
                signRequests: [
                    {
                        publicKeySignaturePairs: $publicKeySignaturePairs
                    }
                ]
            }
        ) {
            requestId
        }
    }`,
                variables: {
                    'requestId': requestId,
                    'publicKeySignaturePairs':
                        [newSignature]
                }
            });

            var config = {
                method: 'post',
                url: process.env.TRUSTOLOGY_URL,
                headers: {
                    'x-api-key': process.env.TRUST_VAULT_API_KEY,
                    'Content-Type': 'application/json'
                },
                data: data
            };

            await axios(config)
                .then(function (response) {
                    if (response.data.data) {
                        res = {
                            flag: false,
                            data: response.data.data
                        };
                    } else {
                        throw response.data.errors;
                    }

                })
                .catch(function (error) {
                    console.error(error);
                    res = {
                        flag: true,
                        data: error
                    };
                });
        } else {
            res = {
                flag: true,
                data: 'Failed to generate signature'
            };
        }
        if (!res.flag) {
            let startNewTransaction = true;
            let findStatus;
            if (from) {
                // if ((from).toLowerCase() == (process.env.CENTRAL_WALLET_ADDRESS).toLowerCase()) {
                //     let projectSetting = await dbService.getDocumentByQuery(ProjectSetting, { type: PROJECT_SETTING.NONCE_MANAGEMENT });
                //     let newCount = projectSetting.setting.pending - 1;
                //     await dbService.findOneAndUpdateDocument(ProjectSetting, { type: PROJECT_SETTING.NONCE_MANAGEMENT }, { setting: { pending: newCount } });
                // }
                while (startNewTransaction) {
                    findStatus = await this.findStatusOfRequest(res.data.addSignature.requestId);
                    if (findStatus && !findStatus.flag && findStatus.data.status == 'CONFIRMED') {
                        startNewTransaction = false;
                        res = findStatus;
                    }
                    if (findStatus && findStatus.flag) {
                        startNewTransaction = false;
                        res = {
                            flag: true,
                            data: 'Error in transaction after confirm the transaction from signature'
                        };
                    }
                }
            }
        }
        return res;
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

trans.getVerifiedSignature = async (message) => {
    try {
        const awsKeyId = process.env.AWS_KEY_ID;
        let publicKey = await this.getPublicKey(awsKeyId);
        if (publicKey) {
            const params = {
                KeyId: awsKeyId,
                Message: Buffer.from(message, 'hex'),
                SigningAlgorithm: 'ECDSA_SHA_256',
                MessageType: 'RAW',
            };
            const sig = await kms.sign(params).promise();
            if (sig.SigningAlgorithm && sig.SigningAlgorithm === 'ECDSA_SHA_256') {
                const signature = sig.Signature;
                const hexSignature = signature.toString('hex');
                const {
                    r, s
                } = this.decodeSignature(Buffer.from(hexSignature, 'hex'));
                const paddedR = Buffer.from(r.toString('hex').padStart(64, '0'), 'hex');
                const paddedS = Buffer.from(s.toString('hex').padStart(64, '0'), 'hex');
                return {
                    publicKey: publicKey,
                    signature: Buffer.concat([paddedR, paddedS]).toString('hex')
                };
            }
        }
        return null;
    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }
};

trans.decodeSignature = (signature) => {
    const sig = SIGNATURE_SCHEMA.decode(signature, 'der');
    if (sig && sig.r && sig.s) {
        return {
            r: sig.r.toBuffer(),
            s: sig.s.toBuffer(),
        };
    }
    throw Error(`Unable to decode the signature`);
};
trans.encodeSignature = (signature) => {
    try {
        const input = {
            r: new BN(signature.r),
            s: new BN(signature.s)
        };
        return SIGNATURE_SCHEMA.encode(input, 'der');
    } catch ({
        error, stack, code
    }) {
        throw Error(`Unable to encode the signature`);
    }
};
trans.decodePublicKey = (publicKey) => {
    const publicKeyInfo = SUBJECT_PUBLICKEY_INFO_SCHEMA.decode(publicKey, 'der');
    if (publicKeyInfo && publicKeyInfo.subjectPublicKey && publicKeyInfo.subjectPublicKey.data) {
        return {
            algorithmIdentifier: {
                algorithm: publicKeyInfo.algorithm.join('.'),
                parameters: publicKeyInfo.parameters.join('.'),
            },
            subjectPublicKey: publicKeyInfo.subjectPublicKey.data,
        };
    }
    throw Error(`Unable to decode the publicKey`);
};
trans.getPublicKey = async (awsKeyId) => {
    const key = await kms.getPublicKey({ KeyId: awsKeyId, }).promise();
    const keyVerify = await this.validatePublicKey(awsKeyId);
    if (!keyVerify.flag) {
        const publicKeyDer = key.PublicKey;
        const publicKeyHex = publicKeyDer.toString('hex');
        let decodeKey = Buffer.from(publicKeyHex, 'hex');
        const decodedPublicKey = this.decodePublicKey(decodeKey);
        if (
            decodedPublicKey?.algorithmIdentifier.algorithm === '1.2.840.10045.2.1' &&
            decodedPublicKey?.algorithmIdentifier.parameters === '1.2.840.10045.3.1.7'
        ) {
            return decodedPublicKey.subjectPublicKey.toString('hex');
        }
    }
    return null;
};
trans.validatePublicKey = async (awsKeyId) => {
    let key = await kms.describeKey({ KeyId: awsKeyId, }).promise();
    let errorMessage;
    if (key.KeyMetadata && key.KeyMetadata.Enabled !== true) {
        return {
            flag: true,
            message: `Key ${awsKeyId} is not enabled`
        };
    } else {
        if (key.KeyMetadata && key.KeyMetadata.CustomerMasterKeySpec !== 'ECC_NIST_P256') {
            errorMessage = `Key ${awsKeyId} is not correct type, must be ECC_NIST_P256 but is ${key.KeyMetadata?.CustomerMasterKeySpec}`;
            return {
                flag: true,
                message: errorMessage
            };
        } else {
            if (key.KeyMetadata.KeyUsage !== 'SIGN_VERIFY') {
                errorMessage = `Key ${awsKeyId} does not have the correct usage type.`;
                return {
                    flag: true,
                    message: errorMessage
                };
            }
        }
    }
    return { flag: false };
};
