const Web3 = require('web3');
const dbService = require('../../utils/dbService');
const transactionService = require('./transaction');
const historyTransactionService = require('./historyTransaction');
const swapTransactionService = require('./swapTransaction');
const Contract = require('../../contract/ABI.json');
const ContractToken = require('../../contract/token.json');
const provider = new Web3.providers.HttpProvider(process.env.WEB3_PROVIDER_URL);
const web3 = new Web3(provider);
const token = new web3.eth.Contract(ContractToken, process.env.WBRICKS);
const RouterContract = new web3.eth.Contract(Contract, process.env.ROUTER);
const path = [process.env.WBNB, process.env.WBRICKS];
const Transaction = require('../../model/transaction');
const moment = require('moment');
const activityService = require('../activity');

let bricksTrans = module.exports;

bricksTrans.transactionPreRequiredCalculation = async (Input, from, to, flag) => {
    try {
        let gasData = await this.calculateGasPrice(Input, from, to, flag);
        if (gasData.flag) {
            return {
                flag: true,
                data: 'Error in calculate gas fee.'
            };
        }

        let BNBToBricksTransactionCalculation = await swapTransactionService.BNBtoBricksCalculation(gasData.data, from);
        if (BNBToBricksTransactionCalculation.flag) {
            return {
                flag: true,
                data: 'Error in calculate BNB to Bricks.'
            };
        }
        let valueForBricksTrans = BNBToBricksTransactionCalculation.data.bricks;
        const taxFromContract = await token.methods._enableTax().call();
        if (taxFromContract) {
            valueForBricksTrans = Math.floor((BNBToBricksTransactionCalculation.data.bricks + BNBToBricksTransactionCalculation.data.bricks * 0.07) * 1000000000).toString();
        }
        let outputData = {
            tokenAmount: (Input / 1000000000).toString(),
            fees: (valueForBricksTrans / 1000000000).toString(),
            tax: gasData.data,
            receiveAmount: ((Input - valueForBricksTrans) / 1000000000).toString()
        };
        if (parseFloat(outputData.fees * 1.05) > parseFloat(outputData.tokenAmount)) {
            return {
                flag: true,
                data: 'Please increase the withdrawal amount in order to cover the gas fees.'
            };
        } else {
            return {
                flag: false,
                data: outputData
            };
        }

    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }

};

bricksTrans.calculateGasPrice = async (Input, from, to, flag) => {
    try {
        let gasValue;
        if (flag == 'BNB') {
            let BrickTokenAmount = await swapTransactionService.BNBtoBricksCalculation(Input, from);
            const date = Math.floor(Date.now() / 1000);
            gasValue = await RouterContract.methods.swapExactETHForTokens(Math.floor(BrickTokenAmount.data.bricks * 1000000000).toString(), path, from, date + 300).estimateGas({
                from: from,
                value: web3.utils.toWei(Input)
            });
        } else if (flag == 'BRICKS_TRANSFER') {
            gasValue = await token.methods.transfer(to, Input).estimateGas({ from: from });
        } else {
            return {
                flag: true,
                data: 'invalid flag'
            };
        }
        const gasPrice = await web3.eth.getGasPrice();
        return {
            flag: false,
            data: web3.utils.fromWei((Math.floor(gasValue * gasPrice * 1.2)).toString())
        };

    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }

};

bricksTrans.BToBTransactionWithGas = async (loginUser, BRICKSInput, from, to) => {
    try {
        let requiredData = await this.transactionPreRequired(loginUser, BRICKSInput, from, to, 'BRICKS_TRANSFER');
        if (requiredData.flag) {
            return {
                flag: true,
                data: requiredData.data
            };
        }
        let originalBricksAmount = BRICKSInput / 1000000000;
        BRICKSInput = BRICKSInput - requiredData.data;
        const data = await token.methods.transfer(to, BRICKSInput).encodeABI();
        const gasValue = await token.methods.transfer(to, BRICKSInput).estimateGas({ from: from });
        if (data) {
            let storedData = await historyTransactionService.createTransactionWithOutCheck(loginUser, from, to, originalBricksAmount.toString(), (BRICKSInput / 1000000000).toString(), 'BRICKS', 'BRICKS', null, requiredData.data / 1000000000, 'PENDING', 'WITHDRAW', null, moment().format('X'));
            let transData = await transactionService.createEthTransaction(loginUser, from, process.env.WBRICKS, 0, process.env.ASSET_SYMBOL, data, true, gasValue);
            if (!transData.flag) {
                await activityService.sendActivityEmail('MyBricks Withdrawal Confirmation', loginUser.email, '/views/withdraw', {
                    amount: originalBricksAmount.toString(),
                    currency: 'BRICKS',
                    firstName: loginUser.firstName,
                    link: `${process.env.TRANSACTION_URL}${transData.data.transactionHash}`,
                    server: process.env.NODE_ENV
                });
                await dbService.updateDocument(Transaction, { _id: storedData.id }, {
                    transactionHash: transData.data.transactionHash,
                    status: transData.data.status,
                    requestId: transData.data.requestId
                });
                return {
                    flag: false,
                    data: transData.data
                };
            }
            return {
                flag: true,
                data: transData.data
            };
        }

        return {
            flag: true,
            data: 'Enter valid Input'
        };

    }
    catch (error) {
        console.error(error);
        throw new Error(error.message);
    }

};

bricksTrans.transactionPreRequired = async (loginUser, Input, from, to, flag) => {
    try {
        let valueForBricksTrans = 0;
        let gasData = await this.calculateGasPrice(Input, from, to, flag);
        if (gasData.flag) {
            return {
                flag: true,
                data: 'Error in calculate gas fee.'
            };
        }
        let balance = await transactionService.getBalance(from);
        if (balance.flag) {
            return {
                flag: true,
                data: 'Error in find wallet balance fee.'
            };
        }
        if (parseFloat(balance.data.BNBbalance) < parseFloat(gasData.data)) {
        let BNBtransaction = await transactionService.createEthTransaction(loginUser, process.env.CENTRAL_WALLET_ADDRESS, from, web3.utils.toWei(gasData.data));
        if (BNBtransaction.flag) {
            return {
                flag: true,
                data: BNBtransaction.data
            };
        }
        let BNBToBricksTransactionCalculation = await swapTransactionService.BNBtoBricksCalculation(gasData.data, from);
        if (BNBToBricksTransactionCalculation.flag) {
            return {
                flag: true,
                data: BNBToBricksTransactionCalculation.data
            };
        }
        valueForBricksTrans = BNBToBricksTransactionCalculation.data.bricks;
        const taxFromContract = await token.methods._enableTax().call();
        if (taxFromContract) {
            valueForBricksTrans = Math.floor((BNBToBricksTransactionCalculation.data.bricks + BNBToBricksTransactionCalculation.data.bricks * 0.07) * 1000000000).toString();
        }
        let BricksTransaction = await this.BricksToBricksTransaction(loginUser, valueForBricksTrans, from, process.env.CENTRAL_WALLET_ADDRESS);
        if (BricksTransaction.flag) {
            return {
                flag: true,
                data: BricksTransaction.data
            };
        }
    }
        return {
            flag: false,
            data: valueForBricksTrans
        };

    } catch (error) {
        console.error(error);
        throw new Error(error.message);
    }

};

bricksTrans.BricksToBricksTransaction = async (loginUser, BRICKSInput, from, to) => {
    try {
        const data = await token.methods.transfer(to, BRICKSInput).encodeABI();
        const gasValue = await token.methods.transfer(to, BRICKSInput).estimateGas({ from: from });
        if (data) {
            let transData = await transactionService.createEthTransaction(loginUser, from, process.env.WBRICKS, 0, process.env.ASSET_SYMBOL, data, true, gasValue);
            if (!transData.flag) {
                return {
                    flag: false,
                    data: transData.data
                };
            }
        }

        return {
            flag: true,
            data: 'Enter valid Input'
        };

    }
    catch (error) {
        console.error(error);
        throw new Error(error.message);
    }

};