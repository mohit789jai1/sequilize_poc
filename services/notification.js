const PromotionalUser = require('../model/promotionalUser');
const emailService = require('./email/emailService');
const dbService = require('../utils/dbService');
const _ = require('lodash');
/**
 * subscribe User
 * @param {Object} email
 * @returns {Promise<File>}
 */
const subscribeUser = async (email) => {
	let exist = await dbService.getDocumentByQuery(PromotionalUser, { email });
	if (!exist) {
		return PromotionalUser.create({ email });
	}
	return false;
};

const launchNotification = async (message) => {
	let emails = await dbService.getDocumentByQuery(PromotionalUser, {}, ['email']);
	let mailObj = {
		subject: 'Launch Portfolio',
		to: _.map(emails, 'email'),
		template: '/views/launchNotification',
		data: { message }
	};
	await emailService.sendMail(mailObj);
	return true;
};

module.exports = {
	subscribeUser,
	launchNotification
};
