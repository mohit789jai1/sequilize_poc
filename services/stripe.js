const stripe = require('stripe')(process.env.STRIPE_SECRET_KET, { apiVersion: '2020-08-27', });
const ProjectSetting = require('../model/projectSetting');
const { PROJECT_SETTING } = require('../constants/userConstant');
const { request } = require('express');

const createUpdateCustomer = async (user, card = null) => {
	try {
		let customer;
		let address = {
			line1: null,
			postal_code: null,
			city: null,
			state: null,
			country: null
		},
			data = {
				email: user.email,
				name: `${user.firstName} ${user.lastName}`
			};
		if (user.stripeCustomerId === null || user.stripeCustomerId === '') {
			customer = await stripe.customers.create(data);
		} else {
			if (user.stripeCustomerId) {
				const u = await stripe.customers.update(user.stripeCustomerId, data);
			}
			customer = await stripe.customers.retrieve(user.stripeCustomerId);
		}
		return customer;
	} catch (error) {
		throw new Error(error.message);
	}
};

const addCardForCustomer = async (stripeCustomerId, cardNo, expYear, expMonth, cvc) => {
	try {
		let cardToken = await createCardToken(cardNo, expYear, expMonth, cvc);
		const customerCard = await stripe.customers.createSource(
			stripeCustomerId,
			{ source: cardToken.id }
		);
		return customerCard;
	} catch (error) {
		throw new Error(error.message);
	}
};

const createCardToken = async (cardNo, expYear, expMonth, cvc) => {
	try {
		const token = await stripe.tokens.create({
			card: {
				number: cardNo,
				exp_month: expMonth,
				exp_year: expYear,
				cvc: cvc,
			},
		});
		return token;
	} catch (error) {
		throw new Error(error.message);
	}
};

const getUserCardDetails = async (stripeCustomerId, stripeCardId = null) => {
	try {
		const userCard = await stripe.paymentMethods.list({
			customer: stripeCustomerId,
			type: 'card'
		});
		return userCard.data;
	} catch (error) {
		throw new Error(error.message);
	}
};

const deleteCardForCustomer = async (stripeCustomerId, cardToken) => {
	try {
		const customerCardDeletion = await stripe.customers.deleteSource(
			stripeCustomerId,
			cardToken
		);
		return Object.create(customerCardDeletion);
	} catch (error) {
		throw new Error(error.message);
	}
};

const createPaymentIntent = async (stripeCustomerId, stripeCardId, amount, description) => {
	try {
		if (stripeCustomerId && stripeCardId) {
			const paymentIntent = await stripe.paymentIntents.create({
				amount: amount * 100,
				currency: 'gbp',
				customer: stripeCustomerId,
				payment_method: stripeCardId,
				off_session: true,
				confirm: true,
				description: description
			});
			return paymentIntent;
		} else {
			return false;
		}
	} catch (error) {
		throw new Error(error.message);
	}
};

const getBricksPrice = async (req, res) => {
	try {
		let data = await ProjectSetting.findOne({ type: PROJECT_SETTING.CURRENCY_TOKEN_VALUE });
		return {
			'usd': data.setting.mybricks.usd,
			'eur': data.setting.mybricks.eur,
			'gbp': data.setting.mybricks.gbp
		};
	}
	catch (error) {
		return res.failureResponse(error.message);
	}
};

const makePayment = async (request, response) => {
	try {
		/* Create the customer or update him */
		const { id } = await createUpdateCustomer(request.user);
		let intent;

		if (request.body.paymentMethodId) {
			/* Create the PaymentIntent */
			intent = await stripe.paymentIntents.create({
				payment_method: request.body.paymentMethodId,
				amount: request.body.amount * 100,
				currency: 'gbp',
				confirmation_method: 'manual',
				confirm: true,
				customer: id
			});
		} else if (request.body.paymentIntentId) {
			intent = await stripe.paymentIntents.confirm(
				request.body.paymentIntentId
			);
		}

		/* Send the response to the client */
		return generateResponse(intent);
	}
	catch (error) {
		return response.failureResponse(error.message);
	}
};

const generateResponse = (intent) => {
	/*
	 * Note that if your API version is before 2019-02-11, 'requires_action'
	 * appears as 'requires_source_action'.
	 */

	if (
		intent.status === 'requires_action' &&
		intent.next_action.type === 'use_stripe_sdk'
	) {
		// Tell the client to handle the action
		return {
			requires_action: true,
			payment_intent_client_secret: intent.client_secret
		};
	} else if (intent.status === 'succeeded') {
		/*
		 * The payment didn’t need any additional actions and completed!
		 * Handle post-payment fulfillment
		 */
		return { success: true };
	} else {
		// Invalid status
		return { error: 'Invalid PaymentIntent status' };
	}
};

module.exports = {
	createUpdateCustomer,
	addCardForCustomer,
	createCardToken,
	getUserCardDetails,
	deleteCardForCustomer,
	createPaymentIntent,
	getBricksPrice,
	makePayment
};
