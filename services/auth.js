const User = require('../model/user');
const dbService = require('../utils/dbService');
const userTokens = require('../model/userTokens');
const {
  JWT, LOGIN_ACCESS,
  PLATFORM, MAX_LOGIN_RETRY_LIMIT, LOGIN_REACTIVE_TIME, DEFAULT_SEND_LOGIN_OTP, SEND_LOGIN_OTP, FORGOT_PASSWORD_WITH
} = require('../constants/authConstant');
const jwt = require('jsonwebtoken');
const common = require('../utils/common');
const moment = require('moment');
const bcrypt = require('bcrypt');
const emailService = require('./email/emailService');
const tokenService = require('./token');
const sendSMS = require('./sms/smsService');
const uuid = require('uuid').v4;
const ejs = require('ejs');
const _ = require('lodash');
const { STATUS } = require('../constants/userConstant');
const axios = require('axios');
const activityService = require('./activity');

async function sendSMSForLoginOtp (user) {
  try {
    let otp = common.randomNumber();
    let expires = moment();
    expires = expires.add(30, 'minutes').toISOString();
    await dbService.updateDocument(User, user.id, {
      loginOTP: {
        code: otp,
        expireTime: expires
      }
    });
    let updatedUser = await dbService.getDocumentByQuery(User, { _id: user.id });
    let renderData = { ...updatedUser.toJSON() };
    const msg = await ejs.renderFile(`${__basedir}/views/AuthOTP/html.ejs`, renderData);
    let smsObj = {
      to: updatedUser.phoneNo,
      message: msg
    };
    return await sendSMS(smsObj);
  } catch (error) {
    console.error(error);
    return false;
  }
}
async function sendEmailForLoginOtp (user) {
  try {
    let otp = common.randomNumber();
    let expires = moment();
    expires = expires.add(30, 'minutes').toISOString();
    await dbService.updateDocument(User, user.id, {
      loginOTP: {
        code: otp,
        expireTime: expires
      }
    });
    let updatedUser = await dbService.getDocumentByQuery(User, { _id: user.id });

    let mailObj = {
      subject: 'Login Email OTP',
      to: user.email,
      template: '/views/sendOTP',
      data: updatedUser.toJSON()
    };
    await emailService.sendMail(mailObj);
    return true;
  } catch (error) {
    console.error(error);
    return false;
  }
}

let auth = module.exports;

auth.loginUser = async (username, userPassword, url) => {
  try {
    let where = { 'email': username };
    let user = await dbService.getDocumentByQuery(User, where);
    if (user) {
      if (user.loginRetryLimit >= MAX_LOGIN_RETRY_LIMIT) {
        let now = moment();
        if (user.loginReactiveTime) {
          let limitTime = moment(user.loginReactiveTime);
          if (limitTime > now) {
            let expireTime = moment().add(LOGIN_REACTIVE_TIME, 'minutes');
            if (limitTime <= expireTime) {
              return {
                flag: true,
                data: `You have exceeded the number of login attempts. Please try again in ${common.getDifferenceOfTwoDatesInTime(now, limitTime)}.`
              };
            }
            await dbService.updateDocument(User, user.id, {
              loginReactiveTime: expireTime.toISOString(),
              loginRetryLimit: user.loginRetryLimit + 1
            });
            return {
              flag: true,
              data: `You have exceeded the number of login attempts. Please try again in ${common.getDifferenceOfTwoDatesInTime(now, expireTime)}.`
            };
          } else {
            user = await dbService.findOneAndUpdateDocument(User, { _id: user.id }, {
              loginReactiveTime: '',
              loginRetryLimit: 0
            }, { new: true });
          }
        } else {
          // send error
          let expireTime = moment().add(LOGIN_REACTIVE_TIME, 'minutes');
          await dbService.updateDocument(User, user.id, {
            loginReactiveTime: expireTime.toISOString(),
            loginRetryLimit: user.loginRetryLimit + 1
          });
          return {
            flag: true,
            data: `You have exceeded the number of login attempts. Please try again in ${common.getDifferenceOfTwoDatesInTime(now, expireTime)}.`
          };
        }
      }
      const isPasswordMatched = await user.isPasswordMatch(userPassword);
      if (isPasswordMatched) {
        const {
          password, ...userData
        } = user.toJSON();
        let token = null;
        if (!user.role) {
          return {
            flag: true,
            data: 'You have not assigned any role'
          };
        }
        if (user.isEmailVerified && user.isPhoneNoVerified && user.isKycVerified && user.isRecoveryAddressExists && user.isCustodialWalletExists) {
          if (url.includes('client')) {
            if (!LOGIN_ACCESS[user.role].includes(PLATFORM.CLIENT)) {
              return {
                flag: true,
                data: 'you are unable to access this platform'
              };
            }
            token = await tokenService.generateAuthTokens(userData, JWT.CLIENT_SECRET);
          } else if (url.includes('admin')) {
            if (!LOGIN_ACCESS[user.role].includes(PLATFORM.ADMIN)) {
              return {
                flag: true,
                data: 'you are unable to access this platform'
              };
            }
            token = await tokenService.generateAuthTokens(userData, JWT.ADMIN_SECRET);
          }
        }
        if (user.loginRetryLimit) {
          await dbService.updateDocument(User, user.id, {
            loginRetryLimit: 0,
            loginReactiveTime: '',
            loginResendOTPReactiveTime: moment().toISOString(),
            loginResendOTPRetryLimit: 0,
            validateLoginOTPRetryLimit: 0
          });
        }
        let userToReturn = {
          ...userData,
          ...{ token }
        };
        userToReturn = _.omit(userToReturn, ['password', 'loginOTP', 'emailOTP', 'phoneNoOTP', 'resetPasswordOTP', 'recoveryAddress', 'kycInqId', 'settingPhoneVerified', 'settingEmailVerified', 'resetPasswordPermission', 'kyc', 'subWalletId']);
        return {
          flag: false,
          data: userToReturn
        };
      } else {
        await dbService.updateDocument(User, user.id, { loginRetryLimit: user.loginRetryLimit + 1 });
        return {
          flag: true,
          data: 'Incorrect Password'
        };
      }
    } else {
      return {
        flag: true,
        data: 'User not exists'
      };
    }
  } catch (error) {
    throw new Error(error.message);
  }
};
auth.changePassword = async (params) => {
  try {
    let password = params.newPassword;
    let where = {
      _id: params.userId,
      role: { $ne: 10 }
    };
    let user = await dbService.getDocumentByQuery(User, where);
    if (user && user.id) {
      password = await bcrypt.hash(password, 8);
      let updateData = {
        password: password,
        settingEmailVerified: false,
        settingPhoneVerified: false
      };
      let accountLockTime = user.accountLockTime;
      if (moment(accountLockTime) < moment().add('24', 'hours')) {
        updateData.isAccountLock = true;
        updateData.reasonOfLock = 'change password';
        updateData.accountLockTime = moment().add('24', 'hours').toISOString();
        accountLockTime = updateData.accountLockTime;
      }
      await activityService.sendActivityEmail('Withdrawals from your account have been locked', user.email, '/views/accountLock', {
        firstName: _.capitalize(user.firstName),
        accountLockTime: moment(accountLockTime).format('YYYY-MM-DD HH:mm:ss')
      });
      let updatedUser = dbService.updateDocument(User, user.id, updateData);
      if (updatedUser) {
        return {
          flag: false,
          data: 'Password changed successfully'
        };
      }
      return {
        flag: true,
        data: 'password can not changed due to some error.please try again'
      };
    }
    return {
      flag: true,
      data: 'User not found'
    };
  } catch (error) {
    throw new Error(error.message);
  }
};
auth.sendResetPasswordNotification = async (user, flag) => {
  let resultOfEmail = false;
  let resultOfSMS = false;
  try {
    let token = common.randomNumber();
    let expires = moment();
    expires = expires.add(FORGOT_PASSWORD_WITH.EXPIRETIME, 'minutes').toISOString();
    await dbService.updateDocument(User, user.id,
      {
        resetPasswordOTP: {
          code: token,
          expireTime: expires
        }
      });
    if (flag.email) {
      let updatedUser = await dbService.getDocumentByQuery(User, {
        _id: user.id,
        role: { $ne: 10 }
      });
      let mailObj = {
        subject: 'Reset Password',
        to: user.email,
        template: '/views/resetPasswordSendOTP',
        data: updatedUser
      };
      try {
        await emailService.sendMail(mailObj);
        resultOfEmail = true;
      } catch (error) {
        console.error(error);
      }
    }
    if (flag.sms) {
      let viewType = '/reset-password/';
      let msg = `Click on the link to reset your password.
                http://localhost:${process.env.PORT}${viewType + token}`;
      let smsObj = {
        to: user.mobileNo,
        message: msg
      };
      try {
        await sendSMS(smsObj);
        resultOfSMS = true;
      } catch (error) {
        console.error(error);
      }
    }
    return {
      resultOfEmail,
      resultOfSMS
    };
  } catch (error) {
    throw new Error(error.message);
  }
};
auth.resetPassword = async (user, newPassword) => {
  try {
    let where = {
      _id: user.id,
      role: { $ne: 10 }
    };
    const dbUser = await dbService.getDocumentByQuery(User, where);
    if (!dbUser) {
      return {
        flag: true,
        data: 'User not found',
      };
    }
    newPassword = await bcrypt.hash(newPassword, 8);
    await dbService.updateDocument(User, user.id, {
      password: newPassword,
      resetPasswordOTP: null,
      loginRetryLimit: 0,
      resetPasswordPermission: {
        Permissions: false,
        expireTime: moment().toISOString()
      }
    });
    let mailObj = {
      subject: 'Reset Password',
      to: user.email,
      template: '/views/successfullyResetPassword',
      data: {
        isWidth: true,
        email: user.email || '-',
        message: 'Password Successfully Reset'
      }
    };
    await emailService.sendMail(mailObj);
    return {
      flag: false,
      data: 'Password reset successfully',
    };
  } catch (error) {
    throw new Error(error.message);
  }
};
auth.sendLoginOTP = async (username, password, flag) => {
  try {
    let where = {
      'email': username,
      role: { $ne: 10 }
    };
    let user = await dbService.getDocumentByQuery(User, where);
    if (!user) {
      return {
        flag: true,
        data: 'User not found'
      };
    }
    if (user.loginRetryLimit >= MAX_LOGIN_RETRY_LIMIT) {
      if (user.loginReactiveTime) {
        let now = moment();
        let limitTime = moment(user.loginReactiveTime);
        if (limitTime > now) {
          let expireTime = moment().add(LOGIN_REACTIVE_TIME, 'minutes').toISOString();
          await dbService.updateDocument(User, user.id, {
            loginReactiveTime: expireTime,
            loginRetryLimit: user.loginRetryLimit + 1
          });
          return {
            flag: true,
            data: `You have exceeded the number of login attempts. Please try again in ${LOGIN_REACTIVE_TIME} minutes.`
          };
        }
      } else {
        // send error
        let expireTime = moment().add(LOGIN_REACTIVE_TIME, 'minutes').toISOString();
        await dbService.updateDocument(User, user.id, {
          loginReactiveTime: expireTime,
          loginRetryLimit: user.loginRetryLimit + 1
        });
        return {
          flag: true,
          data: `You have exceeded the number of login attempts. Please try again in ${LOGIN_REACTIVE_TIME} minutes.`
        };
      }
    }
    const isPasswordMatched = await user.isPasswordMatch(password);
    if (!isPasswordMatched) {
      await dbService.updateDocument(User, user.id, { loginRetryLimit: user.loginRetryLimit + 1 });
      return {
        flag: true,
        data: 'Incorrect Password'
      };
    }
    let res, message;
    if (user.kycInqId) {
      let kycDetails = await this.checkKycStatus({ inqId: user.kycInqId }, user);
      await dbService.updateDocument(User, user.id, {
        isKycVerified: kycDetails.isKycVerified,
        kycStatus: kycDetails.kycStatus
      });
      user.isKycVerified = kycDetails.isKycVerified;
      user.kycStatus = kycDetails.kycStatus;
    }
    if (flag.email || flag.sms) {
      if (flag.email) {
        // send Email here
        res = await sendEmailForLoginOtp(user);
        message = 'Please check your Email for OTP';
      } else if (flag.sms) {
        // send SMS here
        res = await sendSMSForLoginOtp(user);
        message = 'Please check your mobile for OTP';
      }
      if (!res) {
        return {
          flag: true,
          data: 'otp can not be sent due to some issue try again later.'
        };
      }
      return {
        flag: false,
        data: message
      };
    }
    let responseUserData = _.pick(user, ['email', 'phoneNo', 'isEmailVerified', 'isPhoneNoVerified', 'isKycVerified', 'isCustodialWalletExists', 'isRecoveryAddressExists33', 'dob']);
    return {
      flag: false,
      data: {
        message: 'Select method for Two-factor authentication',
        user: responseUserData
      }
    };
  } catch (error) {
    throw new Error(error.message);
  }
};
auth.loginWithOTP = async (username, password, url) => {
  try {
    let result = await auth.loginUser(username, password, url);
    if (!result.flag) {
      result.loginOTP = null;
      await dbService.updateDocument(User, { _id: result.data.id }, { loginOTP: null });
    }
    return result;
  } catch (error) {
    throw new Error(error.message);
  }
};
auth.sendEmailOtp = async (email, newEmail = false, altEmail = false) => {
  try {
    let where = {
      'email': email,
      role: { $ne: 10 }
    };
    let user = await dbService.getDocumentByQuery(User, where);
    if (!user) {
      return {
        flag: true,
        data: 'User not found'
      };
    }
    if (newEmail) {
      await dbService.updateDocument(User, { _id: user.id }, { 'email': newEmail });
      email = newEmail;
    }
    if (altEmail) {
      await dbService.updateDocument(User, { _id: user.id }, { 'altEmail': altEmail });
      email = altEmail;
    }
    let res = await this.sendVerifyEmailOtp(user.id, email);
    if (!res) {
      return {
        flag: true,
        data: 'otp can not be sent due to some issue try again later.'
      };
    }
    return {
      flag: false,
      data: 'Please check your Email for OTP'
    };
  } catch (error) {
    throw new Error(error.message);
  }
};
auth.sendPhoneNoOtp = async (phoneNo, email, update = true, altPhoneNo = false) => {
  try {
    let where = {
      'email': email,
      role: { $ne: 10 }
    };
    let user = await dbService.getDocumentByQuery(User, where);
    if (!user) {
      return {
        flag: true,
        data: 'User not found'
      };
    }
    if (update) {
      await dbService.updateDocument(User, { _id: user.id }, { 'phoneNo': phoneNo });
    }
    if (altPhoneNo) {
      await dbService.updateDocument(User, { _id: user.id }, { 'altPhoneNo': phoneNo });
    }
    let res = await this.sendVerifyPhoneNoOtp(user.id, phoneNo);
    if (!res) {
      return {
        flag: true,
        data: 'otp can not be sent due to some issue try again later.'
      };
    }
    return {
      flag: false,
      data: 'Please check your phone for OTP'
    };
  } catch (error) {
    throw new Error(error.message);
  }
};
auth.verifyEmail = async (email, otp) => {
  try {
    let where = {
      'email': email,
      role: { $ne: 10 }
    };
    let user = await dbService.getDocumentByQuery(User, where);
    if (user && user.emailOTP.code == otp) {
      user.emailOTP = null;
      await dbService.updateDocument(User, { _id: user.id }, {
        emailOTP: null,
        isEmailVerified: true,
        verifyEmailResendOTPReactiveTime: moment().toISOString(),
        verifyEmailResendOTPRetryLimit: 0
      });
      return {
        flag: false,
        data: 'Email verified successfully!'
      };
    }
    return {
      flag: true,
      data: 'Invalid code. Please try again.'
    };
  } catch (error) {
    console.error(error);
    throw new Error(error.message);
  }
};
auth.verifyPhoneNo = async (phoneNo, otp) => {
  try {
    let where = {
      'phoneNo': phoneNo,
      role: { $ne: 10 }
    };
    let user = await dbService.getDocumentByQuery(User, where);
    if (user && user.phoneNoOTP && user.phoneNoOTP.code == otp) {
      user.phoneNoOTP = null;
      await dbService.updateDocument(User, { _id: user.id }, {
        phoneNoOTP: null,
        isPhoneNoVerified: true,
        verifyPhoneNoResendOTPReactiveTime: moment().toISOString(),
        verifyPhoneNoResendOTPRetryLimit: 0
      });
      return {
        flag: false,
        data: 'Phone number verified successfully!'
      };
    }
    return {
      flag: true,
      data: 'Invalid code. Please try again.'
    };
  } catch (error) {
    console.error(error);
    throw new Error(error.message);
  }
};

auth.sendVerifyEmailOtp = async (userId, email) => {
  try {
    let otp = common.randomNumber();
    let expires = moment();
    expires = expires.add(30, 'minutes').toISOString();
    await dbService.updateDocument(User, userId, {
      emailOTP: {
        code: otp,
        expireTime: expires
      }
    });
    let updatedUser = await dbService.getDocumentByQuery(User, {
      _id: userId,
      role: { $ne: 10 }
    });

    let mailObj = {
      subject: 'Verify Email OTP',
      to: email,
      template: '/views/verifyEmailSendOTP',
      data: updatedUser.toJSON()
    };
    await emailService.sendMail(mailObj);
    return true;
  } catch (error) {
    console.error(error);
    return false;
  }
};
auth.sendVerifyPhoneNoOtp = async (userId, phoneNo) => {
  try {
    let otp = common.randomNumber();
    let expires = moment();
    expires = expires.add(30, 'minutes').toISOString();
    await dbService.updateDocument(User, userId, {
      phoneNoOTP: {
        code: otp,
        expireTime: expires
      }
    });
    let updatedUser = await dbService.getDocumentByQuery(User, {
      _id: userId,
      role: { $ne: 10 }
    });
    let renderData = { ...updatedUser.toJSON() };
    const msg = await ejs.renderFile(`${__basedir}/views/verifyPhoneNumberOTP/html.ejs`, renderData);
    let smsObj = {
      to: phoneNo,
      message: msg
    };
    await sendSMS(smsObj);
    return true;
  } catch (error) {
    console.error(error);
    return false;
  }
};
auth.checkKycStatus = async (params, user) => {
  try {
    let kycStatus = STATUS.NOT_CREATED;
    params.isKycVerified = false;
    const config = {
      method: 'get',
      url: `https://withpersona.com/api/v1/inquiries/${params.inqId}`,
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${process.env.PERSONA_KEY}`,
        'Persona-Version': '2021-05-14'
      }
    };
    await axios(config).then((response) => {
      if (response.data.data) {
        let status = response.data.data.attributes.status;
        switch (status) {
          case 'created':
            kycStatus = STATUS.CREATED;
            break;
          case 'pending':
            kycStatus = STATUS.PENDING;
            break;
          case 'completed':
            kycStatus = STATUS.COMPLETED;
            params.isKycVerified = true;
            break;
          case 'failed':
            kycStatus = STATUS.FAILED;
            break;
          case 'expired':
            kycStatus = STATUS.EXPIRED;
            break;
          case 'needs-review':
            kycStatus = STATUS.NEEDS_REVIEW;
            break;
          case 'approved':
            kycStatus = STATUS.APPROVED;
            params.isKycVerified = true;
            break;
          case 'declined':
            kycStatus = STATUS.DECLINED;
            break;
          default:
            kycStatus = STATUS.NOT_CREATED;
        }
      } else {
        kycStatus = STATUS.NOT_CREATED;
      }
    }, (error) => {
      console.error(error);
      kycStatus = STATUS.NOT_CREATED;
    });
    await dbService.updateDocument(User, { _id: user.id }, {
      kyc: params.kyc || null,
      isKycVerified: params.isKycVerified,
      kycStatus: kycStatus
    });
    return {
      kycStatus: kycStatus,
      isKycVerified: params.isKycVerified
    };
  } catch (error) {
    console.error(error);
    throw new Error(error.message);
  }

};