
const File = require('../model/file');
const fs = require('fs');
const mime = require('mime-types');
const AWS = require('aws-sdk');
AWS.config.loadFromPath('./config.json');
const s3 = new AWS.S3({ region: 'eu-west-2' });
/**
 * Create a file
 * @param {Object} fileBody
 * @returns {Promise<File>}
 */
const createFile = async (fileBody) => {
	return File.create(fileBody);
};

/**
 * find a file
 * @param {Object} fileBody
 * @returns {Promise<File>}
 */
const find = async (filter) => {
	return File.find(filter).populate(['posterId']);
};

/**
 * upload File
 * @param {file} file
 * @returns {Promise<File>}
 */
const uploadFile = async (file, id) => {
	const ext = mime.extension(file.mimetype);
	const fileContent = fs.readFileSync(file.path);
	const params = {
		Bucket: process.env.BUCKET_NAME,
		Key: `${id}.${ext}`, // File name you want to save as in S3
		Body: fileContent,
		ContentType: file.mimetype,
		ACL: 'public-read'
	};
	return s3.upload(params).promise();
};
/**
 * Get file by id
 * @param {ObjectId} id
 * @returns {Promise<File>}
 */
const getFileById = async (id) => {
	return File.findById(id).populate(['posterId']);
};

/**
 * Get file by type
 * @param {String} type
 * @returns {Promise<File>}
 */
const findByType = async (type) => {
	return File.find({ type: type }).populate(['posterId']);
};

/**
 * Update file by id
 * @param {ObjectId} fileId
 * @param {Object} updateBody
 * @returns {Promise<File>}
 */
const updateFileById = async (fileId, updateBody) => {
	const file = await getFileById(fileId);
	if (!file) {
		throw new Error('File not found');
	}
	Object.assign(file, updateBody);
	await file.save();
	return file;
};

/**
 * Delete file by id
 * @param {ObjectId} fileId
 * @returns {Promise<File>}
 */
const deleteFileById = async (fileId) => {
	const file = await getFileById(fileId);
	if (!file) {
		throw new Error('File not found');
	}
	if (file.uri) {
		let key = file.uri.split('/');
		key = key.slice(-1)[0];
		let params = {
			Bucket: process.env.BUCKET_NAME,
			Key: key
		};
		await s3.deleteObject(params).promise();
	}
	await file.remove();
	return file;
};

module.exports = {
	find,
	createFile,
	uploadFile,
	getFileById,
	findByType,
	updateFileById,
	deleteFileById
};
