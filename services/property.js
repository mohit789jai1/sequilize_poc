const Property = require('../model/property');

const findOne = async (filter, populate = []) => {
	return Property.findOne(filter).populate(populate);
};

const findById = async (id, populate = []) => {
	return Property.findById(id).populate(populate);
};

const find = async (filter, populate = []) => {
	return Property.find(filter).populate(populate);
};

const deleteOne = async (filter) => {
	return Property.deleteOne(filter);
};

module.exports = {
	findOne,
	findById,
	find,
	deleteOne
};