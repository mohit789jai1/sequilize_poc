const accountSid = process.env.TWILIO_ACCOUNT_SID; // Paste your AccountSid
const authToken = process.env.TWILIO_AUTH_TOKEN; // Paste your Auth Token
//create instance of twilio
const client = require('twilio')(accountSid, authToken);
const _ = require('lodash');
const { phone } = require('phone');
const { TWILIO_NOT_ALLOW_COUNTRY_LIST } = require('../../constants/userConstant');
const sendSms = async (obj) => {
  try {
    let recipients = [];
    if (Array.isArray(obj.to)) {
      recipients = _.compact(recipients.concat(obj.to));
    } else if (typeof obj.to === 'string' && !_.isEmpty(obj.to)) {
      const mobileArray = obj.to.split(',');
      recipients = recipients.concat(mobileArray);
    }
    if (recipients.length) {
      for (let i = 0; i < recipients.length; i++) {
        // send message using twilio
        let messageBody = {
          body: obj.message,
          messagingServiceSid: process.env.TWILIO_MESSAGING_ID,
          to: recipients[i],
        };
        let country = phone(recipients[i]);;
        if (country.isValid && !TWILIO_NOT_ALLOW_COUNTRY_LIST.includes(country.countryIso2)) {
          messageBody.from = 'MyBricks';
        }
        client.messages.create(messageBody).done();
      }
      return true;
    }
    return false;
  } catch (error) {
    console.error(error);
    return false;
  }
};

module.exports = sendSms;