const express = require('express');
const routes = express.Router();
const auth = require('../../middleware/auth');
const authController = require('../../controller/client/authController');

routes.route('/client/auth/register').post(authController.register);
routes.route('/client/auth/send_login_otp').post(authController.sendOtpForLogin);
routes.route('/client/auth/refresh_tokens').post(authController.refreshTokens);
routes.route('/client/auth/send_email_verify_otp').post(authController.sendOtpForVerifyEmail);
routes.route('/client/auth/send_phone_no_verify_otp').post(authController.sendOtpForVerifyPhoneNo);
routes.route('/client/auth/verify_phone_no').post(authController.verifyPhoneNo);
routes.route('/client/auth/verify_email').post(authController.verifyEmail);
routes.route('/client/auth/update_inq_id').post(authController.updateInqIdForKyc);
routes.route('/client/auth/kyc').post(authController.verifyKyc);
routes.route('/client/auth/recovery_code').post(authController.recoveryAddress);
routes.route('/client/auth/recover_send_email_verify_otp').post(authController.sendOtpForRecoverEmail);
routes.route('/client/auth/recover_send_phone_no_verify_otp').post(authController.sendOtpForRecoverPhoneNo);
routes.route('/client/auth/recover_verify_phone_no').post(authController.RecoverPhoneNo);
routes.route('/client/auth/recover_verify_email').post(authController.RecoverEmail);
routes.route('/client/auth/recover_resend_otp').post(authController.recoverResendOtp);
routes.route('/client/auth/recover_account').post(authController.recoverAccount);
routes.route('/client/auth/reset_account').post(authController.giveResetPasswordPermission);
routes.route('/client/auth/create_wallet').post(authController.createWallet);
routes.route('/client/auth/login_with_otp').post(authController.loginWithOTP);
routes.route('/client/auth/forgot-password').post(authController.forgotPassword);
routes.route('/client/auth/resend-otp').post(authController.resendOtp);
routes.route('/client/auth/validate-otp').post(authController.validateResetPasswordOtp);
routes.route('/client/auth/reset-password').put(authController.resetPassword);
routes.route('/client/auth/logout').post(authController.logout);
routes.route('/client/auth/session').get(authController.sessionStatus);

//admin
routes.route('/admin/auth/login').post(authController.loginInAdmin);
routes.route('/admin/auth/refresh_tokens').post(authController.refreshAdminTokens);

module.exports = routes;