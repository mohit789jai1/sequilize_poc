const express = require('express');
const router = express.Router();
const stripeController = require('../../controller/client/stripeController');
const auth = require('../../middleware/auth');

/*
 * router.route('/client/api/v1/stripe/create_customer/:id').get(stripeController.createCustomer);
 * router.route('/client/api/v1/stripe/create_customer_card/:id').post(auth(), stripeController.createCustomerCard);
 * router.route('/client/api/v1/stripe/customer_cards/:stripeCustomerId').get(stripeController.getCustomerCards);
 * router.route('/client/api/v1/stripe/delete_customer_card/:stripeCustomerId/:stripeCardId').delete(stripeController.deleteCustomerCard);
 * router.route('/client/api/v1/stripe/make_payment/:stripeCustomerId').post(auth(), stripeController.makePayment);
 * router.route('/client/api/v1/stripe/latest_bricks_price').get(stripeController.getBrikePrice);
 */

module.exports = router;