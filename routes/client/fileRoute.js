const express = require('express');
const router = express.Router();
const fileController = require('../../controller/client/fileController');
const auth = require('../../middleware/auth');

//client
router.route('/client/api/v1/file/').get(fileController.getFileByTypeWithOutAuth); // without Auth

//admin
router.route('/admin/api/v1/file/').get(auth(), fileController.getFileByTypeWithOutAuth);
router.route('/admin/api/v1/file/create').post(auth(), fileController.createFile);
router.route('/admin/api/v1/file/update/:id').put(auth(), fileController.updateFile);
router.route('/admin/api/v1/file/delete/:id').post(auth(), fileController.deleteFile);

module.exports = router;
