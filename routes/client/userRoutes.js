const express = require('express');
const router = express.Router();
const userController = require('../../controller/client/userController');
const auth = require('../../middleware/auth');

//client

router.route('/client/api/v1/user/user_by_id/:id').get(auth(), userController.getUser);
router.route('/client/api/v1/user/send_email_otp/').get(auth(), userController.sendEmailOTP);
router.route('/client/api/v1/user/send_phone_otp/').get(auth(), userController.sendPhoneOTP);
router.route('/client/api/v1/user/verify_email/').post(auth(), userController.verifyEmail);
router.route('/client/api/v1/user/verify_phone/').post(auth(), userController.verifyPhoneNo);
router.route('/client/api/v1/user/resend_otp/').post(auth(), userController.resendOtp);
router.route('/client/api/v1/user/softDelete/:id').delete(auth(), userController.deleteUser);
router.route('/client/api/v1/user/change_password').put(auth(), userController.changePassword);
router.route('/client/api/v1/user/send_alt_email_otp/').post(auth(), userController.sendAltEmailOTP);
router.route('/client/api/v1/user/change_email/').post(auth(), userController.changeEmail);
router.route('/client/api/v1/user/send_alt_phone_otp/').post(auth(), userController.sendAltPhoneOTP);
router.route('/client/api/v1/user/change_phone/').post(auth(), userController.changePhoneNo);
router.route('/client/api/v1/user/change_recovery_code/').get(auth(), userController.generateNewRecoveryCode);
router.route('/client/api/v1/user/referral_data/').get(auth(), userController.getUserReferralData);

//admin
router.route('/admin/api/v1/user/list').post(auth(), userController.getUserByFilter);

module.exports = router;
