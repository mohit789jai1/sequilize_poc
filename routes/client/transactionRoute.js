const express = require('express');
const router = express.Router();
const transactionController = require('../../controller/client/transactionController');
const NFTController = require('../../controller/client/NFTController');
const auth = require('../../middleware/auth');

router.route('/client/api/v1/trans/history').get(auth(), transactionController.getTransactionHistory);
router.route('/client/api/v1/trans/history_detail/:id').get(auth(), transactionController.getTransactionDetail);
router.route('/client/api/v1/trans/moonpay_transaction/:id').get(auth(), transactionController.storeMoonPayTransaction);
router.route('/client/api/v1/trans/sign_transaction').post(auth(), transactionController.createEthTrans);
router.route('/client/api/v1/trans/bnb_bnb_calculate').post(auth(), transactionController.BNBtoBNBCalculation);
router.route('/client/api/v1/trans/buy_bnb').get(auth(), transactionController.buyBNB);
router.route('/client/api/v1/trans/balance').get(auth(), transactionController.getBalance);
router.route('/client/api/v1/trans/bnb_bricks_calculate').post(auth(), transactionController.BNBtoBricksCalculation);
router.route('/client/api/v1/trans/bnb_bricks_transaction').post(auth(), transactionController.BNBtoBricksTransaction);
router.route('/client/api/v1/trans/bricks_to_bricks_transaction').post(auth(), transactionController.BricksToBricksTransaction);
router.route('/client/api/v1/trans/price_chart').post(transactionController.getChartValue);
router.route('/client/api/v1/trans/bricks_bricks_calculate').post(auth(), transactionController.BricksToBricksCalculation);
router.route('/client/api/v1/trans/status_of_transaction').get(auth(), transactionController.statusOfTransaction);
router.route('/client/api/v1/trans/nft').get(auth(), NFTController.getNFTs);
router.route('/client/api/v1/trans/nft_prices/:tokenId').get(auth(), NFTController.getNFTPrices);
router.route('/client/api/v1/trans/nft_transaction').post(auth(), NFTController.NFTTransaction);
router.route('/client/api/v1/trans/nft_calculate').post(auth(), NFTController.NFTCalculation);

// admin access
router.route('/admin/api/v1/trans/change_policy').post(auth(), transactionController.changePolicy);
router.route('/admin/api/v1/trans/get_public_key').get(auth(), transactionController.getPublicKey);

module.exports = router;
