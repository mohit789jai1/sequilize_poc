const express = require('express');
const router = express.Router();
const propertyController = require('../../controller/client/propertyController');
const auth = require('../../middleware/auth');

router.route('/admin/api/v1/property/:id').get(auth(), propertyController.getProperty);
router.route('/admin/api/v1/property/create').post(auth(), propertyController.createProperty);
router.route('/admin/api/v1/property/update/:id').put(auth(), propertyController.updateProperty);
router.route('/admin/api/v1/property/delete/:id').post(auth(), propertyController.deleteProperty);

module.exports = router;
