const express = require('express');
const router = express.Router();
const projectSettingController = require('../../controller/client/projectSettingController');
const auth = require('../../middleware/auth');

router.route('/client/api/v1/setting/').get(projectSettingController.getSettingByType);

module.exports = router;
