const express = require('express');
const router = express.Router();
const activityController = require('../../controller/client/activityController');
const auth = require('../../middleware/auth');
//admin
router.route('/admin/api/v1/activity/get').post(auth(), activityController.getActivityByFilter);

module.exports = router;
