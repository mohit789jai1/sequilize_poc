const express = require('express');
const router = express.Router();
const notificationController = require('../../controller/client/notificationController');
const auth = require('../../middleware/auth');

router.route('/client/api/v1/notification/subscribe').post(notificationController.subscribe);

// admin access
router.route('/admin/api/v1/notification/launch_notification').post(auth(), notificationController.launchNotification);

module.exports = router;
