const express = require('express');
const router = express.Router();
const perkController = require('../../controller/client/perkController');
const auth = require('../../middleware/auth');

router.route('/admin/api/v1/perk/:id').get(auth(), perkController.getPerk);
router.route('/admin/api/v1/perk/create').post(auth(), perkController.createPerk);
router.route('/admin/api/v1/perk/update/:id').put(auth(), perkController.updatePerk);
router.route('/admin/api/v1/perk/delete/:id').post(auth(), perkController.deletePerk);

module.exports = router;
