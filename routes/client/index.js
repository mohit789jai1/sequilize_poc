const express = require('express');
const router = express.Router();

router.use(require('./auth'));
router.use(require('./userRoutes'));
router.use(require('./transactionRoute'));
router.use(require('./portfolioRoute'));
router.use(require('./propertyRoute'));
router.use(require('./perkRoute'));
router.use(require('./fileRoute'));
router.use(require('./projectSettingRoute'));
router.use(require('./notificationRoute'));
router.use(require('./activityRoute'));
router.use(require('./stripe'));

module.exports = router;
