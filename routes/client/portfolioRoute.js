const express = require('express');
const router = express.Router();
const portfolioController = require('../../controller/client/portfolioController');
const auth = require('../../middleware/auth');

router.route('/client/api/v1/portfolio/').get(portfolioController.getFullPortfolio);

//admin
router.route('/admin/api/v1/portfolio/:id').get(auth(), portfolioController.getPortfolio);
router.route('/admin/api/v1/portfolio/').get(auth(), portfolioController.getFullPortfolioAdmin);
router.route('/admin/api/v1/portfolio/create').post(auth(), portfolioController.createPortfolio);
router.route('/admin/api/v1/portfolio/update/:id').put(auth(), portfolioController.updatePortfolio);
router.route('/admin/api/v1/portfolio/delete/:id').post(auth(), portfolioController.deletePortfolio);

module.exports = router;
