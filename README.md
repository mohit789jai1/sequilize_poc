Google doc Link :https://docs.google.com/document/d/19n4SDOGnmbBqPfbJy2K2BhDUFH-z3VJzdu_ARxcW4HU/edit

# NodeJS,Mongoose,Express Project in MVC Architecture

**supported version of nodejs-15.13.0**,
**supported version of mongoose-4.0**

- This is a Web application, developed using MVC pattern with Node.js, ExpressJS, and Mongoose ODM. 
- Basic boilerplate for web applications, built on Express.js using the Model–View–Controller architectural pattern.
- MongoDB database is used for data storage, with object modeling provided by Mongoose.

# Initial
- Configure a basic server in app.js.
- Organize the routes with Express Router.
- Use the mainRoutes in app as middleware.
- Set a final use after the routes, to display a 404 message for the unhandled requests.
1. Install needed Node.js modules:
    ```$ npm install```
2. execute server:
    ```$ npm start```
3. When the app will run successfully,

# Default folder structure:

	--project_folder
		--config
		--controllers
		--middleware
		--models
		--postman
		--routes
		--services
		--utils
		--views
		--app.js
		--.env
		--.gitignore
		--.eslintrc.js
# app.js
- entry point of application.
# config
- passport strategy for all platforms.
- based on Auth Model - authentication files has been generated.
- Auth constant File that has authentication configuration constants
- Used .env file and configure the db connection string to use in the project.
# controllers
- includes controller files per model
     	  -controller
     	        -client
     	          -modelController.js
     
# logs
- Log file
# middleware
- User authentication Middleware based on Roles and permission for Routes' access
- Custom Policy files
# models
- Mongoose Models , as per user defined schema 
# postman
- Postman collection File for Platform based APIs that are generated.
- Import this JSON in Postman to test the APIs.
# public 
- You can add static files like like images, pdf etc.
# routes
- based on platform,separate folder is generated,within those folders model wise route files are that has model crud APIs' routes.
- index.js file, main file which includes all platform routes.
- added index files in app.js to access the routes of the application.
# services
     	-jobs
       		-cron job services
     	-auth.js
       		-Logic for JWT Tokenization for user to login into Application using username and password along with otp if required.
# utils
	     -validation
     		   -joi validations files.
     		   -files are separated by models.
     	 -common.js
       		   -converted object to enum function.
     	 -dbService.js
       		 -common Database functionalities
     	  	 -getAllDocuments(find all documents)
     	  	 -updateDocuments(update single documents in db)
     	  	 -deleteDocuments(delete single documents in db)
     	  	 -createDocuments(create single documents in db)
     	  	 -getDocumentByQuery(find single document)
			 -getSingleDocumentById(find by id)
     	  	 -softDelete
     	  	 -findExistData
     	  	 -bulkInsert(insert multiple documents in db)
     	  	 -bulkUpdate(update multiple documents in db)
     	  	 -countDocument
			 -Aggregation
     	 -messages.js
  		     -static messages that are sent with response - contains status and Data
	      -responseCode.js
  		     -codes for responses
	      -validateRequest.js
  		     -validate schema based on joi validation
# views
- add ejs files

#flow and details of every API

# REGISTRATION FLOW
1. Register
   * api: user register 
   * request type : POST
   * url : {{url}}/client/auth/register
   * description : first time user registration.
   * request body : {
      "password": "Test@123",
      "email": "tarparaajay@gmail.com",
      "firstName": "Ajay",
      "phoneNo": "",
      "lastName": "Tarpara",
      "role": 1
		}
   * response :{
    "status": "SUCCESS",
    "message": "Your request is successfully executed",
    "data": {
        "loginOTP": null,
        "emailOTP": null,
        "phoneNoOTP": null,
        "resetPasswordLink": null,
        "isEmailVerified": false,
        "isPhoneNoVerified": false,
        "loginRetryLimit": 0,
        "resendOTPRetryLimit": 0,
        "validateOTPRetryLimit": 0,
        "custodialWalletAddress": null,
        "isKycVerified": false,
        "_id": "618a25a3b6aecf5338a9d5cb",
        "password": "$2b$08$Pww2i4RyBbAnsxH7LqAS3uoaHlbkVQEOViXI/weEwB13E8Qx5pfpu",
        "email": "tarparaajay@gmail.com",
        "firstName": "Ajay",
        "phoneNo": "",
        "lastName": "Tarpara",
        "role": 1,
        "createdAt": "2021-11-09T07:39:15.193Z",
        "updatedAt": "2021-11-09T07:39:15.193Z",
        "isDeleted": false,
        "isActive": true,
        "id": "618a25a3b6aecf5338a9d5cb"
    }
}

2. send otp for verify Email

	* api : send email of otp
	* request type : POST
	* description : send otp to user for verify email
	* url : {{url}}/client/auth/send_email_verify_otp
	* request body : 
		1. for send otp on email 
			{
			"email": "tarparaajay@gmail.com"
			}
		2. change email and send otp
			{
			"email": "tarparaajay@gmail.com",
			"newEmail" :"test@gmail.com"
			}
	* response :{
		"status": "SUCCESS",
		"message": "Please check your Email for OTP",
		"data": "Please check your Email for OTP"
		}

3. verify email otp
	* api : verify email
	* request type : POST
	* description : after successfully sent otp to user, user can  verify email, you can enter wrong otp 3 time after that otp will expire and you have to call **resendOtp** API.
	* url : {{url}}/client/auth/verify_email
	* request body : {
		"email": "tarparaajay@gmail.com",
		"code": "3V5M37"
	}
	* response : {
    "status": "SUCCESS",
    "message": "Email verified successfully!",
    "data": "Email verified successfully!"
}

4. send otp for mobile verification
	* api : send sms
	* request type : POST
	* description : send otp to user for verify phone number and add phone number in user record you can call this hapi with one number only one time. you can use this API for change phone number and send otp again.
	* url : {{url}}/client/auth/send_phone_no_verify_otp
	* request body : {
		 "email": "tarparaajay@gmail.com",
    	 "phoneNo": "+917878581382"
		}
	* response :{
    "status": "SUCCESS",
    "message": "Please check your phone for OTP",
    "data": "Please check your phone for OTP"
	}

5. verify mobile otp
	* api : verify phone number
	* request type : POST
	* description : after successfully sent otp to user, user can verify phone number, you can enter otp wrong for 3 time and after that you have to send new otp. using **resend OTP** Api.
	* url : {{url}}/client/auth/verify_phone_no
	* request body : {
		"phoneNo": "+447932935146",
		"code": "7J1D3U"
	}
	* response :{
    "status": "SUCCESS",
    "message": "Phone number verified successfully!",
    "data": "Phone number verified successfully!"
	}

6. update inquiry id for KYC
	* api : update inquiry id
	* request type : POST
	* description : update inquiry id for KYC
	* url : {{url}}/client/auth/update_inq_id
	* request body : 
            {
                "email":"tarparaajay@gmail.com",
                "inqId": <id of inq>
            }
	* response :{
    "status": "SUCCESS",
    "message": "Kyc inquiry key added successfully !!",
    "data": "Kyc inquiry key added successfully !!"
}
7. verify KYC
	* api : verify kyc
	* request type : POST
	* description : after successfully verify phone number, user can  verify kyc
	* url : {{url}}/client/auth/kyc
	* request body : 
            {
                "email":"tarparaajay@gmail.com",
                "inqId":<id of inq>
            }
	* response :{
                "status": "SUCCESS",
                "message": "Your request is successfully executed",
                "data": {
                    "kycStatus": 8,
                    "isKycVerified": true,
                    "access": {
                        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2NDI2NzYyMTQsImV4cCI6MTY0MjY3NjUxNCwiZGF0YSI6eyJ1c2VySWQiOiI2MThiYTc5MDA5ZDZhMDAwMTM2YWY2YTYifX0.kqcKzeSxMYPTslBh4ONzPkSDbHa6ACcq3Z6xTYFFw5I",
                        "expires": "2022-01-20T11:01:54.810Z"
                    },
                    "refresh": {
                        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2NDI2NzYyMTQsImV4cCI6MTY0MjY3NzExNCwiZGF0YSI6eyJ1c2VySWQiOiI2MThiYTc5MDA5ZDZhMDAwMTM2YWY2YTYifX0.b7ZLTArOcmBwli9lify81NpCEvfMnp3tXQ4z5-km6io",
                        "expires": "2022-01-20T11:11:54.822Z"
                    }
                }
            }

8. create custodial wallet
	* api : create Wallet
	* request type : POST
	* description : after KYC create custodial wallet
	* url : {{url}}/client/auth/create_wallet
	* request body : {
                    "email":"tarparaajay@gmail.com"
                }
    * response :{
                "status": "SUCCESS",
                "message": {
                    "message": "Custodial wallet created successfully!",
                    "data": {
                        "subWalletId": "a7af42ec-3685-44a8-bd97-f3d9513724c1/ETH/21",
                        "walletAddress": "0x5F7Ad62FFFEc6f2CD7CcF7bdE4158E87A122eba2"
                    }
                },
                "data": {
                    "message": "Custodial wallet created successfully!",
                    "data": {
                        "subWalletId": "a7af42ec-3685-44a8-bd97-f3d9513724c1/ETH/21",
                        "walletAddress": "0x5F7Ad62FFFEc6f2CD7CcF7bdE4158E87A122eba2"
                    }
                }
            }

9. create recovery code
	* api : create recovery code
	* request type : POST
	* description : after successfully verify phone number, user can  create recovery code
	* url : {{url}}/client/auth/recovery_code
	* request body : 
                {
                    "email":"tarparaajay@gmail.com"
                }
	* response :
    {
        "status": "SUCCESS",
        "message": {
            "recoveryAddress": "4jyk106+ec4q4565+$aswau$",
            "message": "Recovery Code generated successfully !!"
        },
        "data": {
            "recoveryAddress": "4jyk106+ec4q4565+$aswau$",
            "message": "Recovery Code generated successfully !!"
        }
    }
# LOGIN FLOW

1. send login otp
   * api : send email or sms
   * request type : POST
   * description : send otp to user for two-factor authentication as well as to check user is exits or not. also there is limit has set on send otp.
   * url : {{url}}/client/auth/send_login_otp
   * request body : 
     1. for check user is exist or not 
	 	{
			"username": "tarparaajay@gmail.com",
			"password": "Test@123"
    	}
		
	 2. for select 2FA as email
	    {
			"username": "tarparaajay@gmail.com",
			"password": "Test@123",
			"email" : true
    	}

	 3. for select 2FA as phoneNo
	    {
			"username": "tarparaajay@gmail.com",
			"password": "Test@123",
			"sms" : true
    	}

   * response : 
		1. for check user is exist or not 
			{
			"status": "SUCCESS",
			"message": {
				"message": "Select method for Two-factor authentication",
				"user": {
					"email": "tarparaajay@gmail.com",
					"phoneNo": "+917878581382"
				}
			},
			"data": {
				"message": "Select method for Two-factor authentication",
				"user": {
					"email": "tarparaajay@gmail.com",
					"phoneNo": "+917878581382"
				}
			}
		2. for select 2FA as email
			{
				"status": "SUCCESS",
				"message": "Please check your Email for OTP",
				"data": "Please check your Email for OTP"
			}
		 3. for select 2FA as phoneNo
			{
				"status": "SUCCESS",
				"message": "Please check your mobile for OTP",
				"data": "Please check your mobile for OTP"
			}
}

2. login with otp
   * api : two-factor authentication
   * request type : POST
   * description : after successfully sent otp to user, user can login with otp.
   * url : {{url}}/client/auth/login_with_otp
   * request body : {
      "username": "tarparaajay@gmail.com",
      "password": "Test@123",
      "code": "CL5QBP"
    }
   * response : {
        "status": "SUCCESS",
        "message": "Login Successful",
        "data": {
            "loginOTP": {
                "code": "PB5VE7",
                "expireTime": "2022-01-20T16:51:13.612Z"
            },
            "emailOTP": null,
            "phoneNoOTP": null,
            "resetPasswordOTP": null,
            "resetPasswordPermission": {
                "Permissions": false,
                "expireTime": "2022-01-17T11:44:36.301Z"
            },
            "isEmailVerified": true,
            "isPhoneNoVerified": true,
            "loginRetryLimit": 0,
            "resendOTPRetryLimit": 0,
            "verifyEmailResendOTPRetryLimit": 0,
            "verifyPhoneNoResendOTPRetryLimit": 0,
            "loginResendOTPRetryLimit": 0,
            "validateOTPRetryLimit": 0,
            "validateVerifyEmailOTPRetryLimit": 0,
            "validateVerifyPhoneNoOTPRetryLimit": 0,
            "validateLoginOTPRetryLimit": 0,
            "isCustodialWalletExists": true,
            "custodialWalletAddress": "0xdBAddd1760Ff637291af8Dd8d3A9198Fa0550C4B",
            "subWalletId": "a7af42ec-3685-44a8-bd97-f3d9513724c1/ETH/49",
            "isRecoveryAddressExists": true,
            "recoveryAddress": "a39feb17c761fd0d108446b9931866934ebcc59f0835f5457ed719eecc420790cff7a53ea1133aa1ddbff33ea9e6e461X6IzmZ2Rv1H1A6UQilj9hxccaurRu4lZyibe+SYX8jI=",
            "isKycVerified": true,
            "kycStatus": 8,
            "kycInqId": "inq_VcdF9bBWAoLbgAVez8C5MXey",
            "settingEmailVerified": false,
            "settingPhoneVerified": false,
            "lastSyncBlockNumber": 11846538,
            "transInProgress": false,
            "_id": "618ba79009d6a000136af6a6",
            "resetPasswordLink": null,
            "email": "tarparaajay@gmail.com",
            "firstName": "Ajay",
            "phoneNo": "+917878581381",
            "lastName": "Tarpara",
            "role": 1,
            "createdAt": "2021-11-10T11:05:52.506Z",
            "updatedAt": "2022-01-20T10:51:13.614Z",
            "isDeleted": false,
            "isActive": true,
            "loginReactiveTime": null,
            "resendOTPReactiveTime": "2022-01-17T11:44:22.035Z",
            "updatedBy": "618ba79009d6a000136af6a6",
            "verifyPhoneNoResendOTPReactiveTime": "2022-01-20T09:36:34.493Z",
            "verifyEmailResendOTPReactiveTime": "2022-01-20T09:36:19.293Z",
            "lastSyncDate": "2022-01-20T09:27:53.152Z",
            "altEmail": "callum+123@techalchemy.co",
            "altPhoneNo": "+447947684942",
            "kyc": null,
            "dob": "2022-01-18T09:34:21.169Z",
            "loginResendOTPReactiveTime": "2022-01-18T12:13:25.039Z",
            "id": "618ba79009d6a000136af6a6",
            "token": {
                "access": {
                    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2NDI2NzU4ODksImV4cCI6MTY0MjY3NjE4OSwiZGF0YSI6eyJ1c2VySWQiOiI2MThiYTc5MDA5ZDZhMDAwMTM2YWY2YTYifX0.xTdJcKSQAh3iqM9UUMvIB0fSMjz6cFIp_cs3nHjgbr8",
                    "expires": "2022-01-20T10:56:29.379Z"
                },
                "refresh": {
                    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2NDI2NzU4ODksImV4cCI6MTY0MjY3Njc4OSwiZGF0YSI6eyJ1c2VySWQiOiI2MThiYTc5MDA5ZDZhMDAwMTM2YWY2YTYifX0.bWZkKK7Np7uD-A_ITS9BRnszV6fefFFvb5TdzSzW-GI",
                    "expires": "2022-01-20T11:06:29.380Z"
                }
            }
        }
    }


# REFRESH AUTH
1. refresh auth for client panel
* api : refresh auth for client panel
   * request type : POST
   * description :refresh auth for client panel
   * url : {{url}}/client/auth/refresh_tokens
   * request body : {
                    "token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxYzQ1MmViOGYzMDAwM2QyNjRiZWJlZiIsImVtYWlsIjoidGFycGFyYWFqYXlAZ21haWwuY29tIiwiaWF0IjoxNjQxODkxNDEyLCJleHAiOjE2NDE4OTIzMTJ9.3olU33QOK3gZ6gdfe3ZBf2uf9VvmZ3aKJyM7xQ1R-8w"
                }
   * response : {
                "status": "SUCCESS",
                "message": "Your request is successfully executed",
                "data": {
                    "access": {
                        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2NDI2NzU5MjEsImV4cCI6MTY0MjY3NjIyMSwiZGF0YSI6eyJ1c2VySWQiOiI2MThiYTc5MDA5ZDZhMDAwMTM2YWY2YTYifX0.A2OoEz5rDDaFmIkZNifAvgbsxRd7Xr44tzLzqRUADnE",
                        "expires": "2022-01-20T10:57:01.830Z"
                    },
                    "refresh": {
                        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2NDI2NzU5MjEsImV4cCI6MTY0MjY3NjgyMSwiZGF0YSI6eyJ1c2VySWQiOiI2MThiYTc5MDA5ZDZhMDAwMTM2YWY2YTYifX0.8g6clV_6Ww1td9s_gHwpBGTUv6wZ3h3e_e2VarfbyM8",
                        "expires": "2022-01-20T11:07:01.833Z"
                    }
                }
            }

2. refresh auth for admin panel  **admin access**
* api : refresh auth for admin panel
   * request type : POST
   * description :refresh auth for admin panel
   * url : {{url}}/admin/auth/refresh_tokens
   * request body : {
                    "token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxYzQ1MmViOGYzMDAwM2QyNjRiZWJlZiIsImVtYWlsIjoidGFycGFyYWFqYXlAZ21haWwuY29tIiwiaWF0IjoxNjQxODkxNDEyLCJleHAiOjE2NDE4OTIzMTJ9.3olU33QOK3gZ6gdfe3ZBf2uf9VvmZ3aKJyM7xQ1R-8w"
                }
   * response : {
                "status": "SUCCESS",
                "message": "Your request is successfully executed",
                "data": {
                    "access": {
                        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2NDI2NzU5MjEsImV4cCI6MTY0MjY3NjIyMSwiZGF0YSI6eyJ1c2VySWQiOiI2MThiYTc5MDA5ZDZhMDAwMTM2YWY2YTYifX0.A2OoEz5rDDaFmIkZNifAvgbsxRd7Xr44tzLzqRUADnE",
                        "expires": "2022-01-20T10:57:01.830Z"
                    },
                    "refresh": {
                        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2NDI2NzU5MjEsImV4cCI6MTY0MjY3NjgyMSwiZGF0YSI6eyJ1c2VySWQiOiI2MThiYTc5MDA5ZDZhMDAwMTM2YWY2YTYifX0.8g6clV_6Ww1td9s_gHwpBGTUv6wZ3h3e_e2VarfbyM8",
                        "expires": "2022-01-20T11:07:01.833Z"
                    }
                }
            }
3. status of session 
   * api : status of session
   * request type : GET
   * description :status of session
   * url : {{url}}/client/auth/session
   * response : {
                    "status": "SUCCESS",
                    "message": "",
                    "data": {
                        "status": false
                    }
                }

# ADMIN LOGIN

1. login in Admin panel
* api : login in admin panel
   * request type : POST
   * description : login in to admin panel (role 10 define that its Admin)
   * url : {{url}}/admin/auth/login
   * request body : {
    "username": "sam+admin@mybricksfinance.com",
    "password": "Test@1234"
}
   * response : 
            {
            "status": "SUCCESS",
            "message": "Login Successful",
            "data": {
                "loginOTP": null,
                "emailOTP": {
                    "code": "OE8JYQ",
                    "expireTime": "2021-11-11T19:49:29.842Z"
                },
                "phoneNoOTP": {
                    "code": "T4VVIX",
                    "expireTime": "2021-11-09T18:59:03.670Z"
                },
                "resetPasswordLink": {
                    "code": "4B1S2A",
                    "expireTime": "2021-11-11T06:01:52.413Z"
                },
                "isEmailVerified": true,
                "isPhoneNoVerified": true,
                "loginRetryLimit": 0,
                "resendOTPRetryLimit": 0,
                "validateOTPRetryLimit": 0,
                "isCustodialWalletExists": true,
                "custodialWalletAddress": "0x0592066b6443E820658A26e2FF1B5D9e7211abeE",
                "subWalletId": "a7af42ec-3685-44a8-bd97-f3d9513724c1/ETH/22",
                "isRecoveryAddressExists": false,
                "recoveryAddress": null,
                "isKycVerified": true,
                "_id": "61a089b9807d8a32ce73b23b",
                "email": "sam+admin@mybricksfinance.com",
                "firstName": "Ajay",
                "phoneNo": "+917878581382",
                "lastName": "Tarpara",
                "role": 10,
                "createdAt": "2021-11-09T12:53:05.211Z",
                "updatedAt": "2021-11-19T08:31:54.696Z",
                "isDeleted": false,
                "isActive": true,
                "resendOTPReactiveTime": "2021-11-16T06:35:51.514Z",
                "kyc": null,
                "loginReactiveTime": null,
                "id": "61a089b9807d8a32ce73b23b",
                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxYTA4OWI5ODA3ZDhhMzJjZTczYjIzYiIsImVtYWlsIjoiYWRtaW5AZ21haWwuY29tIiwiaWF0IjoxNjM3OTExNDExLCJleHAiOjE2Mzg1MTE0MTF9.9S3KuWE1MmK8_UE443dfftIuhlvdAIE-Cn1yFmB24TI"
            }
        }

# RESET PASSWORD FLOW

1. Forgot password
   * api : forgot password
   * request type : POST
   * description : send email or sms to user for forgot password. currently it set to send threw email only
   * url : {{url}}/client/auth/forgot-password
   * request body : {
      "email": "tarparaajay@gmail.com"
    }
   * response :
		{
			"status": "SUCCESS",
			"message": "otp successfully send to your email.",
			"data": "otp successfully send to your email."
		}

2. validate OTP 
   * api : validate forgot password otp 
   * request type : POST
   * description : after successfully sent mail or sms for forgot password validate otp if Otp is correct or not after putting 3 wrong otp in this API the code will expire and you have to generate new code using **resendOTP** API.
   * url : {{url}}/client/auth/validate-otp
   * request body : 
   		{
    		"username": "tarparaajay@gmail.com",
    		"otp": "1184" 
   		}
   * response :
		{
			"status": "SUCCESS",
			"message": "Otp verified",
			"data": "Otp verified"
		}

3. reset password 
   * api : reset password
   * request type : PUT
   * description : after successfully sent email or sms for forgot password,
   *                validate otp or link and reset password
   * url : {{url}}/client/auth/reset-password
   * request body : 
		{
		"email": "tarparaajay@gmail.com",
		"newPassword": "Test@123"
		}
   * response :
		{
			"status": "SUCCESS",
			"message": "Password reset successfully",
			"data": "Password reset successfully"
		}

# RESEND OTP API WITH & WITHOUT LOGIN 

1. resend OTP with out login
   * api : resend OTP
   * request type : POST
   * description : re-send OTP on  email or sms to user for forgot password. in all 3 formate its maximum 3 time allowed to send otp
   * url : {{url}}/client/auth/resend-otp
   * request body : 
      1. resend otp for verify email 
			{
				"email": "tarparaajay@gmail.com",
				"verifyEmail" : true
			}
	  2. resend otp for verify phone number
	        {
				"phoneNo": "+917878581385",
				"verifyPhoneNo": true
			}
	  3. resend otp for forgot password
	  		{
				"email": "tarparaajay@gmail.com",
				"resetPassword":true
			}
	  4. resend otp for login
	  		{
				"username": "tarparaajay@gmail.com",
				"password": "Test@123",
				"login" : true,
				"email" :true  // for email
				"sms" :true  // for phone send only of email or sms flag
			}
	        
   * response :
      1. resend otp for verify email 
	    {
			"status": "SUCCESS",
			"message": "otp successfully send to your email.",
			"data": "otp successfully send to your email."
		}
	  2. resend otp for verify phone number
	    {
			"status": "SUCCESS",
			"message": "otp successfully send to your mobile number.",
			"data": "otp successfully send to your mobile number."
		}
	  3. resend otp for forgot password
	    {
			"status": "SUCCESS",
			"message": "otp successfully send to your email.",
			"data": "otp successfully send to your email."
		}
	  4. resend otp for login 
	    if use email :true than same as 1. 
	    if use sms :true than same as 2. 

2. resend OTP after login
   * api : resend OTP
   * request type : POST
   * description : re-send OTP on  email or sms. its maximum 3 time allowed to send otp
   * require token for access this API please refer **token Format**
   * url : {{url}}/client/api/v1/user/resend_otp/
   * request body : 
      1. resend otp for verify email 
			{
				"verifyEmail" : true,
                "verifyAltEmail":true // if you want to send otp for alternate email send this both flag
			}
	  2. resend otp for verify phone number
	        {
				"verifyPhoneNo": true,
                "verifyAltPhoneNo":true // if you want to send otp for alternate phone number send this both flag
			}
	        
   * response :
      1. resend otp for verify email 
	    {
			"status": "SUCCESS",
			"message": "otp successfully send to your email.",
			"data": "otp successfully send to your email."
		}
	  2. resend otp for verify phone number
	    {
			"status": "SUCCESS",
			"message": "otp successfully send to your mobile number.",
			"data": "otp successfully send to your mobile number."
		}

3. resend OTP for recovery flow
   * api : resend OTP
   * request type : POST
   * description : re-send OTP on  email or sms. its maximum 3 time allowed to send otp
   * require token for access this API please refer 
   * url : {{url}}/client/auth/recover_resend_otp/
   * request body : 
      1. resend otp for verify email 
			{
				"verifyEmail" : true,
                "email":"alt email of user"
			}
	  2. resend otp for verify phone number
	        {
				"verifyPhoneNo": true,
                "phoneNo":"alt phoneNo of user"
			}
	        
   * response :
      1. resend otp for verify email 
	    {
			"status": "SUCCESS",
			"message": "otp successfully send to your email.",
			"data": "otp successfully send to your email."
		}
	  2. resend otp for verify phone number
	    {
			"status": "SUCCESS",
			"message": "otp successfully send to your mobile number.",
			"data": "otp successfully send to your mobile number."
		}

# RECOVERY CODE FLOW
 1. verify recovery code
	* api : verify recovery code
	* request type : POST
	* description : verify recovery code
	* url : {{url}}/client/auth/recover_account
	* request body : 
    {
        "recoveryAddress": "4jyk106+ec4q4565+$aswau$",
        "email":"tarparaajay@gmail.com"
    }
    * response :
    {
        "status": "SUCCESS",
        "message": {
            "email": "tarparaajay@gmail.com",
            "phoneNo": "+917878581382",
            "recoveryAddress": true, // only allow to next step if this flag is true
            "message": "Recovery Code match successfully !!"
        },
        "data": {
            "email": "tarparaajay@gmail.com",
            "phoneNo": "+917878581382",
            "recoveryAddress": true,
            "message": "Recovery Code match successfully !!"
        }
    }

2. send otp for verify Email

	* api : send email of otp
	* request type : POST
	* description : send otp to user for verify email
	* url : {{url}}/client/auth/recover_send_email_verify_otp
	* request body : 
		1. for send otp on email 
			{
			"email": "tarparaajay@gmail.com"
			}
		2. change email and send otp
			{
			"email": "tarparaajay@gmail.com",
			"newEmail" :"test@gmail.com"
			}
	* response :{
		"status": "SUCCESS",
		"message": "Please check your Email for OTP",
		"data": "Please check your Email for OTP"
		}

3. verify email otp
	* api : verify email
	* request type : POST
	* description : after successfully sent otp to user, user can  verify email, you can enter wrong otp 3 time after that otp will expire and you have to call **resendOtp** API.
	* url : {{url}}/client/auth/recover_verify_email
	* request body : {
		"email": "tarparaajay@gmail.com",
		"code": "3V5M37"
	}
	* response : {
    "status": "SUCCESS",
    "message": "Email verified successfully!",
    "data": "Email verified successfully!"
}

4. send otp for mobile verification
	* api : send sms
	* request type : POST
	* description : send otp to user for verify phone number and add phone number in user record you can call this hapi with one number only one time. you can use this API for change phone number and send otp again.
	* url : {{url}}/client/auth/recover_send_phone_no_verify_otp
	* request body : {
		 "email": "tarparaajay@gmail.com",
    	 "phoneNo": "+917878581382"
		}
	* response :{
    "status": "SUCCESS",
    "message": "Please check your phone for OTP",
    "data": "Please check your phone for OTP"
	}

5. verify mobile otp
	* api : verify phone number
	* request type : POST
	* description : after successfully sent otp to user, user can verify phone number, you can enter otp wrong for 3 time and after that you have to send new otp. using **resend OTP** Api.
	* url : {{url}}/client/auth/recover_verify_phone_no
	* request body : {
		"phoneNo": "+447932935146",
		"code": "7J1D3U"
	}
	* response :{
    "status": "SUCCESS",
    "message": "Phone number verified successfully!",
    "data": "Phone number verified successfully!"
	}
2. verify email
    - use **2. send otp for verify Email from register flow**
    - is email is same as existing one which is send on verify recovery code response than pass it in key "email" else in "newEmail"
3. verify phone number
    - use **4. send otp for mobile verification from register flow**
    - is phone number is same as existing one which is send on verify recovery code response than pass it in key "phoneNo" and "isRecover": true with that else the number is different than only pass "phoneNo"
4. reset account
	* api : reset account
	* request type : POST
	* description : reset account
	* url : {{url}}/client/auth/reset_account
	* request body : 
        {
            "email":"tarparaajay@gmail.com",
            "recoveryAddress":"asdfaljbadkabsj3789ryhuasio"
        }
	* response :
    {
        "status": "SUCCESS",
        "message": {
                        recoveryAddress: true,
                        message: "Reset password permission granted !!" 
                   }
        "data": {
                    recoveryAddress: true,
                    message: "Reset password permission granted !!" 
                }
    }

# TOKEN FORMAT

1. headers: {Authorization: Bearer <token> }

# USER MANAGEMENT
1. get User by id
   * api :get User by id
   * request type : GET
   * description : get User by id
   * url : {{url}}/client/api/v1/user/user_by_id/61a089b9807d8a32ce73b23b
   * require token for access this API please refer **token Format**
   * request body : 
		- pass id of file in query param 
   * response :
        {
            "status": "SUCCESS",
            "message": "Your request is successfully executed",
            "data": { response of user data }          
        }

2. send otp for verify email after login
   * api :send otp for verify email after login
   * request type : GET
   * description : send otp for verify email after login
   * url : {{url}}/client/api/v1/user/send_email_otp
   * require token for access this API please refer **token Format**
   * response :
        {
            "status": "SUCCESS",
            "message": "Please check your Email for OTP",
            "data": "Please check your Email for OTP"
        }

3. verify email after login
   * api :verify email after login
   * request type : POST
   * description : verify email after login
   * url : {{url}}/client/api/v1/user/verify_email
   * require token for access this API please refer **token Format**
   * request body : 
		{
            "code":"IB1DT2"
        }
   * response :
        {
            "status": "SUCCESS",
            "message": "Email verified successfully!",
            "data": "Email verified successfully!"
        }

4. send otp for verify phone no after login
   * api :send otp for verify phone no after login
   * request type : GET
   * description : send otp for verify phone no after login
   * url : {{url}}/client/api/v1/user/send_phone_otp
   * require token for access this API please refer **token Format**
   * response :
       {
            "status": "SUCCESS",
            "message": "Please check your phone for OTP",
            "data": "Please check your phone for OTP"
        }

5. verify phone no after login
   * api :verify phone no after login
   * request type : POST
   * description : verify phone no after login
   * url : {{url}}/client/api/v1/user/verify_phone
   * require token for access this API please refer **token Format**
   * request body : 
		{
            "code":"IB1DT2"
        }
   * response :
        {
            "status": "SUCCESS",
            "message": "Phone number verified successfully!",
            "data": "Phone number verified successfully!"
        }

6. change password after verify email and phone number
   * api :change password after verify email and phone number
   * request type : PUT
   * description : change password after verify email and phone number
   * url : {{url}}/client/api/v1/user/change_password
   * require token for access this API please refer **token Format**
   * request body : 
		{
        "oldPassword": "Test@123",
        "newPassword": "Test@123"
        }
   * response :
        {
            "status": "SUCCESS",
            "message": "Password changed successfully",
            "data": "Password changed successfully"
        }

7. send otp for alt email after verify email and phone number
   * api :send otp for alt email after verify email and phone number
   * request type : POST
   * description : send otp for alt email after verify email and phone number
   * url : {{url}}/client/api/v1/user/send_alt_email_otp/
   * require token for access this API please refer **token Format**
   * request body : 
		{
            "email":"tarparaajay1@gmail.com"
        }
   * response :
        {
            "status": "SUCCESS",
            "message": "Please check your Email for OTP",
            "data": "Please check your Email for OTP"
        }

8. change email after verify alt email otp
   * api :change email after verify alt email otp
   * request type : POST
   * description : change email after verify alt email otp
   * url : {{url}}/client/api/v1/user/change_email/
   * require token for access this API please refer **token Format**
   * request body : 
		{
            "code":"IB1DT2"
        }
   * response :
        {
            "status": "SUCCESS",
            "message": "Email verified successfully!",
            "data": "Email verified successfully!"
        }

9. send otp for alt email after verify email and phone number
   * api :send otp for alt email after verify email and phone number
   * request type : POST
   * description : send otp for alt email after verify email and phone number
   * url : {{url}}/client/api/v1/user/send_alt_phone_otp/
   * require token for access this API please refer **token Format**
   * request body : 
		{
            "phoneNo":"+917878581381"
        }
   * response :
       {
            "status": "SUCCESS",
            "message": "Please check your phone for OTP",
            "data": "Please check your phone for OTP"
        }

10. change phone number after verify alt email otp
   * api :change phone number after verify alt email otp
   * request type : POST
   * description : change phone number after verify alt email otp
   * url : {{url}}/client/api/v1/user/change_phone/
   * require token for access this API please refer **token Format**
   * request body : 
		{
            "code":"IB1DT2"
        }
   * response :
        {
            "status": "SUCCESS",
            "message": "Phone number verified successfully!",
            "data": "Phone number verified successfully!"
        }
11. generate new recovery code 
   * api :generate new recovery code after verify email otp and phone otp
   * request type : GET
   * description : generate new recovery code after verify email otp and phone otp
   * url : {{url}}/client/api/v1/user/change_recovery_code/
   * require token for access this API please refer **token Format**
   * response :
        {
            "status": "SUCCESS",
            "message": {
                "recoveryAddress": "g^m$$#z1@4bwn^8yf0u3uy21",
                "message": "New recovery Code generated successfully !!"
            },
            "data": {
                "recoveryAddress": "g^m$$#z1@4bwn^8yf0u3uy21",
                "message": "New recovery Code generated successfully !!"
            }
        }

12. get user list by filter **admin access**
   * api : get user by filter 
   * request type : POST
   * description :  get user by filter 
   * url : {{url}}/admin/api/v1/user/list
   * require token for access this API please refer **token Format**
   * request body:{
                    "query": {
                        "isKycVerified": true
                    },
                    "options": {
                        "sort": "createdAt",
                        "offset": 0,
                        "page": 1,
                        "limit": 10
                    },
                    "isCountOnly": false
                }
   * response :{
                "status": "SUCCESS",
                "message": "Your request is successfully executed",
                "data": {
                    "docs": [
                        {
                            "isEmailVerified": true,
                            "isPhoneNoVerified": true,
                            "loginRetryLimit": 0,
                            "custodialWalletAddress": "0xdBAddd1760Ff637291af8Dd8d3A9198Fa0550C4B",
                            "isKycVerified": true,
                            "kycStatus": 8,
                            "kycInqId": "inq_VcdF9bBWAoLbgAVez8C5MXey",
                            "_id": "618ba79009d6a000136af6a6",
                            "email": "tarparaajay@gmail.com",
                            "firstName": "Ajay",
                            "phoneNo": "+917878581381",
                            "lastName": "Tarpara",
                            "isDeleted": false,
                            "isActive": true,
                            "dob": "2022-01-18T09:34:21.169Z",
                            "id": "618ba79009d6a000136af6a6"
                        },
                        ...
                        ],
                    "totalDocs": 9,
                    "offset": 0,
                    "limit": 10,
                    "totalPages": 1,
                    "page": 1,
                    "pagingCounter": 1,
                    "hasPrevPage": false,
                    "hasNextPage": false,
                    "prevPage": null,
                    "nextPage": null
                }
            }
# PORTFOLIO

1. get full portfolio 
   * api : get full portfolio
   * request type : GET
   * description : get full and all available portfolio 
   * url : {{url}}/client/api/v1/portfolio/
   * require token for access this API please refer **token Format**
   * request body : 
   * response :
		{
    "status": "SUCCESS",
    "message": "Your request is successfully executed",
    "data": [
        {
            "portfolioImage": [
                {
                    "isActive": true,
                    "_id": "6196332351ea81649e7fb107",
                    "name": "image",
                    "uri": "http://localhost:4444/portfolio/6196332351ea81649e7fb107.jpeg",
                    "mimeType": "image/jpeg",
                    "size": 529645,
                    "slug": "image-6196332351ea81649e7fb107",
                    "destinationPath": "/home/ajay/Downloads/MyBricks-Backend/uploads/portfolio/6196332351ea81649e7fb107.jpeg",
                    "uploadedBy": "618a6f311d99f59b52fb8999",
                    "height": 3024,
                    "width": 4032,
                    "createdAt": "2021-11-18T11:04:03.669Z",
                    "updatedAt": "2021-11-18T11:04:03.669Z",
                    "__v": 0
                }
            ],
            "isLive": true,
            "isDeleted": false,
            "isActive": true,
            "_id": "6196360c3995837b78eaf6c0",
            "portfolioName": "MyBricks Cornwall Portfolio",
            "portfolioMainDescription": "We’ll soon be launching our first portfolio based in Cornwall. A prime coastal location in a rapidly growing market will help generate signficant yearly rental yeilds.",
            "portfolioLaunchDate": "2021-11-16T06:35:51.540Z",
            "availableNFTs": "5,000",
            "NFTCost": "£290",
            "portfolioDetailDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae orci pulvinar tempus. Nulla egestas nulla quam porta sapien, nunc tellus fringilla sed. Eu maecenas nec scelerisque at ornare ultricies. Quis convallis ultricies adipiscing tortor odio quisque.",
            "portfolioDetailLocation": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae orci pulvinar tempus. Nulla egestas nulla quam porta sapien, nunc tellus fringilla sed. Eu maecenas nec scelerisque at ornare ultricies. Quis convallis ultricies adipiscing tortor odio quisque.",
            "portfolioDetailResearch": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae orci pulvinar tempus. Nulla egestas nulla quam porta sapien, nunc tellus fringilla sed. Eu maecenas nec scelerisque at ornare ultricies. Quis convallis ultricies adipiscing tortor odio quisque.",
            "portfolioEstimatedYield": "8.35%",
            "portfolioAssetValue": "£1,450,000",
            "portfolioVideo": {
                "isActive": true,
                "_id": "6196338751ea81649e7fb109",
                "name": "image",
                "uri": "http://localhost:4444/portfolio/6196338751ea81649e7fb109.mkv",
                "mimeType": "video/x-matroska",
                "size": 9717742,
                "slug": "image-6196338751ea81649e7fb109",
                "destinationPath": "/home/ajay/Downloads/MyBricks-Backend/uploads/portfolio/6196338751ea81649e7fb109.mkv",
                "uploadedBy": "618a6f311d99f59b52fb8999",
                "createdAt": "2021-11-18T11:05:43.608Z",
                "updatedAt": "2021-11-18T11:05:43.608Z",
                "__v": 0
            },
            "portfolioPDF": {
                "isActive": true,
                "_id": "619633c551ea81649e7fb10b",
                "name": "pdf",
                "uri": "http://localhost:4444/portfolio/619633c551ea81649e7fb10b.pdf",
                "mimeType": "application/pdf",
                "size": 13796,
                "slug": "pdf-619633c551ea81649e7fb10b",
                "destinationPath": "/home/ajay/Downloads/MyBricks-Backend/uploads/portfolio/619633c551ea81649e7fb10b.pdf",
                "uploadedBy": "618a6f311d99f59b52fb8999",
                "createdAt": "2021-11-18T11:06:45.568Z",
                "updatedAt": "2021-11-18T11:06:45.568Z",
                "__v": 0
            },
            "createdAt": "2021-11-18T11:16:28.792Z",
            "updatedAt": "2021-11-18T11:16:28.792Z",
            "__v": 0,
            "properties": [
                {
                    "propertyImageCollection": [
                        {
                            "isActive": true,
                            "_id": "6196332351ea81649e7fb107",
                            "name": "image",
                            "uri": "http://localhost:4444/portfolio/6196332351ea81649e7fb107.jpeg",
                            "mimeType": "image/jpeg",
                            "size": 529645,
                            "slug": "image-6196332351ea81649e7fb107",
                            "destinationPath": "/home/ajay/Downloads/MyBricks-Backend/uploads/portfolio/6196332351ea81649e7fb107.jpeg",
                            "uploadedBy": "618a6f311d99f59b52fb8999",
                            "height": 3024,
                            "width": 4032,
                            "createdAt": "2021-11-18T11:04:03.669Z",
                            "updatedAt": "2021-11-18T11:04:03.669Z",
                            "__v": 0
                        }
                    ],
                    "isDeleted": false,
                    "isActive": true,
                    "_id": "61964aa504ae138af8881d6b",
                    "portfolioId": "6196360c3995837b78eaf6c0",
                    "propertyName": "Property 1",
                    "propertyDescription": "This converted farmhouse in Fallmouth overlooks the magnificant Cornish beaches from it’s striking position on the tops of the cliffs. Sleeping up to 8, this is expected to provide significant year-round returns.",
                    "propertyPrice": "£450,000",
                    "propertyRenovationCost": "£50,000",
                    "propertyEstimatedIncome": "£47,300",
                    "propertyEstimatedAnnualYield": "9.46%",
                    "propertyMainImage": {
                        "isActive": true,
                        "_id": "6196332351ea81649e7fb107",
                        "name": "image",
                        "uri": "http://localhost:4444/portfolio/6196332351ea81649e7fb107.jpeg",
                        "mimeType": "image/jpeg",
                        "size": 529645,
                        "slug": "image-6196332351ea81649e7fb107",
                        "destinationPath": "/home/ajay/Downloads/MyBricks-Backend/uploads/portfolio/6196332351ea81649e7fb107.jpeg",
                        "uploadedBy": "618a6f311d99f59b52fb8999",
                        "height": 3024,
                        "width": 4032,
                        "createdAt": "2021-11-18T11:04:03.669Z",
                        "updatedAt": "2021-11-18T11:04:03.669Z",
                        "__v": 0
                    },
                    "createdAt": "2021-11-18T12:44:21.075Z",
                    "updatedAt": "2021-11-18T12:44:21.075Z",
                    "__v": 0
                }
            ],
            "perks": [
                {
                    "isDeleted": false,
                    "isActive": true,
                    "_id": "61965210e1eb2591ffb28ccc",
                    "portfolioId": "6196360c3995837b78eaf6c0",
                    "portfolioPerkIcon": {
                        "isActive": true,
                        "_id": "619633c551ea81649e7fb10b",
                        "name": "pdf",
                        "uri": "http://localhost:4444/portfolio/619633c551ea81649e7fb10b.pdf",
                        "mimeType": "application/pdf",
                        "size": 13796,
                        "slug": "pdf-619633c551ea81649e7fb10b",
                        "destinationPath": "/home/ajay/Downloads/MyBricks-Backend/uploads/portfolio/619633c551ea81649e7fb10b.pdf",
                        "uploadedBy": "618a6f311d99f59b52fb8999",
                        "createdAt": "2021-11-18T11:06:45.568Z",
                        "updatedAt": "2021-11-18T11:06:45.568Z",
                        "__v": 0
                    },
                    "portfolioPerkName": "Discounts on accomodation rentals",
                    "portfolioPerkDescription": "Holding at least 50 of the NFTs for this portfolio will entitle you to a 10% discount on all stays at any of the properties in this portfolio.",
                    "createdAt": "2021-11-18T13:16:00.981Z",
                    "updatedAt": "2021-11-18T13:16:00.981Z",
                    "__v": 0
                }
            ]
        }
    ]
}

2. get portfolio id
   * api : get only portfolio
   * request type : GET
   * description : get portfolio based on id
   * url : {{url}}/admin/api/v1/portfolio/6196360c3995837b78eaf6c0
   * request body : 
		- pass id of portfolio in query param 
   * response :
		{
    "status": "SUCCESS",
    "message": "Your request is successfully executed",
    "data": {
        "portfolioImage": [
            {
                "isActive": true,
                "_id": "6196332351ea81649e7fb107",
                "name": "image",
                "uri": "http://localhost:4444/portfolio/6196332351ea81649e7fb107.jpeg",
                "mimeType": "image/jpeg",
                "size": 529645,
                "slug": "image-6196332351ea81649e7fb107",
                "destinationPath": "/home/ajay/Downloads/MyBricks-Backend/uploads/portfolio/6196332351ea81649e7fb107.jpeg",
                "uploadedBy": "618a6f311d99f59b52fb8999",
                "height": 3024,
                "width": 4032,
                "createdAt": "2021-11-18T11:04:03.669Z",
                "updatedAt": "2021-11-18T11:04:03.669Z",
                "__v": 0
            }
        ],
        "isLive": true,
        "isDeleted": false,
        "isActive": true,
        "_id": "6196360c3995837b78eaf6c0",
        "portfolioName": "MyBricks Cornwall Portfolio",
        "portfolioMainDescription": "We’ll soon be launching our first portfolio based in Cornwall. A prime coastal location in a rapidly growing market will help generate signficant yearly rental yeilds.",
        "portfolioLaunchDate": "2021-11-16T06:35:51.540Z",
        "availableNFTs": "5,000",
        "NFTCost": "£290",
        "portfolioDetailDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae orci pulvinar tempus. Nulla egestas nulla quam porta sapien, nunc tellus fringilla sed. Eu maecenas nec scelerisque at ornare ultricies. Quis convallis ultricies adipiscing tortor odio quisque.",
        "portfolioDetailLocation": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae orci pulvinar tempus. Nulla egestas nulla quam porta sapien, nunc tellus fringilla sed. Eu maecenas nec scelerisque at ornare ultricies. Quis convallis ultricies adipiscing tortor odio quisque.",
        "portfolioDetailResearch": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae orci pulvinar tempus. Nulla egestas nulla quam porta sapien, nunc tellus fringilla sed. Eu maecenas nec scelerisque at ornare ultricies. Quis convallis ultricies adipiscing tortor odio quisque.",
        "portfolioEstimatedYield": "8.35%",
        "portfolioAssetValue": "£1,450,000",
        "portfolioVideo": {
            "isActive": true,
            "_id": "6196338751ea81649e7fb109",
            "name": "image",
            "uri": "http://localhost:4444/portfolio/6196338751ea81649e7fb109.mkv",
            "mimeType": "video/x-matroska",
            "size": 9717742,
            "slug": "image-6196338751ea81649e7fb109",
            "destinationPath": "/home/ajay/Downloads/MyBricks-Backend/uploads/portfolio/6196338751ea81649e7fb109.mkv",
            "uploadedBy": "618a6f311d99f59b52fb8999",
            "createdAt": "2021-11-18T11:05:43.608Z",
            "updatedAt": "2021-11-18T11:05:43.608Z",
            "__v": 0
        },
        "portfolioPDF": {
            "isActive": true,
            "_id": "619633c551ea81649e7fb10b",
            "name": "pdf",
            "uri": "http://localhost:4444/portfolio/619633c551ea81649e7fb10b.pdf",
            "mimeType": "application/pdf",
            "size": 13796,
            "slug": "pdf-619633c551ea81649e7fb10b",
            "destinationPath": "/home/ajay/Downloads/MyBricks-Backend/uploads/portfolio/619633c551ea81649e7fb10b.pdf",
            "uploadedBy": "618a6f311d99f59b52fb8999",
            "createdAt": "2021-11-18T11:06:45.568Z",
            "updatedAt": "2021-11-18T11:06:45.568Z",
            "__v": 0
        },
        "createdAt": "2021-11-18T11:16:28.792Z",
        "updatedAt": "2021-11-18T11:16:28.792Z",
        "__v": 0
    }
}


3. create Portfolio **admin access**
   * api : create Portfolio
   * request type : POST
   * description : create portfolio with all description of it
   * url : {{url}}/admin/api/v1/portfolio/create
   * require token for access this API please refer **token Format**
   * request body : 
   {
        "portfolioName": "MyBricks Cornwall Portfolio",
        "portfolioMainDescription": "We’ll soon be launching our first portfolio based in Cornwall. A prime coastal location in a rapidly growing market will help generate signficant yearly rental yeilds.",
        "portfolioLaunchDate": "2021-11-16T06:35:51.540+00:00",
        "availableNFTs": "5,000",
        "NFTCost": "£290",
        "portfolioDetailDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae orci pulvinar tempus. Nulla egestas nulla quam porta sapien, nunc tellus fringilla sed. Eu maecenas nec scelerisque at ornare ultricies. Quis convallis ultricies adipiscing tortor odio quisque.",
        "portfolioDetailLocation": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae orci pulvinar tempus. Nulla egestas nulla quam porta sapien, nunc tellus fringilla sed. Eu maecenas nec scelerisque at ornare ultricies. Quis convallis ultricies adipiscing tortor odio quisque.",
        "portfolioDetailResearch": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae orci pulvinar tempus. Nulla egestas nulla quam porta sapien, nunc tellus fringilla sed. Eu maecenas nec scelerisque at ornare ultricies. Quis convallis ultricies adipiscing tortor odio quisque.",
        "portfolioEstimatedYield": "8.35%",
        "portfolioAssetValue": "£1,450,000",
        "isLive": true,
        "portfolioVideo": <id of video file>,
        "portfolioImage": [ <array of id of image file>],
        "portfolioPDF" : <id of pdf file>,
    }
   * response :
	    {
            "status": "SUCCESS",
            "message": "Your request is successfully executed",
            "data": {
                "portfolioImage": [
                    "6196332351ea81649e7fb107"
                ],
                "isLive": true,
                "isDeleted": false,
                "isActive": true,
                "_id": "61a0c44769d48e72b80b6d05",
                "portfolioName": "MyBricks Cornwall Portfolio",
                "portfolioMainDescription": "We’ll soon be launching our first portfolio based in Cornwall. A prime coastal location in a rapidly growing market will help generate signficant yearly rental yeilds.",
                "portfolioLaunchDate": "2021-11-16T06:35:51.540Z",
                "availableNFTs": "5,000",
                "NFTCost": "£290",
                "portfolioDetailDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae orci pulvinar tempus. Nulla egestas nulla quam porta sapien, nunc tellus fringilla sed. Eu maecenas nec scelerisque at ornare ultricies. Quis convallis ultricies adipiscing tortor odio quisque.",
                "portfolioDetailLocation": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae orci pulvinar tempus. Nulla egestas nulla quam porta sapien, nunc tellus fringilla sed. Eu maecenas nec scelerisque at ornare ultricies. Quis convallis ultricies adipiscing tortor odio quisque.",
                "portfolioDetailResearch": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae orci pulvinar tempus. Nulla egestas nulla quam porta sapien, nunc tellus fringilla sed. Eu maecenas nec scelerisque at ornare ultricies. Quis convallis ultricies adipiscing tortor odio quisque.",
                "portfolioEstimatedYield": "8.35%",
                "portfolioAssetValue": "£1,450,000",
                "portfolioVideo": "6196332351ea81649e7fb107",
                "portfolioPDF": "6196332351ea81649e7fb107",
                "createdAt": "2021-11-26T11:25:59.905Z",
                "updatedAt": "2021-11-26T11:25:59.905Z",
                "__v": 0
            }
        }	

4. update Portfolio **admin access**
   * api : update Portfolio
   * request type : PUT
   * description : update portfolio with all description of it
   * url : {{url}}/admin/api/v1/portfolio/update/:id
   * require token for access this API please refer **token Format**
   * request body : 
   {
       key pair which field need to update
    }
   * response :
	    {
            "status": "SUCCESS",
            "message": "Your request is successfully executed",
            "data": {
                "portfolioImage": [
                    "6196332351ea81649e7fb107"
                ],
                "isLive": true,
                "isDeleted": false,
                "isActive": true,
                "_id": "61a0c44769d48e72b80b6d05",
                "portfolioName": "MyBricks Cornwall Portfolio",
                "portfolioMainDescription": "We’ll soon be launching our first portfolio based in Cornwall. A prime coastal location in a rapidly growing market will help generate signficant yearly rental yeilds.",
                "portfolioLaunchDate": "2021-11-16T06:35:51.540Z",
                "availableNFTs": "5,000",
                "NFTCost": "£290",
                "portfolioDetailDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae orci pulvinar tempus. Nulla egestas nulla quam porta sapien, nunc tellus fringilla sed. Eu maecenas nec scelerisque at ornare ultricies. Quis convallis ultricies adipiscing tortor odio quisque.",
                "portfolioDetailLocation": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae orci pulvinar tempus. Nulla egestas nulla quam porta sapien, nunc tellus fringilla sed. Eu maecenas nec scelerisque at ornare ultricies. Quis convallis ultricies adipiscing tortor odio quisque.",
                "portfolioDetailResearch": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae orci pulvinar tempus. Nulla egestas nulla quam porta sapien, nunc tellus fringilla sed. Eu maecenas nec scelerisque at ornare ultricies. Quis convallis ultricies adipiscing tortor odio quisque.",
                "portfolioEstimatedYield": "8.35%",
                "portfolioAssetValue": "£1,450,000",
                "portfolioVideo": "6196332351ea81649e7fb107",
                "portfolioPDF": "6196332351ea81649e7fb107",
                "createdAt": "2021-11-26T11:25:59.905Z",
                "updatedAt": "2021-11-26T11:25:59.905Z",
                "__v": 0
            }
        }

5. delete Portfolio **admin access**
   * api : delete Portfolio
   * request type : POST
   * description : delete portfolio with all properies and perk too.
   * url : {{url}}/admin/api/v1/portfolio/delete/:id
   * require token for access this API please refer **token Format**
   * request body : {}
   * response :
	    {
            "status": "SUCCESS",
            "message": "Your request is successfully executed",
            "data": "Portfolio deleted Successfully!"
        }

# PROPERTY

1. get Property      **admin access**
   * api : get only Property
   * request type : GET
   * description : get Property based on id
   * url : {{url}}/admin/api/v1/property/6196360c3995837b78eaf6c0
   * require token for access this API please refer **token Format**
   * request body : 
		- pass id of Property in query param 
   * response :
        {
            "status": "SUCCESS",
            "message": "Your request is successfully executed",
            "data": {
                "propertyImageCollection": [
                    {
                        "isActive": true,
                        "_id": "6196332351ea81649e7fb107",
                        "name": "image",
                        "uri": "http://localhost:4444/portfolio/6196332351ea81649e7fb107.jpeg",
                        "mimeType": "image/jpeg",
                        "size": 529645,
                        "slug": "image-6196332351ea81649e7fb107",
                        "destinationPath": "/home/ajay/Downloads/MyBricks-Backend/uploads/portfolio/6196332351ea81649e7fb107.jpeg",
                        "uploadedBy": "618a6f311d99f59b52fb8999",
                        "height": 3024,
                        "width": 4032,
                        "createdAt": "2021-11-18T11:04:03.669Z",
                        "updatedAt": "2021-11-18T11:04:03.669Z",
                        "__v": 0
                    }
                ],
                "isDeleted": false,
                "isActive": true,
                "_id": "61a0cb65239f7f79df54d878",
                "portfolioId": "6196360c3995837b78eaf6c0",
                "propertyName": "Property 2",
                "propertyDescription": "This converted farmhouse in Fallmouth overlooks the magnificant Cornish beaches from it’s striking position on the tops of the cliffs. Sleeping up to 8, this is expected to provide significant year-round returns.",
                "propertyPrice": "£450,000",
                "propertyRenovationCost": "£50,000",
                "propertyEstimatedIncome": "£47,300",
                "propertyEstimatedAnnualYield": "9.46%",
                "propertyMainImage": {
                    "isActive": true,
                    "_id": "6196332351ea81649e7fb107",
                    "name": "image",
                    "uri": "http://localhost:4444/portfolio/6196332351ea81649e7fb107.jpeg",
                    "mimeType": "image/jpeg",
                    "size": 529645,
                    "slug": "image-6196332351ea81649e7fb107",
                    "destinationPath": "/home/ajay/Downloads/MyBricks-Backend/uploads/portfolio/6196332351ea81649e7fb107.jpeg",
                    "uploadedBy": "618a6f311d99f59b52fb8999",
                    "height": 3024,
                    "width": 4032,
                    "createdAt": "2021-11-18T11:04:03.669Z",
                    "updatedAt": "2021-11-18T11:04:03.669Z",
                    "__v": 0
                },
                "createdAt": "2021-11-26T11:56:21.275Z",
                "updatedAt": "2021-11-26T11:56:21.275Z",
                "__v": 0
            }
        }

2. create property	 **admin access**
   * api : create property
   * request type : POST
   * description : create property under the portfolioid
   * require token for access this API please refer **token Format**
   * url : {{url}}/admin/api/v1/property/create
   * request body : 
		{
            "portfolioId":"6196360c3995837b78eaf6c0",
            "propertyName":"Property 2",
            "propertyDescription":"This converted farmhouse in Fallmouth overlooks the magnificant Cornish beaches from it’s striking position on the tops of the cliffs. Sleeping up to 8, this is expected to provide significant year-round returns.",
            "propertyPrice":"£450,000",
            "propertyRenovationCost":"£50,000",
            "propertyEstimatedIncome":"£47,300",
            "propertyEstimatedAnnualYield":"9.46%",
            "propertyMainImage":<id of image file>,
            "propertyImageCollection":[ <array of id of image file> ]
        }
   * response :
        {
            "status": "SUCCESS",
            "message": "Your request is successfully executed",
            "data": {
                "propertyImageCollection": [
                    "6196332351ea81649e7fb107"
                ],
                "isDeleted": false,
                "isActive": true,
                "_id": "61a0cb65239f7f79df54d878",
                "portfolioId": "6196360c3995837b78eaf6c0",
                "propertyName": "Property 2",
                "propertyDescription": "This converted farmhouse in Fallmouth overlooks the magnificant Cornish beaches from it’s striking position on the tops of the cliffs. Sleeping up to 8, this is expected to provide significant year-round returns.",
                "propertyPrice": "£450,000",
                "propertyRenovationCost": "£50,000",
                "propertyEstimatedIncome": "£47,300",
                "propertyEstimatedAnnualYield": "9.46%",
                "propertyMainImage": "6196332351ea81649e7fb107",
                "createdAt": "2021-11-26T11:56:21.275Z",
                "updatedAt": "2021-11-26T11:56:21.275Z",
                "__v": 0
            }
        }

3. update property	 **admin access**
   * api : update Property
   * request type : PUT
   * description : Update Property based on id
   * url : {{url}}/admin/api/v1/property/update/6196360c3995837b78eaf6c0
   * require token for access this API please refer **token Format**
   * request body : 
		- pass  key pair of uwant to update field (same as create)
   * response :
         - (same as create)

4. delete property	 **admin access**
   * api : delete Property
   * request type : POST
   * description : delete Property based on id
   * url : {{url}}/admin/api/v1/property/delete/61a0cb65239f7f79df54d878
   * require token for access this API please refer **token Format**
   * request body : 
		- pass id of Property in query param 
   * response :
            {
                "status": "SUCCESS",
                "message": "Your request is successfully executed",
                "data": "Property deleted successfully!"
            }

# PERK
1. get perk      **admin access**
   * api : get only perk
   * request type : GET
   * description : get perk based on id
   * url : {{url}}/admin/api/v1/perk/6196360c3995837b78eaf6c0
   * require token for access this API please refer **token Format**
   * request body : 
		- pass id of perk in query param 
   * response :
        {
            "status": "SUCCESS",
            "message": "Your request is successfully executed",
            "data": {
                "isDeleted": false,
                "isActive": true,
                "_id": "61965210e1eb2591ffb28ccc",
                "portfolioId": "6196360c3995837b78eaf6c0",
                "portfolioPerkIcon": {
                    "isActive": true,
                    "_id": "619633c551ea81649e7fb10b",
                    "name": "pdf",
                    "uri": "http://localhost:4444/portfolio/619633c551ea81649e7fb10b.pdf",
                    "mimeType": "application/pdf",
                    "size": 13796,
                    "slug": "pdf-619633c551ea81649e7fb10b",
                    "destinationPath": "/home/ajay/Downloads/MyBricks-Backend/uploads/portfolio/619633c551ea81649e7fb10b.pdf",
                    "uploadedBy": "618a6f311d99f59b52fb8999",
                    "createdAt": "2021-11-18T11:06:45.568Z",
                    "updatedAt": "2021-11-18T11:06:45.568Z",
                    "__v": 0
                },
                "portfolioPerkName": "Discounts on accomodation rentals",
                "portfolioPerkDescription": "Holding at least 50 of the NFTs for this portfolio will entitle you to a 10% discount on all stays at any of the properties in this portfolio.",
                "createdAt": "2021-11-18T13:16:00.981Z",
                "updatedAt": "2021-11-18T13:16:00.981Z",
                "__v": 0
            }
        }

2. create perk	 **admin access**
   * api : create perk
   * request type : POST
   * description : create perk under the portfolioid
   * require token for access this API please refer **token Format**
   * url : {{url}}/admin/api/v1/perk/create
   * request body : 
		{
            "portfolioId": "6196360c3995837b78eaf6c0",
            "portfolioPerkIcon": "619633c551ea81649e7fb10b",
            "portfolioPerkName": "Discounts on accomodation rentals",
            "portfolioPerkDescription": "Holding at least 50 of the NFTs for this portfolio will entitle you to a 10% discount on all stays at any of the properties in this portfolio."
        }
   * response :
        {
            "status": "SUCCESS",
            "message": "Your request is successfully executed",
            "data": {
                "isDeleted": false,
                "isActive": true,
                "_id": "61a0ce75a97fb2819836a921",
                "portfolioId": "6196360c3995837b78eaf6c0",
                "portfolioPerkIcon": "619633c551ea81649e7fb10b",
                "portfolioPerkName": "Discounts on accomodation rentals",
                "portfolioPerkDescription": "Holding at least 50 of the NFTs for this portfolio will entitle you to a 10% discount on all stays at any of the properties in this portfolio.",
                "createdAt": "2021-11-26T12:09:25.385Z",
                "updatedAt": "2021-11-26T12:09:25.385Z",
                "__v": 0
            }
        }

3. update perk	 **admin access**
   * api : update perk
   * request type : PUT
   * description : Update perk based on id
   * url : {{url}}/admin/api/v1/perk/update/6196360c3995837b78eaf6c0
   * require token for access this API please refer **token Format**
   * request body : 
		- pass  key pair of uwant to update field (same as create)
   * response :
         - (same as create)

4. delete perk	 **admin access**
   * api : delete perk
   * request type : POST
   * description : delete perk based on id
   * url : {{url}}/admin/api/v1/perk/delete/61a0cb65239f7f79df54d878
   * require token for access this API please refer **token Format**
   * request body : 
		- pass id of perk in query param 
   * response :
            {
                "status": "SUCCESS",
                "message": "Your request is successfully executed",
                "data": "Perk deleted successfully!"
            }
# NFT & LEARNING SECTION

1. get NFT & learnig video
   **refere get file by type API** (type : learning)

# Home Page
 
2. get Video of HomePage.
     **refere get file by type API** (type : homepage)

# FILE

1. get files by type
   * api : get files by type
   * request type : GET
   * description : get file by it type (which is given when uploaded)
   * url : {{url}}/client/api/v1/file/?type=homepage   (**WITHOUT AUTH**)
   * request query : 
		- type= <type of file i.e.homepage>
   * response :
		{
            "status": "SUCCESS",
            "message": "Your request is successfully executed",
            "data": [
                {
                    "isActive": true,
                    "_id": "619e03316a78e14516d3803a",
                    "name": "Video",
                    "uri": "http://localhost:4444/homepage/619e03316a78e14516d3803a.mp4",
                    "mimeType": "video/mp4",
                    "size": 44475475,
                    "slug": "video-619e03316a78e14516d3803a",
                    "destinationPath": "/home/ajay/Downloads/MyBricks-Backend/uploads/homepage/619e03316a78e14516d3803a.mp4",
                    "uploadedBy": "618a6f311d99f59b52fb8999",
                    "type": "homepage",
                    "createdAt": "2021-11-24T09:17:37.354Z",
                    "updatedAt": "2021-11-24T09:17:37.354Z",
                    "__v": 0
                }
            ]
        }
2. get files by type **admin access**
   * api : get files by type
   * request type : GET
   * description : get file by it type (which is given when uploaded)
   * url : {{url}}/admin/api/v1/file/?type=homepage  
   * request query : 
		- type= <type of file i.e.homepage>
   * response :
		{
            "status": "SUCCESS",
            "message": "Your request is successfully executed",
            "data": [
                {
                    "isActive": true,
                    "_id": "619e03316a78e14516d3803a",
                    "name": "Video",
                    "uri": "http://localhost:4444/homepage/619e03316a78e14516d3803a.mp4",
                    "mimeType": "video/mp4",
                    "size": 44475475,
                    "slug": "video-619e03316a78e14516d3803a",
                    "destinationPath": "/home/ajay/Downloads/MyBricks-Backend/uploads/homepage/619e03316a78e14516d3803a.mp4",
                    "uploadedBy": "618a6f311d99f59b52fb8999",
                    "type": "homepage",
                    "createdAt": "2021-11-24T09:17:37.354Z",
                    "updatedAt": "2021-11-24T09:17:37.354Z",
                    "__v": 0
                }
            ]
        }
3. get file      **admin access**
   * api : get only file
   * request type : GET
   * description : get file based on id
   * url : {{url}}/admin/api/v1/file/6196360c3995837b78eaf6c0
   * require token for access this API please refer **token Format**
   * request body : 
		- pass id of file in query param 
   * response :
        {
            "status": "SUCCESS",
            "message": "Your request is successfully executed",
            "data": {
                "isActive": true,
                "_id": "6196332351ea81649e7fb107",
                "name": "image",
                "uri": "http://localhost:4444/portfolio/6196332351ea81649e7fb107.jpeg",
                "mimeType": "image/jpeg",
                "size": 529645,
                "slug": "image-6196332351ea81649e7fb107",
                "destinationPath": "/home/ajay/Downloads/MyBricks-Backend/uploads/portfolio/6196332351ea81649e7fb107.jpeg",
                "uploadedBy": "618a6f311d99f59b52fb8999",
                "height": 3024,
                "width": 4032,
                "createdAt": "2021-11-18T11:04:03.669Z",
                "updatedAt": "2021-11-18T11:04:03.669Z",
                "__v": 0
            }
        }
                "slug": "image-6196332351ea81649e7fb107",
                "destinationPath": "/home/ajay/Downloads/MyBricks-Backend/uploads/portfolio/6196332351ea81649e7fb107.jpeg",
                "uploadedBy": "618a6f311d99f59b52fb8999",
                "height": 3024,
                "width": 4032,
                "createdAt": "2021-11-18T11:04:03.669Z",
                "updatedAt": "2021-11-18T11:04:03.669Z",
                "__v": 0
            }
        }

4. create file	 **admin access**
   * api : create file
   * request type : POST
   * description : create file 
   * require token for access this API please refer **token Format**
   * url : {{url}}/admin/api/v1/file/create
   * request body : 
		- form data:
        {
          files : select file 
          name : <name of file>
          type: "other" (type : "learning" for NFT learning section) (type : homepage for homepage video)
          title: "title if any",
          subTitle: "subTitle if any",
          description: "description if any",
        }
   * response :
        {
            "status": "SUCCESS",
            "message": "Your request is successfully executed",
            "data": [
                {
                    "isActive": true,
                    "_id": "61a0d30f811e968810c6fbf5",
                    "name": "pdf",
                    "uri": "https://devmybricks.s3.eu-west-2.amazonaws.com/61a0d30f811e968810c6fbf5.jpeg",
                    "mimeType": "image/jpeg",
                    "size": 2428603,
                    "slug": "61a0d30f811e968810c6fbf5-jpeg-61a0d30f811e968810c6fbf5",
                    "uploadedBy": "61a089b9807d8a32ce73b23b",
                    "type": null,
                    "link": null,
                    "createdAt": "2021-11-26T12:29:06.193Z",
                    "updatedAt": "2021-11-26T12:29:06.193Z",
                    "__v": 0
                }
            ]
        }

5. update file	 **admin access**
   * api : update file
   * request type : PUT
   * description : Update file based on id
   * url : {{url}}/admin/api/v1/file/update/61a0d30f811e968810c6fbf5
   * require token for access this API please refer **token Format**
   * request body : 
		- pass  key pair of uwant to update field (same as create)
   * response :
         - (same as create)

6. delete file	 **admin access**
   * api : delete file
   * request type : POST
   * description : delete file based on id
   * url : {{url}}/admin/api/v1/file/delete/61a0d30f811e968810c6fbf5
   * require token for access this API please refer **token Format**
   * request body : 
		- pass id of file in query param 
   * response :
            {
                "status": "SUCCESS",
                "message": "Your request is successfully executed",
                "data": "File deleted Successfully!"
            }

# TRASACTION CONTROL

1. change wallet policy      **admin access**
   * api : change wallet policy
   * request type : POST
   * description : change wallet policy after get public key 
   * url : {{url}}/admin/api/v1/trans/change-policy
   * require token for access this API please refer **token Format**
   * request body : 
		{
            "publicKey": [
                "043287efc89a4503feae3a225b16017d77067e1140bb67003b7260a6f994b6bd485e78766b41785878bfcde1b7f31a6fd5ada01338cc2ac8b4622408483b73992c",
                "041321769f9ceadebd7a4e54d7f5cb0933c2ef8cb93013f29afef56e56b22d7bdd0df4e6d760175196370d8b8303cf9ef6c6a764a24f4700e49d6263dd1efce027"
            ]
        }
   * response :
        {
            "status": "SUCCESS",
            "message": {
                "addSignature": {
                    "requestId": "e22ce49c-fbc6-4c4b-ddc0-49e11070eb44"
                }
            },
            "data": {
                "addSignature": {
                    "requestId": "e22ce49c-fbc6-4c4b-ddc0-49e11070eb44"
                }
            }
        }

2. get Public key      **admin access**
   * api : get Public key
   * request type : GET
   * description : get Public key
   * url : {{url}}/admin/api/v1/trans/get_public_key
   * require token for access this API please refer **token Format**
   * response :
        {
            "status": "SUCCESS",
            "message": "043287efc89a4503feae3a225b16017d77067e1140bb67003b7260a6f994b6bd485e78766b41785878bfcde1b7f31a6fd5ada01338cc2ac8b4622408483b73992c",
            "data": "043287efc89a4503feae3a225b16017d77067e1140bb67003b7260a6f994b6bd485e78766b41785878bfcde1b7f31a6fd5ada01338cc2ac8b4622408483b73992c"
        }

3. buy BNB    
   * api :  buy BNB  
   * request type : GET
   * description : buy BNB  from Moon pay
   * url : {{url}}/client/api/v1/trans/buy_bnb
   * require token for access this API please refer **token Format**
   * response :
        {
            "status": "SUCCESS",
            "message": "https://buy-staging.moonpay.com/?apiKey=pk_test_SEFKFX78C3FR3MpGu2OTPHPMis7nk5w&currencyCode=eth&walletAddress=0x0592066b6443E820658A26e2FF1B5D9e7211abeE&email=tarparaajay%40gmail.com&signature=KyjwTicL4oIINjxzYZEY5GHBFy3SEBnMY28u2N7tsvQ%3D",
            "data": "https://buy-staging.moonpay.com/?apiKey=pk_test_SEFKFX78C3FR3MpGu2OTPHPMis7nk5w&currencyCode=eth&walletAddress=0x0592066b6443E820658A26e2FF1B5D9e7211abeE&email=tarparaajay%40gmail.com&signature=KyjwTicL4oIINjxzYZEY5GHBFy3SEBnMY28u2N7tsvQ%3D"
        }

4. bnb transfer calculation  
   * api :  bnb transfer calculation
   * request type : POST
   * description : bnb transfer calculation    
   * url : {{url}}/client/api/v1/trans/bnb_bnb_calculate
   * require token for access this API please refer **token Format**
   * request body :{
                        "to": "0x0592066b6443E820658A26e2FF1B5D9e7211abeE",
                        "value": 1000000000000000
                    }
   * response :
        {
            "status": "SUCCESS",
            "message": {
                "actualValue": "0.001",
                "fees": "0.00003150000021",
                "receiveAmount": 0.00096849999979
            },
            "data": {
                "actualValue": "0.001",
                "fees": "0.00003150000021",
                "receiveAmount": 0.00096849999979
            }
        }

5. bnb transfer 	 
   * api : transfer BNB
   * request type : POST
   * description : transfer BNB
   * require token for access this API please refer **token Format**
   * url : {{url}}/client/api/v1/trans/sign_transaction
   * request body : 
		{
            "to": "0x0592066b6443E820658A26e2FF1B5D9e7211abeE",
            "value": 1000000000000000
        }
   * response :
       {
            "status": "SUCCESS",
            "message": {
                "addSignature": {
                    "requestId": "e22ce49c-fbc6-4c4b-ddc0-49e11070eb44"
                }
            },
            "data": {
                "addSignature": {
                    "requestId": "e22ce49c-fbc6-4c4b-ddc0-49e11070eb44"
                }
            }
        }

6. bricks transfer calculation
   * api :  bricks transfer calculation
   * request type : POST
   * description :bricks transfer calculation
   * url : {{url}}/client/api/v1/trans/bricks_bricks_calculate
   * require token for access this API please refer **token Format**
   * request body :{
                        "BRICKSInput": "1",
                        "to":"0xb538293c3780Bb843763075E4cf5304996F1ebCc"
                    }
   * response :
        {
            "status": "SUCCESS",
            "message": {
                "tokenAmount": "125",
                "fees": "0.00024857550165717",
                "tax": "23.318098259",
                "receiveAmount": "101.681901741"
            },
            "data": {
                "tokenAmount": "125",
                "fees": "0.00024857550165717",
                "tax": "23.318098259",
                "receiveAmount": "101.681901741"
            }
        }
        
7. bricks transfer   
   * api :  bricks transfer 
   * request type : POST
   * description :bricks transfer
   * url : {{url}}/client/api/v1/trans/bricks_to_bricks_transaction
   * require token for access this API please refer **token Format**
   * request body :{
                        "BRICKSInput": "1",
                        "to":"0xb538293c3780Bb843763075E4cf5304996F1ebCc"
                    }
   * response :
        {
            "status": "SUCCESS",
            "message": {
                "addSignature": {
                    "requestId": "2e5bfde1-3af3-58c9-d6fd-165abba6d698"
                }
            },
            "data": {
                "addSignature": {
                    "requestId": "2e5bfde1-3af3-58c9-d6fd-165abba6d698"
                }
            }
        }

8. BNB to bricks calculation  
   * api : BNB to bricks calculation  
   * request type : POST
   * description : BNB to bricks calculation  
   * url : {{url}}/client/api/v1/trans/bnb_bricks_calculate
   * require token for access this API please refer **token Format**
   * request body :{
                    "BNBInput": "0.0001"
                }
   * response :
       {
            "status": "SUCCESS",
            "message": {
                "bricks": 8.872732358,
                "fees": "0.000222639001038982"
            },
            "data": {
                "bricks": 8.872732358,
                "fees": "0.000222639001038982"
            }
        }

9. BNB to bricks conversion transaction 
   * api : BNB to bricks conversion  
   * request type : POST
   * description : BNB to bricks conversion  
   * url : {{url}}/client/api/v1/trans/bnb_bricks_transaction
   * require token for access this API please refer **token Format**
   * request body :{
                    "BNBInput": "0.0001"
                }
   * response :
        {
            "status": "SUCCESS",
            "message": {
                "addSignature": {
                    "requestId": "4fe78bdb-2bf6-80c0-ed7d-6a13dd522832"
                }
            },
            "data": {
                "addSignature": {
                    "requestId": "4fe78bdb-2bf6-80c0-ed7d-6a13dd522832"
                }
            }
        }

10. get transaction detail of specific  transaction using id of that transaction     
   * api :  get transaction detail of specific  transaction  using id of that transaction   
   * request type : GET
   * description : get transaction detail of specific  transaction  using id of that transaction 
   * url : {{url}}/client/api/v1/trans/history_detail/61d56c8123316447a4bff0d8            its id of record  or you can use below
   * url : {{url}}/client/api/v1/trans/history_detail/58ef206e-b5e6-822e-053f-1fa7daf4a29b  its requestId
   * require token for access this API please refer **token Format**
   * response :
        {
            "status": "SUCCESS",
            "message": {
                "fromAddress": "0x5F7Ad62FFFEc6f2CD7CcF7bdE4158E87A122eba2",
                "toAddress": "0x0592066b6443E820658A26e2FF1B5D9e7211abeE",
                "fromValue": "500",
                "toValue": "453.137928962",
                "fromTokenSymbol": "BRICKS",
                "toTokenSymbol": "BRICKS",
                "transactionHash": "0x909eb72d1419a92ea9c0ba5b1838ebd512740a8734a191393ca42efbedb11d51",
                "timestamps": null,
                "gasFees": "0.000098780659583382",
                "status": "CONFIRMED",
                "type": "WITHDRAW",
                "createdAt": "2021-12-30T11:59:08.528Z",
                "updatedAt": "2021-12-30T11:59:08.528Z",
                "requestId": "528bf45a-f30e-08ed-284f-4e0a06cfe185",
                "_id": "61cd9f0c11120ea80024ae93",
                "currentTokenValue": {
                    "binancecoin": {
                        "usd": 528.52,
                        "usd_24h_change": -1.6031203664999267,
                        "eur": 466.88,
                        "eur_24h_change": -1.771252301227429,
                        "gbp": 392.04,
                        "gbp_24h_change": -1.9258592335267184
                    },
                    "mybricks": {
                        "usd": 0.02093749,
                        "usd_24h_change": -6.748560350027609,
                        "eur": 0.01849674,
                        "eur_24h_change": -6.9013664321073644,
                        "gbp": 0.01552938,
                        "gbp_24h_change": -7.063318761453363
                    }
                },
                "user": "61c452eb8f30003d264bebef",
                "__v": 0
            },
            
        }

11. get transaction history     
   * api :  get transaction history of specific user
   * request type : GET
   * description : get transaction history of specific user
   * url : {{url}}/client/api/v1/trans/history
   * require token for access this API please refer **token Format**
   * response :
        {
            "status": "SUCCESS",
            "message": [
                {
                    "fromAddress": "0x5F7Ad62FFFEc6f2CD7CcF7bdE4158E87A122eba2",
                    "toAddress": "0x0592066b6443E820658A26e2FF1B5D9e7211abeE",
                    "fromValue": "0.1940290517554",
                    "toValue": "0.1940290517554",
                    "fromTokenSymbol": "BNB",
                    "toTokenSymbol": "BNB",
                    "transactionHash": "0x1a5634b8d0df9b2f52bb2d8d0b241783f5741ad30b2b8200a81ad9869c3a4af4",
                    "timestamps": null,
                    "gasFees": "0.000043206246489",
                    "status": "CONFIRMED",
                    "type": "WITHDRAW",
                    "createdAt": "2021-12-30T11:11:58.345Z",
                    "updatedAt": "2021-12-30T11:11:58.345Z",
                    "requestId": "2a3f1457-a062-ec72-a51e-6ecaa418fdd7",
                    "_id": "61cd93fe29b7bf9ac4bd1761",
                    "currentTokenValue": {
                        "binancecoin": {
                            "usd": 522.37,
                            "usd_24h_change": -2.975111671943069,
                            "eur": 461.51,
                            "eur_24h_change": -3.3114233808377045,
                            "gbp": 387.42,
                            "gbp_24h_change": -3.4322646285629252
                        },
                        "mybricks": {
                            "usd": 0.0207734,
                            "usd_24h_change": -7.8645951256969795,
                            "eur": 0.01835284,
                            "eur_24h_change": -8.14676364993203,
                            "gbp": 0.01540667,
                            "gbp_24h_change": -8.277169402748262
                        }
                    },
                    "user": "61c452eb8f30003d264bebef",
                    "__v": 0
                },.....
                }
            ]
        }

12. get balance of perticuler user wallet address   
   * api :  get balance of perticuler user wallet address   
   * request type : GET
   * description : get balance of perticuler user wallet address  
   * url : {{url}}/client/api/v1/trans/balance
   * require token for access this API please refer **token Format**
   * response :
        {
            "status": "SUCCESS",
            "message": {
                "BNBbalance": "0.10013484962413338",
                "maxUsedBNBBalance": "0.09663484962413338",
                "BRICKSBalance": 4213.049736501,
                "walletAddress": "0x5F7Ad62FFFEc6f2CD7CcF7bdE4158E87A122eba2",
                "currency": {
                    "updatedAt": "2021-12-31T06:52:01.826Z",
                    "_id": "61b87a43725b726aecc2fa6d",
                    "type": 1,
                    "__v": 0,
                    "setting": {
                        "binancecoin": {
                            "usd": 515.99,
                            "usd_24h_change": -0.7783032286521374,
                            "eur": 456.17,
                            "eur_24h_change": -0.7122659421818325,
                            "gbp": 382.27,
                            "gbp_24h_change": -0.9595796391711006
                        },
                        "mybricks": {
                            "usd": 0.02052338,
                            "usd_24h_change": -0.6449086570032742,
                            "eur": 0.01814398,
                            "eur_24h_change": -0.6071331076360913,
                            "gbp": 0.01520477,
                            "gbp_24h_change": -0.8264287772139467
                        }
                    }
                }
            },
            "data": {
                "BNBbalance": "0.10013484962413338",
                "maxUsedBNBBalance": "0.09663484962413338",
                "BRICKSBalance": 4213.049736501,
                "walletAddress": "0x5F7Ad62FFFEc6f2CD7CcF7bdE4158E87A122eba2",
                "currency": {
                    "updatedAt": "2021-12-31T06:52:01.826Z",
                    "_id": "61b87a43725b726aecc2fa6d",
                    "type": 1,
                    "__v": 0,
                    "setting": {
                        "binancecoin": {
                            "usd": 515.99,
                            "usd_24h_change": -0.7783032286521374,
                            "eur": 456.17,
                            "eur_24h_change": -0.7122659421818325,
                            "gbp": 382.27,
                            "gbp_24h_change": -0.9595796391711006
                        },
                        "mybricks": {
                            "usd": 0.02052338,
                            "usd_24h_change": -0.6449086570032742,
                            "eur": 0.01814398,
                            "eur_24h_change": -0.6071331076360913,
                            "gbp": 0.01520477,
                            "gbp_24h_change": -0.8264287772139467
                        }
                    }
                }
            }
        }
      
13. get chart value   
   * api : get chart value  
   * request type : POST
   * description :get chart value  
   * url : {{url}}/client/api/v1/trans/price_chart
   * request body :{
                "currency":"usd",
                "fromDate":"2021-12-14T05:53:04.563Z", // fromdate and toDate require if specific range is Specified
                "toDate":"2021-12-15T05:53:04.563Z"
            }
   * response :
       {
            "status": "SUCCESS",
            "message": {
                "prices": [
                    [
                        1639461274940,
                        0.011823694469911009
                    ],
                    [
                        1639461708777,
                        0.011806151558310348
                    ],
                    ...
                ],
                "market_caps": [
                    [
                        1639461274940,
                        0
                    ],
                    [
                        1639461708777,
                        0
                    ],
                    ...
                ],
                "total_volumes": [
                    [
                        1639461274940,
                        16606.583783777754
                    ],
                    [
                        1639461708777,
                        16368.379960983479
                    ],
                    ...
                ],
                
            }
        }
    - without fromDate and toDate
       {
    "status": "SUCCESS",
    "message": {
        "updatedAt": "2021-12-15T06:45:09.504Z",
        "_id": "61b982c00803f43969053855",
        "type": 2,
        "__v": 0,
        "createdAt": "2021-12-15T05:53:04.563Z",
        "setting": {
            "1Dusd":{
                "prices": [
                    [
                        1639461274940,
                        0.011823694469911009
                    ],
                    [
                        1639461708777,
                        0.011806151558310348
                    ],
                    ...
                ],
                "market_caps": [
                    [
                        1639461274940,
                        0
                    ],
                    [
                        1639461708777,
                        0
                    ],
                    ...
                ],
                "total_volumes": [
                    [
                        1639461274940,
                        16606.583783777754
                    ],
                    [
                        1639461708777,
                        16368.379960983479
                    ],
                    ...
                ],
            },
            "7Dusd":{ same as above}
            "1Musd":{ same as above}
            "3Musd":{ same as above}
            "ALLusd":{ same as above}
            "1Dgbp":{ same as above}
            "7Dgbp":{ same as above}
            "1Mgbp":{ same as above}
            "3Mgbp":{ same as above}
            "ALLgbp":{ same as above}
            "1Deur":{ same as above}
            "7Deur":{ same as above}
            "1Meur":{ same as above}
            "3Meur":{ same as above}
            "ALLeur":{ same as above}
        },
        data: same as message

14. get status of transaction 
   * api : get status of transaction 
   * request type : POST
   * description :get status of transaction using request ID and using transaction hash you have to redirect user to this URL https://ropsten.etherscan.io/tx/0x65d17b2a2ed8017ecf11112333f03c483004b9e37b80e1d8cef0b31689c4aa0a <- this is transaction hash
   * url : {{url}}/client/api/v1/trans/status_of_request
   * request body :{
                    "requestId":"c9a98c45-8e24-5a5c-025c-1466642661e2"
                }
   * response :{
    "status": "SUCCESS",
    "message": {
                    "requestId": "c9a98c45-8e24-5a5c-025c-1466642661e2",
                    "status": "CONFIRMED",
                    "type": "ETH_TRANSACTION",
                    "transactionHash": "0xafc79a297f3cfaaf4b76c68e78d1fea8403455d3c19cf58ebbee710879922c34"
                },
                "data": {
                    "requestId": "c9a98c45-8e24-5a5c-025c-1466642661e2",
                    "status": "CONFIRMED",
                    "type": "ETH_TRANSACTION",
                    "transactionHash": "0xafc79a297f3cfaaf4b76c68e78d1fea8403455d3c19cf58ebbee710879922c34"
                }
            }

15. Store moonpay transaction using transaction ID
   * api :  Store moonpay transaction using transaction ID
   * request type : GET
   * description : Store moonpay transaction using transaction ID
   * url : {{url}}/client/api/v1/trans/moonpay_transaction/06092506-66e8-41f9-a1f9-75c78843fdd9
   * response :{
    "status": "SUCCESS",
    "message": { same as data },
    "data": {
        "fromAddress": "0x5F7Ad62FFFEc6f2CD7CcF7bdE4158E87A122eba2",
        "toAddress": null,
        "fromValue": "100",
        "toValue": "0.0219",
        "fromTokenSymbol": "USD",
        "toTokenSymbol": "BNB",
        "transactionHash": "0xd6d278c7a838e134d8db54da7bbe107b5b34f972b7390f8bb732de35c14f0224",
        "gasFees": "13.91",
        "status": "completed",
        "type": "PURCHASE",
        "createdAt": "2021-12-31T08:45:07.698Z",
        "updatedAt": "2021-12-31T08:45:07.698Z",
        "requestId": "06092506-66e8-41f9-a1f9-75c78843fdd9",
        "_id": "61cec313114cfdacf72df7aa",
        "currentTokenValue": {
            "binancecoin": {
                "usd": 531.61,
                "usd_24h_change": 1.9317652809906511,
                "eur": 469.81,
                "eur_24h_change": 1.8702106816494126,
                "gbp": 393.37,
                "gbp_24h_change": 1.5287812743916325
            },
            "mybricks": {
                "usd": 0.02091297,
                "usd_24h_change": 0.03979749393108568,
                "eur": 0.01848207,
                "eur_24h_change": -0.02061362663589392,
                "gbp": 0.01547263,
                "gbp_24h_change": -0.323552471850689
            }
        },
        "user": "61c452eb8f30003d264bebef",
        "__v": 0
    }
}

16. checking any pending transaction is exists or not
   * api :  checking any pending transaction is exists or not
   * request type : GET
   * description : checking any pending transaction is exists or not
   * url : {{url}}/client/api/v1/trans/status_of_transaction
   * response :
            {
                "status": "SUCCESS",
                "message": "Your request is successfully executed",
                "data": {
                    "status": false,
                    "accountStatus": true,
                    "reason": "change phone number",
                    "unlockTime": "23 hour, 59 minute and 54 second"
                }
            }

17. get list of NFTs
   * api :  get list of NFTs
   * request type : GET
   * description : get list of NFTs
   * url : {{url}}/client/api/v1/trans/nft
   * require token for access this API please refer **token Format**
   * response :{
                "status": "SUCCESS",
                "message": "Your request is successfully executed",
                "data": {
                    "1": {
                        "name": "Legendary Elephant Plasterer",
                        "description": "This legendary comes with MyBricks Property Portfolio early access, 600 GBP Property Portfolio NFT voucher and automatic entry into the MyBricks Community Collection quarterly draw. If this NFT is drawn, the owner wins $10,000 worth of BRICKS.\n\nFind out more about the full collection and perks at mybricksfinance.com",
                        "image": "https://ipfs.featured.market/ipfs/QmfWe4u5K7nH47tyWqurxkBpLVrr1xVjpqdLc1fMn7aiiV",
                        "external_url": "https://mybricksfinance.com/",
                        "attributes": {
                            "thumbnail_url": "https://ipfs.featured.market/ipfs/QmTR8CvJGRfc7VTjDjgWuyFd5xZkwchM7MxwKJ1D1dfb3v",
                            "content_type": "image/jpeg"
                        }
                    },
                    "2": {
                        "name": "Legendary Dolphin Plumber",
                        "description": "This legendary comes with MyBricks Property Portfolio early access, 600 GBP Property Portfolio NFT voucher and automatic entry into the MyBricks Community Collection quarterly draw. If this NFT is drawn, the owner wins $10,000 worth of BRICKS.\n\nFind out more about the full collection and perks at mybricksfinance.com",
                        "image": "https://ipfs.featured.market/ipfs/QmZyNRaqTmPXi3vDAfMgWjKee4eYuc8tyUsiH23otUPmNA",
                        "external_url": "https://mybricksfinance.com/",
                        "attributes": {
                            "thumbnail_url": "https://ipfs.featured.market/ipfs/QmdimHJVdbjRdy3BFsRcgqjwrECL7b4BZshjSwkW2xschy",
                            "content_type": "image/jpeg"
                        }
                    }
                }
            }

18. get last buy price NFT
   * api :  get last buy price NFT
   * request type : GET
   * description : get last buy price NFT
   * url : {{url}}/client/api/v1/trans/nft_prices/2
   * require token for access this API please refer **token Format**
   * response :{
    "status": "SUCCESS",
    "message": "Your request is successfully executed",
    "data": {
        "value": "0.1"
    }
}

19. NFT to NFT calculation 
   * api : NFT to NFT calculation
   * request type : POST
   * description : NFT to NFT calculation
   * url : {{url}}/client/api/v1/trans/nft_calculate
   * require token for access this API please refer **token Format**
   * request body :{
                        "to": "0x6169D703D5FCC13d93B46084D82cf6c6b39A4C1E",
                        "value": 2
                    }
   * response :{
                "status": "SUCCESS",
                "message": "Your request is successfully executed",
                "data": {
                    fees: 0.00030
                }
            }

19. NFT to NFT transaction 
   * api : NFT to NFT transaction
   * request type : POST
   * description : NFT to NFT transaction
   * url : {{url}}/client/api/v1/trans/nft_transaction
   * require token for access this API please refer **token Format**
   * request body :{
                        "to": "0x6169D703D5FCC13d93B46084D82cf6c6b39A4C1E",
                        "value": 2
                    }
   * response :{
                "status": "SUCCESS",
                "message": "Your request is successfully executed",
                "data": {
                    "requestId": "ea954390-dd41-99b1-71a5-563550024ace",
                    "status": "CONFIRMED",
                    "type": "ETH_TRANSACTION",
                    "transactionHash": "0x8c6bd185bcd6f4a1dbaeb02eb49e7e9a8437670a55e8028c03355d55925b23f9"
                }
            }
# PROJECT SETTING

1. get setting by type
   * api : get setting by type
   * request type : GET
   * description : get setting by type
   * url : {{url}}/client/api/v1/setting/?type=1   (**WITHOUT AUTH**)
   * request query : 
		- type= <type of setting  1, 2 etc>
   * response :
		- as per setting added

# NOTIFICATION

1. subscribe email for launch notification
   * api : subscribe email for launch notification
   * request type : POST
   * description :subscribe email for launch notification
   * url : {{url}}/client/api/v1/notification/subscribe  (**WITHOUT AUTH**)
   * request query : 
		{
            "email":"tarparaajay@gmail.com"
        }
   * response :
		{
            "status": "SUCCESS",
            "message": "Your request is successfully executed",
            "data": {
                "createdAt": "2021-12-16T09:04:15.002Z",
                "_id": "61bb010e47f3bb13b8c0511e",
                "email": "tarparaajay1@gmail.com",
                "updatedAt": "2021-12-16T09:04:15.002Z",
                "__v": 0
            }
        }

2. send email for launch notification  **admin access**
   * api : send email for launch notification
   * request type : POST
   * description :send email for launch notification
   * url : {{url}}/admin/api/v1/notification/launch_notification  
   * require token for access this API please refer **token Format**
   * request body : {
                        "message":"write message here"
                    }
   * response :
		{
            "status": "SUCCESS",
            "message": "Your request is successfully executed",
            "data": true
        }



# ACTIVITY MANAGMRNT

1. get Activity by filter **admin access**
   * api :  get Activity by filter
   * request type : POST
   * description : get Activity by filter response as per filter provided
   * url : {{{url}}/admin/api/v1/activity/get 
   * require token for access this API please refer **token Format**
   * request query : 
		{
            "query": {
                "user": "61a4b7b811983000135118c9",
                "type":["LOGIN"]
            },
            "options": {
                "sort": "createdAt",
                "offset": 0, // means how many record you want to skip formula = (page-1)*limit
                "page": 1, // page number
                "limit": 10 // record per page
            },
            "isCountOnly": false
        }
   * response :
		{
            "status": "SUCCESS",
            "message": "Your request is successfully executed",
            "data": {
                "docs": [
                    {
                        "name": "login",
                        "ip": "::ffff:192.168.2.222",
                        "type": "LOGIN",
                        "createdAt": "2022-01-13T13:41:01.320Z",
                        "updatedAt": "2022-01-13T13:41:01.320Z",
                        "_id": "61e02bedcacf340013963b11",
                        "user": "61a4b7b811983000135118c9",
                        "data": {
                            "username": "arindam@techalchemy.co",
                            "password": "********",
                            "code": "R63JP6"
                        },
                        "__v": 0
                    },
                    ....
                ],
                "totalDocs": 103,
                "offset": 0,
                "limit": 10,
                "totalPages": 11,
                "page": 1,
                "pagingCounter": 1,
                "hasPrevPage": false,
                "hasNextPage": true,
                "prevPage": null,
                "nextPage": 2
            }
        }
     *  "isCountOnly": true
     *response :{
                    "status": "SUCCESS",
                    "message": "Your request is successfully executed",
                    "data": {
                        "totalRecords": 103
                    }
                }

# LOGOUT

1. logout
   * api :  logout
   * request type : POST
   * description : logout
   * url : {{url}}/client/auth/logout
   * request body : 
		{
            "token": "referesh token"
        }
   * response :{
                    "status": "SUCCESS",
                    "message": "Logged Out Successfully",
                    "data": "Logged Out Successfully"
                }

# STRIPE

1. create customer      **client access**
    * api :  create customer
    * request type : GET
    * description : create customer
    * url : {{url}}/client/api/v1/stripe/create_customer/620bd732185be72db2d1e002
    * response :
        {
            "status": "SUCCESS",
            "message": "Your request is successfully executed",
            "data": {
                "id": "cus_LAR0SvnEWaYc5c",
                "object": "customer",
                "address": null,
                "balance": 0,
                "created": 1645089532,
                "currency": null,
                "default_source": null,
                "delinquent": false,
                "description": null,
                "discount": null,
                "email": "aash@mailinator.com",
                "invoice_prefix": "D910EEB7",
                "invoice_settings": {
                    "custom_fields": null,
                    "default_payment_method": null,
                    "footer": null
                },
                "livemode": false,
                "metadata": {},
                "name": "Aash Kothari",
                "next_invoice_sequence": 1,
                "phone": null,
                "preferred_locales": [],
                "shipping": null,
                "tax_exempt": "none"
            }
        }

2. create customer card     **client access**
    * api :  create customer card
    * request type : POST
    * description : create customer card
    * url : {{url}}/client/api/v1/stripe/create_customer_card/620bd732185be72db2d1e002
    * request body : 
		{
            "cardNo": "4242424242424242",
            "expMonth": 2,
            "expYear": 2023,
            "cvc": "314"
        }
    * response :
        {
            "status": "SUCCESS",
            "message": "Your request is successfully executed",
            "data": {
                "id": "card_1KU6Y5SCdXuDFJaCC92UcQfl",
                "object": "card",
                "address_city": null,
                "address_country": null,
                "address_line1": null,
                "address_line1_check": null,
                "address_line2": null,
                "address_state": null,
                "address_zip": null,
                "address_zip_check": null,
                "brand": "Visa",
                "country": "US",
                "customer": "cus_LAR0SvnEWaYc5c",
                "cvc_check": "pass",
                "dynamic_last4": null,
                "exp_month": 2,
                "exp_year": 2023,
                "fingerprint": "rKkJXnfHs28RTOAw",
                "funding": "credit",
                "last4": "4242",
                "metadata": {},
                "name": null,
                "tokenization_method": null
            }
        }

3. get customer cards      **client access**
    * api :  get customer cards 
    * request type : GET
    * description : get customer cards 
    * url : {{url}}/client/api/v1/stripe/customer_cards/cus_LAnrGA7wjQWGO8
    * response :
        {
            "status": "SUCCESS",
            "message": "Your request is successfully executed",
            "data": {
                "id": "cus_LAR0SvnEWaYc5c",
                "object": "customer",
                "address": null,
                "balance": 0,
                "created": 1645089532,
                "currency": null,
                "default_source": null,
                "delinquent": false,
                "description": null,
                "discount": null,
                "email": "aash@mailinator.com",
                "invoice_prefix": "D910EEB7",
                "invoice_settings": {
                    "custom_fields": null,
                    "default_payment_method": null,
                    "footer": null
                },
                "livemode": false,
                "metadata": {},
                "name": "Aash Kothari",
                "next_invoice_sequence": 1,
                "phone": null,
                "preferred_locales": [],
                "shipping": null,
                "tax_exempt": "none"
            }
        }

4. delete customer card      **client access**
    * api :  delete customer card
    * request type : GET
    * description : delete customer card
    * url : {{url}}/client/api/v1/stripe/delete_customer_card/cus_LAnrGA7wjQWGO8/card_1KUSMbSCdXuDFJaCl3QEsKSP
    * response :
        {
            "status": "SUCCESS",
            "message": "Your request is successfully executed",
            "data": {}
        }

5. make stripe payment      **client access**
    * api :  make stripe payment
    * request type : GET
    * description : make stripe payment
    * url : {{url}}/client/api/v1/stripe/create_customer/620bd732185be72db2d1e002
    * request body : 
		{
            "cardId": "card_1KUAmrHX7HqXNuuekCbLQL4N",
            "amount": 20,
            "description": "bricks",
            "bricks":100
        }
    * response :
        {
    "status": "SUCCESS",
    "message": "Your request is successfully executed",
    "data": {
        "stripe": {
            "id": "pi_3KWJKWHX7HqXNuue0GCl0UTB",
            "object": "payment_intent",
            "amount": 2000,
            "amount_capturable": 0,
            "amount_received": 2000,
            "application": null,
            "application_fee_amount": null,
            "automatic_payment_methods": null,
            "canceled_at": null,
            "cancellation_reason": null,
            "capture_method": "automatic",
            "charges": {
                "object": "list",
                "data": [
                    {
                        "id": "ch_3KWJKWHX7HqXNuue08x7p3mN",
                        "object": "charge",
                        "amount": 2000,
                        "amount_captured": 2000,
                        "amount_refunded": 0,
                        "application": null,
                        "application_fee": null,
                        "application_fee_amount": null,
                        "balance_transaction": "txn_3KWJKWHX7HqXNuue0vQMUES5",
                        "billing_details": {
                            "address": {
                                "city": null,
                                "country": null,
                                "line1": null,
                                "line2": null,
                                "postal_code": null,
                                "state": null
                            },
                            "email": null,
                            "name": null,
                            "phone": null
                        },
                        "calculated_statement_descriptor": "MYBRICKSFINANCE.COM",
                        "captured": true,
                        "created": 1645616905,
                        "currency": "gbp",
                        "customer": "cus_LCfhcXxiXjvCml",
                        "description": "bricks",
                        "destination": null,
                        "dispute": null,
                        "disputed": false,
                        "failure_code": null,
                        "failure_message": null,
                        "fraud_details": {},
                        "invoice": null,
                        "livemode": false,
                        "metadata": {},
                        "on_behalf_of": null,
                        "order": null,
                        "outcome": {
                            "network_status": "approved_by_network",
                            "reason": null,
                            "risk_level": "normal",
                            "risk_score": 30,
                            "seller_message": "Payment complete.",
                            "type": "authorized"
                        },
                        "paid": true,
                        "payment_intent": "pi_3KWJKWHX7HqXNuue0GCl0UTB",
                        "payment_method": "card_1KWGOrHX7HqXNuuerud7TrGr",
                        "payment_method_details": {
                            "card": {
                                "brand": "visa",
                                "checks": {
                                    "address_line1_check": null,
                                    "address_postal_code_check": null,
                                    "cvc_check": null
                                },
                                "country": "US",
                                "exp_month": 2,
                                "exp_year": 2023,
                                "fingerprint": "WcUgJqNTjiK7VHT3",
                                "funding": "credit",
                                "installments": null,
                                "last4": "4242",
                                "network": "visa",
                                "three_d_secure": null,
                                "wallet": null
                            },
                            "type": "card"
                        },
                        "receipt_email": null,
                        "receipt_number": null,
                        "receipt_url": "https://pay.stripe.com/receipts/acct_1JPMexHX7HqXNuue/ch_3KWJKWHX7HqXNuue08x7p3mN/rcpt_LCimfiJDLFRd3ow1nwpW9MdnFCPqkqM",
                        "refunded": false,
                        "refunds": {
                            "object": "list",
                            "data": [],
                            "has_more": false,
                            "total_count": 0,
                            "url": "/v1/charges/ch_3KWJKWHX7HqXNuue08x7p3mN/refunds"
                        },
                        "review": null,
                        "shipping": null,
                        "source": null,
                        "source_transfer": null,
                        "statement_descriptor": null,
                        "statement_descriptor_suffix": null,
                        "status": "succeeded",
                        "transfer_data": null,
                        "transfer_group": null
                    }
                ],
                "has_more": false,
                "total_count": 1,
                "url": "/v1/charges?payment_intent=pi_3KWJKWHX7HqXNuue0GCl0UTB"
            },
            "client_secret": "pi_3KWJKWHX7HqXNuue0GCl0UTB_secret_smM8JXHSrxFkH34oi9fFq5xg9",
            "confirmation_method": "automatic",
            "created": 1645616904,
            "currency": "gbp",
            "customer": "cus_LCfhcXxiXjvCml",
            "description": "bricks",
            "invoice": null,
            "last_payment_error": null,
            "livemode": false,
            "metadata": {},
            "next_action": null,
            "on_behalf_of": null,
            "payment_method": "card_1KWGOrHX7HqXNuuerud7TrGr",
            "payment_method_options": {
                "card": {
                    "installments": null,
                    "network": null,
                    "request_three_d_secure": "automatic"
                }
            },
            "payment_method_types": [
                "card"
            ],
            "processing": null,
            "receipt_email": null,
            "review": null,
            "setup_future_usage": null,
            "shipping": null,
            "source": null,
            "statement_descriptor": null,
            "statement_descriptor_suffix": null,
            "status": "succeeded",
            "transfer_data": null,
            "transfer_group": null
        },
        "bricks": {
            "flag": false,
            "data": {
                "requestId": "455235d8-bb23-1141-2017-74ba8bd1cba4",
                "status": "CONFIRMED",
                "type": "ETH_TRANSACTION",
                "transactionHash": "0x0dd6c2165856dff34a8f7617c751c252501a0344f4d41fe3f8329b647f532e8e"
            }
        },
        "dbTransaction": {
            "flag": false,
            "data": {
                "fromAddress": "0xF8C2cfDB29Af6F69ccc7314Fd9eF3037485edF42",
                "toAddress": "0x5C4D6ECAABc25205358062B6C45b44Ef5E53cD09",
                "fromValue": "100",
                "toValue": "100",
                "fromTokenSymbol": "BRICKS",
                "toTokenSymbol": "BRICKS",
                "transactionHash": "0x0dd6c2165856dff34a8f7617c751c252501a0344f4d41fe3f8329b647f532e8e",
                "timestamps": 1645616964,
                "gasFees": "0.000083652683476536",
                "status": "CONFIRMED",
                "type": "PURCHASE",
                "createdAt": "2022-02-23T11:49:25.385Z",
                "updatedAt": "2022-02-23T11:49:25.385Z",
                "requestId": "455235d8-bb23-1141-2017-74ba8bd1cba4",
                "_id": "62161f45851995fdd4fb75b2",
                "currentTokenValue": {
                    "binancecoin": {
                        "usd": 381.71,
                        "usd_24h_change": 4.496296792404827,
                        "eur": 336.58,
                        "eur_24h_change": 4.519410695579097,
                        "gbp": 280.8,
                        "gbp_24h_change": 4.291860491359991
                    },
                    "mybricks": {
                        "usd": 0.01075482,
                        "usd_24h_change": 1.399778752769252,
                        "eur": 0.00948099,
                        "eur_24h_change": 1.4864654996515876,
                        "gbp": 0.00790608,
                        "gbp_24h_change": 1.1618463982931715
                    }
                },
                "user": "6215e0a2df43384aaf644682",
                "__v": 0
            }
        }
    }
}

6. Get 1 Brick Latest Price      **client access**
    * api :  Latest Brick Price
    * request type : GET
    * description : Get Latest Brick Price from local DB
    * url : {{url}}/client/api/v1/stripe/latest_bricks_price
    * response :
        {
            "status": "SUCCESS",
            "message": "Your request is successfully executed",
            "data": {
                "usd": 0.00919517,
                "eur": 0.00818025,
                "gbp": 0.00680949
            }
        }


7. Make PI and Confirm      **client access**
    * api :  Create Payment Intent via UserID and Confirm it
    * request type : POST
    * description : This API is in replacement of the #5
    * url : {{mybricks-url}}/client/api/v1/stripe/pay
    * response :
      {
		"status": "SUCCESS",
		"message": "Your request is successfully executed",
		"data": {
			"requires_action": true,
			"payment_intent_client_secret": "pi_3KY6zOHX7HqXNuue1VFjP8ZO_secret_fZUImVF1FSIjkw80QCoVlrTpA"
		}
	}
# Referral

1. referral_data
    * api : referral_data
    * request type : GET
    * url: {{url}}/client/api/v1/user/referral_data
    * description : Gets the data of user from GrowSurf
    * reqyest body : Just needs the token (user logged in)
    * response : {
    "status": "SUCCESS",
    "message": "Your request is successfully executed",
    "data": {
        "data": {
            "id": "nqbs6p",
            "firstName": "Rajnish",
            "lastName": "kaushi",
            "referralCount": 0,
            "inviteCount": 0,
            "monthlyReferralCount": 0,
            "prevMonthlyReferralCount": 0,
            "rank": 12,
            "monthlyRank": 12,
            "shareUrl": "https://devdevbrickbrick.com/home?grsf=nqbs6p",
            "rewards": [],
            "email": "rajnish+referred@techalchemy.co",
            "createdAt": 1644992962756,
            "referralSource": "PARTICIPANT",
            "referralStatus": "CREDIT_AWARDED",
            "fraudRiskLevel": "LOW",
            "fraudReasonCode": "UNIQUE_IDENTITY",
            "isWinner": false,
            "shareCount": {
                "email": 0,
                "facebook": 0,
                "twitter": 0,
                "linkedin": 0,
                "pinterest": 0,
                "sms": 0,
                "messenger": 0,
                "whatsapp": 0,
                "wechat": 0
            },
            "impressionCount": 11,
            "uniqueImpressionCount": 1,
            "referrals": [],
            "monthlyReferrals": [],
            "metadata": {},
            "referrer": {
                "id": "0oa7qc",
                "firstName": "rajnish",
                "lastName": "kaushi",
                "referralCount": 1,
                "inviteCount": 0,
                "monthlyReferralCount": 1,
                "prevMonthlyReferralCount": 0,
                "rank": 5,
                "monthlyRank": 5,
                "shareUrl": "https://devdevbrickbrick.com/home?grsf=0oa7qc",
                "rewards": [
                    {
                        "id": "s6xru1",
                        "rewardId": "ah119p",
                        "status": "PENDING",
                        "unread": true,
                        "isReferrer": true,
                        "isFulfilled": false,
                        "isAvailable": false,
                        "approved": false,
                        "referredId": "nqbs6p",
                        "referrerId": "0oa7qc"
                    }
                ],
                "email": "rajnish+reff@techalchemy.co",
                "createdAt": 1644992682493,
                "referralSource": "DIRECT",
                "referralStatus": null,
                "fraudRiskLevel": "LOW",
                "fraudReasonCode": "UNIQUE_IDENTITY",
                "isWinner": false,
                "shareCount": {
                    "email": 0,
                    "facebook": 0,
                    "twitter": 0,
                    "linkedin": 0,
                    "pinterest": 0,
                    "sms": 0,
                    "messenger": 0,
                    "whatsapp": 0,
                    "wechat": 0
                },
                "impressionCount": 50,
                "uniqueImpressionCount": 8,
                "referrals": [
                    "nqbs6p"
                ],
                "monthlyReferrals": [
                    "nqbs6p"
                ],
                "metadata": {},
                "referrer": null
                    }
                }
            }
        }