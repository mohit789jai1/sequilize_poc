FROM public.ecr.aws/docker/library/node:14.16-alpine
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 4444
CMD ["npm", "start"]
