const Portfolio = require('../../model/portfolio');
const Property = require('../../model/property');
const Perk = require('../../model/perk');
const File = require('../../model/file');
const portfolioService = require('../../services/portfolio');
const perkService = require('../../services/perk');
const propertyService = require('../../services/property');
const dbService = require('../../utils/dbService');
const _ = require('lodash');
const { USER_ROLE } = require('../../constants/authConstant');
module.exports = {
    getFullPortfolio: async (req, res) => {
        try {

            let results = await portfolioService.find({
                isLive: true,
                isActive: true,
                isDeleted: false
            }, ['portfolioVideo', 'portfolioPDF', 'portfolioImage', 'portfolioVideoThumbnail']);
            if (results) {
                for (let index = 0; index < results.length; index++) {
                    results[index] = results[index].toJSON();
                    results[index].properties = await propertyService.find({ portfolioId: results[index]._id }, ['propertyMainImage', 'propertyImageCollection']);
                    results[index].perks = await perkService.find({ portfolioId: results[index]._id }, ['portfolioPerkIcon']);
                }
                return res.ok(results);
            }

            return res.badRequest({});
        }
        catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },
    getFullPortfolioAdmin: async (req, res) => {
        try {
            if (req.user && req.user.role != USER_ROLE.Admin) {
                return res.unAuthorizedRequest();
            }
            let results = await portfolioService.find({
                isActive: true,
                isDeleted: false
            }, ['portfolioVideo', 'portfolioPDF', 'portfolioImage', 'portfolioVideoThumbnail']);
            if (results) {
                for (let index = 0; index < results.length; index++) {
                    results[index] = results[index].toJSON();
                    results[index].properties = await propertyService.find({ portfolioId: results[index]._id }, ['propertyMainImage', 'propertyImageCollection']);
                    results[index].perks = await perkService.find({ portfolioId: results[index]._id }, ['portfolioPerkIcon']);
                }
                return res.ok(results);
            }
            return res.badRequest({});
        }
        catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },
    getPortfolio: async (req, res) => {
        try {
            if (req.user && req.user.role != USER_ROLE.Admin) {
                return res.unAuthorizedRequest();
            }
            if (req.params.id) {
                let result = await portfolioService.findById(req.params.id, ['portfolioVideo', 'portfolioPDF', 'portfolioImage', 'portfolioVideoThumbnail']);
                if (result) {
                    result = result.toJSON();
                    result.properties = await propertyService.find({ portfolioId: result._id }, ['propertyMainImage', 'propertyImageCollection']);
                    result.perks = await perkService.find({ portfolioId: result._id }, ['portfolioPerkIcon']);
                    return res.ok(result);
                }
            }
            return res.badRequest({});
        }
        catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },

    createPortfolio: async (req, res) => {
        try {
            if (req.user && req.user.role != USER_ROLE.Admin) {
                return res.unAuthorizedRequest();
            }
            let data = req.body;
            const result = await dbService.createDocument(Portfolio, data);
            if (!result) {
                return res.badRequest({});
            }
            let newResult = await portfolioService.findById(result.id, ['portfolioVideo', 'portfolioPDF', 'portfolioImage', 'portfolioVideoThumbnail']);
            return res.ok(newResult);
        }
        catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },

    updatePortfolio: async (req, res) => {
        try {
            if (req.user && req.user.role != USER_ROLE.Admin) {
                return res.unAuthorizedRequest();
            }
            delete req.body['addedBy'];
            delete req.body['updatedBy'];
            if (!req.params.id) {
                return res.insufficientParameters();
            }
            let data = {
                ...req.body,
                id: req.params.id,
                updatedBy: req.user.id
            };
            let query = { _id: req.params.id };
            let result = await dbService.findOneAndUpdateDocument(Portfolio, query, data, { new: false });
            if (!result) {
                return res.recordNotFound({});
            }
            let newResult = await portfolioService.findById(req.params.id, ['portfolioVideo', 'portfolioPDF', 'portfolioImage', 'portfolioVideoThumbnail']);
            return res.ok(newResult);
        }
        catch (error) {
            return res.failureResponse(error.message);
        }
    },

    deletePortfolio: async (req, res) => {
        try {
            if (req.user && req.user.role != USER_ROLE.Admin) {
                return res.unAuthorizedRequest();
            }
            if (!req.params.id) {
                return res.insufficientParameters();
            }
            let query = {};
            query._id = req.params.id;
            if (!_.isEmpty(query)) {
                let result = await dbService.findOneAndUpdateDocument(Portfolio, query, {
                    isActive: false,
                    isDeleted: true
                });
                if (result) {
                    await dbService.bulkUpdate(Property, { portfolioId: req.params.id }, {
                        isActive: false,
                        isDeleted: true
                    });
                    await dbService.bulkUpdate(Perk, { portfolioId: req.params.id }, {
                        isActive: false,
                        isDeleted: true
                    });
                    return res.ok('Portfolio deleted Successfully!');
                }
            }
            return res.recordNotFound({});
        }
        catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },
};
