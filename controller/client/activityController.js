const activityService = require('../../services/activity');
const _ = require('lodash');
const dbService = require('../../utils/dbService');
const Activity = require('../../model/activity');

module.exports = {
    getActivityByFilter: async (req, res) => {
        try {
            let options = {};
            let query = {};
            let result;
            if (req.body.query !== undefined) {
                query = { ...req.body.query };
            }
            if (req.body.isCountOnly) {
                result = await dbService.countDocument(Activity, query);
                if (result) {
                    result = { totalRecords: result };
                    return res.ok(result);
                }
                return res.recordNotFound({});
            }
            else {
                if (req.body.options !== undefined) {
                    options = { ...req.body.options };
                }
                options.customLabels = {};
                options.pagination = true;
                result = await dbService.getAllDocuments(Activity, query, options);
                if (result && result.docs && result.docs.length) {
                    return res.ok(result);
                }
                return res.recordNotFound({});
            }
        }
        catch (error) {
            return res.failureResponse(error.message);
        }
    },

};
