const NFTService = require('../../services/NFTTransaction');
const dbService = require('../../utils/dbService');
const User = require('../../model/user');
const activityService = require('../../services/activity');

module.exports = {

    getNFTs: async (req, res) => {
        try {
            let loginUser = req.user;
            if (!loginUser) {
                return res.unAuthorizedRequest();
            }
            if (!loginUser.isCustodialWalletExists) {
                return res.invalidRequest('Custodial wallet address not exists.');
            }
            let transaction = await NFTService.getNFTByWalletAddress(loginUser.custodialWalletAddress);
            if (!transaction.flag) {
                return res.requestValidated(transaction.data);
            }
            return res.successMessage('You don\'t own any NFT', transaction.data);
        } catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },

    getNFTPrices: async (req, res) => {
        const params = req.params;
        let loginUser = req.user;
        try {
            if (!loginUser) {
                return res.unAuthorizedRequest();
            }
            if (!params.tokenId) {
                return res.insufficientParameters();
            }
            if (!loginUser.isCustodialWalletExists) {
                return res.invalidRequest('Custodial wallet address not exists.');
            }
            let transaction = await NFTService.getNFTPrice(params.tokenId);
            if (!transaction.flag) {
                return res.requestValidated(transaction.data);
            }
            return res.invalidRequest(transaction.data);
        } catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },

    NFTCalculation: async (req, res) => {
        const params = req.body;
        let loginUser = req.user;
        try {
            if (!loginUser) {
                return res.unAuthorizedRequest();
            }
            if (!params.to || !params.value) {
                return res.insufficientParameters();
            }
            if (!loginUser.isCustodialWalletExists) {
                return res.invalidRequest('Custodial wallet address not exists.');
            }
            let transaction = await NFTService.NFTtoNFTCalculation(loginUser.custodialWalletAddress, params.to, params.value);
            if (!transaction.flag) {
                return res.requestValidated(transaction.data);
            }
            return res.invalidRequest(transaction.data);
        } catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },

    NFTTransaction: async (req, res) => {
        const params = req.body;
        let loginUser = req.user;
        try {
            if (!loginUser) {
                return res.unAuthorizedRequest();
            }
            if (!loginUser.settingPhoneVerified || !loginUser.settingEmailVerified) {
                return res.invalidRequest('Please verify email and phone no first');
            }
            if (!params.to || !params.value) {
                return res.insufficientParameters();
            }
            if (!loginUser.isCustodialWalletExists) {
                return res.invalidRequest('Custodial wallet address not exists.');
            }
            await dbService.updateDocument(User, { _id: loginUser.id }, {
                settingEmailVerified: false,
                settingPhoneVerified: false,
                transInProgress: true
            });
            let transaction = await NFTService.NFTtoNFTTransaction(loginUser, loginUser.custodialWalletAddress, params.to, params.value);
            if (!transaction.flag) {
                await dbService.updateDocument(User, { _id: loginUser.id }, { transInProgress: false });
                await activityService.createActivity(req, loginUser.id, 'Withdraw NFT', 'TRANSACTION');
                return res.requestValidated(transaction.data);
            }
            await dbService.updateDocument(User, { _id: loginUser.id }, { transInProgress: false });
            return res.invalidRequest(transaction.data);
        } catch (error) {
            console.error(error);
            await dbService.updateDocument(User, { _id: loginUser.id }, { transInProgress: false });
            return res.failureResponse(error.message);
        }
    },
};