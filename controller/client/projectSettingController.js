const ProjectSetting = require('../../model/projectSetting');
const dbService = require('../../utils/dbService');

module.exports = {
    getSettingByType: async (req, res) => {
        try {
            if (!req.query.type) {
                return res.insufficientParameters();
            }
            const setting = await dbService.getDocumentByQuery(ProjectSetting, { type: req.query.type });
            if (!setting) {
                return res.recordNotFound({});
            }
            return res.ok(setting);
        }
        catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },
};
