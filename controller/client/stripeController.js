const User = require('../../model/user');
const Web3 = require('web3');
const userSchemaKey = require('../../utils/validation/userValidation');
const dbService = require('../../utils/dbService');
const common = require('../../utils/common');
const stripeUtil = require('../../services/stripe');
const bricksTransactionService = require('../../services/transaction/bricksTransaction');
const historyTransactionService = require('../../services/transaction/historyTransaction');
const provider = new Web3.providers.HttpProvider(process.env.WEB3_PROVIDER_URL);
const Contract = require('../../contract/ABI.json');
const ContractToken = require('../../contract/token.json');
const web3 = new Web3(provider);
const token = new web3.eth.Contract(ContractToken, process.env.WBRICKS);
const RouterContract = new web3.eth.Contract(Contract, process.env.ROUTER);
const moment = require('moment');
const growSruf = require('../../utils/growSruf');
const activityService = require('../../services/activity');
const { SEND_ADMIN_EMAIL } = require('../../constants/authConstant');

module.exports = {
	/*
	 * api: get user
	 * request type : GET
	 * url : {{url}}/client/api/v1/user/6177c67cd47f8b6fb34eba81
	 * headers : Auth token (i.e. Bearer {{token}})
	 * description : get user which id passed in api.
	 */
	createCustomer: async (req, res) => {
		try {
			let query = {};
			query._id = req.params.id;
			let result = await dbService.getDocumentByQuery(User, query);
			if (result) {
				let data = await stripeUtil.createUpdateCustomer(result);
				return res.ok(data);
			}
		}
		catch (error) {
			return res.failureResponse(error.message);
		}
	},
	createCustomerCard: async (req, res) => {
		try {
			let loginUser = req.user;
			if (!loginUser) {
				return res.unAuthorizedRequest();
			}
			if (loginUser.id == req.params.id) {
				let stripeCustomerId, query = {};
				const {
					cardNo, expYear, expMonth, cvc
				} = req.body;
				query._id = req.params.id;
				let dbUser = await dbService.getDocumentByQuery(User, query);
				if (!dbUser.stripeCustomerId || dbUser.stripeCustomerId === null || dbUser.stripeCustomerId === '') {
					let customer = await stripeUtil.createUpdateCustomer(dbUser);
					stripeCustomerId = customer.id;
					if (stripeCustomerId) {
						/* Updating local DB with the stripe customer Id*/
						await dbService.findOneAndUpdateDocument(User, { _id: loginUser.id }, { stripeCustomerId }, { new: true });
					}
				} else {
					stripeCustomerId = dbUser.stripeCustomerId;
				}
				let data = await stripeUtil.addCardForCustomer(stripeCustomerId, cardNo, expYear, expMonth, cvc);
				return res.ok(data);
			}
			return res.recordNotFound('User not found or token not matching with this user.');
		}
		catch (error) {
			return res.failureResponse(error.message);
		}
	},
	getCustomerCards: async (req, res) => {
		try {
			const stripeCustomerId = req.params.stripeCustomerId;
			let data = await stripeUtil.getUserCardDetails(stripeCustomerId);
			return res.ok(data);
		}
		catch (error) {
			return res.failureResponse(error.message);
		}
	},
	deleteCustomerCard: async (req, res) => {
		try {
			const {
				stripeCustomerId, stripeCardId
			} = req.params;
			let data = await stripeUtil.deleteCardForCustomer(stripeCustomerId, stripeCardId);
			return res.ok(data);
		}
		catch (error) {
			return res.failureResponse(error.message);
		}
	},

	getBricksPrice: async (req, res) => {
		try {
			let data = await stripeUtil.getBricksPrice(req, res);
			return res.ok(data);
		}
		catch (error) {
			return res.failureResponse(error.message);
		}
	},

	pay: async (req, res) => {
		try {
			if (!req.body.userId) {
				return res.invalidRequest('`userId` is required field. User unidentified.');
			}
			let dbUser = await dbService.getDocumentByQuery(User, { _id: req.body.userId });
			if (!dbUser) {
				return res.recordNotFound('No such user found.');
			}
			if (!dbUser.custodialWalletAddress || dbUser.custodialWalletAddress === '') {
				return res.failureResponse('No wallet address attached to user.');
			}
			if (!req.body.paymentIntentId && !req.body.amount || req.body.amount <= 0) {
				return res.failureResponse('Invalid amount.');
			}

			const singleBrickPrice = await stripeUtil.getBricksPrice(req, res);
			if (!singleBrickPrice) {
				return res.failureResponse('Not able to fetch single brick price.');
			}

			const bricks = Number(req.body.amount / singleBrickPrice['gbp']);

			if (!req.body.paymentIntentId && !bricks) {
				return res.failureResponse('Bricks is a required field');
			}
			req.user = dbUser;
			let data = await stripeUtil.makePayment(req, res);
			if (data && data.success) {
				let updateData = { isStripePaymentSuccess: true };
				if (!dbUser['referralId'] || dbUser['referralId'] === '') {
					let postData = {
						email: dbUser.email ? dbUser.email : '',
						firstName: dbUser.firstName ? dbUser.firstName : '',
						lastName: dbUser.lastName ? dbUser.lastName : '',
					};
					if (dbUser.referredBy) {
						postData.referredBy = dbUser.referredBy;
					}
					const referralData = await growSruf.createReferralId(postData);
					if (referralData.data && referralData.data.id) {
						updateData['referralId'] = referralData.data.id;
					}

				}
				await dbService.updateDocument(User, { _id: dbUser._id }, updateData);
				try {
					const transferValue = ((bricks * 1000000000).toFixed(0)).toString();
					const gasValue = await token.methods.transfer(dbUser.custodialWalletAddress, transferValue).estimateGas({ from: process.env.CENTRAL_WALLET_ADDRESS });
					const gasPrice = await web3.eth.getGasPrice();
					const bricksTransaction = await bricksTransactionService.BricksToBricksTransaction(dbUser, transferValue, process.env.CENTRAL_WALLET_ADDRESS, dbUser.custodialWalletAddress);
					if (!bricksTransaction.flag) {
						data.transferSuccess = true; 
						await historyTransactionService.storeTransaction(dbUser, process.env.CENTRAL_WALLET_ADDRESS, dbUser.custodialWalletAddress, req.body.amount, bricks, 'GBP', 'BRICKS', bricksTransaction.data.transactionHash, Web3.utils.fromWei((gasValue * gasPrice).toString()), bricksTransaction.data.status, 'PURCHASE', bricksTransaction.data.requestId, moment().format('X'));
					}
				} catch (err) {
					console.log(err);
					data.transferSuccess = false;
					await activityService.sendActivityEmail('Client transaction Failed', SEND_ADMIN_EMAIL, '/views/transactionFailed', {
						userPaid: `${req.body.amount} GBP`,
						userGetBricks: `${bricks} BRICKS`,
						firstName: dbUser.firstName,
						userEmail: dbUser.email,
						userWalletAddress: dbUser.custodialWalletAddress,
						paymentId: req.body.paymentIntentId,
						transactionTime: moment().format('YYYY-MM-DD HH:mm:ss'),
					});
				}

			}
			return res.ok(data);
		}
		catch (error) {
			console.log(error);
			return res.failureResponse(error);
		}
	},
};