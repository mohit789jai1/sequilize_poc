const Property = require('../../model/property');
const dbService = require('../../utils/dbService');
const _ = require('lodash');
const { USER_ROLE } = require('../../constants/authConstant');
const propertyService = require('../../services/property');

module.exports = {
    getProperty: async (req, res) => {
        try {
            if (req.user && req.user.role != USER_ROLE.Admin) {
                return res.unAuthorizedRequest();
            }
            if (req.params.id) {
                let result = await propertyService.findById(req.params.id, ['propertyMainImage', 'propertyImageCollection']);
                if (result) {
                    return res.ok(result);
                }
            }
            return res.badRequest({});
        }
        catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },

    createProperty: async (req, res) => {
        try {
            if (req.user && req.user.role != USER_ROLE.Admin) {
                return res.unAuthorizedRequest();
            }
            let data = req.body;
            let result = await dbService.createDocument(Property, data);
            if (!result) {
                return res.badRequest({});
            }
            result = await propertyService.findById(result.id, ['propertyMainImage', 'propertyImageCollection']);
            return res.ok(result);
        }
        catch (error) {
            return res.failureResponse(error.message);
        }
    },

    updateProperty: async (req, res) => {
        try {
            if (req.user && req.user.role != USER_ROLE.Admin) {
                return res.unAuthorizedRequest();
            }
            if (!req.params.id) {
                return res.insufficientParameters();
            }
            delete req.body['addedBy'];
            delete req.body['updatedBy'];
            let data = {
                ...req.body,
                id: req.params.id,
                updatedBy: req.user.id
            };
            let query = { _id: req.params.id };
            let result = await dbService.findOneAndUpdateDocument(Property, query, data, { new: false });
            if (!result) {
                return res.recordNotFound({});
            }
            result = await propertyService.findById(req.params.id, ['propertyMainImage', 'propertyImageCollection']);
            return res.ok(result);
        }
        catch (error) {
            return res.failureResponse(error.message);
        }
    },

    deleteProperty: async (req, res) => {
        try {
            if (req.user && req.user.role != USER_ROLE.Admin) {
                return res.unAuthorizedRequest();
            }
            if (!req.params.id) {
                return res.insufficientParameters();
            }
            let query = {};
            query._id = req.params.id;
            if (!_.isEmpty(query)) {
                let result = await propertyService.deleteOne(query);
                if (result) {
                    return res.ok('Property deleted successfully!');
                }
            }
            return res.recordNotFound({});
        }
        catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },
};
