const notificationService = require('../../services/notification');

module.exports = {
    subscribe: async (req, res) => {
        let params = req.body;
        try {
            if (!params.email) {
                return res.insufficientParameters();
            }
            const subscribed = await notificationService.subscribeUser(params.email);
            if (!subscribed) {
                return res.validationErrorMessage('It looks like you\'ve already signed up to be notified about the MyBricks portfolio launches.', {});
            }
            return res.successMessage('You will be notified as soon as the first MyBricks portfolio launches.', subscribed);
        }
        catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },
    launchNotification: async (req, res) => {
        let params = req.body;
        try {
            params.message = 'text of launch notification';
            const setting = await notificationService.launchNotification(params.message);
            if (!setting) {
                return res.invalidRequest({});
            }
            return res.ok(setting);
        }
        catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },
};
