const authService = require('../../services/auth');
const tokenService = require('../../services/token');
const transService = require('../../services/transaction/transaction');
const User = require('../../model/user');
const dbService = require('../../utils/dbService');
const userTokens = require('../../model/userTokens');
const moment = require('moment');
const userSchemaKey = require('../../utils/validation/userValidation');
const validation = require('../../utils/validateRequest');
const {
	uniqueValidation, encryption, decryption
} = require('../../utils/common');
const {
	JWT, MAX_RESEND_OTP_RETRY_LIMIT, ADMIN_EMAIL, RESEND_OTP_REACTIVE_TIME, MAX_VALIDATE_OTP_RETRY_LIMIT
} = require('../../constants/authConstant');
const { STATUS } = require('../../constants/userConstant');
const axios = require('axios');
const common = require('../../utils/common');
const activityService = require('../../services/activity');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const emailService = require('../../services/email/emailService');
const RequestIp = require('@supercharge/request-ip');
const growSruf = require('../../utils/growSruf');
const stripe = require('../../services/stripe');

module.exports = {
	/*
	 * api: user register 
	 * request type : POST
	 * url : {{url}}/client/auth/register
	 * description : first time user registration.
	 * request body : {
	 *  "password": "Test@123",
	 *  "email": "tarparaajay@gmail.com",
	 *  "firstName": "Ajay",
	 *  "phoneNo": "8545253615",
	 *  "lastName": "Tarpara",
	 *  "role": 1
	 * }
	 */
	register: async (req, res) => {
		try {
			let isValid = validation.validateParamsWithJoi(
				req.body,
				userSchemaKey.schemaKeys
			);
			if (isValid.error) {
				return res.inValidParam(isValid.error);
			}
			if (req.body.role != 1) {
				return res.inValidParam('Please enter valid user role.');
			}
			req.body = _.pick(req.body, ['dob', 'email', 'firstName', 'lastName', 'password', 'phoneNo', 'role', 'referredBy']);
			const data = new User({ ...req.body });
			let unique = await uniqueValidation(User, { email: req.body.email });
			if (!unique) {
				return res.inValidParam('Sorry! An account with that email already exists. Please try again or login to your account. ');
			}
			const result = await dbService.createDocument(User, data);
			await activityService.sendActivityEmail('MyBricks Account Created', result.email, '/views/register', result.toJSON());
			await activityService.createActivity(req, result.id, 'Register', 'SIGN_UP');
			return res.ok(result);
		} catch (error) {
			console.error(error);
			if (error.name === 'ValidationError') {
				return res.validationError(error.message);
			}
			if (error.code && error.code == 11000) {
				return res.isDuplicate(error.message);
			}
			return res.failureResponse(error.message);
		}
	},

	/*
	 * api : send email of otp
	 * request type : POST
	 * description : send otp to user for verify email
	 * url : {{url}}/client/auth/send_email_verify_otp
	 * request body : {
	 * "email": "tarparaajay@gmail.com"
	 * }
	 */
	sendOtpForVerifyEmail: async (req, res) => {
		try {
			let params = req.body;
			if (!params.email) {
				return res.insufficientParameters();
			}
			if (params.newEmail) {
				let unique = await uniqueValidation(User, { email: params.newEmail });
				if (!unique) {
					return res.inValidParam('Sorry! That email is already in use on the platform. Please try again.');
				}
			}
			let result = await authService.sendEmailOtp(params.email, params.newEmail);
			if (!result.flag) {
				return res.requestValidated(result.data);
			}
			return res.invalidRequest(result.data);
		} catch (error) {
			console.error(error);
			return res.failureResponse(error.message);
		}
	},

	/*
	 * api : send sms
	 * request type : POST
	 * description : send otp to user for verify phone number
	 * url : {{url}}/client/auth/send_phone_no_verify_otp
	 * request body : {
	 * "phoneNo": "+917878581381"
	 * }
	 */
	sendOtpForVerifyPhoneNo: async (req, res) => {
		try {
			let params = req.body;
			if (!params.phoneNo || !params.email) {
				return res.insufficientParameters();
			}
			if (!params.isRecover) {
				let unique = await uniqueValidation(User, { phoneNo: params.phoneNo });
				if (!unique) {
					return res.inValidParam('Sorry! That phone number is already in use on the platform. Please try again.');
				}
			}
			let result = await authService.sendPhoneNoOtp(params.phoneNo, params.email);
			if (!result.flag) {
				return res.requestValidated(result.data);
			}
			return res.invalidRequest(result.data);
		} catch (error) {
			console.error(error);
			return res.failureResponse(error.message);
		}
	},

	/*
	 * api : verify email
	 * request type : POST
	 * description : after successfully sent otp to user, user can  verify email
	 * url : {{url}}/client/auth/verify_email
	 * request body : {
	 * "email": "tarparaajay@gmail.com",
	 * "code": "3V5M37"
	 * }
	 */
	verifyEmail: async (req, res) => {
		const params = req.body;
		try {
			let updateData;
			if (!params.code || !params.email) {
				return res.insufficientParameters();
			}
			let where = {
				'email': params.email,
				role: { $ne: 10 }
			};
			let user = await dbService.getDocumentByQuery(User, where);
			if (user && user.emailOTP.expireTime) {
				if (moment(new Date()).isAfter(moment(user.emailOTP.expireTime))) {// link expire
					return res.invalidRequest('Your OTP is expired');
				}
				if (user.emailOTP.code !== params.code) {
					updateData = { validateVerifyEmailOTPRetryLimit: user.validateVerifyEmailOTPRetryLimit + 1 };
					if (user.validateVerifyEmailOTPRetryLimit >= MAX_VALIDATE_OTP_RETRY_LIMIT - 1) {
						updateData = {
							validateVerifyEmailOTPRetryLimit: 0,
							'emailOTP.expireTime': moment().toISOString()
						};
					}
					await dbService.updateDocument(User, { _id: user.id }, updateData);
					return res.invalidRequest('Invalid code. Please try again.');
				}
			} else {
				// Invalid code. Please try again.
				return res.invalidRequest('Invalid code. Please try again.');
			}
			let result = await authService.verifyEmail(params.email, params.code);
			if (!result.flag) {
				updateData = {
					verifyEmailResendOTPReactiveTime: moment(),
					verifyEmailResendOTPRetryLimit: 0,
					validateVerifyEmailOTPRetryLimit: 0
				};
				await dbService.updateDocument(User, { _id: user.id }, updateData);
				await activityService.createActivity(req, user.id, 'verify email', 'SIGN_UP');
				return res.requestValidated(result.data);
			}
			return res.invalidRequest(result.data);
		} catch (error) {
			console.error(error);
			return res.failureResponse(error.message);
		}
	},

	/*
	 * api : verify phone number
	 * request type : POST
	 * description : after successfully sent otp to user, user can verify phone number
	 * url : {{url}}/client/auth/verify_phone_no
	 * request body : {
	 * "phoneNo": "+447932935146",
	 * "code": "7J1D3U"
	 * }
	 */
	verifyPhoneNo: async (req, res) => {
		const params = req.body;
		let updateData;
		try {
			if (!params.code || !params.phoneNo) {
				return res.insufficientParameters();
			}
			let where = {
				'phoneNo': params.phoneNo,
				role: { $ne: 10 }
			};
			let user = await dbService.getDocumentByQuery(User, where);
			if (user && user.phoneNoOTP && user.phoneNoOTP.expireTime) {
				if (moment(new Date()).isAfter(moment(user.phoneNoOTP.expireTime))) {// link expire
					return res.invalidRequest('Your OTP is expired');
				}
				if (user.phoneNoOTP.code !== params.code) {
					updateData = { validateVerifyPhoneNoOTPRetryLimit: user.validateVerifyPhoneNoOTPRetryLimit + 1 };
					if (user.validateVerifyPhoneNoOTPRetryLimit >= MAX_VALIDATE_OTP_RETRY_LIMIT - 1) {
						updateData = {
							validateVerifyPhoneNoOTPRetryLimit: 0,
							'phoneNoOTP.expireTime': moment().toISOString()
						};
					}
					await dbService.updateDocument(User, { _id: user.id }, updateData);
					return res.invalidRequest('Invalid code. Please try again.');
				}
			} else {
				// Invalid code. Please try again.
				return res.invalidRequest('Invalid code. Please try again.');
			}
			let result = await authService.verifyPhoneNo(params.phoneNo, params.code);
			if (!result.flag) {
				updateData = {
					verifyPhoneNoResendOTPReactiveTime: moment(),
					verifyPhoneNoResendOTPRetryLimit: 0,
					validateVerifyPhoneNoOTPRetryLimit: 0
				};
				await dbService.updateDocument(User, { _id: user.id }, updateData);
				await activityService.createActivity(req, user.id, 'verify phone', 'SIGN_UP');
				return res.requestValidated(result.data);
			}
			return res.invalidRequest(result.data);
		} catch (error) {
			console.error(error);
			return res.failureResponse(error.message);
		}
	},

	updateInqIdForKyc: async (req, res) => {
		const params = req.body;
		try {
			if (!params.inqId || !params.email) {
				return res.insufficientParameters();
			}
			let where = {
				'email': params.email,
				role: { $ne: 10 }
			};
			let user = await dbService.getDocumentByQuery(User, where);
			if (user) {
				if (user.isEmailVerified && user.isPhoneNoVerified && user.isRecoveryAddressExists) {
					await dbService.updateDocument(User, { _id: user.id }, {
						kycInqId: params.inqId,
						kycStatus: STATUS.CREATED
					});
					await activityService.createActivity(req, user.id, 'update kyc Id', 'SIGN_UP');
					return res.requestValidated('Kyc inquiry key added successfully !!');
				} else {
					return res.invalidRequest('Please complete essential verification before kyc');
				}
			} else {
				// User not found
				return res.invalidRequest('User Not Found');
			}
		} catch (error) {
			console.error(error);
			return res.failureResponse(error.message);
		}
	},

	/*
	 * api : verify kyc
	 * request type : POST
	 * description : after successfully verify phone number, user can  verify kyc
	 * url : {{url}}/client/auth/kyc
	 * request body : {
	 * "email":"tarparaajay@gmail.com",
	 * "inqId":"inq_jr8iNtJS6Wgs1FQz9F74boUY"
	 * }
	 */
	verifyKyc: async (req, res) => {
		const params = req.body;
		try {
			if (!params.email) {
				return res.insufficientParameters();
			}
			let where = {
				'email': params.email,
				role: { $ne: 10 }
			};
			let user = await dbService.getDocumentByQuery(User, where);
			if (user) {
				if (!user.isEmailVerified && !user.isPhoneNoVerified && !user.isRecoveryAddressExists) {
					return res.invalidRequest('Please complete essential verification before kyc');
				}
				if (user.isKycVerified) {
					return res.invalidRequest('Kyc already verified');
				}
				if (params.inqId !== user.kycInqId) {
					return res.invalidRequest('Inquiry id is invalid');
				}
				let result = await authService.checkKycStatus(params, user);
				let token = {};
				if (result.isKycVerified) {
					token = await tokenService.generateAuthTokens(user, JWT.CLIENT_SECRET);
				}
				await activityService.createActivity(req, user.id, 'verify kyc', 'SIGN_UP');
				return res.requestValidated({
					...result,
					...token
				});
			} else {
				// User not found
				return res.invalidRequest('User Not Found');
			}
		} catch (error) {
			console.error(error);
			return res.failureResponse(error.message);
		}
	},
	/*
	 * api : create recoveryCode
	 * request type : POST
	 * description : after successfully verify phone number, user can  verify kyc
	 * url : {{url}}/client/auth/recovery_code
	 * request body : {
	 * "email":"tarparaajay@gmail.com"
	 * }
	 */
	recoveryAddress: async (req, res) => {
		const params = req.body;
		try {
			if (!params.email) {
				return res.insufficientParameters();
			}
			let where = {
				'email': params.email,
				role: { $ne: 10 }
			};
			let user = await dbService.getDocumentByQuery(User, where);
			let recoveryAddress = common.generateCode();
			if (user) {
				if (user.isEmailVerified && user.isPhoneNoVerified) {
					if (!user.isRecoveryAddressExists) {
						await dbService.updateDocument(User, { _id: user.id }, {
							recoveryAddress: encryption(recoveryAddress),
							isRecoveryAddressExists: true
						});
						await activityService.createActivity(req, user.id, 'generate recovery code', 'SIGN_UP');
						return res.requestValidated({
							recoveryAddress: recoveryAddress,
							message: 'Recovery Code generated successfully !!'
						});
					} else {
						// recovery code already exists
						return res.invalidRequest('Recovery code already exist.');
					}
				} else {
					return res.invalidRequest('Email or Phone number not verified');
				}
			} else {
				// User not found
				return res.invalidRequest('User Not Found');
			}
		} catch (error) {
			console.error(error);
			return res.failureResponse(error.message);
		}
	},

	/*
	 * api : send email of otp
	 * request type : POST
	 * description : send otp to user for verify email
	 * url : {{url}}/client/auth/send_email_verify_otp
	 * request body : {
	 * "email": "tarparaajay@gmail.com"
	 * }
	 */
	sendOtpForRecoverEmail: async (req, res) => {
		try {
			let params = req.body;
			if (!params.email) {
				return res.insufficientParameters();
			}
			if (params.newEmail) {
				let unique = await uniqueValidation(User, { email: params.newEmail });
				if (!unique) {
					return res.inValidParam('Sorry! That email is already in use on the platform. Please try again.');
				}
			}
			let result = await authService.sendEmailOtp(params.email, false, params.newEmail || params.email);
			if (!result.flag) {
				return res.requestValidated(result.data);
			}
			return res.invalidRequest(result.data);
		} catch (error) {
			console.error(error);
			return res.failureResponse(error.message);
		}
	},

	/*
	 * api : send sms
	 * request type : POST
	 * description : send otp to user for verify phone number
	 * url : {{url}}/client/auth/send_phone_no_verify_otp
	 * request body : {
	 * "phoneNo": "+917878581381"
	 * }
	 */
	sendOtpForRecoverPhoneNo: async (req, res) => {
		try {
			let params = req.body;
			if (!params.phoneNo || !params.email) {
				return res.insufficientParameters();
			}
			if (!params.isRecover) {
				let unique = await uniqueValidation(User, { phoneNo: params.phoneNo });
				if (!unique) {
					return res.inValidParam('Sorry! That phone number is already in use on the platform. Please try again.');
				}
			}
			let result = await authService.sendPhoneNoOtp(params.phoneNo, params.email, false, true);
			if (!result.flag) {
				return res.requestValidated(result.data);
			}
			return res.invalidRequest(result.data);
		} catch (error) {
			console.error(error);
			return res.failureResponse(error.message);
		}
	},

	/*
	 * api : verify email for recover
	 * request type : POST
	 * description : after successfully sent otp to user, user can  verify email
	 * url : {{url}}/client/auth/verify_email
	 * request body : {
	 * "email": "tarparaajay@gmail.com",
	 * "code": "3V5M37"
	 * }
	 */
	RecoverEmail: async (req, res) => {
		const params = req.body;
		try {
			let updateData;
			if (!params.code || !params.email) {
				return res.insufficientParameters();
			}
			let where = {
				'altEmail': params.email,
				role: { $ne: 10 }
			};
			let user = await dbService.getDocumentByQuery(User, where);
			if (user && user.emailOTP.expireTime) {
				if (moment(new Date()).isAfter(moment(user.emailOTP.expireTime))) {// link expire
					return res.invalidRequest('Your OTP is expired');
				}
				if (user.emailOTP.code !== params.code) {
					updateData = { validateVerifyEmailOTPRetryLimit: user.validateVerifyEmailOTPRetryLimit + 1 };
					if (user.validateVerifyEmailOTPRetryLimit >= MAX_VALIDATE_OTP_RETRY_LIMIT - 1) {
						updateData = {
							validateVerifyEmailOTPRetryLimit: 0,
							'emailOTP.expireTime': moment().toISOString()
						};
					}
					await dbService.updateDocument(User, { _id: user.id }, updateData);
					return res.invalidRequest('Invalid code. Please try again.');
				}
			} else {
				// Invalid code. Please try again.
				return res.invalidRequest('Invalid code. Please try again.');
			}
			updateData = {
				verifyEmailResendOTPReactiveTime: moment().toISOString(),
				verifyEmailResendOTPRetryLimit: 0,
				validateVerifyEmailOTPRetryLimit: 0,
				email: user.altEmail,
				altEmail: null,
				emailOTP: null
			};
			await dbService.updateDocument(User, { _id: user.id }, updateData);
			await activityService.createActivity(req, user.id, 'verify email', 'RECOVER');
			return res.requestValidated('Email Verified Successfully!');
		} catch (error) {
			console.error(error);
			return res.failureResponse(error.message);
		}
	},

	/*
	 * api : verify phone number
	 * request type : POST
	 * description : after successfully sent otp to user, user can verify phone number
	 * url : {{url}}/client/auth/verify_phone_no
	 * request body : {
	 * "phoneNo": "+447932935146",
	 * "code": "7J1D3U"
	 * }
	 */
	RecoverPhoneNo: async (req, res) => {
		const params = req.body;
		let updateData;
		try {
			if (!params.code || !params.phoneNo) {
				return res.insufficientParameters();
			}
			let where = {
				'altPhoneNo': params.phoneNo,
				role: { $ne: 10 }
			};
			let user = await dbService.getDocumentByQuery(User, where);
			if (user && user.phoneNoOTP && user.phoneNoOTP.expireTime) {
				if (moment(new Date()).isAfter(moment(user.phoneNoOTP.expireTime))) {// code expire
					return res.invalidRequest('Your OTP is expired');
				}
				if (user.phoneNoOTP.code !== params.code) {
					updateData = { validateVerifyPhoneNoOTPRetryLimit: user.validateVerifyPhoneNoOTPRetryLimit + 1 };
					if (user.validateVerifyPhoneNoOTPRetryLimit >= MAX_VALIDATE_OTP_RETRY_LIMIT - 1) {
						updateData = {
							validateVerifyPhoneNoOTPRetryLimit: 0,
							'phoneNoOTP.expireTime': moment().toISOString()
						};
					}
					await dbService.updateDocument(User, { _id: user.id }, updateData);
					return res.invalidRequest('Invalid code. Please try again.');
				}
			} else {
				// Invalid code. Please try again.
				return res.invalidRequest('Invalid code. Please try again.');
			}
			updateData = {
				verifyPhoneNoResendOTPReactiveTime: moment(),
				verifyPhoneNoResendOTPRetryLimit: 0,
				validateVerifyPhoneNoOTPRetryLimit: 0,
				phoneNoOTP: null,
				phoneNo: user.altPhoneNo,
				altPhoneNo: null
			};
			await dbService.updateDocument(User, { _id: user.id }, updateData);
			await activityService.createActivity(req, user.id, 'verify phone', 'RECOVER');
			return res.requestValidated('Phone Number Verified Successfully!');
		} catch (error) {
			console.error(error);
			return res.failureResponse(error.message);
		}
	},
	recoverAccount: async (req, res) => {
		const params = req.body;
		try {
			if (!params.email || !params.recoveryAddress) {
				return res.insufficientParameters();
			}
			let where = {
				'email': params.email,
				role: { $ne: 10 }
			};
			let user = await dbService.getDocumentByQuery(User, where);
			if (user) {
				if (decryption(user.recoveryAddress) == params.recoveryAddress) {
					await dbService.updateDocument(User, { _id: user.id }, {
						altEmail: null,
						altPhoneNo: null
					});
					return res.requestValidated({
						email: user.email,
						phoneNo: user.phoneNo,
						recoveryAddress: true,
						message: 'Recovery Code match successfully !!'
					});
				} else {
					return res.invalidRequest({
						recoveryAddress: false,
						message: 'Recovery Code invalid'
					});
				}
			} else {
				// User not found
				return res.invalidRequest({
					recoveryAddress: false,
					message: 'User Not Found'
				});
			}
		} catch (error) {
			console.error(error);
			return res.failureResponse(error.message);
		}
	},

	giveResetPasswordPermission: async (req, res) => {
		const params = req.body;
		try {
			if (!params.email || !params.recoveryAddress) {
				return res.insufficientParameters();
			}
			let where = {
				'email': params.email,
				role: { $ne: 10 }
			};
			let user = await dbService.getDocumentByQuery(User, where);
			if (user) {
				if (decryption(user.recoveryAddress) == params.recoveryAddress) {
					await dbService.updateDocument(User, { _id: user.id }, {
						isAccountLock: true,
						reasonOfLock: 'use recovery address',
						accountLockTime: moment().add(7, 'days').toISOString(),
						resetPasswordPermission: {
							Permissions: true,
							expireTime: moment().add(RESEND_OTP_REACTIVE_TIME, 'minutes').toISOString()
						}
					});
					await activityService.sendActivityEmail('Withdrawals from your account have been locked', user.email, '/views/accountLock', {
						firstName: _.capitalize(user.firstName),
						accountLockTime: moment().add(7, 'days').format('YYYY-MM-DD HH:mm:ss')
					});
					await activityService.createActivity(req, user.id, 'use recovery code', 'RESET');
					return res.requestValidated({
						recoveryAddress: true,
						message: 'Reset password permission granted !!'
					});
				} else {
					return res.invalidRequest({
						recoveryAddress: false,
						message: 'Recovery Code invalid'
					});
				}
			} else {
				// User not found
				return res.invalidRequest({
					recoveryAddress: false,
					message: 'User Not Found'
				});
			}
		} catch (error) {
			console.error(error);
			return res.failureResponse(error.message);
		}
	},

	/*
	 * api : create Wallet
	 * request type : POST
	 * description : after KYC create custodial wallet
	 * url : {{url}}/client/auth/create_wallet
	 * request body : {
	 * pending
	 * }
	 */
	createWallet: async (req, res) => {
		const params = req.body;
		try {
			if (!params.email) {
				return res.insufficientParameters();
			}
			let where = {
				'email': params.email,
				role: { $ne: 10 }
			};
			let user = await dbService.getDocumentByQuery(User, where);
			if (!user) {
				return res.invalidRequest('User not found');
			}
			let result = await transService.createCustodialWallet(user, params.walletName || null);
			if (!result.flag) {
				let token = await tokenService.generateAuthTokens(user, JWT.CLIENT_SECRET);
				await activityService.createActivity(req, user.id, 'create wallet', 'SIGN_UP');
				/*
				 * let postData = {
				 * 	email: user.email,
				 * 	firstName: user.firstName,
				 * 	lastName: user.lastName,
				 * };
				 * if (user.referredBy) {
				 * 	postData.referredBy = user.referredBy;
				 * }
				 * let referralData = await growSruf.createReferralId(postData);
				 *
				 * let customerData = await stripe.createUpdateCustomer(user);
				 * let updateData = null;
				 *
				 * if (referralData.data) {
				 * 	updateData['referralId'] = referralData.data.id;
				 * }
				 *
				 * if (customerData.data.id) {
				 *	updateData['stripeCustomerId'] = customerData.data.id;
				 *}
				 * if (updateData) {
				 *	await dbService.updateDocument(User, { _id: user.id }, updateData);
				 *}
				 */
				return res.requestValidated({
					...result.data,
					...token
				});
			}
			return res.invalidRequest(result.data);
		} catch (error) {
			console.error(error);
			return res.failureResponse(error.message);
		}
	},

	/*
	 * api : forgot password
	 * request type : POST
	 * description : send email or sms to user for forgot password.
	 * url : {{url}}/client/auth/forgot-password
	 * request body : {
	 *  "email": "tarparaajay@gmail.com"
	 * }
	 */
	forgotPassword: async (req, res) => {
		const params = req.body;
		try {
			if (!params.email) {
				return res.insufficientParameters();
			}
			let where = {
				email: params.email,
				role: { $ne: 10 }
			};
			params.email = params.email.toString().toLowerCase();
			let isUser = await dbService.getDocumentByQuery(User, where);
			if (isUser) {
				let {
					resultOfEmail, resultOfSMS
				} = await authService.sendResetPasswordNotification(isUser, {
					email: true,
					sms: false
				});
				if (resultOfEmail && resultOfSMS) {
					return res.requestValidated('otp successfully send.');
				} else if (resultOfEmail && !resultOfSMS) {
					return res.requestValidated('otp successfully send to your email.');
				} else if (!resultOfEmail && resultOfSMS) {
					return res.requestValidated('otp successfully send to your mobile number.');
				} else {
					return res.invalidRequest('otp can not be sent due to some issue try again later');
				}
			} else {
				return res.recordNotFound('No account with that email has been found. Please try again.');
			}
		} catch (error) {
			console.error(error);
			return res.failureResponse(error);
		}
	},

	recoverResendOtp: async (req, res) => {
		const params = req.body;
		try {
			let filter;
			if (params.email) {
				filter = {
					altEmail: params.email,
					role: { $ne: 10 }
				};
			} else if (params.phoneNo) {
				filter = {
					altPhoneNo: params.phoneNo,
					role: { $ne: 10 }
				};
			}
			let isUser = await dbService.getDocumentByQuery(User, filter);
			if (!isUser) {
				return res.recordNotFound({});
			}
			let reactiveTimeKey, retryLimitKey;
			let email = isUser.email;
			let phoneNo = isUser.phoneNo;
			if (params.verifyEmail && isUser.altEmail) {
				reactiveTimeKey = 'verifyEmailResendOTPReactiveTime';
				retryLimitKey = 'verifyEmailResendOTPRetryLimit';
				email = isUser.altEmail;
			} else if (params.verifyPhoneNo && isUser.altPhoneNo) {
				reactiveTimeKey = 'verifyPhoneNoResendOTPReactiveTime';
				retryLimitKey = 'verifyPhoneNoResendOTPRetryLimit';
				phoneNo = isUser.altPhoneNo;
			} else {
				return res.insufficientParameters();
			}
			let updateData;
			if (moment(isUser[reactiveTimeKey] ? isUser[reactiveTimeKey] : 0) < moment() && isUser[retryLimitKey] < MAX_RESEND_OTP_RETRY_LIMIT) {
				updateData = { [retryLimitKey]: isUser[retryLimitKey] + 1 };
				if (isUser[retryLimitKey] == MAX_RESEND_OTP_RETRY_LIMIT - 1)
					updateData = {
						[reactiveTimeKey]: moment().add(RESEND_OTP_REACTIVE_TIME, 'minutes').toISOString(),
						[retryLimitKey]: 0
					};
				await dbService.updateDocument(User, { _id: isUser.id }, updateData);
				let resultOfEmail = false, resultOfSMS = false;
				if (params.verifyEmail) {
					let output = await authService.sendVerifyEmailOtp(isUser.id, email);
					if (output) { resultOfEmail = true; }
				} else if (params.verifyPhoneNo) {
					let output = await authService.sendVerifyPhoneNoOtp(isUser.id, phoneNo);
					if (output) { resultOfSMS = true; }
				}
				if (resultOfEmail && resultOfSMS) {
					return res.requestValidated('otp successfully send.');
				} else if (resultOfEmail && !resultOfSMS) {
					return res.requestValidated('otp successfully send to your email.');
				} else if (!resultOfEmail && resultOfSMS) {
					return res.requestValidated('otp successfully send to your mobile number.');
				} else {
					return res.invalidRequest('otp can not be sent due to some issue try again later');
				}
			}
			else {
				return res.invalidRequest(`You have exceeded the limit for requesting OTP codes. Please try again in  ${common.getDifferenceOfTwoDatesInTime(moment(), moment(isUser[reactiveTimeKey]))}.`);
			}
		} catch (error) {
			console.error(error);
			return res.failureResponse(error);
		}
	},

	/*
	 * api : resend OTP
	 * request type : POST
	 * description : re-send OTP on  email or sms to user for forgot password.
	 * url : {{url}}/client/auth/resend-otp
	 * request body : {
	 * "email": "tarparaajay@gmail.com"
	 * }
	 */
	resendOtp: async (req, res) => {
		const params = req.body;
		try {
			let where;
			let reactiveTimeKey, retryLimitKey;
			if (params.login && params.username && (params.email || params.sms) && params.password) {
				params.username = params.username.toString().toLowerCase();
				where = {
					email: params.username,
					role: { $ne: 10 }
				};
				reactiveTimeKey = 'loginResendOTPReactiveTime';
				retryLimitKey = 'loginResendOTPRetryLimit';
			} else if ((params.verifyEmail || params.resetPassword) && params.email) {
				params.email = params.email.toString().toLowerCase();
				where = {
					email: params.email,
					role: { $ne: 10 }
				};
				if (params.verifyEmail) {
					reactiveTimeKey = 'verifyEmailResendOTPReactiveTime';
					retryLimitKey = 'verifyEmailResendOTPRetryLimit';
				} else if (params.resetPassword) {
					reactiveTimeKey = 'resendOTPReactiveTime';
					retryLimitKey = 'resendOTPRetryLimit';
				}
			} else if (params.verifyPhoneNo && params.phoneNo) {
				where = {
					phoneNo: params.phoneNo,
					role: { $ne: 10 }
				};
				reactiveTimeKey = 'verifyPhoneNoResendOTPReactiveTime';
				retryLimitKey = 'verifyPhoneNoResendOTPRetryLimit';
			} else {
				return res.insufficientParameters();
			}
			let isUser = await dbService.getDocumentByQuery(User, where);
			if (isUser) {
				let updateData;
				if (moment(isUser[reactiveTimeKey] ? isUser[reactiveTimeKey] : 0) < moment() && isUser[retryLimitKey] < MAX_RESEND_OTP_RETRY_LIMIT) {
					updateData = { [retryLimitKey]: isUser[retryLimitKey] + 1 };
					if (isUser[retryLimitKey] == MAX_RESEND_OTP_RETRY_LIMIT - 1)
						updateData = {
							[reactiveTimeKey]: moment().add(RESEND_OTP_REACTIVE_TIME, 'minutes').toISOString(),
							[retryLimitKey]: 0
						};
					await dbService.updateDocument(User, { _id: isUser.id }, updateData);
					let resultOfEmail = false, resultOfSMS = false;
					if (params.verifyEmail) {
						let output = await authService.sendVerifyEmailOtp(isUser.id, isUser.email);
						if (output) { resultOfEmail = true; }
					} else if (params.verifyPhoneNo) {
						let output = await authService.sendVerifyPhoneNoOtp(isUser.id, isUser.phoneNo);
						if (output) { resultOfSMS = true; }
					} else if (params.login) {
						let output = await authService.sendLoginOTP(params.username, params.password, {
							email: params.email || false,
							sms: params.sms || false
						});
						if (!output.flag) {
							resultOfSMS = params.sms;
							resultOfEmail = params.email;
						}
						if (output.flag) {
							return res.invalidRequest(output.data);
						}
					} else if (params.resetPassword) {
						let output = await authService.sendResetPasswordNotification(isUser, {
							email: true,
							sms: false
						});
						resultOfEmail = output.resultOfEmail;
						resultOfSMS = output.resultOfSMS;
					}
					if (resultOfEmail && resultOfSMS) {
						return res.requestValidated('otp successfully send.');
					} else if (resultOfEmail && !resultOfSMS) {
						return res.requestValidated('otp successfully send to your email.');
					} else if (!resultOfEmail && resultOfSMS) {
						return res.requestValidated('otp successfully send to your mobile number.');
					} else {
						return res.invalidRequest('otp can not be sent due to some issue try again later');
					}
				}
				else {
					return res.invalidRequest(`You have exceeded the limit for requesting OTP codes. Please try again in  ${common.getDifferenceOfTwoDatesInTime(moment(), moment(isUser[reactiveTimeKey]))}.`);
				}
			} else {
				return res.recordNotFound({});
			}
		} catch (error) {
			console.error(error);
			return res.failureResponse(error);
		}
	},
	/*
	 * api : validate forgot password otp 
	 * request type : POST
	 * description : after successfully sent mail or sms for forgot password validate otp
	 * url : {{url}}/client/auth/validate-otp
	 * request body : {
	 * "username": "tarparaajay@gmail.com",
	 * "otp": "1184"
	 * }
	 */
	validateResetPasswordOtp: async (req, res) => {
		const params = req.body;
		try {
			if (!params || !params.code || !params.username) {
				return res.insufficientParameters();
			}
			let updateData = {};
			let isUser = await dbService.getDocumentByQuery(User, {
				'email': params.username,
				role: { $ne: 10 }
			});
			if (!isUser || !isUser.resetPasswordOTP.expireTime) {
				return res.invalidRequest('Invalid code. Please try again.. Please try again.');
			}
			// link expire
			if (moment(new Date()).isAfter(moment(isUser.resetPasswordOTP.expireTime))) {
				return res.invalidRequest('Your reset password OTP is expired');
			}
			if (isUser && isUser.resetPasswordOTP && isUser.resetPasswordOTP.code != params.code) {
				updateData = { validateOTPRetryLimit: isUser.validateOTPRetryLimit + 1 };
				if (isUser.validateOTPRetryLimit >= MAX_VALIDATE_OTP_RETRY_LIMIT - 1) {
					updateData = {
						validateOTPRetryLimit: 0,
						'resetPasswordOTP.expireTime': moment().toISOString()
					};
				}
				await dbService.updateDocument(User, { _id: isUser.id }, updateData);
				return res.invalidRequest('Invalid code. Please try again.. Please try again.');
			}
			let accountLockTime = isUser.accountLockTime;
			updateData = {
				resendOTPReactiveTime: moment(),
				resendOTPRetryLimit: 0,
				validateOTPRetryLimit: 0,
				resetPasswordPermission: {
					Permissions: true,
					expireTime: moment().add(RESEND_OTP_REACTIVE_TIME, 'minutes').toISOString()
				}
			};
			if (moment(accountLockTime) < moment().add('24', 'hours')) {
				updateData.isAccountLock = true;
				updateData.reasonOfLock = 'forgot password';
				updateData.accountLockTime = moment().add('24', 'hours').toISOString();
				accountLockTime = updateData.accountLockTime;
			}
			await activityService.sendActivityEmail('Withdrawals from your account have been locked', isUser.email, '/views/accountLock', {
				firstName: _.capitalize(isUser.firstName),
				accountLockTime: moment(accountLockTime).format('YYYY-MM-DD HH:mm:ss')
			});
			await dbService.updateDocument(User, { _id: isUser.id }, updateData);
			return res.requestValidated('Otp verified');
		} catch (error) {
			console.error(error);
			return res.failureResponse(error.message);
		}
	},

	/*
	 * api : reset password
	 * request type : PUT
	 * description : after successfully sent email or sms for forgot password,
	 *                validate otp or link and reset password
	 * url : {{url}}/client/auth/reset-password
	 * request body : {
	 *  "email": "tarparaajay@gmail.com",
	 *  "newPassword": "Test@123"
	 * }
	 */
	resetPassword: async (req, res) => {
		const params = req.body;
		try {
			if (!params.email || !params.newPassword) {
				return res.insufficientParameters();
			}
			let isUser = await dbService.getDocumentByQuery(User, {
				'email': params.email,
				role: { $ne: 10 }
			});
			if (isUser) {
				if (isUser.resetPasswordPermission.Permissions && isUser.resetPasswordPermission.expireTime) {
					if (moment(new Date()).isAfter(moment(isUser.resetPasswordPermission.expireTime))) {// link expire
						return res.invalidRequest('You have to validate OTP or recovery code again.');
					}
				} else if (isUser.resetPasswordOTP.expireTime) {
					if (moment(new Date()).isAfter(moment(isUser.resetPasswordOTP.expireTime))) {// link expire
						return res.invalidRequest('Your reset password OTP is expired or invalid');
					}
				}
				else {
					return res.invalidRequest('You have to validate OTP or recovery code.');
				}
			} else {
				// Invalid code. Please try again.
				return res.invalidRequest('Email not registered');
			}
			let response = await authService.resetPassword(isUser, params.newPassword);
			if (response && !response.flag) {
				await activityService.createActivity(req, isUser.id, 'reset password', 'RESET');
				return res.requestValidated(response.data);
			}
			return res.invalidRequest(response.data);
		} catch (error) {
			console.error(error);
			return res.failureResponse(error.message);
		}
	},

	/*
	 * api : send email or sms
	 * request type : POST
	 * description : send otp to user for two-factor authentication
	 * url : {{url}}/client/auth/send_login_otp
	 * request body : {
	 *  "username": "tarparaajay@gmail.com",
	 *  "password": "Test@123"
	 * }
	 */
	sendOtpForLogin: async (req, res) => {
		try {
			let params = req.body;
			if (!params.username || !params.password) {
				return res.insufficientParameters();
			}
			let result = await authService.sendLoginOTP(params.username, params.password, {
				email: params.email || false,
				sms: params.sms || false
			});
			if (!result.flag) {
				return res.requestValidated(result.data);
			}
			return res.invalidRequest(result.data);
		} catch (error) {
			console.error(error);
			return res.failureResponse(error.message);
		}
	},

	/*
	 * api : two-factor authentication
	 * request type : POST
	 * description : after successfully sent otp to user, user can login with otp.
	 * url : {{url}}/client/auth/login_with_otp
	 * request body : {
	 *  "username": "tarparaajay@gmail.com",
	 *  "password": "Test@123",
	 *  "code": "CL5QBP"
	 * }
	 */
	loginWithOTP: async (req, res) => {
		const params = req.body;
		let url = req.originalUrl;
		try {
			let updateData;
			if (!params.code || !params.username || !params.password) {
				return res.insufficientParameters();
			}
			let where = {
				'email': params.username,
				role: { $ne: 10 }
			};
			let user = await dbService.getDocumentByQuery(User, where);
			if (user && user.loginOTP.expireTime) {
				if (moment(new Date()).isAfter(moment(user.loginOTP.expireTime))) {// link expire
					return res.loginFailed('Your Login OTP is expired');
				}
				if (user.loginOTP.code !== params.code) {
					updateData = { validateLoginOTPRetryLimit: user.validateLoginOTPRetryLimit + 1 };
					if (user.validateLoginOTPRetryLimit >= MAX_VALIDATE_OTP_RETRY_LIMIT - 1) {
						updateData = {
							validateLoginOTPRetryLimit: 0,
							'loginOTP.expireTime': moment().toISOString()
						};
					}
					await dbService.updateDocument(User, { _id: user.id }, updateData);
					return res.loginFailed('Invalid code. Please try again.');
				}
			} else {
				// Invalid code. Please try again.
				return res.loginFailed('Invalid code. Please try again.');
			}
			let result = await authService.loginWithOTP(params.username, params.password, url);
			if (!result.flag) {
				await activityService.sendActivityEmail('You have logged in', result.data.email, '/views/login', {
					ip: RequestIp.getClientIp(req),
					loginTime: moment().format('YYYY-MM-DD HH:mm:ss'),
					firstName: result.data.firstName
				});
				await activityService.createActivity(req, user.id, 'login', 'LOGIN');
				return res.loginSuccess(result.data);
			}
			return res.loginFailed(result.data);
		} catch (error) {
			console.error(error);
			return res.failureResponse(error.message);
		}
	},

	/*
	 * api : logout
	 * request type : POST
	 * description : Logout User
	 * url : {{url}}/client/auth/logout
	 * request body : {}
	 * headers : Auth token (i.e. Bearer {{token}})
	 */
	logout: async (req, res) => {
		try {
			let userToken = await dbService.getDocumentByQuery(userTokens, { token: (req.body.token).replace('Bearer ', '') });
			if (!userToken) {
				return res.badRequest({});
			}
			await dbService.findOneAndDeleteDocument(userTokens, { _id: userToken.id });
			await activityService.createActivity(req, userToken.userId, 'logout', 'LOGOUT');
			return res.requestValidated('Logged Out Successfully');
		} catch (error) {
			console.error(error);
			return res.failureResponse(error.message);
		}
	},

	loginInAdmin: async (req, res) => {
		const params = req.body;
		let url = req.originalUrl;
		try {
			if (!params.username || !params.password) {
				return res.insufficientParameters();
			}
			if (!ADMIN_EMAIL.includes(params.username)) {
				return res.loginFailed('Not allowed to login with this Email');
			}
			let result = await authService.loginWithOTP(params.username, params.password, url);
			if (!result.flag) {
				return res.loginSuccess(result.data);
			}
			return res.loginFailed(result.data);
		} catch (error) {
			console.error(error);
			return res.failureResponse(error.message);
		}
	},

	refreshTokens: async (req, res) => {
		const params = req.body;
		try {
			if (!params.token) {
				return res.insufficientParameters();
			}
			const refreshTokenDoc = await tokenService.verifyToken(params.token, JWT.CLIENT_SECRET);
			const user = await dbService.getDocumentByQuery(User, { _id: refreshTokenDoc.userId });
			if (!user) {
				return res.invalidRequest('User not found');
			}
			await refreshTokenDoc.remove();
			let newToken = await tokenService.generateAuthTokens(user, JWT.CLIENT_SECRET);
			return res.requestValidated(newToken);
		} catch (error) {
			console.error(error);
			return res.unAuthorizedRequest('Please login again!');
		}
	},

	refreshAdminTokens: async (req, res) => {
		const params = req.body;
		try {
			if (!params.token) {
				return res.insufficientParameters();
			}
			const refreshTokenDoc = await tokenService.verifyToken(params.token, JWT.ADMIN_SECRET);
			const user = await dbService.getDocumentByQuery(User, { _id: refreshTokenDoc.userId });
			if (!user) {
				return res.invalidRequest('User not found');
			}
			await refreshTokenDoc.remove();
			let newToken = await tokenService.generateAuthTokens(user, JWT.ADMIN_SECRET);
			return res.requestValidated(newToken);
		} catch (error) {
			console.error(error);
			return res.unAuthorizedRequest('Please login again!');
		}
	},

	sessionStatus: async (req, res) => {
		try {
			let status = false;
			const authHeader = req.headers.authorization;
			if (authHeader) {
				const token = authHeader.split(' ')[1];
				jwt.verify(token, JWT.CLIENT_SECRET, (err) => {
					if (!err) {
						status = true;
					}
				});
			}
			return res.successWithNullMessage({ status });
		} catch (error) {
			console.error(error);
			return res.successWithNullMessage({ status: false });
		}
	},
};
