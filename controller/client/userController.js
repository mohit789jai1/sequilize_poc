const User = require('../../model/user');
const userSchemaKey = require('../../utils/validation/userValidation');
const validation = require('../../utils/validateRequest');
const dbService = require('../../utils/dbService');
const auth = require('../../services/auth');
const authService = require('../../services/auth');
const moment = require('moment');
const {
 uniqueValidation, encryption 
} = require('../../utils/common');
const {
 MAX_RESEND_OTP_RETRY_LIMIT, RESEND_OTP_REACTIVE_TIME, MAX_VALIDATE_OTP_RETRY_LIMIT 
} = require('../../constants/authConstant');
const common = require('../../utils/common');
const activityService = require('../../services/activity');
const growSruf = require('../../utils/growSruf');
const _ = require('lodash');

module.exports = {
  /*
   * api: get user
   * request type : GET
   * url : {{url}}/client/api/v1/user/6177c67cd47f8b6fb34eba81
   * headers : Auth token (i.e. Bearer {{token}})
   * description : get user which id passed in api.
   */
  getUser: async (req, res) => {
    try {
      let loginUser = req.user;
      if (!loginUser) {
        return res.unAuthorizedRequest();
      }
      if (loginUser.id == req.params.id) {
        let query = {};
        query._id = req.params.id;
        let result = await dbService.getDocumentByQuery(User, query);
        if (result) {
          return res.ok(result);
        }
      }
      return res.recordNotFound('user not Found or token not match with this user');
    }
    catch (error) {
      console.error(error);
      return res.failureResponse(error.message);
    }
  },

  /*
   * api: update user
   * request type : PUT
   * url : {{url}}/client/api/v1/user/update/6177c67cd47f8b6fb34eba81
   * headers : Auth token (i.e. Bearer {{token}})
   * request body : {
   *    "password": "Test@123", (ignore this line while updating)
   *    "email": "test1@gmail.com",
   *    "firstName": "TEST",
   *    "phoneNo": "+44521125638",
   *    "lastName": "USER",
   *    "role": 1
   * }
   * description : update user which id passed in api.
   */
  updateUser: async (req, res) => {
    try {
      delete req.body['addedBy'];
      delete req.body['updatedBy'];
      delete req.body['password'];
      let data = {
        ...req.body,
        id: req.params.id
        , updatedBy: req.user.id
      };
      let isValid = validation.validateParamsWithJoi(
        data,
        userSchemaKey.updateSchemaKeys
      );
      if (isValid.error) {
        return res.inValidParam(isValid.error);
      }
      let query = { _id: req.params.id };
      let result = await dbService.findOneAndUpdateDocument(User, query, data, { new: true });
      if (!result) {
        return res.recordNotFound({});
      }
      return res.ok(result);
    }
    catch (error) {
      if (error.name === 'ValidationError') {
        return res.validationError(error.message);
      }
      else if (error.code && error.code == 11000) {
        return res.isDuplicate(error.message);
      }
      return res.failureResponse(error.message);
    }
  },

  /*
   * api: delete user
   * request type : PUT
   * url : {{url}}/client/api/v1/user/softDelete/6177c67cd47f8b6fb34eba81
   * headers : Auth token (i.e. Bearer {{token}})
   * description : soft delete user which id passed in api and set isActive flag false isDeleted true.
   * request body : {}
   */
  deleteUser: async (req, res) => {
    try {
      let query = { _id: req.params.id };
      let result = await dbService.deleteDocument(User, query);
      if (!result) {
        return res.recordNotFound({});
      }
      return res.ok(result);
    } catch (error) {
      return res.failureResponse(error.message);
    }
  },

  /*
   * api : send email of otp
   * request type : POST
   * description : send otp to user for verify email
   * url : {{url}}/client/auth/send_email_verify_otp
   * request body : {
   *  "email": "tarparaajay@gmail.com"
   *  }
   */
  sendEmailOTP: async (req, res) => {
    try {
      let loginUser = req.user;
      if (!loginUser) {
        return res.unAuthorizedRequest();
      }
      let result = await authService.sendEmailOtp(loginUser.email);
      if (!result.flag) {
        return res.requestValidated(result.data);
      }
      return res.invalidRequest(result.data);
    } catch (error) {
      console.error(error);
      return res.failureResponse(error.message);
    }
  },

  /*
   * api : send sms
   * request type : POST
   * description : send otp to user for verify phone number
   * url : {{url}}/client/auth/send_phone_no_verify_otp
   * request body : {
   *   "phoneNo": "+917878581381"
   * }
   */
  sendPhoneOTP: async (req, res) => {
    try {
      let loginUser = req.user;
      if (!loginUser) {
        return res.unAuthorizedRequest();
      }
      let result = await authService.sendPhoneNoOtp(loginUser.phoneNo, loginUser.email, false);
      if (!result.flag) {
        return res.requestValidated(result.data);
      }
      return res.invalidRequest(result.data);
    } catch (error) {
      console.error(error);
      return res.failureResponse(error.message);
    }
  },

  /*
   * api : verify email
   * request type : POST
   * description : after successfully sent otp to user, user can  verify email
   * url : {{url}}/client/auth/verify_email
   * request body : {
   * "email": "tarparaajay@gmail.com",
   * "code": "3V5M37"
   * }
   */
  verifyEmail: async (req, res) => {
    const params = req.body;
    try {
      let updateData;
      let loginUser = req.user;
      if (!loginUser) {
        return res.unAuthorizedRequest();
      }
      if (!params.code) {
        return res.insufficientParameters();
      }
      let user = loginUser;
      if (user && user.emailOTP.expireTime) {
        if (moment(new Date()).isAfter(moment(user.emailOTP.expireTime))) {// link expire
          return res.invalidRequest('Your OTP is expired');
        }
        if (user.emailOTP.code !== params.code) {
          updateData = { validateVerifyEmailOTPRetryLimit: user.validateVerifyEmailOTPRetryLimit + 1 };
          if (user.validateVerifyEmailOTPRetryLimit >= MAX_VALIDATE_OTP_RETRY_LIMIT - 1) {
            updateData = {
              validateVerifyEmailOTPRetryLimit: 0,
              'emailOTP.expireTime': moment().toISOString()
            };
          }
          await dbService.updateDocument(User, { _id: user.id }, updateData);
          return res.invalidRequest('Invalid code. Please try again.');
        }
      } else {
        // Invalid code. Please try again.
        return res.invalidRequest('Invalid code. Please try again.');
      }
      let result = await authService.verifyEmail(loginUser.email, params.code);
      if (!result.flag) {
        updateData = {
          verifyEmailResendOTPReactiveTime: moment().toISOString(),
          verifyEmailResendOTPRetryLimit: 0,
          validateVerifyEmailOTPRetryLimit: 0,
          settingEmailVerified: true
        };
        await dbService.updateDocument(User, { _id: user.id }, updateData);
        return res.requestValidated(result.data);
      }
      return res.invalidRequest(result.data);
    } catch (error) {
      console.error(error);
      return res.failureResponse(error.message);
    }
  },

  /*
   * api : verify phone number
   * request type : POST
   * description : after successfully sent otp to user, user can verify phone number
   * url : {{url}}/client/auth/verify_phone_no
   * request body : {
   * "phoneNo": "+447932935146",
   * "code": "7J1D3U"
   * }
   */
  verifyPhoneNo: async (req, res) => {
    const params = req.body;
    let updateData;
    try {
      let loginUser = req.user;
      if (!loginUser) {
        return res.unAuthorizedRequest();
      }
      if (!params.code) {
        return res.insufficientParameters();
      }
      let user = loginUser;
      if (user && user.phoneNoOTP && user.phoneNoOTP.expireTime) {
        if (moment(new Date()).isAfter(moment(user.phoneNoOTP.expireTime))) {// link expire
          return res.invalidRequest('Your OTP is expired');
        }
        if (user.phoneNoOTP.code !== params.code) {
          updateData = { validateVerifyPhoneNoOTPRetryLimit: user.validateVerifyPhoneNoOTPRetryLimit + 1 };
          if (user.validateVerifyPhoneNoOTPRetryLimit >= MAX_VALIDATE_OTP_RETRY_LIMIT - 1) {
            updateData = {
              validateVerifyPhoneNoOTPRetryLimit: 0,
              'phoneNoOTP.expireTime': moment().toISOString()
            };
          }
          await dbService.updateDocument(User, { _id: user.id }, updateData);
          return res.invalidRequest('Invalid code. Please try again.');
        }
      } else {
        // Invalid code. Please try again.
        return res.invalidRequest('Invalid code. Please try again.');
      }
      let result = await authService.verifyPhoneNo(loginUser.phoneNo, params.code);
      if (!result.flag) {
        updateData = {
          verifyPhoneNoResendOTPReactiveTime: moment(),
          verifyPhoneNoResendOTPRetryLimit: 0,
          validateVerifyPhoneNoOTPRetryLimit: 0,
          settingPhoneVerified: true
        };
        await dbService.updateDocument(User, { _id: user.id }, updateData);
        return res.requestValidated(result.data);
      }
      return res.invalidRequest(result.data);
    } catch (error) {
      console.error(error);
      return res.failureResponse(error.message);
    }
  },

  /*
   * api: change password of user
   * request type : PUT
   * url : {{url}}/client/api/v1/user/change-password
   * headers : Auth token (i.e. Bearer {{token}})
   * description : change password of user which user token passed in API.
   * request body : {
   * "oldPassword": "Test@123",
   * "newPassword": "Test@1234"
   * }
   */
  changePassword: async (req, res) => {
    try {
      let params = req.body;
      let loginUser = req.user;
      if (!loginUser) {
        return res.unAuthorizedRequest();
      }
      if (!params.newPassword || !req.user.id) {
        return res.inValidParam('Please Provide userId and new Password');
      }
      if (!loginUser.settingPhoneVerified || !loginUser.settingEmailVerified) {
        return res.invalidRequest('Please verify email and phone no first');
      }
      let result = await auth.changePassword({
        ...params,
        userId: req.user.id
      });
      if (result.flag) {
        return res.invalidRequest(result.data);
      }
      await activityService.createActivity(req, loginUser.id, 'change password', 'SETTING');
      return res.requestValidated(result.data);
    } catch (error) {
      console.error(error);
      return res.failureResponse(error);
    }
  },

  /*
   * api : send alt email of otp
   * request type : POST
   * description : send otp to user for verify alt email
   * url : {{url}}/client/api/v1/user/send_alt_email_otp/
   * request body : {
   *  "email": "tarparaajay@gmail.com"
   *  }
   */
  sendAltEmailOTP: async (req, res) => {
    let params = req.body;
    try {
      let loginUser = req.user;
      if (!loginUser) {
        return res.unAuthorizedRequest();
      }
      if (!loginUser.settingPhoneVerified || !loginUser.settingEmailVerified) {
        return res.invalidRequest('Please verify email and phone no first');
      }
      if (!params.email) {
        return res.insufficientParameters();
      }
      let unique = await uniqueValidation(User, { email: params.email });
      if (!unique) {
        return res.inValidParam('Sorry! That email is already in use on the platform. Please try again.');
      }
      let result = await authService.sendEmailOtp(loginUser.email, false, params.email);
      if (!result.flag) {
        return res.requestValidated(result.data);
      }
      return res.invalidRequest(result.data);
    } catch (error) {
      console.error(error);
      return res.failureResponse(error.message);
    }
  },

  changeEmail: async (req, res) => {
    const params = req.body;
    try {
      let updateData;
      let user = req.user;
      if (!user) {
        return res.unAuthorizedRequest();
      }
      if (!user.settingPhoneVerified || !user.settingEmailVerified) {
        return res.invalidRequest('Please verify email and phone no first');
      }
      if (!params.code) {
        return res.insufficientParameters();
      }
      if (user.emailOTP.expireTime) {
        if (moment(new Date()).isAfter(moment(user.emailOTP.expireTime))) {// link expire
          return res.invalidRequest('Your OTP is expired');
        }
        if (user.emailOTP.code !== params.code) {
          updateData = { validateVerifyEmailOTPRetryLimit: user.validateVerifyEmailOTPRetryLimit + 1 };
          if (user.validateVerifyEmailOTPRetryLimit >= MAX_VALIDATE_OTP_RETRY_LIMIT - 1) {
            updateData = {
              validateVerifyEmailOTPRetryLimit: 0,
              'emailOTP.expireTime': moment().toISOString()
            };
          }
          await dbService.updateDocument(User, { _id: user.id }, updateData);
          return res.invalidRequest('Invalid code. Please try again.');
        }
      } else {
        // Invalid code. Please try again.
        return res.invalidRequest('Invalid code. Please try again.');
      }
      let result = await authService.verifyEmail(user.email, params.code);
      if (!result.flag) {
        let accountLockTime = user.accountLockTime;
        updateData = {
          verifyEmailResendOTPReactiveTime: moment().toISOString(),
          verifyEmailResendOTPRetryLimit: 0,
          validateVerifyEmailOTPRetryLimit: 0,
          settingEmailVerified: false,
          settingPhoneVerified: false,
          email: user.altEmail,
          altEmail: null
        };
        if (moment(accountLockTime) < moment().add('24', 'hours')) {
          updateData.isAccountLock = true;
          updateData.reasonOfLock = 'change email';
          updateData.accountLockTime = moment().add('24', 'hours').toISOString();
          accountLockTime = updateData.accountLockTime;
        }
        await activityService.sendActivityEmail('Withdrawals from your account have been locked', user.email, '/views/accountLock', {
          firstName: _.capitalize(user.firstName),
          accountLockTime: moment(accountLockTime).format('YYYY-MM-DD HH:mm:ss')
        });
        await dbService.updateDocument(User, { _id: user.id }, updateData);
        await activityService.createActivity(req, user.id, 'change email', 'SETTING');
        return res.requestValidated(result.data);
      }
      return res.invalidRequest(result.data);
    } catch (error) {
      console.error(error);
      return res.failureResponse(error.message);
    }
  },

  /*
   * api : send sms
   * request type : POST
   * description : send otp to user for verify phone number
   * url : {{url}}/client/auth/send_phone_no_verify_otp
   * request body : {
   *   "phoneNo": "+917878581381"
   * }
   */
  sendAltPhoneOTP: async (req, res) => {
    let params = req.body;
    try {
      let loginUser = req.user;
      if (!loginUser) {
        return res.unAuthorizedRequest();
      }
      if (!loginUser.settingPhoneVerified || !loginUser.settingEmailVerified) {
        return res.invalidRequest('Please verify email and phone no first');
      }
      let unique = await uniqueValidation(User, { phoneNo: params.phoneNo });
      if (!unique) {
        return res.inValidParam('Sorry! That phone number is already in use on the platform. Please try again.');
      }
      let result = await authService.sendPhoneNoOtp(params.phoneNo, loginUser.email, false, true);
      if (!result.flag) {
        return res.requestValidated(result.data);
      }
      return res.invalidRequest(result.data);
    } catch (error) {
      console.error(error);
      return res.failureResponse(error.message);
    }
  },

  changePhoneNo: async (req, res) => {
    const params = req.body;
    let updateData;
    try {
      let user = req.user;
      if (!user) {
        return res.unAuthorizedRequest();
      }
      if (!params.code) {
        return res.insufficientParameters();
      }
      if (!user.settingPhoneVerified || !user.settingEmailVerified) {
        return res.invalidRequest('Please verify email and phone no first');
      }
      if (user.phoneNoOTP.expireTime) {
        if (moment(new Date()).isAfter(moment(user.phoneNoOTP.expireTime))) {// link expire
          return res.invalidRequest('Your OTP is expired');
        }
        if (user.phoneNoOTP.code !== params.code) {
          updateData = { validateVerifyPhoneNoOTPRetryLimit: user.validateVerifyPhoneNoOTPRetryLimit + 1 };
          if (user.validateVerifyPhoneNoOTPRetryLimit >= MAX_VALIDATE_OTP_RETRY_LIMIT - 1) {
            updateData = {
              validateVerifyPhoneNoOTPRetryLimit: 0,
              'phoneNoOTP.expireTime': moment().toISOString()
            };
          }
          await dbService.updateDocument(User, { _id: user.id }, updateData);
          return res.invalidRequest('Invalid code. Please try again.');
        }
      } else {
        // Invalid code. Please try again.
        return res.invalidRequest('Invalid code. Please try again.');
      }
      updateData = {
        verifyPhoneNoResendOTPReactiveTime: moment(),
        verifyPhoneNoResendOTPRetryLimit: 0,
        validateVerifyPhoneNoOTPRetryLimit: 0,
        settingEmailVerified: false,
        settingPhoneVerified: false,
        phoneNo: user.altPhoneNo,
        altPhoneNo: null,
        phoneNoOTP: null
      };
      let accountLockTime = user.accountLockTime;
      if (moment(accountLockTime) < moment().add('24', 'hours')) {
        updateData.isAccountLock = true;
        updateData.reasonOfLock = 'change phone number';
        updateData.accountLockTime = moment().add('24', 'hours').toISOString();
        accountLockTime = updateData.accountLockTime;
      }
      await activityService.sendActivityEmail('Withdrawals from your account have been locked', user.email, '/views/accountLock', {
        firstName: _.capitalize(user.firstName),
        accountLockTime: moment(accountLockTime).format('YYYY-MM-DD HH:mm:ss')
      });
      let update = await dbService.updateDocument(User, { _id: user.id }, updateData);
      await activityService.createActivity(req, user.id, 'change phone number', 'SETTING');
      if (update) {
        return res.requestValidated('Phone Number verified successfully');
      }
      return res.invalidRequest('Error while updating data');
    } catch (error) {
      console.error(error);
      return res.failureResponse(error.message);
    }
  },

  generateNewRecoveryCode: async (req, res) => {
    try {
      let user = req.user;
      if (!user) {
        return res.unAuthorizedRequest();
      }
      let recoveryAddress = common.generateCode();
      if (user) {
        if (user.settingPhoneVerified && user.settingEmailVerified) {
          await dbService.updateDocument(User, { _id: user.id },
            {
              recoveryAddress: encryption(recoveryAddress),
              isRecoveryAddressExists: true
            });
          await activityService.createActivity(req, user.id, 'generate new recovery code', 'SETTING');
          return res.requestValidated({
 recoveryAddress: recoveryAddress,
message: 'New recovery Code generated successfully !!' 
});
        } else {
          return res.invalidRequest('Email or Phone number not verified');
        }
      } else {
        // User not found
        return res.invalidRequest('User Not Found');
      }
    } catch (error) {
      console.error(error);
      return res.failureResponse(error.message);
    }
  },
  /*
   * api : resend OTP
   * request type : POST
   * description : re-send OTP on  email or sms to user for forgot password.
   * url : {{url}}/client/auth/resend-otp
   * request body : {
   *    "email": "tarparaajay@gmail.com"
   *  }
   */
  resendOtp: async (req, res) => {
    const params = req.body;
    try {
      let isUser = req.user;
      if (!isUser) {
        return res.unAuthorizedRequest();
      }
      let reactiveTimeKey, retryLimitKey;
      let email = isUser.email;
      let phoneNo = isUser.phoneNo;
      if (params.verifyEmail) {
        reactiveTimeKey = 'verifyEmailResendOTPReactiveTime';
        retryLimitKey = 'verifyEmailResendOTPRetryLimit';
        if (params.verifyAltEmail && isUser.altEmail) { email = isUser.altEmail; }
      } else if (params.verifyPhoneNo) {
        reactiveTimeKey = 'verifyPhoneNoResendOTPReactiveTime';
        retryLimitKey = 'verifyPhoneNoResendOTPRetryLimit';
        if (params.verifyAltPhoneNo && isUser.altPhoneNo) { phoneNo = isUser.altPhoneNo; }
      } else {
        return res.insufficientParameters();
      }
      if (isUser) {
        let updateData;
        if (moment(isUser[reactiveTimeKey] ? isUser[reactiveTimeKey] : 0) < moment() && isUser[retryLimitKey] < MAX_RESEND_OTP_RETRY_LIMIT) {
          updateData = { [retryLimitKey]: isUser[retryLimitKey] + 1 };
          if (isUser[retryLimitKey] == MAX_RESEND_OTP_RETRY_LIMIT - 1)
            updateData = {
              [reactiveTimeKey]: moment().add(RESEND_OTP_REACTIVE_TIME, 'minutes').toISOString(),
              [retryLimitKey]: 0
            };
          await dbService.updateDocument(User, { _id: isUser.id }, updateData);
          let resultOfEmail = false, resultOfSMS = false;
          if (params.verifyEmail) {
            let output = await authService.sendVerifyEmailOtp(isUser.id, email);
            if (output) { resultOfEmail = true; }
          } else if (params.verifyPhoneNo) {
            let output = await authService.sendVerifyPhoneNoOtp(isUser.id, phoneNo);
            if (output) { resultOfSMS = true; }
          }
          if (resultOfEmail && resultOfSMS) {
            return res.requestValidated('otp successfully send.');
          } else if (resultOfEmail && !resultOfSMS) {
            return res.requestValidated('otp successfully send to your email.');
          } else if (!resultOfEmail && resultOfSMS) {
            return res.requestValidated('otp successfully send to your mobile number.');
          } else {
            return res.invalidRequest('otp can not be sent due to some issue try again later');
          }
        }
        else {
          return res.invalidRequest(`You have exceeded the limit for requesting OTP codes. Please try again in  ${common.getDifferenceOfTwoDatesInTime(moment(), moment(isUser[reactiveTimeKey]))}.`);
        }
      } else {
        return res.recordNotFound({});
      }
    } catch (error) {
      console.error(error);
      return res.failureResponse(error);
    }
  },

  getUserByFilter: async (req, res) => {
    try {
      let options = {};
      let query = {};
      let result;
      if (req.body.query !== undefined) {
        query = {
 ...req.body.query,
...{ 'role': 1 } 
};
      }
      if (req.body.isCountOnly) {
        result = await dbService.countDocument(User, query);
        if (result) {
          result = { totalRecords: result };
          return res.ok(result);
        }
        return res.recordNotFound({});
      }
      else {
        if (req.body.options !== undefined) {
          options = { ...req.body.options };
        }
        options.customLabels = {};
        options.pagination = true;
        options.select = ['firstName', 'lastName', 'dob', 'phoneNo', 'email', 'isEmailVerified', 'isPhoneNoVerified', 'isKycVerified', 'custodialWalletAddress', 'loginRetryLimit', 'kycInqId', 'kycStatus', 'isActive', 'isDeleted', 'createdAt'];
        result = await dbService.getAllDocuments(User, query, options);
        if (result && result.docs && result.docs.length) {
          return res.ok(result);
        }
        return res.recordNotFound({});
      }
    }
    catch (error) {
      return res.failureResponse(error.message);
    }
  },

  getUserReferralData: async (req, res) => {
    try {
      let user = req.user;
      if (!user) {
        return res.unAuthorizedRequest();
      }
      if (user.referralId) {
        let referralData = await growSruf.getReferralData(user.referralId);
        res.ok(referralData);
      } else {
        // User not found
        return res.invalidRequest('User Not Found');
      }
    } catch (error) {
      console.error(error);
      return res.failureResponse(error.message);
    }
  }
};