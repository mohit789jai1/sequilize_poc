const fileService = require('../../services/file');
const _ = require('lodash');
const mongoose = require('mongoose');
const slugify = require('@sindresorhus/slugify');
const { USER_ROLE } = require('../../constants/authConstant');

module.exports = {
    getFileByTypeWithOutAuth: async (req, res) => {
        try {
            if (req.query.type) {
                const file = await fileService.findByType(req.query.type);
                if (!file) {
                    throw new Error('File not found');
                }
                return res.ok(file);
            }
            return res.recordNotFound({});
        }
        catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },
    getFile: async (req, res) => {
        try {
            if (req.user && req.user.role != USER_ROLE.Admin) {
                return res.unAuthorizedRequest();
            }
            if (req.params.id) {
                const file = await fileService.getFileById(req.params.id);
                if (!file) {
                    throw new Error('File not found');
                }
                return res.ok(file);
            }
            return res.recordNotFound({});
        }
        catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },

    createFile: async (req, res) => {
        try {
            if (req.user && req.user.role != USER_ROLE.Admin) {
                return res.unAuthorizedRequest();
            }
            if (!req.files.length || !req.body.name) {
                return res.insufficientParameters();
            }
            const files = [];
            for (const file of req.files) {
                const id = mongoose.Types.ObjectId();
                let fileData = await fileService.uploadFile(file, id);
                if (fileData && ['image/jpeg', 'application/pdf', 'image/png', 'video/mp4'].includes(file.mimetype)) {
                    const fileObj = {
                        _id: id,
                        name: req.body.name || fileData.key,
                        uri: fileData.Location,
                        mimeType: file.mimetype,
                        size: file.size,
                        slug: slugify(`${fileData.key} ${id}`),
                        uploadedBy: req.user.id,
                        type: req.body.type || null,
                        link: req.body.link || null,
                        title: req.body.title || null,
                        subTitle: req.body.subTitle || null,
                        description: req.body.description || null,
                        posterId: req.body.posterId || null
                    };
                    files.push(fileObj);
                } else {
                    return res.invalidRequest('File type is not supported.');
                }

            }
            const createdFiles = await fileService.createFile(files);
            let result = await fileService.find({ _id: { $in: _.map(createdFiles, 'id') } });
            return res.ok(result);
        } catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },

    updateFile: async (req, res) => {
        try {
            if (req.user && req.user.role != USER_ROLE.Admin) {
                return res.unAuthorizedRequest();
            }
            if (!req.params.id) {
                return res.insufficientParameters();
            }
            delete req.body['addedBy'];
            delete req.body['updatedBy'];
            delete req.body['uri'];
            delete req.body['mimeType'];
            delete req.body['size'];
            let data = {
                ...req.body,
                updatedBy: req.user.id
            };
            if (req.files) {
                for (const file of req.files) {
                    const id = mongoose.Types.ObjectId();
                    let fileData = await fileService.uploadFile(file, id);
                    if (fileData && ['image/jpeg', 'application/pdf', 'image/png', 'video/mp4'].includes(file.mimetype)) {
                        data.name = req.body.name || fileData.key;
                        data.uri = fileData.Location;
                        data.mimeType = file.mimetype;
                        data.size = file.size;
                        data.slug = slugify(`${fileData.key || fileData.Key} ${id}`);
                    }
                }
            }
            let result = await fileService.updateFileById(req.params.id, data);
            if (!result) {
                return res.recordNotFound({});
            }
            result = await fileService.getFileById(req.params.id);
            return res.ok(result);
        }
        catch (error) {
            return res.failureResponse(error.message);
        }
    },

    deleteFile: async (req, res) => {
        try {
            if (req.user && req.user.role != USER_ROLE.Admin) {
                return res.unAuthorizedRequest();
            }
            if (!req.params.id) {
                return res.insufficientParameters();
            }
            let result = await fileService.deleteFileById(req.params.id);
            if (result) {
                return res.ok('File deleted Successfully!');
            }
            return res.recordNotFound({});
        }
        catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },
};
