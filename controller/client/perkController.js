const Perk = require('../../model/perk');
const perkService = require('../../services/perk');
const dbService = require('../../utils/dbService');
const _ = require('lodash');
const { USER_ROLE } = require('../../constants/authConstant');

module.exports = {
    getPerk: async (req, res) => {
        try {
            if (req.user && req.user.role != USER_ROLE.Admin) {
                return res.unAuthorizedRequest();
            }
            if (req.params.id) {
                let result = await perkService.findById(req.params.id, ['portfolioPerkIcon']);
                if (result) {
                    return res.ok(result);
                }
            }
            return res.badRequest({});
        }
        catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },

    createPerk: async (req, res) => {
        try {
            if (req.user && req.user.role != USER_ROLE.Admin) {
                return res.unAuthorizedRequest();
            }
            let data = req.body;
            let result = await dbService.createDocument(Perk, data);
            if (!result) {
                return res.badRequest({});
            }
            result = await perkService.findById(result.id, ['portfolioPerkIcon']);
            return res.ok(result);
        }
        catch (error) {
            return res.failureResponse(error.message);
        }
    },

    updatePerk: async (req, res) => {
        try {
            if (req.user && req.user.role != USER_ROLE.Admin) {
                return res.unAuthorizedRequest();
            }
            if (!req.params.id) {
                return res.insufficientParameters();
            }
            delete req.body['addedBy'];
            delete req.body['updatedBy'];
            let data = {
                ...req.body,
                id: req.params.id,
                updatedBy: req.user.id
            };
            let query = { _id: req.params.id };
            let result = await dbService.findOneAndUpdateDocument(Perk, query, data, { new: false });
            if (!result) {
                return res.recordNotFound({});
            }
            result = await perkService.findById(req.params.id, ['portfolioPerkIcon']);
            return res.ok(result);
        }
        catch (error) {
            return res.failureResponse(error.message);
        }
    },

    deletePerk: async (req, res) => {
        try {
            if (req.user && req.user.role != USER_ROLE.Admin) {
                return res.unAuthorizedRequest();
            }
            if (!req.params.id) {
                return res.insufficientParameters();
            }
            let query = {};
            query._id = req.params.id;
            if (!_.isEmpty(query)) {
                let result = await perkService.deleteOne(query);
                if (result) {
                    return res.ok('Perk deleted Successfully!');
                }
            }
            return res.recordNotFound({});
        }
        catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },
};
