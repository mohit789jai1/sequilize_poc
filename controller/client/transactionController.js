const transactionService = require('../../services/transaction/transaction');
const historyTransactionService = require('../../services/transaction/historyTransaction');
const BNBTransactionService = require('../../services/transaction/BNBTransaction');
const swapTransactionService = require('../../services/transaction/swapTransaction');
const bricksTransactionService = require('../../services/transaction/bricksTransaction');
const _ = require('lodash');
const dbService = require('../../utils/dbService');
const User = require('../../model/user');
const activityService = require('../../services/activity');
const common = require('../../utils/common');
const moment = require('moment');

module.exports = {
    getTransactionHistory: async (req, res) => {
        try {
            let loginUser = req.user;
            if (!loginUser) {
                return res.unAuthorizedRequest();
            }
            let transaction = await historyTransactionService.getHistory(loginUser);
            if (!transaction.flag) {
                return res.requestValidated(transaction.data);
            }
            return res.invalidRequest(transaction.data);
        } catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },

    getTransactionDetail: async (req, res) => {
        let params = req.params;
        try {
            let loginUser = req.user;
            if (!loginUser) {
                return res.unAuthorizedRequest();
            }
            let transaction = await historyTransactionService.getTransactionDetail(loginUser.id, params.id);
            if (!transaction.flag) {
                return res.requestValidated(transaction.data);
            }
            return res.invalidRequest(transaction.data);
        } catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },

    storeMoonPayTransaction: async (req, res) => {
        let params = req.params;
        try {
            let loginUser = req.user;
            if (!loginUser) {
                return res.unAuthorizedRequest();
            }
            if (!params.id) {
                return res.insufficientParameters();
            }
            let transaction = await historyTransactionService.storeMoonPayTransaction(loginUser, params.id);
            if (!transaction.flag) {
                return res.requestValidated(transaction.data);
            }
            return res.invalidRequest(transaction.data);
        } catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },

    createEthTrans: async (req, res) => {
        const params = req.body;
        let loginUser = req.user;
        try {
            if (!loginUser) {
                return res.unAuthorizedRequest();
            }
            // if (!loginUser.settingPhoneVerified || !loginUser.settingEmailVerified) {
            //     return res.invalidRequest('Please verify email and phone no first');
            // }
            if (!params.to || !params.value) {
                return res.insufficientParameters();
            }
            if (!loginUser.isCustodialWalletExists) {
                return res.invalidRequest('Custodial wallet address not exists.');
            }
            let from = req.user.custodialWalletAddress;
            await dbService.updateDocument(User, { _id: loginUser.id }, {
                settingEmailVerified: false,
                settingPhoneVerified: false,
                transInProgress: true
            });
            let transaction = await BNBTransactionService.transferBNB(loginUser, from, params.to, params.value);
            if (!transaction.flag) {
                await dbService.updateDocument(User, { _id: loginUser.id }, { transInProgress: false });
                await activityService.createActivity(req, loginUser.id, 'Withdraw BNB', 'TRANSACTION');
                return res.requestValidated(transaction.data);
            }
            await dbService.updateDocument(User, { _id: loginUser.id }, { transInProgress: false });
            return res.invalidRequest(transaction.data);
        } catch (error) {
            console.error(error);
            await dbService.updateDocument(User, { _id: loginUser.id }, { transInProgress: false });
            return res.failureResponse(error.message);
        }
    },

    BNBtoBNBCalculation: async (req, res) => {
        const params = req.body;
        try {
            let loginUser = req.user;
            if (!loginUser) {
                return res.unAuthorizedRequest();
            }
            if (!params.to || !params.value) {
                return res.insufficientParameters();
            }
            if (!loginUser.isCustodialWalletExists) {
                return res.invalidRequest('Custodial wallet address not exists.');
            }
            let transaction = await BNBTransactionService.BNBtoBNBCalculation(loginUser.custodialWalletAddress, params.to, params.value);
            if (!transaction.flag) {
                return res.requestValidated(transaction.data);
            }
            return res.invalidRequest(transaction.data);
        } catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },

    changePolicy: async (req, res) => {
        try {
            let loginUser = req.user;
            let params = req.body;
            if (!loginUser) {
                return res.unAuthorizedRequest();
            }
            if (params.publicKey && !params.publicKey.length) {
                return res.insufficientParameters();
            }
            let transaction = await transactionService.changePolicy(process.env.WALLET_ID, params.publicKey);
            if (!transaction.flag) {
                return res.requestValidated(transaction.data);
            }
            return res.invalidRequest(transaction.data);
        } catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },

    getPublicKey: async (req, res) => {
        try {
            let loginUser = req.user;
            if (!loginUser) {
                return res.unAuthorizedRequest();
            }
            let publicKey = await transactionService.getPublicKey(process.env.AWS_KEY_ID);
            if (publicKey) {
                return res.requestValidated(publicKey);
            }
            return res.invalidRequest('Failed to get public Key.');
        } catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },

    buyBNB: async (req, res) => {
        try {
            let loginUser = req.user;
            if (!loginUser) {
                return res.unAuthorizedRequest();
            }
            if (!loginUser.isCustodialWalletExists) {
                return res.invalidRequest('Custodial wallet address not exists.');
            }
            let transaction = await BNBTransactionService.buyBNB(loginUser.custodialWalletAddress, loginUser.email);
            if (!transaction.flag) {
                await activityService.createActivity(req, loginUser.id, 'call moonpay widget', 'TRANSACTION');
                return res.requestValidated(transaction.data);
            }
            return res.invalidRequest(transaction.data);
        } catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },

    BNBtoBricksCalculation: async (req, res) => {
        const params = req.body;
        try {
            let loginUser = req.user;
            if (!loginUser) {
                return res.unAuthorizedRequest();
            }
            if (!params.BNBInput) {
                return res.insufficientParameters();
            }
            if (!loginUser.isCustodialWalletExists) {
                return res.invalidRequest('Custodial wallet address not exists.');
            }
            let transaction = await swapTransactionService.BNBtoBricksCalculation(params.BNBInput, loginUser.custodialWalletAddress, 'SWAP');
            if (!transaction.flag) {
                return res.requestValidated(transaction.data);
            }
            return res.invalidRequest(transaction.data);
        } catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },

    BNBtoBricksTransaction: async (req, res) => {
        const params = req.body;
        let loginUser = req.user;
        try {
            if (!loginUser) {
                return res.unAuthorizedRequest();
            }
            if (!params.BNBInput) {
                return res.insufficientParameters();
            }
            if (!loginUser.isCustodialWalletExists) {
                return res.invalidRequest('Custodial wallet address not exists.');
            }
            await dbService.updateDocument(User, { _id: loginUser.id }, { transInProgress: true });
            let transaction = await swapTransactionService.BNBtoBricksTransaction(loginUser, params.BNBInput, loginUser.custodialWalletAddress);
            if (!transaction.flag) {
                await dbService.updateDocument(User, { _id: loginUser.id }, { transInProgress: false });
                return res.requestValidated(transaction.data);
            }
            await activityService.createActivity(req, loginUser.id, 'Swap BNB to Bricks', 'TRANSACTION');
            await dbService.updateDocument(User, { _id: loginUser.id }, { transInProgress: false });
            return res.invalidRequest(transaction.data);
        } catch (error) {
            console.error(error);
            await dbService.updateDocument(User, { _id: loginUser.id }, { transInProgress: false });
            return res.failureResponse(error.message);
        }
    },

    getBalance: async (req, res) => {
        try {
            let loginUser = req.user;
            if (!loginUser) {
                return res.unAuthorizedRequest();
            }
            if (!loginUser.isCustodialWalletExists) {
                return res.invalidRequest('Custodial wallet address not exists.');
            }
            let transaction = await transactionService.getBalance(loginUser.custodialWalletAddress);
            if (!transaction.flag) {
                return res.requestValidated(transaction.data);
            }
            return res.invalidRequest(transaction.data);
        } catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },

    BricksToBricksCalculation: async (req, res) => {
        const params = req.body;
        try {
            let loginUser = req.user;
            if (!loginUser) {
                return res.unAuthorizedRequest();
            }
            if (!params.BRICKSInput || !params.to) {
                return res.insufficientParameters();
            }
            if (!loginUser.isCustodialWalletExists) {
                return res.invalidRequest('Custodial wallet address not exists.');
            }
            let transaction = await bricksTransactionService.transactionPreRequiredCalculation(params.BRICKSInput * 1000000000, loginUser.custodialWalletAddress, params.to, 'BRICKS_TRANSFER');
            if (!transaction.flag) {
                return res.requestValidated(transaction.data);
            }
            return res.invalidRequest(transaction.data);
        } catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },

    BricksToBricksTransaction: async (req, res) => {
        const params = req.body;
        let loginUser = req.user;
        try {
            if (!loginUser) {
                return res.unAuthorizedRequest();
            }
            // if (!loginUser.settingPhoneVerified || !loginUser.settingEmailVerified) {
            //     return res.invalidRequest('Please verify email and phone no first');
            // }
            if (!params.BRICKSInput || !params.to) {
                return res.insufficientParameters();
            }
            if (!loginUser.isCustodialWalletExists) {
                return res.invalidRequest('Custodial wallet address not exists.');
            }
            await dbService.updateDocument(User, { _id: loginUser.id }, {
                settingEmailVerified: false,
                settingPhoneVerified: false,
                transInProgress: true
            });
            let transaction = await bricksTransactionService.BToBTransactionWithGas(loginUser, params.BRICKSInput * 1000000000, loginUser.custodialWalletAddress, params.to);
            if (!transaction.flag) {
                await dbService.updateDocument(User, { _id: loginUser.id }, { transInProgress: false });
                await activityService.createActivity(req, loginUser.id, 'Withdraw Bricks', 'TRANSACTION');
                return res.requestValidated(transaction.data);
            }
            await dbService.updateDocument(User, { _id: loginUser.id }, { transInProgress: false });
            return res.invalidRequest(transaction.data);
        } catch (error) {
            console.error(error);
            await dbService.updateDocument(User, { _id: loginUser.id }, { transInProgress: false });
            return res.failureResponse(error.message);
        }
    },

    getChartValue: async (req, res) => {
        let params = req.body;
        try {
            if (!params.currency) {
                return res.insufficientParameters();
            }
            let transaction = await transactionService.getChartValue(params.currency, params.fromDate, params.toDate);
            if (!transaction.flag) {
                return res.requestValidated(transaction.data);
            }
            return res.invalidRequest(transaction.data);
        } catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },

    statusOfTransaction: async (req, res) => {
        try {
            let loginUser = req.user;
            if (!loginUser) {
                return res.unAuthorizedRequest();
            }
            return res.requestValidated({
                status: loginUser.transInProgress,
                accountStatus: loginUser.isAccountLock,
                reason: loginUser.reasonOfLock,
                unlockTime: loginUser.isAccountLock ? common.getDifferenceOfTwoDatesInTime(moment(), moment(loginUser.accountLockTime)) : null
            });
        } catch (error) {
            console.error(error);
            return res.failureResponse(error.message);
        }
    },
};