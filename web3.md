
const Web3 = require("web3");

//Import the ABI of the smart contract to interact with. 
const cont = require("./build/contracts/Token.json"); 

const init = async() => {
    const provider = new Web3.providers.HttpProvider('https://bsc-dataseed1.ninicoin.io'); //Http provider for the required network
    const web3 = new Web3(provider);

    const chainId = await web3.eth.getChainId(); //returns the chain ID of the connected blockchain

    if(chainId === 56) { //BSC Mainnet - 56, BSC Testnet - 97

        //To build a transaction, following parameters are required.

        /*Nonce is the current index of the user successful transactions on this chain.
        * @param, userAddress -> the user trying to make the transaction
        */
        let userAddress = "0xF8C2cfDB29Af6F69ccc7314Fd9eF3037485edF42";
        const nonce = await web3.eth.getTransactionCount(userAddress);
        console.log("Nonce is ", nonce);

        //For a simple BNB transaction, from and to Address will be default shared by the user.
        let fromAddress = userAddress;
        let toAddress = "0x4B7b81fc5f699e2B70Fbc3362d7489bA6c7A53a9";

        //For contract interaction, toAddress will be the smart contract address that user wants to interact with
        //and the data field be encoded using the contract ABI.


        // Gas and gas Price needs to be manually checked and passed from the backend for the current config of Trustology - need  to check for future releases
        const gasPrice = await web3.eth.getGasPrice(); //Returns the current gasPrice depending on the current successful transactions on the chain.
        console.log("Gas Price is ", gasPrice);

        const gas = await web3.eth.estimateGas({to: toAddress});
        console.log("Required gas is ", gas);

        //If in any case the transaction fails then the gas can be set to 60000.

        //we don't have to pass any data in the BNB transfer.
        let data = "";

        let value = "10000000000000000" //0.01BNB

        const rawTx = {
            nonce: nonce,
            from: fromAddress,
            to: toAddress,
            value: value,
            gasPrice: web3.utils.toHex(gasPrice),
            gas: web3.utils.toHex(gas),
            data: data,
            chainId: chainId
        }

        //The raw tx object should be passed with the API call to the Trustology and get a signed transaction
        console.log("Raw transaction is ", rawTx); // the same can be passed to the trustology API

        //once the signed data is received use the below method to broadcast the transaction to the network
        web3.eth.sendSignedTransaction(data, function(error, hash) {
            if (!error) {
              console.log("Hash is ", hash, "\n"); //This will return the hash of the transaction.
            } else {
              console.log("Error: ", error)
            }
        })
        
        

    } else {
        console.log("Invalid ChainID");
    }
}

init();



#for BNB to Bricks

const WBNB = "0xc778417e063141139fce010982780140aa0cd5ab";
const WBRICKS = "0x5A78a16cf3D1ed1982ACbdbA82c9413ff4aA9B2d";
const Router = "0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D";
const path = [WBNB, WBRICKS];


Steps to get BRICKS output from BNB Input:

1. Initialise the rotuer contract using web3
const router = new web3.eth.Contract(Contract, Router);

2. Using the getAmountsOut function and "BNBInput" from the user as variable call the router contract.
const output = await router.methods.getAmountsOut(web3.utils.toWei(BNBInput), path).call();

web3.utils.toWei(BNBInput) => BNBInput should in Decimal format and not followed by 18 zeroes standard
path => array of token addresses in the LP pool

3. Output received will be in ERC20 Standard format, i.e.. token balance * no of decimals of the token,
so convert that number to user readable value.

const taxFromContract = await token.methods._enableTax().call(); // Only for myBricks token specific
if tax is enabled, remove 6% from the output and pass it to the swap process.
newOutput = Math.floor(output[1] - (output[1] * 0.06));


4. Once the user confirms the transaction output make a transaction to initialise the swap process
const data = await router.methods.swapExactETHForTokens(Math.floor(BRICKSInput * 1000000000), [WBNB, WBRICKS], accounts, date + 300).send({from: accounts, value: web3.utils.toWei(BNBInput)})

Math.floor(BRICKSInput * 1000000000) => Care should be taken that the token required output must be passed in the lowest format.
accounts => user wallet address that is making the transaction.\
date + 300 => unix timestamp, should be greater than the current time.
web3.utils.toWei(BNBInput) => User BNB amount in the lowest form.


