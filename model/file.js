const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
var idValidator = require('mongoose-id-validator');
const uniqueValidator = require('mongoose-unique-validator');
const myCustomLabels = {
	totalDocs: 'itemCount',
	docs: 'data',
	limit: 'perPage',
	page: 'currentPage',
	nextPage: 'next',
	prevPage: 'prev',
	totalPages: 'pageCount',
	pagingCounter: 'slNo',
	meta: 'paginator',
};
mongoosePaginate.paginate.options = { customLabels: myCustomLabels };
const Schema = mongoose.Schema;
const schema = new Schema(
	{
		name: {
			type: String,
			required: true,
		},
		uri: { type: String, },
		width: { type: Number, },
		height: { type: Number, },
		size: { type: Number, },
		mimeType: { type: String, },
		slug: { type: String, },
		type: { type: String },
		link: { type: String, },
		title: { type: String, },
		subTitle: { type: String, },
		description: { type: String, },
		isActive: {
			type: Boolean,
			default: true,
		},
		modelName: { type: String, },
		modelId: { type: mongoose.SchemaTypes.ObjectId, },
		posterId: {
			type: mongoose.SchemaTypes.ObjectId,
			ref: 'file',
		},
		uploadedBy: {
			type: mongoose.SchemaTypes.ObjectId,
			ref: 'user',
		},
	},
	{ timestamps: true, }
);

schema.plugin(mongoosePaginate);
schema.plugin(idValidator);

schema.plugin(uniqueValidator, { message: 'Error, expected {VALUE} to be unique.' });

const file = mongoose.model('file', schema, 'file');
module.exports = file;
