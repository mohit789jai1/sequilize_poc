const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const schema = new Schema(
	{
		setting: { type: mongoose.Mixed, },

		type: { type: Number, },
		updatedAt: {
			type: Date,
			default: null
		},
	},
	{ timestamps: { updatedAt: 'updatedAt' } }
);

const projectSetting = mongoose.model('projectSetting', schema, 'projectSetting');
module.exports = projectSetting;