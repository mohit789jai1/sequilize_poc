const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
var idValidator = require('mongoose-id-validator');
const uniqueValidator = require('mongoose-unique-validator');
const myCustomLabels = {
	totalDocs: 'itemCount',
	docs: 'data',
	limit: 'perPage',
	page: 'currentPage',
	nextPage: 'next',
	prevPage: 'prev',
	totalPages: 'pageCount',
	pagingCounter: 'slNo',
	meta: 'paginator',
};
mongoosePaginate.paginate.options = { customLabels: myCustomLabels };
const Schema = mongoose.Schema;
const schema = new Schema(
	{
		portfolioId: {
			type: Schema.Types.ObjectId,
			ref: 'portfolio'
		},

		propertyName: { type: String, },

		propertyDescription: { type: String, },

		propertyPrice: { type: String, },

		propertyRenovationCost: { type: String },

		propertyEstimatedIncome: { type: String },

		propertyEstimatedAnnualYield: { type: String },

		propertyMainImage: {
			type: Schema.Types.ObjectId,
			ref: 'file'
		},

		propertyImageCollection: [{
			type: Schema.Types.ObjectId,
			ref: 'file'
		}],

		propertyType: { type: Number },

		isDeleted: {
			type: Boolean,
			default: false
		},

		isActive: {
			type: Boolean,
			default: true
		},

		createdAt: { type: Date },

		updatedAt: { type: Date },

		addedBy: {
			type: Schema.Types.ObjectId,
			ref: 'user'
		},

		updatedBy: {
			type: Schema.Types.ObjectId,
			ref: 'user'
		},
	},
	{
		timestamps: {
			createdAt: 'createdAt',
			updatedAt: 'updatedAt'
		}
	}
);

schema.plugin(mongoosePaginate);
schema.plugin(idValidator);

schema.plugin(uniqueValidator, { message: 'Error, expected {VALUE} to be unique.' });

const property = mongoose.model('property', schema, 'property');
module.exports = property;