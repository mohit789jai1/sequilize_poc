const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
var idValidator = require('mongoose-id-validator');
const uniqueValidator = require('mongoose-unique-validator');
const myCustomLabels = {
	totalDocs: 'itemCount',
	docs: 'data',
	limit: 'perPage',
	page: 'currentPage',
	nextPage: 'next',
	prevPage: 'prev',
	totalPages: 'pageCount',
	pagingCounter: 'slNo',
	meta: 'paginator',
};
mongoosePaginate.paginate.options = { customLabels: myCustomLabels };
const Schema = mongoose.Schema;
const schema = new Schema(
	{
		portfolioName: {
			type: String,
			default: ''
		},

		portfolioMainDescription: {
			type: String,
			default: ''
		},

		portfolioVideo: {
			type: Schema.Types.ObjectId,
			ref: 'file',
			default: null
		},
		portfolioVideoThumbnail: {
			type: Schema.Types.ObjectId,
			ref: 'file',
			default: null
		},

		portfolioImage: [{
			type: Schema.Types.ObjectId,
			ref: 'file',
			default: null
		}],

		portfolioLaunchDate: {
			type: Date,
			default: null
		},

		availableNFTs: {
			type: String,
			default: ''
		},

		NFTCost: {
			type: String,
			default: ''
		},

		portfolioDetailDescription: {
			type: String,
			default: ''
		},

		portfolioDetailLocation: {
			type: String,
			default: ''
		},

		portfolioDetailResearch: {
			type: String,
			default: ''
		},

		portfolioEstimatedYield: {
			type: String,
			default: ''
		},

		portfolioAssetValue: {
			type: String,
			default: ''
		},

		portfolioPDF: {
			type: Schema.Types.ObjectId,
			ref: 'file',
			default: null
		},

		isLive: {
			type: Boolean,
			default: false
		},

		isDeleted: {
			type: Boolean,
			default: false
		},

		isActive: {
			type: Boolean,
			default: true
		},

		createdAt: {
			type: Date,
			default: null
		},

		updatedAt: {
			type: Date,
			default: null
		},

		addedBy: {
			type: Schema.Types.ObjectId,
			ref: 'user'
		},

		updatedBy: {
			type: Schema.Types.ObjectId,
			ref: 'user'
		},
	},
	{
		timestamps: {
			createdAt: 'createdAt',
			updatedAt: 'updatedAt'
		}
	}
);

schema.plugin(mongoosePaginate);
schema.plugin(idValidator);

schema.plugin(uniqueValidator, { message: 'Error, expected {VALUE} to be unique.' });

const portfolio = mongoose.model('portfolio', schema, 'portfolio');
module.exports = portfolio;