const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
var idValidator = require('mongoose-id-validator');
const uniqueValidator = require('mongoose-unique-validator');
const myCustomLabels = {
	totalDocs: 'itemCount',
	docs: 'data',
	limit: 'perPage',
	page: 'currentPage',
	nextPage: 'next',
	prevPage: 'prev',
	totalPages: 'pageCount',
	pagingCounter: 'slNo',
	meta: 'paginator',
};
mongoosePaginate.paginate.options = { customLabels: myCustomLabels };
const Schema = mongoose.Schema;
const schema = new Schema(
	{
		portfolioId: {
			type: Schema.Types.ObjectId,
			ref: 'portfolio'
		},

		portfolioPerkIcon: {
			type: Schema.Types.ObjectId,
			ref: 'file'
		},

		portfolioPerkName: { type: String, },

		portfolioPerkDescription: { type: String, },

		isDeleted: {
			type: Boolean,
			default: false
		},

		isActive: {
			type: Boolean,
			default: true
		},

		createdAt: { type: Date },

		updatedAt: { type: Date },

		addedBy: {
			type: Schema.Types.ObjectId,
			ref: 'user'
		},

		updatedBy: {
			type: Schema.Types.ObjectId,
			ref: 'user'
		},
	},
	{
		timestamps: {
			createdAt: 'createdAt',
			updatedAt: 'updatedAt'
		}
	}
);

schema.plugin(mongoosePaginate);
schema.plugin(idValidator);

schema.plugin(uniqueValidator, { message: 'Error, expected {VALUE} to be unique.' });

const perk = mongoose.model('perk', schema, 'perk');
module.exports = perk;