const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const myCustomLabels = {
	totalDocs: 'itemCount',
	docs: 'data',
	limit: 'perPage',
	page: 'currentPage',
	nextPage: 'next',
	prevPage: 'prev',
	totalPages: 'pageCount',
	pagingCounter: 'slNo',
	meta: 'paginator',
};
mongoosePaginate.paginate.options = { customLabels: myCustomLabels };

const Schema = mongoose.Schema;
const schema = new Schema(
	{
		user: {
			type: Schema.Types.ObjectId,
			ref: 'user'
		},

		name: {
			type: String,
			default: ''
		},

		ip: {
			type: String,
			default: ''
		},

		type: {
			type: String,
			default: ''
		},

		data: { type: mongoose.Mixed, },

		createdAt: {
			type: Date,
			default: null
		},

		updatedAt: {
			type: Date,
			default: null
		},

	},
	{ timestamps: { updatedAt: 'updatedAt' } }
);

schema.plugin(mongoosePaginate);

const activity = mongoose.model('activity', schema, 'activity');
module.exports = activity;
