const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
var idValidator = require('mongoose-id-validator');
const uniqueValidator = require('mongoose-unique-validator');
const bcrypt = require('bcrypt');
const { USER_ROLE } = require('../constants/authConstant');
const { convertObjectToEnum } = require('../utils/common');
const _ = require('lodash');
const myCustomLabels = {
	totalDocs: 'itemCount',
	docs: 'data',
	limit: 'perPage',
	page: 'currentPage',
	nextPage: 'next',
	prevPage: 'prev',
	totalPages: 'pageCount',
	pagingCounter: 'slNo',
	meta: 'paginator',
};
mongoosePaginate.paginate.options = { customLabels: myCustomLabels };
const Schema = mongoose.Schema;
const schema = new Schema(
	{

		password: {
			type: String,
			required: true,
			minLength: 8
		},

		email: {
			type: String,
			required: true,
			unique: true,
			lowercase: true,
			uniqueCaseInsensitive: true
		},

		firstName: { type: String },

		isActive: { type: Boolean },

		createdAt: { type: Date },

		updatedAt: { type: Date },

		addedBy: {
			type: Schema.Types.ObjectId,
			ref: 'user'
		},

		updatedBy: {
			type: Schema.Types.ObjectId,
			ref: 'user'
		},

		phoneNo: {
			type: String,
			/*
			 * required: true,
			 * unique: true,
			 * uniqueCaseInsensitive: true
			 */
		},

		lastName: { type: String },

		isDeleted: { type: Boolean },

		loginOTP: {
			code: String,
			expireTime: Date
		},

		emailOTP: {
			code: String,
			expireTime: Date
		},

		isEmailVerified: {
			type: Boolean,
			default: false
		},

		phoneNoOTP: {
			code: String,
			expireTime: Date
		},

		isPhoneNoVerified: {
			type: Boolean,
			default: false
		},

		role: {
			type: Number,
			enum: convertObjectToEnum(USER_ROLE),
			required: true
		},

		resetPasswordOTP: {
			code: String,
			expireTime: Date
		},

		resetPasswordPermission: {
			Permissions: Boolean,
			expireTime: Date
		},

		loginRetryLimit: {
			type: Number,
			default: 0
		},

		loginReactiveTime: { type: Date },

		resendOTPRetryLimit: {
			type: Number,
			default: 0
		},

		resendOTPReactiveTime: { type: Date },

		verifyEmailResendOTPRetryLimit: {
			type: Number,
			default: 0
		},

		verifyEmailResendOTPReactiveTime: { type: Date },

		verifyPhoneNoResendOTPRetryLimit: {
			type: Number,
			default: 0
		},

		verifyPhoneNoResendOTPReactiveTime: { type: Date },

		loginResendOTPRetryLimit: {
			type: Number,
			default: 0
		},

		loginResendOTPReactiveTime: { type: Date },

		validateOTPRetryLimit: {
			type: Number,
			default: 0
		},

		validateVerifyEmailOTPRetryLimit: {
			type: Number,
			default: 0
		},

		validateVerifyPhoneNoOTPRetryLimit: {
			type: Number,
			default: 0
		},

		validateLoginOTPRetryLimit: {
			type: Number,
			default: 0
		},

		isCustodialWalletExists: {
			type: Boolean,
			default: false
		},

		custodialWalletAddress: {
			type: String,
			default: null
		},

		subWalletId: {
			type: String,
			default: null
		},

		isRecoveryAddressExists: {
			type: Boolean,
			default: false
		},

		recoveryAddress: {
			type: String,
			default: null
		},

		transactionReactiveTime: { type: Date },

		kyc: { type: mongoose.Mixed, },

		isKycVerified: {
			type: Boolean,
			default: false
		},

		kycStatus: {
			type: Number,
			default: 0
		},

		kycInqId: {
			type: String,
			default: null
		},

		settingEmailVerified: {
			type: Boolean,
			default: false
		},

		settingPhoneVerified: {
			type: Boolean,
			default: false
		},

		altEmail: {
			type: String,
			lowercase: true,
		},

		altPhoneNo: { type: String, },

		lastSyncDate: { type: Date, },

		lastSyncBlockNumber: {
			type: Number,
			default: 0
		},

		transInProgress: {
			type: Boolean,
			default: false
		},

		dob: { type: Date, },

		isAccountLock: {
			type: Boolean,
			default: false
		},

		accountLockTime: { type: Date },

		reasonOfLock: { type: String, },

		referralId: {
			type: String,
			default: null
		},

		referredBy: {
			type: String,
			default: null
		},

		stripeCustomerId: {
			type: String,
			default: null
		},

		isStripePaymentSuccess: {
			type: Boolean,
			default: false
		}

	},
	{
		timestamps: {
			createdAt: 'createdAt',
			updatedAt: 'updatedAt'
		}
	}
);
schema.pre('save', async function (next) {
	this.isDeleted = false;
	this.isActive = true;
	this.resetPasswordOTP = null;
	this.loginOTP = null;
	this.emailOTP = null;
	this.phoneNoOTP = null;
	if (this.password) {
		this.password = await bcrypt.hash(this.password, 8);
	}
	next();
});

schema.pre('insertMany', async function (next, docs) {
	if (docs && docs.length) {
		for (let index of docs) {
			const element = docs[index];
			element.isDeleted = false;
			element.isActive = true;
			element.isEmailVerified = false;
			element.loginOTP = null;
			element.emailOTP = null;
			element.phoneNoOTP = null;
		}
	}
	next();
});

schema.methods.isPasswordMatch = async function (password) {
	const userData = this;
	return bcrypt.compare(password, userData.password);
};
schema.method('toJSON', function () {
	const {
		__v, ...object
	} = this.toObject({ virtuals: true });
	object.id = object._id;
	object.firstName = _.capitalize(object.firstName);
	object.lastName = _.capitalize(object.lastName);

	return object;
});
schema.plugin(mongoosePaginate);
schema.plugin(idValidator);

schema.plugin(uniqueValidator, { message: 'Error, expected {VALUE} to be unique.' });

const user = mongoose.model('user', schema, 'user');
module.exports = user;