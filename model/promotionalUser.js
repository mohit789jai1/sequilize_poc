const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const schema = new Schema(
	{
		email: { type: mongoose.Mixed, },
		createdAt: {
			type: Date,
			default: null
		},
	},
	{ timestamps: { createdAt: 'createdAt' } }
);

const promotionalUser = mongoose.model('promotionalUser', schema, 'promotionalUser');
module.exports = promotionalUser;