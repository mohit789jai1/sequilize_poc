const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const schema = new Schema(
	{
		fromAddress: {
			type: String,
			default: ''
		},

		toAddress: {
			type: String,
			default: ''
		},

		fromValue: {
			type: String,
			default: ''
		},

		toValue: {
			type: String,
			default: ''
		},

		fromTokenSymbol: {
			type: String,
			default: ''
		},

		toTokenSymbol: {
			type: String,
			default: ''
		},

		transactionHash: {
			type: String,
			default: ''
		},

		timestamps: {
			type: Number,
			default: null
		},

		gasFees: {
			type: String,
			default: ''
		},

		status: {
			type: String,
			default: ''
		},

		type: {
			type: String,
			default: ''
		},

		currentTokenValue: { type: mongoose.Mixed, },

		createdAt: {
			type: Date,
			default: null
		},

		updatedAt: {
			type: Date,
			default: null
		},

		user: {
			type: Schema.Types.ObjectId,
			ref: 'user'
		},

		requestId: {
			type: String,
			default: ''
		},

	},
	{ timestamps: { updatedAt: 'updatedAt' } }
);

const transaction = mongoose.model('transaction', schema, 'transaction');
module.exports = transaction;
